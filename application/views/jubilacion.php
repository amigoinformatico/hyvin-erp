<div class="content-header">
	<span class="icon-sound">
        <img src="img/icon-sound.png" alt="">    
    </span>

    <h1>Añadir nuevo cliente</h1> 

    <?php 
    polizaLine(6);
    ?>

</div>

<div class="content-body">
	<h2><span class="paso">6</span> Resultados jubilación</h2>
	<br><br>

	<form action="<?php e(base_url()); ?>" method="post" accept-charset="utf-8" role="form">
		<input type="hidden" name="page" id="page" value="dependencia/report"/>
		<input type="hidden" name="current_page" id="current_page" value="6"/>
		<input type="hidden" name="tiene_conyugue" id="tiene_conyugue" value="<?php echo $tiene_conyugue; ?>"/>

		<?php 
		$class_col = ($tiene_conyugue == 0)? 'col-xs-12 col-md-6 col-lg-3':'col-xs-12 col-sm-4';
		$oculto = ($tiene_conyugue == 0)? 'hidden':'';
		?>

		<div class="row no-mb">
			<div class="head-frm form-group text-center <?php echo $class_col; ?>">
        		&nbsp;
    		</div>
		    <div class="head-frm form-group <?php echo $class_col; ?>">
		        Titular
		    </div>
		    <div class="head-frm form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        Cónyuge
		    </div>

			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Fecha de nacimiento</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly placeholder="" value="<?php print_date($titular_fecha_nacimiento);?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly placeholder="" value="<?php print_date($conyugue_fecha_nacimiento);?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Fecha de su jubilación</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_date($titular_fecha_jubilacion);?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_date($conyugue_fecha_jubilacion);?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Edad a la jubilación</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_edad_jubilacion); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_edad_jubilacion); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Años hasta la jubilación</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_anyos_hasta_jubilacion); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_anyos_hasta_jubilacion); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Gastos mensuales familiares HOY</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_gastos_mensuales_familiares_hoy); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_gastos_mensuales_familiares_hoy); ?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">IPC estimado</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_ipc_estimado); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_ipc_estimado); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Gastos actualizados</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_gastos_reducidos_actualizado); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_gastos_reducidos_actualizado); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Pensión de jubilación propia ACTUALIZADO</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_pension_jubilacion_propia_actualizado); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_pension_jubilacion_propia_actualizado); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Ingresos cónyuge ACTUALIZADO</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_ingresos_conyugue_actualizado); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_ingresos_conyugue_actualizado); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Saldo diferencial mensual a cubrir</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right <?php resalta_signe($titular_saldo_diferencial_mensual_cubrir);?>" readonly value="<?php print_number($titular_saldo_diferencial_mensual_cubrir); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?> <?php resalta_signe($conyugue_saldo_diferencial_mensual_cubrir);?>" readonly value="<?php print_number($conyugue_saldo_diferencial_mensual_cubrir); ?>">    
		    </div>
		    <div class="clearfix"></div>
		    <hr class="head-hr">

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Años de cobertura</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right" id="jubilacion_titular_anyos_cobertura" name="jubilacion_titular_anyos_cobertura" value="<?php print_number($jubilacion_titular_anyos_cobertura); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Años entre jubilación de uno y otro cónyuge</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right" readonly value="<?php echo print_number($anyos_jubilacion_entre_conyugues); ?>">  
		    </div>
		    <div class="clearfix"></div>

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Déficit generado en período 1</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right <?php resalta_signe($diferencial_generado_periodo_1);?>" readonly value="<?php echo print_number($diferencial_generado_periodo_1); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Resto de período de cobertura</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($resto_periodo_cobertura); ?>">   
		    </div>
		    <div class="clearfix"></div>   

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Déficit generado en período 2</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right <?php resalta_signe($diferencial_generado_periodo_2);?>" readonly value="<?php print_number($diferencial_generado_periodo_2); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Déficit total</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right <?php resalta_signe($diferencial_generado_periodo_1_mas_2);?>" readonly value="<?php print_number($diferencial_generado_periodo_1_mas_2); ?>">   
		    </div>
		    <div class="clearfix"></div> 

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Porcentaje de cobertura</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right" name="jubilacion_porcentaje_cobertura" id="jubilacion_porcentaje_cobertura" value="<?php print_number($jubilacion_porcentaje_cobertura); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Diferencial a cubrir</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right <?php resalta_signe($diferencial_cubrir);?>" readonly value="<?php print_number($diferencial_cubrir); ?>">   
		    </div>
		    <div class="clearfix"></div>  

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Deudas familiares a día de HOY</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($deudas_familiares_hoy); ?>">   
		    </div> 
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Deudas previstas a la jubilación</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right" name="jubilacion_deudas_previstas_jubilacion" id="jubilacion_deudas_previstas_jubilacion" value="<?php print_number($jubilacion_deudas_previstas_jubilacion); ?>">    
		    </div>
		    <div class="clearfix"></div>   

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Capital necesario a la jubilación</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right <?php resalta_signe($capital_necesario_jubilacion);?>" readonly value="<?php print_number($capital_necesario_jubilacion); ?>">        
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Ahorro e inversión ya realizado</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($ahorro_inversion_realizado); ?>">   
		    </div>
		    <div class="clearfix"></div> 

		    <div class="form-group col-xs-6 col-sm-3 text-right">
		        <label for="" class="frm-label">Descobertura</label>
		    </div>
		    <div class="form-group col-xs-6 col-sm-3">
		        <input type="text" class="form-control text-right <?php resalta_signe($descobertura);?>" readonly value="<?php print_number($descobertura); ?>">   
		    </div>  
		</div>

		<div class="row no-mb">
			<div class="form-group col-sm-12">
	        	<h3>Soluciones</h3>
	        </div>	

			<div class="form-group col-sm-12">
				<div class="table-responsive">
		            <table class="tabla table table-bordered">
		                <tr class="tbl-head">
	                        <th>Contratante</th>
	                        <th colspan="3">Titular</th>
	                        <th colspan="3" class='<?php hide_no_conyugue($tiene_conyugue);?>'>Cónyuge</th>
		                </tr>
		                <tr>
		                    <td width='16%'>Modalidad</td>
		                    <td width='<?php width_soluciones_1($tiene_conyugue);?>'>Plan Pensiones</td>
		                    <td width='<?php width_soluciones_1($tiene_conyugue);?>'>PIAS</td>
		                    <td width='<?php width_soluciones_1($tiene_conyugue);?>'>Seguro Vida</td>
		                    
		                    <td width='<?php width_soluciones_2($tiene_conyugue);?>' class='<?php hide_no_conyugue($tiene_conyugue);?>'>Plan Pensiones</td>
		                    <td width='<?php width_soluciones_2($tiene_conyugue);?>' class='<?php hide_no_conyugue($tiene_conyugue);?>'>PIAS</td>
		                    <td width='<?php width_soluciones_2($tiene_conyugue);?>' class='<?php hide_no_conyugue($tiene_conyugue);?>'>Seguro Vida</td>
		                    
		                </tr>
		                <tr>
		                    <td>Traspaso Plan Pensiones</td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" id="jubilacion_titular_traspaso_plan_pensiones"
		                                name="jubilacion_titular_traspaso_plan_pensiones" placeholder="" value="<?php print_number($jubilacion_titular_traspaso_plan_pensiones); ?>">
		                        </div>
		                    </td>                            
		                    <td></td>
		                    <td></td>
		                    
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" id="jubilacion_conyugue_traspaso_plan_pensiones"
		                                name="jubilacion_conyugue_traspaso_plan_pensiones" placeholder="" value="<?php print_number($jubilacion_conyugue_traspaso_plan_pensiones); ?>">
		                        </div>
		                    </td>                            
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    
		                </tr>
		                
		                <tr>
		                    <td>¿Anterior al 31/12/2006?</td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" id="jubilacion_titular_anterior_2006" name ="jubilacion_titular_anterior_2006"
		                                name="jubilacion_titular_anterior_2006" placeholder="" value="<?php print_number($jubilacion_titular_anterior_2006); ?>">
		                        </div>
		                    </td>                            
		                    <td></td>
		                    <td></td>
		                    
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control " id="jubilacion_conyugue_anterior_2006"
		                                name="jubilacion_conyugue_anterior_2006" placeholder="" value="<?php print_number($jubilacion_conyugue_anterior_2006); ?>">
		                        </div>
		                    </td>                            
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    
		                </tr>
		                <tr>
		                    <td>Forma de Cálculo</td>
		                    <td>
		                        <select class="form-control" id="jubilacion_titular_forma_calculo_pensiones" name="jubilacion_titular_forma_calculo_pensiones" >
		                            <option value='' <?php if( !isset($jubilacion_titular_forma_calculo_pensiones) || (isset($jubilacion_titular_forma_calculo_pensiones) && $jubilacion_titular_forma_calculo_pensiones == '')) echo "selected"; ?> ></option>
		                            <option value="aportacion"<?php if( isset($jubilacion_titular_forma_calculo_pensiones) && $jubilacion_titular_forma_calculo_pensiones=='aportacion') echo "selected"; ?> >Aportación</option>
		                            <option value="capital" <?php if( isset($jubilacion_titular_forma_calculo_pensiones) && $jubilacion_titular_forma_calculo_pensiones=='capital') echo "selected"; ?>>Capital</option>
		                        </select>
		                    </td>                            
		                    <td>
		                        <select class="form-control" id="jubilacion_titular_forma_calculo_pias" name="jubilacion_titular_forma_calculo_pias" >
		                            <option value='' <?php if( !isset($jubilacion_titular_forma_calculo_pias) || (isset($jubilacion_titular_forma_calculo_pias) && $jubilacion_titular_forma_calculo_pias == '')) echo "selected"; ?> ></option>
		                            <option value="aportacion"<?php if( isset($jubilacion_titular_forma_calculo_pias) && $jubilacion_titular_forma_calculo_pias=='aportacion') echo "selected"; ?> >Aportación</option>
		                            <option value="capital" <?php if( isset($jubilacion_titular_forma_calculo_pias) && $jubilacion_titular_forma_calculo_pias=='capital') echo "selected"; ?>>Capital</option>
		                        </select>
		                    </td>
		                    <td>
		                        <select class="form-control" id="jubilacion_titular_forma_calculo_seguro_vida" name="jubilacion_titular_forma_calculo_seguro_vida" >
		                            <option value='' <?php if( !isset($jubilacion_titular_forma_calculo_seguro_vida) || (isset($jubilacion_titular_forma_calculo_seguro_vida) && $jubilacion_titular_forma_calculo_seguro_vida == '')) echo "selected"; ?> ></option>
		                            <option value="aportacion"<?php if( isset($jubilacion_titular_forma_calculo_seguro_vida) && $jubilacion_titular_forma_calculo_seguro_vida=='aportacion') echo "selected"; ?> >Aportación</option>
		                            <option value="capital" <?php if( isset($jubilacion_titular_forma_calculo_seguro_vida) && $jubilacion_titular_forma_calculo_seguro_vida=='capital') echo "selected"; ?>>Capital</option>
		                        </select>
		                    </td>
		                    
		                   <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                        <select class="form-control " id="jubilacion_conyugue_forma_calculo_pensiones" name="jubilacion_conyugue_forma_calculo_pensiones" >
		                            <option value='' <?php if( !isset($jubilacion_conyugue_forma_calculo_pensiones) || (isset($jubilacion_conyugue_forma_calculo_pensiones) && $jubilacion_conyugue_forma_calculo_pensiones == '')) echo "selected"; ?> ></option>
		                            <option value="aportacion"<?php if( isset($jubilacion_conyugue_forma_calculo_pensiones) && $jubilacion_conyugue_forma_calculo_pensiones=='aportacion') echo "selected"; ?> >Aportación</option>
		                            <option value="capital" <?php if( isset($jubilacion_conyugue_forma_calculo_pensiones) && $jubilacion_conyugue_forma_calculo_pensiones=='capital') echo "selected"; ?>>Capital</option>
		                        </select>
		                    </td>                            
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                        <select class="form-control " id="jubilacion_conyugue_forma_calculo_pias" name="jubilacion_conyugue_forma_calculo_pias" >
		                            <option value='' <?php if( !isset($jubilacion_conyugue_forma_calculo_pias) || (isset($jubilacion_conyugue_forma_calculo_pias) && $jubilacion_conyugue_forma_calculo_pias == '')) echo "selected"; ?> ></option>
		                            <option value="aportacion"<?php if( isset($jubilacion_conyugue_forma_calculo_pias) && $jubilacion_conyugue_forma_calculo_pias=='aportacion') echo "selected"; ?> >Aportación</option>
		                            <option value="capital" <?php if( isset($jubilacion_conyugue_forma_calculo_pias) && $jubilacion_conyugue_forma_calculo_pias=='capital') echo "selected"; ?>>Capital</option>
		                        </select>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                        <select class="form-control " id="jubilacion_conyugue_forma_calculo_seguro_vida" name="jubilacion_conyugue_forma_calculo_seguro_vida" >
		                            <option value='' <?php if( !isset($jubilacion_conyugue_forma_calculo_seguro_vida) || (isset($jubilacion_conyugue_forma_calculo_seguro_vida) && $jubilacion_conyugue_forma_calculo_seguro_vida == '')) echo "selected"; ?> ></option>
		                            <option value="aportacion"<?php if( isset($jubilacion_conyugue_forma_calculo_seguro_vida) && $jubilacion_conyugue_forma_calculo_seguro_vida=='aportacion') echo "selected"; ?> >Aportación</option>
		                            <option value="capital" <?php if( isset($jubilacion_conyugue_forma_calculo_seguro_vida) && $jubilacion_conyugue_forma_calculo_seguro_vida=='capital') echo "selected"; ?>>Capital</option>
		                        </select>
		                    </td>
		                    
		                </tr>
		                
		                <tr>
		                    <td>Aportación mensual propuesta</td>
		                    <td>
		                        <input type="text" step="any"  class="form-control" placeholder="" name="jubilacion_titular_aportacion_pensiones" id="jubilacion_titular_aportacion_pensiones" 
		                            <?php if(!isset($jubilacion_titular_forma_calculo_pensiones) || (isset($jubilacion_titular_forma_calculo_pensiones) && $jubilacion_titular_forma_calculo_pensiones!='aportacion')) echo "readonly"; ?>     
		                               value="<?php print_number($jubilacion_titular_aportacion_pensiones); ?>">
		                    </td>                            
		                    <td>
		                        <input type="text" step="any"  class="form-control" placeholder="" name="jubilacion_titular_aportacion_pias" id="jubilacion_titular_aportacion_pias" 
		                            <?php if(!isset($jubilacion_titular_forma_calculo_pias) || (isset($jubilacion_titular_forma_calculo_pias) && $jubilacion_titular_forma_calculo_pias!='aportacion')) echo "readonly"; ?>  
		                               value="<?php print_number($jubilacion_titular_aportacion_pias); ?>">
		                    </td>
		                    <td>
		                        <input type="text" step="any"  class="form-control" placeholder="" name="jubilacion_titular_aportacion_seguro_vida" id="jubilacion_titular_aportacion_seguro_vida" 
		                            <?php if(!isset($jubilacion_titular_forma_calculo_seguro_vida) || (isset($jubilacion_titular_forma_calculo_seguro_vida) && $jubilacion_titular_forma_calculo_seguro_vida!='aportacion')) echo "readonly"; ?>                                  
		                               value="<?php print_number($jubilacion_titular_aportacion_seguro_vida); ?>">
		                    </td>
		                    
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                        <input type="text" step="any"  class="form-control " placeholder="" name="jubilacion_conyugue_aportacion_pensiones" id="jubilacion_conyugue_aportacion_pensiones" 
		                            <?php if(!isset($jubilacion_conyugue_forma_calculo_pensiones) || (isset($jubilacion_conyugue_forma_calculo_pensiones) && $jubilacion_conyugue_forma_calculo_pensiones!='aportacion')) echo "readonly"; ?> 
		                               value="<?php print_number($jubilacion_conyugue_aportacion_pensiones); ?>">
		                    </td>                            
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                        <input type="text" step="any"  class="form-control " placeholder="" name="jubilacion_conyugue_aportacion_pias" id="jubilacion_conyugue_aportacion_pias" 
		                            <?php if(!isset($jubilacion_conyugue_forma_calculo_pias) || (isset($jubilacion_conyugue_forma_calculo_pias) && $jubilacion_conyugue_forma_calculo_pias!='aportacion')) echo "readonly"; ?> 
		                               value="<?php print_number($jubilacion_conyugue_aportacion_pias); ?>">
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                        <input type="text" step="any"  class="form-control " placeholder="" name="jubilacion_conyugue_aportacion_seguro_vida" id="jubilacion_conyugue_aportacion_seguro_vida" 
		                            <?php if(!isset($jubilacion_conyugue_forma_calculo_seguro_vida) || (isset($jubilacion_conyugue_forma_calculo_seguro_vida) && $jubilacion_conyugue_forma_calculo_seguro_vida!='aportacion')) echo "readonly"; ?> 
		                               value="<?php print_number($jubilacion_conyugue_aportacion_seguro_vida); ?>">
		                    </td>
		                    
		                </tr>

		                <tr>
		                    <td>Capital a la Jubilación</td>
		                    <td>
		                        <input type="text" class="form-control" placeholder="" name="jubilacion_titular_capital_pensiones" id="jubilacion_titular_capital_pensiones" 
		                        <?php if(!isset($jubilacion_titular_forma_calculo_pensiones) || (isset($jubilacion_titular_forma_calculo_pensiones) && $jubilacion_titular_forma_calculo_pensiones!='capital')) echo "readonly"; ?> 
		                           value="<?php print_number($jubilacion_titular_capital_pensiones); ?>">
		                    </td>
		                    <td>
		                        <input type="text" class="form-control" placeholder="" name="jubilacion_titular_capital_pias" id="jubilacion_titular_capital_pias" 
		                        <?php if(!isset($jubilacion_titular_forma_calculo_pias) || (isset($jubilacion_titular_forma_calculo_pias) && $jubilacion_titular_forma_calculo_pias!='capital')) echo "readonly"; ?> 
		                           value="<?php print_number($jubilacion_titular_capital_pias); ?>">
		                    </td>
		                    <td>
		                        <input type="text" class="form-control" placeholder="" name="jubilacion_titular_capital_seguro_vida" id="jubilacion_titular_capital_seguro_vida" 
		                        <?php if(!isset($jubilacion_titular_forma_calculo_seguro_vida) || (isset($jubilacion_titular_forma_calculo_seguro_vida) && $jubilacion_titular_forma_calculo_seguro_vida!='capital')) echo "readonly"; ?> 
		                           value="<?php print_number($jubilacion_titular_capital_seguro_vida); ?>">
		                    </td>
		                    
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                        <input type="text" class="form-control " placeholder="" name="jubilacion_conyugue_capital_pensiones" id="jubilacion_conyugue_capital_pensiones" 
		                        <?php if(!isset($jubilacion_conyugue_forma_calculo_pensiones) || (isset($jubilacion_conyugue_forma_calculo_pensiones) && $jubilacion_conyugue_forma_calculo_pensiones!='capital')) echo "readonly"; ?> 
		                           value="<?php print_number($jubilacion_conyugue_capital_pensiones); ?>">
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                        <input type="text" class="form-control " placeholder="" name="jubilacion_conyugue_capital_pias" id="jubilacion_conyugue_capital_pias" 
		                        <?php if(!isset($jubilacion_conyugue_forma_calculo_pias) || (isset($jubilacion_conyugue_forma_calculo_pias) && $jubilacion_conyugue_forma_calculo_pias!='capital')) echo "readonly"; ?> 
		                           value="<?php print_number($jubilacion_conyugue_capital_pias); ?>">
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                        <input type="text" class="form-control " placeholder="" name="jubilacion_conyugue_capital_seguro_vida" id="jubilacion_conyugue_capital_seguro_vida" 
		                        <?php if(!isset($jubilacion_conyugue_forma_calculo_seguro_vida) || (isset($jubilacion_conyugue_forma_calculo_seguro_vida) && $jubilacion_conyugue_forma_calculo_seguro_vida!='capital')) echo "readonly"; ?> 
		                           value="<?php print_number($jubilacion_conyugue_capital_seguro_vida); ?>">
		                    </td>
		                </tr>
		            </table>
	        	</div>
			</div>
		</div>
		<br>

		<div class="row">
		    <div class="form-group col-sm-12">
		        <table class="tabla table table-no-bordered">
	                <tr>
	                    <td width="33%">Suma de Aportaciones Familiares</td>
	                    <td width="33%"><input type="text" class="form-control" readonly placeholder="" value="<?php print_number($suma_aportaciones_familiares); ?>"></td>
	                    <td width="33%"></td>
	                </tr>
	                <tr>
	                    <td>Suma de Capitales Obtenidos</td>
	                    <td><input type="text" class="form-control" readonly value="<?php print_number($suma_capitales_obtenidos); ?>"></td>
	                    <td></td>
	                </tr>
		        </table>
		    </div>
		</div>







		<br><br>
		<div class="row">
			<div class="form-group col-sm-12">
				<br>
				<input type="submit" name="submit_anterior" class="hyvin-boton" value="ANTERIOR">
				&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit" class="hyvin-boton" value="SIGUIENTE">
			</div>
		</div>
	</form>
</div>