<div class="content-header">
	<span class="icon-sound">
        <img src="img/icon-sound.png" alt="">    
    </span>

    <h1>Añadir nuevo cliente</h1> 

    <?php 
    polizaLine(4);
    ?>

</div>

<div class="content-body">
	<h2><span class="paso">4</span> Resultados fallecimiento</h2>
	<br><br>

	<form action="<?php e(base_url()); ?>" method="post" accept-charset="utf-8" role="form">
		<input type="hidden" name="page" id="page" value="fallecimiento/report"/>
		<input type="hidden" name="current_page" id="current_page" value="4" />
		<input type="hidden" name="tiene_conyugue" id="tiene_conyugue" value="<?php echo $tiene_conyugue; ?>"/>

		<?php 
		$class_col = ($tiene_conyugue == 0)? 'col-xs-12 col-md-6 col-lg-3':'col-xs-12 col-sm-4';
		$oculto = ($tiene_conyugue == 0)? 'hidden':'';
		?>

		<div class="row no-mb">
			<div class="head-frm form-group text-center <?php echo $class_col; ?>">
        		Persona que fallece
    		</div>
		    <div class="head-frm form-group <?php echo $class_col; ?>">
		        Titular
		    </div>
		    <div class="head-frm form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        Cónyuge
		    </div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Gastos mensuales familiares</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly placeholder="" value="<?php print_number($titular_gastos_mensuales_familiares); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?> text-right" readonly placeholder="" value="<?php print_number($conyugue_gastos_mensuales_familiares); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Ingresos cónyuge</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_ingresos_conyugue); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_ingresos_conyugue); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Pensión de viudedad</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_pension_viudedad); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_pension_viudedad); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Pensión orfandad (todas)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_pension_orfandad_todas); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_pension_orfandad_todas); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Saldo mensual</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_saldo_mensual); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_saldo_mensual); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Porcentaje de cobertura</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" 
		        name="fallecimiento_titular_porcentaje_cobertura" id="fallecimiento_titular_porcentaje_cobertura"
		        value="<?php print_number($fallecimiento_titular_porcentaje_cobertura); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" 
		        name="fallecimiento_conyugue_porcentaje_cobertura" id="fallecimiento_conyugue_porcentaje_cobertura"
		        value="<?php print_number($fallecimiento_conyugue_porcentaje_cobertura); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Años de cobertura</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" 
		        id="fallecimiento_titular_anyos_cobertura" name="fallecimiento_titular_anyos_cobertura"
		        value="<?php print_number($fallecimiento_titular_anyos_cobertura); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" 
		        id="fallecimiento_conyugue_anyos_cobertura" name="fallecimiento_conyugue_anyos_cobertura"
		        value="<?php print_number($fallecimiento_conyugue_anyos_cobertura); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Cobertura necesaria</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_cobertura_necesaria); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_cobertura_necesaria); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Deudas pendientes</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_deudas_pendientes); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_deudas_pendientes); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Gastos de entierro</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_gastos_entierro); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_gastos_entierro); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Costes de aceptación herencia</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_costes_aceptacion_herencia); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_costes_aceptacion_herencia); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Capitales ahorro titular</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_capitales_ahorro_titular); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_capitales_ahorro_conyugue); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Capitales ahorro de ambos</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_capitales_ahorro_ambos); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_capitales_ahorro_ambos); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Saldo: seguro de vida necesario</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_saldo_seguro_vida_necesario); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_saldo_seguro_vida_necesario); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Seguros ya contratados</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_seguros_ya_contratados); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_seguros_ya_contratados); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Necesidad de seguro de vida</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_necesidad_seguro_vida); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_necesidad_seguro_vida); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Capital propuesto</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" 
		        name="fallecimiento_titular_capital_propuesto" id="fallecimiento_titular_capital_propuesto" placeholder="" value="<?php print_number($fallecimiento_titular_capital_propuesto); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" 
		        name="fallecimiento_conyugue_capital_propuesto" id="fallecimiento_conyugue_capital_propuesto"
		        value="<?php print_number($fallecimiento_conyugue_capital_propuesto); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Prima aproximada</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" 
		        name="fallecimiento_titular_prima_aproximada" id="fallecimiento_titular_prima_aproximada"
		        value="<?php print_number($fallecimiento_titular_prima_aproximada);?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" 
		        name="fallecimiento_conyugue_prima_aproximada" id="fallecimiento_conyugue_prima_aproximada"
		        value="<?php print_number($fallecimiento_conyugue_prima_aproximada);?>">
		    </div>
		</div>
		<br><br>
		
		<div class="row no-mb">
			<div class="form-group col-sm-12">
	        	<h3>Estudio aproximado de impacto fiscal, según reparto de capital entre beneficiarios</h3>
	        </div>

			<div class="form-group col-sm-12">
		        <div class="table-responsive">
					<table class="tabla table table-bordered" id="tblBeneficiarios">
		            	<tr class="tbl-head">
		                	<th></th>
		                    <th>Relación</th>
		                    <th>Edad (Descendiente)</th>
		                    <th>Importe a cobrar del Seguro de Vida</th>
		                    <th>Cuota Tributaria aproximada de ISD</th>
		                </tr>
		                <tr>
		                    <td>Beneficiario 1</td>
		                    <td>
		                    	<select class="form-control text-right" id="fallecimiento_beneficiario_1_relacion" name="fallecimiento_beneficiario_1_relacion" >
                                    <option value='' <?php if(!isset($fallecimiento_beneficiario_1_relacion) || (isset($fallecimiento_beneficiario_1_relacion) && $fallecimiento_beneficiario_1_relacion=='')) echo "selected"; ?> ></option>
                                    <option value='conyugue' <?php if(isset($fallecimiento_beneficiario_1_relacion) && $fallecimiento_beneficiario_1_relacion=='conyugue') echo "selected"; ?> >Cónyuge</option>
                                    <option value='descendiente' <?php if(isset($fallecimiento_beneficiario_1_relacion) && $fallecimiento_beneficiario_1_relacion=='descendiente') echo "selected" ?>>Descendiente</option>
		                        </select>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right text-right" id="fallecimiento_beneficiario_1_edad_descendiente"
		                                name="fallecimiento_beneficiario_1_edad_descendiente" placeholder="" 
		                                value="<?php print_number($fallecimiento_beneficiario_1_edad_descendiente);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right text-right"
		                                id="fallecimiento_beneficiario_1_importe_cobrar" name="fallecimiento_beneficiario_1_importe_cobrar"
		                                placeholder="" value="<?php print_number($fallecimiento_beneficiario_1_importe_cobrar);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right text-right" readonly
		                                    placeholder="" value="<?php print_number($fallecimiento_beneficiario_1_cuota); ?>">
		                        </div>
		                    </td>
		                </tr>
		                <tr>
		                    <td>Beneficiario 2</td>
		                    <td>
		                    	<select class="form-control text-right" id="fallecimiento_beneficiario_2_relacion" name="fallecimiento_beneficiario_2_relacion" >
                                    <option value='' <?php if(!isset($fallecimiento_beneficiario_2_relacion) || (isset($fallecimiento_beneficiario_2_relacion) && $fallecimiento_beneficiario_2_relacion=='')) echo "selected"; ?> ></option>
                                    <option value='conyugue' <?php if(isset($fallecimiento_beneficiario_2_relacion) && $fallecimiento_beneficiario_2_relacion=='conyugue') echo "selected"; ?> >Cónyuge</option>
                                    <option value='descendiente' <?php if(isset($fallecimiento_beneficiario_2_relacion) && $fallecimiento_beneficiario_2_relacion=='descendiente') echo "selected" ?>>Descendiente</option>
		                        </select>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right text-right" id="fallecimiento_beneficiario_2_edad_descendiente"
		                                name="fallecimiento_beneficiario_2_edad_descendiente" placeholder="" 
		                                value="<?php print_number($fallecimiento_beneficiario_2_edad_descendiente);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right text-right"
		                                id="fallecimiento_beneficiario_2_importe_cobrar" name="fallecimiento_beneficiario_2_importe_cobrar"
		                                placeholder="" value="<?php print_number($fallecimiento_beneficiario_2_importe_cobrar);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right text-right" readonly
		                                    placeholder="" value="<?php print_number($fallecimiento_beneficiario_2_cuota); ?>">
		                        </div>
		                    </td>
		                </tr>
		            </table>

		        </div>
		        <div class="text-right">
		            <button class="btn add-line" id="add-line-beneficiario">
		                <i class="fa fa-plus"></i> Añadir beneficiario
		            </button>
		        </div>
		    </div>	
		</div>
		<br>

		<div class="row">
			<div class="form-group col-sm-12">
				<h3>Bonificaciones de la Comunidad Autónoma</h3>
				<br>
				<textarea class="form-control text-right" readonly rows="5">
		        	<?php print_ifset($texto_bonificaciones_cuota); ?>
				</textarea>
			</div>
		</div>





		<br><br>
		<div class="row">
			<div class="form-group col-sm-12">
				<br>
				<input type="submit" name="submit_anterior" class="hyvin-boton" value="ANTERIOR">
				&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit" class="hyvin-boton" value="SIGUIENTE">
			</div>
		</div>
	</form>
</div>