<div class="content-header">
	<span class="icon-sound">
        <img src="img/icon-sound.png" alt="">    
    </span>

    <h1>Añadir nuevo cliente</h1> 

    <?php 
    polizaLine(2);
    ?>

</div>

<div class="content-body">
	<h2><span class="paso">2</span> Toma de datos económicos</h2>
	<br><br>

	<form action="<?php e(base_url()); ?>" method="post" accept-charset="utf-8" role="form">
		<input type="hidden" name="page" id="page" value="incapacidadtemporal/report"/>
		<input type="hidden" name="current_page" id="current_page" value="2"/>
		<input type="hidden" name="tiene_conyugue" id="tiene_conyugue" value="<?php //echo $tiene_conyugue; ?>"/>

		<!-- Situación Seguridad Social: -->
		<div class="row">
			<div class="form-group col-sm-12">
				<h3>Situación Seguridad Social <i>(Titular)</i></h3>
			</div>
			<div class="clearfix"></div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="titular_regimen_ss">Régimen de la Seguridad Social:</label>
				<br><br>
				<select name="" id="titular_regimen_ss" class="form-control">
					<option value="autonomo" <?php if( $titular_regimen_ss=='autonomo') e('selected'); ?> >Autónomo</option>
					<option value="rg" <?php if( $titular_regimen_ss=='rg') e('selected'); ?> >Régimen General</option>
					<option value="ambos" <?php if( $titular_regimen_ss=='ambos') e('selected'); ?> >Ambos</option>
					<option value="ninguno" <?php if( $titular_regimen_ss=='ninguno' || $titular_regimen_ss=='') e('selected'); ?> >Ninguno</option>
				</select>
			</div>		

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="">Tiempo cotizado en la Seguridad Social hasta hoy (Régimen General):</label>
				<br><br>
				<div class="input-group">
	        		<span class="input-group-addon" id="sizing-addon1">años</span>
	        		<input type="text" class="form-control text-right" aria-describedby="sizing-addon1" id="titular_cotizado_ss_rg_anyos" name="titular_cotizado_ss_rg_anyos" value="<?php print_number($titular_cotizado_ss_rg_anyos);?>" placeholder="0.00">

	        		<span class="input-group-addon" id="sizing-addon1">meses</span>
	        		<input type="text" class="form-control text-right" aria-describedby="sizing-addon1" id="titular_cotizado_ss_rg_meses" name="titular_cotizado_ss_rg_meses" value="<?php print_number($titular_cotizado_ss_rg_meses);?>" placeholder="0.00">
				</div>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="">Tiempo cotizado en la Seguridad Social hasta hoy (Régimen Autónomo, no simultáneo RG):</label>
				<div class="input-group">
			        <span class="input-group-addon" id="sizing-addon1">años</span>
			        <input type="text" class="form-control text-right" aria-describedby="sizing-addon1" id="titular_cotizado_ss_aut_anyos" name="titular_cotizado_ss_aut_anyos" value="<?php print_number($titular_cotizado_ss_aut_anyos);?>" placeholder="0.00">

			        <span class="input-group-addon" id="sizing-addon1">meses</span>
			        <input type="text" class="form-control text-right" aria-describedby="sizing-addon1" id="titular_cotizado_ss_aut_meses" name="titular_cotizado_ss_aut_meses" value="<?php print_number($titular_cotizado_ss_aut_meses);?>" placeholder="0.00">
				</div>
			</div>
		</div>

		<div class="row <?php hide_no_conyugue($tiene_conyugue); ?>">
			<div class="form-group col-sm-12">
				<h3>Situación Seguridad Social <i>(Cónyuge)</i></h3>
			</div>
			<div class="clearfix"></div>
	
		    <div class="form-group col-xs-12 col-sm-6 col-md-4">
		        <label for="conyugue_regimen_ss" class="control-label doble-linea">Régimen de la seguridad Social:</label>
		        <br><br>
		        <select class="form-control" name="conyugue_regimen_ss" id="conyugue_regimen_ss">
		            <option value="autonomo" <?php if( $conyugue_regimen_ss=='autonomo') echo "selected"; ?> >Autónomo</option>
		            <option value="rg" <?php if( $conyugue_regimen_ss=='rg') echo "selected"; ?> >Régimen General</option>
		            <option value="ambos" <?php if( $conyugue_regimen_ss=='ambos') echo "selected"; ?> >Ambos</option>
		            <option value="ninguno" <?php if( $conyugue_regimen_ss=='ninguno' || $conyugue_regimen_ss=='') echo "selected"; ?> >Ninguno</option>
		        </select>
		    </div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="data" class="control-label">Tiempo cotizado en la Seguridad Social hasta hoy (Régimen General):</label>
				<br><br>
				<div class="input-group">
			        <span class="input-group-addon" id="sizing-addon1">años</span>
			        <input type="text" class="form-control text-right" aria-describedby="sizing-addon1" id="conyugue_cotizado_ss_rg_anyos" name="conyugue_cotizado_ss_rg_anyos" 
		                           value="<?php print_number($conyugue_cotizado_ss_rg_anyos);?>" placeholder="0.00">
			        <span class="input-group-addon" id="sizing-addon1">meses</span>
			        <input type="text" class="form-control text-right" aria-describedby="sizing-addon1" id="conyugue_cotizado_ss_rg_meses" name="conyugue_cotizado_ss_rg_meses" 
		                           value="<?php print_number($conyugue_cotizado_ss_rg_meses);?>" placeholder="0.00">
				</div>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="data" class="control-label">Tiempo cotizado en la Seguridad Social hasta hoy (Régimen Autónomo, no simultáneo RG):</label>
				<div class="input-group">
			        <span class="input-group-addon" id="sizing-addon1">años</span>
			        <input type="text" class="form-control text-right" aria-describedby="sizing-addon1" id="conyugue_cotizado_ss_aut_anyos" name="conyugue_cotizado_ss_aut_anyos" 
		                           value="<?php print_number($conyugue_cotizado_ss_aut_anyos);?>" placeholder="0.00">
			        <span class="input-group-addon" id="sizing-addon1">meses</span>
			        <input type="text" class="form-control text-right" aria-describedby="sizing-addon1" id="conyugue_cotizado_ss_aut_meses" name="conyugue_cotizado_ss_aut_meses" 
		                           value="<?php print_number($conyugue_cotizado_ss_aut_meses);?>" placeholder="0.00">
				</div>
			</div>
		</div>
		<!-- / Situación Seguridad Social -->

		<!-- Autónomos: -->
		<div class="row <?php hide_no_autonomo($titular_regimen_ss);?>">
			<div class="form-group col-sm-12">
				<h3>Autónomos <i>(Titular)</i></h3>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="titular_autonomos_cuota_mensual" class="control-label">Cuota Mensual:</label>
				<input type="text" class="form-control text-right" name="titular_autonomos_cuota_mensual" id="titular_autonomos_cuota_mensual" value="<?php print_number($titular_autonomos_cuota_mensual);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="titular_autonomos_ingreso_bruto_mensual" class="control-label">Ingreso Bruto Mensual:</label>
				<input type="text" class="form-control text-right" name="titular_autonomos_ingreso_bruto_mensual" id="titular_autonomos_ingreso_bruto_mensual" value="<?php print_number($titular_autonomos_ingreso_bruto_mensual);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="titular_autonomos_retencion_irpf" class="control-label">Retención por IRPF:</label>
				<input type="text" class="form-control text-right" name="titular_autonomos_retencion_irpf" id="titular_autonomos_retencion_irpf" value="<?php print_number($titular_autonomos_retencion_irpf);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="titular_autonomos_neto_mensual_media" class="control-label">Neto Mensual (Media):</label>
				<input type="text" class="form-control text-right" name="titular_autonomos_neto_mensual_media" id="titular_autonomos_neto_mensual_media" value="<?php print_number($titular_autonomos_neto_mensual_media);?>" readonly placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="titular_autonomos_base_cotizacion" class="control-label">Base Cotización:</label>
				<input type="text" class="form-control text-right" name="titular_autonomos_base_cotizacion" id="titular_autonomos_base_cotizacion" value="<?php print_number($titular_autonomos_base_cotizacion);?>" readonly placeholder="0.00">
			</div>
		</div>
		
		<div class="row <?php hide_no_conyugue($tiene_conyugue); ?> <?php hide_no_autonomo($conyugue_regimen_ss);?>">
			<div class="form-group col-sm-12">
				<h3>Autónomos <i>(Cónyuge)</i></h3>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="conyugue_autonomos_cuota_mensual" class="control-label">Cuota Mensual:</label>
				<input type="text" class="form-control text-right" name="conyugue_autonomos_cuota_mensual" id="conyugue_autonomos_cuota_mensual" value="<?php print_number($conyugue_autonomos_cuota_mensual);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="conyugue_autonomos_ingreso_bruto_mensual" class="control-label">Ingreso Bruto Mensual:</label>
				<input type="text" class="form-control text-right" name="conyugue_autonomos_ingreso_bruto_mensual" id="conyugue_autonomos_ingreso_bruto_mensual" value="<?php print_number($conyugue_autonomos_ingreso_bruto_mensual);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="conyugue_autonomos_retencion_irpf" class="control-label">Retención por IRPF:</label>
				<input type="text" class="form-control text-right" name="conyugue_autonomos_retencion_irpf" id="conyugue_autonomos_retencion_irpf" value="<?php print_number($conyugue_autonomos_retencion_irpf);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="conyugue_autonomos_neto_mensual_media" class="control-label">Neto Mensual (Media):</label>
				<input type="text" class="form-control text-right" name="conyugue_autonomos_neto_mensual_media" id="conyugue_autonomos_neto_mensual_media" value="<?php print_number($conyugue_autonomos_neto_mensual_media);?>" readonly placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-4">
				<label for="conyugue_autonomos_base_cotizacion" class="control-label">Base Cotización:</label>
				<input type="text" class="form-control text-right" name="conyugue_autonomos_base_cotizacion" id="conyugue_autonomos_base_cotizacion" value="<?php print_number($conyugue_autonomos_base_cotizacion);?>" readonly placeholder="0.00">
			</div>
		</div>
		<!-- / Autónomos -->

		<!-- Régimen General: -->
		<div class="row <?php hide_no_rg($titular_regimen_ss);?>">
			<div class="form-group col-sm-12">
				<h3>Régimen General <i>(Titular)</i></h3>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="titular_rg_neto_mes_ordinario" class="control-label">Neto mes ordinario:</label>
				<input type="text" class="form-control text-right" name="titular_rg_neto_mes_ordinario" id="titular_rg_neto_mes_ordinario" value="<?php print_number($titular_rg_neto_mes_ordinario);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="titular_rg_paga_extra" class="control-label">Paga Extra:</label>
				<input type="text" class="form-control text-right" name="titular_rg_paga_extra" id="titular_rg_paga_extra" value="<?php print_number($titular_rg_paga_extra);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="titular_rg_numero_pagas_extra" class="control-label">Número de paga(s) extra:</label>
				<input type="text" class="form-control text-right" name="titular_rg_numero_pagas_extra" id="titular_rg_numero_pagas_extra" value="<?php print_number($titular_rg_numero_pagas_extra);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="titular_rg_retencion" class="control-label">Retención:</label>
				<input type="text" class="form-control text-right" name="titular_rg_retencion" id="titular_rg_retencion" value="<?php print_number($titular_rg_retencion);?>"  placeholder="0.00">
			</div>
		</div>

		<div class="row <?php hide_no_conyugue($tiene_conyugue); ?> <?php hide_no_rg($conyugue_regimen_ss);?>">
			<div class="form-group col-sm-12">
				<h3>Régimen General <i>(Cónyuge)</i></h3>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="conyugue_rg_neto_mes_ordinario" class="control-label">Neto mes ordinario:</label>
				<input type="text" class="form-control text-right" name="conyugue_rg_neto_mes_ordinario" id="conyugue_rg_neto_mes_ordinario" value="<?php print_number($conyugue_rg_neto_mes_ordinario);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="conyugue_rg_paga_extra" class="control-label">Paga Extra:</label>
				<input type="text" class="form-control text-right" name="conyugue_rg_paga_extra" id="conyugue_rg_paga_extra" value="<?php print_number($conyugue_rg_paga_extra);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="conyugue_rg_numero_pagas_extra" class="control-label">Número de paga(s) extra:</label>
				<input type="text" class="form-control text-right" name="conyugue_rg_numero_pagas_extra" id="conyugue_rg_numero_pagas_extra" value="<?php print_number($conyugue_rg_numero_pagas_extra);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="conyugue_rg_retencion" class="control-label">Retención:</label>
				<input type="text" class="form-control text-right" name="conyugue_rg_retencion" id="conyugue_rg_retencion" value="<?php print_number($conyugue_rg_retencion);?>"  placeholder="0.00">
			</div>
		</div>
		<!-- / Régimen General -->

		<!-- Ingresos distintos: -->
		<div class="row">
			<div class="form-group col-sm-12">
				<h3>Ingresos distintos al trabajo <i>(Titular)</i></h3>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="titular_distintos_ingresos" class="control-label">Ingresos:</label>
				<input type="text" class="form-control text-right" name="titular_distintos_ingresos" id="titular_distintos_ingresos" value="<?php print_number($titular_distintos_ingresos);?>" placeholder="0.00">
			</div>

		    <div class="form-group col-xs-12 col-sm-6 col-md-3">
		        <label for="titular_distintos_periodicidad" class="control-label">Periodicidad:</label>
		        <select class="form-control" name="titular_distintos_periodicidad" id="titular_distintos_periodicidad">
		                <option value="anual" <?php if($titular_distintos_periodicidad=='anual') echo "selected=''"; ?> >Anual</option>
		                <option value="semestral" <?php if($titular_distintos_periodicidad=='semestral') echo "selected=''"; ?>>Semestral</option>
		                <option value="trimestral" <?php if($titular_distintos_periodicidad=='trimestral') echo "selected=''"; ?>>Trimestral</option>
		                <option value="mensual" <?php if($titular_distintos_periodicidad=='mensual') echo "selected=''"; ?>>Mensual</option>
		        </select>
		    </div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="titular_distintos_retencion" class="control-label">Retención:</label>
				<input type="text" class="form-control text-right" name="titular_distintos_retencion" id="titular_distintos_retencion" value="<?php print_number($titular_distintos_retencion);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="titular_ahorro_mensual" class="control-label">Capacidad Mensual Ahorro:</label>
				<input type="text" class="form-control text-right" name="titular_ahorro_mensual" id="titular_ahorro_mensual" value="<?php print_number($titular_ahorro_mensual);?>"  placeholder="0.00">
			</div>
		</div>

		<div class="row <?php hide_no_conyugue($tiene_conyugue); ?>">
			<div class="form-group col-sm-12">
				<h3>Ingresos distintos al trabajo <i>(Cónyuge)</i></h3>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="conyugue_distintos_ingresos" class="control-label">Ingresos:</label>
				<input type="text" class="form-control text-right" name="conyugue_distintos_ingresos" id="conyugue_distintos_ingresos" value="<?php print_number($conyugue_distintos_ingresos);?>" placeholder="0.00">
			</div>

		    <div class="form-group col-xs-12 col-sm-6 col-md-3">
		        <label for="conyugue_distintos_periodicidad" class="control-label">Periodicidad:</label>
		        <select class="form-control" name="conyugue_distintos_periodicidad" id="conyugue_distintos_periodicidad">
		                <option value="anual" <?php if($conyugue_distintos_periodicidad=='anual') echo "selected=''"; ?> >Anual</option>
		                <option value="semestral" <?php if($conyugue_distintos_periodicidad=='semestral') echo "selected=''"; ?>>Semestral</option>
		                <option value="trimestral" <?php if($conyugue_distintos_periodicidad=='trimestral') echo "selected=''"; ?>>Trimestral</option>
		                <option value="mensual" <?php if($conyugue_distintos_periodicidad=='mensual') echo "selected=''"; ?>>Mensual</option>
		        </select>
		    </div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="conyugue_distintos_retencion" class="control-label">Retención:</label>
				<input type="text" class="form-control text-right" name="conyugue_distintos_retencion" id="conyugue_distintos_retencion" value="<?php print_number($conyugue_distintos_retencion);?>" placeholder="0.00">
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-3">
				<label for="conyugue_ahorro_mensual" class="control-label">Capacidad Mensual Ahorro:</label>
				<input type="text" class="form-control text-right" name="conyugue_ahorro_mensual" id="conyugue_ahorro_mensual" value="<?php print_number($conyugue_ahorro_mensual);?>"  placeholder="0.00">
			</div>
		</div>
		<!-- / Ingresos distintos -->

		<!-- Deudas pendientes: -->
		<div class="row no-mb">
			<div class="form-group col-sm-12">
	        	<h3>Deudas pendientes</h3>
	        </div>

			<div class="form-group col-sm-12">
		        <div class="table-responsive">
		            <table class="tabla table table-bordered" id="tblDeudas">
		                <tr class="tbl-head">
		                    <th>Nombre</th>
		                    <th>Capital pendiente</th>
		                    <th>Años pendientes</th>
		                    <th>Cuota</th>
		                </tr>
		                <tr>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control" id="deuda1_nombre"
		                                    name="deuda1_nombre" placeholder="" value="<?php print_ifset($deuda1_nombre);?>">
		                        </div>
		                    </td>
		                    <td>
		                        <input type="text" min="0" step="any" class="form-control text-right" id="deuda1_capital_pendiente"
		                                        name="deuda1_capital_pendiente" placeholder="" 
		                                        value="<?php print_number($deuda1_capital_pendiente);?>"  >
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" min="1" max="100" class="form-control text-right" id="deuda1_anyos_pendientes"
		                                    name="deuda1_anyos_pendientes" placeholder=""  
		                                    value="<?php print_number($deuda1_anyos_pendientes);?>">
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" min="0" step="any" class="form-control text-right" id="deuda1_cuota"
		                                    name="deuda1_cuota" placeholder="" 
		                                    value="<?php print_number($deuda1_cuota);?>" >
		                        </div>
		                    </td>
		                </tr>
		                <tr>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control" id="deuda2_nombre"
		                                    name="deuda2_nombre" placeholder="" value="<?php print_ifset($deuda2_nombre);?>">
		                        </div>
		                    </td>
		                    <td>
		                        <input type="text" min="0" step="any"  class="form-control text-right" id="deuda2_capital_pendiente"
		                                        name="deuda2_capital_pendiente" placeholder="" 
		                                        value="<?php print_number($deuda2_capital_pendiente);?>"  >
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" min="1" max="100" class="form-control text-right" id="deuda2_anyos_pendientes"
		                                    name="deuda2_anyos_pendientes" placeholder=""  
		                                    value="<?php print_number($deuda2_anyos_pendientes);?>">
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" min="0" step="any" class="form-control text-right" id="deuda2_cuota"
		                                    name="deuda2_cuota" placeholder="" 
		                                    value="<?php print_number($deuda2_cuota);?>" >
		                        </div>
		                    </td>
		                </tr>
		            </table>
		        </div>
		        <div class="text-right">
		            <button class="btn add-line" id="add-line-deuda">
		                <i class="fa fa-plus"></i> Añadir deuda
		            </button>
		        </div>
		    </div>
		</div>
		<br>
		<!-- / Deudas pendientes -->

		<!-- Activos ahorros: -->
		<div class="row no-mb">
			<div class="form-group col-sm-12">
				<h3>Activos (Ahorro e inversión)</h3>
			</div>

			<div class="form-group col-sm-12">
		        <div class="table-responsive">
		            <table class="tabla table table-bordered" id="tblActivosAhorros">
		                <tr class="tbl-head">
		                    <th>A quién afecta</th>
		                    <th>Tipo</th>
		                    <th>Capital Hoy</th>
		                    <th>Capital a la Jubilación</th>
		                    <th>Prima o aportación mensual</th>
		                    <th>Derechos Consolidados 31/12/2006</th>
		                </tr>
		                <tr>
		                    <td><select class="form-control" id="activos_ahorros_1_afectado" name="activos_ahorros_1_afectado" >
		                        <option value='' <?php if($activos_ahorros_1_afectado=="") {echo 'selected';}?> ></option>
		                        <option value='titular' <?php if($activos_ahorros_1_afectado=='titular') echo 'selected';?> >Titular</option>
		                        <option value='conyugue' <?php if($activos_ahorros_1_afectado=='conyugue') echo 'selected';?>  >Cónyuge</option>
		                        <option value='ambos' <?php if($activos_ahorros_1_afectado=='ambos') echo 'selected';?> >Ambos</option>
		                    </select></td>
		                    <td><select class="form-control" id="activos_ahorros_1_tipo" name="activos_ahorros_1_tipo">
		                        <option value='' <?php if($activos_ahorros_1_tipo=='') echo 'selected';?> >Plan de Pensiones</option>
		                        <option value='PPA' <?php if($activos_ahorros_1_tipo=='PPA') echo 'selected';?> >PPA</option>
		                        <option value='PIAS' <?php if($activos_ahorros_1_tipo=='PIAS') echo 'selected';?> >PIAS</option>
		                        <option value='seguro_ahorro' <?php if($activos_ahorros_1_tipo=='seguro_ahorro') echo 'selected';?> >Seguro de Ahorro</option>
		                        <option value='unit_linked' <?php if($activos_ahorros_1_tipo=='unit_linked') echo 'selected';?>  >Unit Linked</option>
		                        <option value='fondos_inversion' <?php if($activos_ahorros_1_tipo=='fondos_inversion') echo 'selected';?> >Fondos de Inversión</option>
		                        <option value='otros' <?php if($activos_ahorros_1_tipo=='otros') echo 'selected';?> >Otros</option>
		                    </select></td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_ahorros_1_capital_hoy"
		                                    name="activos_ahorros_1_capital_hoy" placeholder="" value="<?php print_number($activos_ahorros_1_capital_hoy);?>">
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_ahorros_1_capital_jubilacion"
		                                    name="activos_ahorros_1_capital_jubilacion" placeholder="" value="<?php print_number($activos_ahorros_1_capital_jubilacion);?>">
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_ahorros_1_prima_mensual"
		                                    name="activos_ahorros_1_prima_mensual" placeholder="" value="<?php print_number($activos_ahorros_1_prima_mensual);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_ahorros_1_derechos_consolidados" <?php readonly_if_ppa($activos_ahorros_1_tipo);?>
		                                      name="activos_ahorros_1_derechos_consolidados" placeholder=""  
		                                      value="<?php print_number($activos_ahorros_1_derechos_consolidados);?>">
		                        </div>
		                    </td>
		                </tr>
		                
		                <tr>
		                    <td><select class="form-control" id="activos_ahorros_2_afectado" name="activos_ahorros_2_afectado" >
		                        <option value='' <?php if($activos_ahorros_2_afectado=="") {echo 'selected';}?> ></option>
		                        <option value='titular' <?php if($activos_ahorros_2_afectado=='titular') echo 'selected';?> >Titular</option>
		                        <option value='conyugue' <?php if($activos_ahorros_2_afectado=='conyugue') echo 'selected';?>  >Cónyuge</option>
		                        <option value='ambos' <?php if($activos_ahorros_2_afectado=='ambos') echo 'selected';?> >Ambos</option>
		                    </select></td>
		                    <td><select class="form-control" id="activos_ahorros_2_tipo" name="activos_ahorros_2_tipo">
		                        <option value='' <?php if($activos_ahorros_2_tipo=='') echo 'selected';?> >Plan de Pensiones</option>
		                        <option value='PPA' <?php if($activos_ahorros_2_tipo=='PPA') echo 'selected';?> >PPA</option>
		                        <option value='PIAS' <?php if($activos_ahorros_2_tipo=='PIAS') echo 'selected';?> >PIAS</option>
		                        <option value='seguro_ahorro' <?php if($activos_ahorros_2_tipo=='seguro_ahorro') echo 'selected';?> >Seguro de Ahorro</option>
		                        <option value='unit_linked' <?php if($activos_ahorros_2_tipo=='unit_linked') echo 'selected';?>  >Unit Linked</option>
		                        <option value='fondos_inversion' <?php if($activos_ahorros_2_tipo=='fondos_inversion') echo 'selected';?> >Fondos de Inversión</option>
		                        <option value='otros' <?php if($activos_ahorros_2_tipo=='otros') echo 'selected';?> >Otros</option>
		                    </select></td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_ahorros_2_capital_hoy"
		                                    name="activos_ahorros_2_capital_hoy" placeholder="" value="<?php print_number($activos_ahorros_2_capital_hoy);?>">
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_ahorros_2_capital_jubilacion"
		                                    name="activos_ahorros_2_capital_jubilacion" placeholder="" value="<?php print_number($activos_ahorros_2_capital_jubilacion);?>">
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_ahorros_2_prima_mensual"
		                                    name="activos_ahorros_2_prima_mensual" placeholder="" value="<?php print_number($activos_ahorros_2_prima_mensual);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_ahorros_2_derechos_consolidados"  <?php readonly_if_ppa($activos_ahorros_2_tipo);?>
		                                      name="activos_ahorros_2_derechos_consolidados" placeholder=""  
		                                      value="<?php print_number($activos_ahorros_2_derechos_consolidados);?>">
		                        </div>
		                    </td>
		                </tr>
		            </table>
		        </div>
		        <div class="text-right">
		            <button class="btn add-line" id="add-line-activo-ahorro">
		                <i class="fa fa-plus"></i> Añadir activo ahorros
		            </button>
		        </div>
			</div>
		</div>
		<br>
		<!-- / Activos ahorros -->

		<!-- Activos seguros vida: -->
		<div class="row no-mb">
			<div class="form-group col-sm-12">
				<h3>Activos (Seguros de Vida Contratados)</h3>
			</div>

			<div class="form-group col-sm-12">
		        <div class="table-responsive">
					<table class="tabla table table-bordered" id="tblActivosSeguros">
		                <tr class="tbl-head">
		                    <th>Asegurado</th>
		                    <th>Fallecimiento</th>
		                    <th>IPA</th>
		                    <th>IPT</th>
		                    <th>Prima anual</th>
		                </tr>
		                <tr>
		                    <td><select class="form-control" id="activos_sv_1_asegurado" name="activos_sv_1_asegurado" >
		                        <option value="" <?php if($activos_sv_1_asegurado=='') echo 'selected';?> ></option>
		                        <option value="titular" <?php if($activos_sv_1_asegurado=='titular') echo 'selected';?> >Titular</option>
		                        <option value="conyugue" <?php if($activos_sv_1_asegurado=='conyugue') echo 'selected';?> >Cónyuge</option>
		                    </select></td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_sv_1_fallecimiento"
		                                    name="activos_sv_1_fallecimiento" placeholder="" 
		                                    value="<?php print_number($activos_sv_1_fallecimiento);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_sv_1_ipa" name="activos_sv_1_ipa"
		                                    placeholder="" value="<?php print_number($activos_sv_1_ipa);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_sv_1_ipt" name="activos_sv_1_ipt"
		                                    placeholder="" value="<?php print_number($activos_sv_1_ipt);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_sv_1_prima_anual"
		                                    name="activos_sv_1_prima_anual" placeholder="" 
		                                    value="<?php print_number($activos_sv_1_prima_anual);?>" >
		                        </div>
		                    </td>
		                </tr>

		                <tr>
		                    <td><select class="form-control" id="activos_sv_2_asegurado" name="activos_sv_2_asegurado" >
		                        <option value="" <?php if($activos_sv_2_asegurado=='') echo 'selected';?> ></option>
		                        <option value="titular" <?php if($activos_sv_2_asegurado=='titular') echo 'selected';?> >Titular</option>
		                        <option value="conyugue" <?php if($activos_sv_2_asegurado=='conyugue') echo 'selected';?> >Cónyuge</option>
		                    </select></td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_sv_2_fallecimiento"
		                                    name="activos_sv_2_fallecimiento" placeholder="" 
		                                    value="<?php print_number($activos_sv_2_fallecimiento);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_sv_2_ipa" name="activos_sv_2_ipa"
		                                    placeholder="" value="<?php print_number($activos_sv_2_ipa);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_sv_2_ipt" name="activos_sv_2_ipt"
		                                    placeholder="" value="<?php print_number($activos_sv_2_ipt);?>" >
		                        </div>
		                    </td>
		                    <td>
		                        <div class="input-group">
		                            <input type="text" class="form-control text-right" id="activos_sv_2_prima_anual"
		                                    name="activos_sv_2_prima_anual" placeholder="" 
		                                    value="<?php print_number($activos_sv_2_prima_anual);?>" >
		                        </div>
		                    </td>
		                </tr>
		            </table>
		        </div>
		        <div class="text-right">
		            <button class="btn add-line" id="add-line-activo-seguros">
		                <i class="fa fa-plus"></i> Añadir activo seguros
		            </button>
		        </div>
			</div>
		</div>
		<br>
		<!-- / Activos seguros vida -->

		<div class="row">
			<div class="form-group col-sm-12">
				<br>
				<input type="submit" name="submit_anterior" class="hyvin-boton" value="ANTERIOR">
				&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit_siguiente" class="hyvin-boton" value="SIGUIENTE">
			</div>
		</div>
	</form>
</div>