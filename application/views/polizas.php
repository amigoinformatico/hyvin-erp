<div class="content-header">
	<span class="icon-sound">
        <img src="img/icon-sound.png" alt="">    
    </span>

    <h1>Añadir nuevo cliente</h1> 

    <?php 
    polizaLine(1);
    ?>

</div>

<div class="content-body">
	<h2><span class="paso">1</span> Toma de datos personales</h2>
	<br><br>

	<form action="<?php e(base_url()); ?>" method="post" accept-charset="utf-8" role="form">
		<div class="row">
			<div class="form-group col-xs-12 col-sm-6 col-md-5">
				<label for="nombre">NOMBRE</label>
				<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Introduce el nombre completo del cliente" required>
			</div>	

			<div class="form-group col-xs-12 col-sm-6 col-md-5 offset-md-1">
				<label for="sexo">SEXO</label>
				<div class="rdb-square">
					<label>
						<input type="radio" name="sexo" value="1"/>
					  	<div class="front-end box">
					    	<span>Hombre</span>
					  	</div>
					</label>

					<label>
						<input type="radio" name="sexo" value="0"/>
					  	<div class="front-end box">
					    	<span>Mujer</span>
					  	</div>
					</label>
				</div>
			</div>	

			<div class="form-group col-xs-12 col-sm-6 col-md-5">
				<label for="nacimiento">FECHA DE NACIMIENTO</label>	
				<input type='text' class="form-control calendario" name="nacimiento" placeholder="Ej: 10-10-70" value="" />
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-5 offset-md-1">
				<label for="edad">EDAD</label>
				<input type="text" class="form-control" name="edad" id="edad" placeholder="Introduce la edad" maxlength="3" required>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-5">
				<label for="conyuge">CÓNYUGE</label>
				<div class="rdb-square">
					<label>
						<input type="radio" name="conyuge" id="conyuge_si" value="1"/>
					  	<div class="front-end box">
					    	<span>Sí</span>
					  	</div>
					</label>

					<label>
						<input type="radio" name="conyuge" id="conyuge_no" value="0" checked />
					  	<div class="front-end box">
					    	<span>No</span>
					  	</div>
					</label>
				</div>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-5 offset-md-1">
				<label for="nhijos">NÚMERO DE HIJOS</label>
				<input type="text" class="form-control" name="nhijos" id="nhijos" placeholder="Introduce el número de hijos" maxlength="2" required>
			</div>

			<div class="form-group col-xs-12 col-sm-6 col-md-5">
				<label for="telefono">TELÉFONO</label>
				<input type="text" class="form-control" name="telefono" id="telefono" placeholder="Introduce el teléfono del cliente" required>
			</div>	

			<div class="form-group col-xs-12 col-sm-6 col-md-5 offset-md-1">
				<label for="email">MAIL</label>
				<input type="text" class="form-control" name="email" id="email" placeholder="Introduce el mail del cliente" required>
			</div>	
			<div class="clearfix"></div>

			<div class="form-group">
				<br>
				<input type="submit" name="submit" class="hyvin-boton" value="SIGUIENTE">
			</div>
		</div>
	</form>	
	
</div>