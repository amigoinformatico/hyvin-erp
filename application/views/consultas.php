<div class="content-header">
	<span class="icon-sound">
        <img src="img/icon-sound.png" alt="">
    </span>

    <h1>Consulta de archivos</h1>
</div>

<ul class="nav nav-tabs hyvin-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" href="#todos" role="tab" data-toggle="tab">Todos</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="#plantillas" role="tab" data-toggle="tab">Plantillas</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="#consultas" role="tab" data-toggle="tab">Consultas</a>
    </li>

    <li style="margin-left: 42%;">
        <button type="button" class="hyvin-boton"  data-toggle="modal" data-target="#uploadConsultas">
            <i class="material-icons">Agregar</i>
        </button>   
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <!-- Todos: -->
    <div role="tabpanel" class="tab-pane fade in show active" id="todos">
        <div class="row">

            <?php foreach ($consultas as $consulta): ?>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="card card-consulta">
                        <!--<img class="card-img-top" src="<?= base_url() ?>img/<?= $consulta->name ?>"
                             alt="Card image cap"> -->
                            <img src="img/icon-pdf.jpg" alt="">
                        <div class="card-body">
                            <!--<p class="card-text"><?= $consulta->name ?></p> -->
                            <p class="card-text">TEXTO PRUEBA</p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
    <!-- / Todos -->

    <!-- Plantillas: -->
    <div role="tabpanel" class="tab-pane fade" id="plantillas">


    </div>
    <!-- / Plantillas -->

    <!-- Consultas: -->
    <div role="tabpanel" class="tab-pane fade" id="consultas">

    </div>
    <!-- / Consultas -->
</div>

<!-- Modal Upload consultas -->
<div class="modal fade" id="uploadConsultas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Selecciona o arrastra los documentos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('/consultas/upload'); ?>" class="dropzone">
                    <div class="dz-message" data-dz-message><span>Selecciona o arrastra los documentos</span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

