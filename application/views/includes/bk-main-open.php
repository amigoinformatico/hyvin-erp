<section id="main">
    <!-- START Bootstrap Navbar -->
    <div class="navbar navbar-static-top">
        <div class="navbar-inner">
            <!-- Breadcrumb -->
            <ul class="breadcrumb">
                <li><a href=""><?php e(ucfirst($activo)); ?></a> <span class="divider"></span></li>
                <li class="active"><?php //e(ucfirst($subop)); ?></li>
            </ul><!--/ Breadcrumb -->

            <!-- Daterange Picker -->
            <div id="reportrange1" class="pull-right hidden-phone" style="padding: 11px;">
                <span class="icon icon-calendar"></span>
                <span id="rangedate"><?php e(idiomasFecha(IDM_, getHoy())); ?></span>
                <!--<span class="caret"></span>-->
            </div><!--/ Daterange Picker -->
        </div>
    </div>
    