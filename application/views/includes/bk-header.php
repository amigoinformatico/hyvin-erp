<?php flush(); 
?>

<body>
    <div id="wrapper">
        <!-- Menú: -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <img class="logo" src="./img/logo.png" alt="">

                <ul class="nav metismenu" id="side-menu">
                    <li class="active">
                        <a href="<?php e(base_url()); ?>admin"><i class="fa fa-fw fa-home"></i> <span class="nav-label">INICIO</span> </a>
                    </li>
                    <li>
                        <a href="<?= base_url()?>polizas"><i class="fa fa-suitcase"></i> <span class="nav-label">PÓLIZAS</span></a>
                    </li>
                    <li>
                        <a href="<?= base_url()?>consultas"><i class="fa fa-cloud"></i> <span class="nav-label">CONSULTAS</span></a>
                        
                    </li>
                    <li>
                        <a href=""><i class="fa fa-circle"></i> <span class="nav-label">CLIENTES </span><span class="label  float-right">117</span></a>
                    </li>
                    <li class="bg-down">
                        <a href="" id="btnCalculadora"><i class="fa fa-circle"></i> <span class="nav-label">CALCULADORA</span> </a>
                    </li> 
                    <li class="bg-down">
                        <a href="<?=base_url()?>calendar"><i class="fa fa-circle"></i> <span class="nav-label">CALENDARIO</span> </a>
                    </li> 
                    <li class="bg-down">
                        <a href="<?=base_url()?>link"><i class="fa fa-circle"></i> <span class="nav-label">ENLACES RECOMEND.</span> </a>
                    </li>  

                    <li class="bg-down salir">
                        <a href=""><i class="fa fa-circle"></i> <span class="nav-label">SALIR</span> </a>       
                    </li>  
                </ul>
                <br><br>

                <p class="text-center">
                    <small>HYVIN&trade; BY MANUEL MORA</small>
                </p>
            </div>
        </nav>
        <!-- / Menú -->

        <!-- Modal calculadora -->
        <div id="modalCalculadora" class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Calculadora</h5>
                    <button type="button" class="close closeCalculadora" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="calculator">

                        <div id="display"><span id="displayTxt">0</span></div>
                        <button style="margin-left:2px" class="button btnDG clear">AC</button>
                        <button class="button btnDG" value="+/-">+/-</button>
                        <button class="button btnDG" value="%">%</button>
                        <button class="button btnO" value="/">÷</button>
                        <button style="margin-left:2px" class="button btnG" value="7">7</button>
                        <button class="button btnG" value="8">8</button>
                        <button class="button btnG" value="9">9</button>
                        <button class="button btnO" value="*">×</button>
                        <button style="margin-left:2px" class="button btnG" value="4">4</button>
                        <button class="button btnG" value="5">5</button>
                        <button class="button btnG" value="6">6</button>
                        <button class="button btnO" value="-">−</button>
                        <button style="margin-left:2px" class="button btnG" value="1">1</button>
                        <button class="button btnG" value="2">2</button>
                        <button class="button btnG" value="3">3</button>
                        <button class="button btnO" value="+">+</button>
                        <button style="margin-left:2px;width:130px;text-align:left;padding-left:30px" class="button btnG" value="0">0</button>
                        <button class="button btnG" value=".">.</button>
                        <button class="button btnO btnEquals" value="=">=</button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeCalculadora" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>


        <div id="page-wrapper" class="gray-bg dashbard-1">


