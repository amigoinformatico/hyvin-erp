<aside id="sidebar" class="">
    <!-- START Sidebar Tab -->
    <!--<ul class="nav nav-tabs">
        <li class="active"><a href="#tab-menu" data-toggle="tab"><span class="icon icone-file"></span></a></li>
        <li class=""><a href="#tab-overview" data-toggle="tab"><span class="icon icone-beaker"></span></a></li>
    </ul>-->
    <!--/ END Sidebar Tab -->

    <!-- START Tab Content -->
    <div class="tab-content">
        <!-- START Tab Pane(menu) -->
        <div class="tab-pane active" id="tab-menu">
            <!-- START Sidebar Menu -->
            <nav id="nav" class="accordion">
                <ul id="navigation">
                    <!-- START Menu Divider -->
                    <li class="divider">Main Menu</li>
                    <!--/ END Menu Divider -->

                    <!-- DASHBOARD: -->
                    <li class="accordion-group <?php if($activo == 'dashboard') e('active'); ?>">
                        <a href="<?php e(base_url()); ?>admin">
                            <span class="icon icone-dashboard"></span>
                            <span class="text">Dashboard</span>
                        </a>
                    </li>
                    <!--/ DASHBOARD -->

                    <!-- ADMINISTRADORES: -->
                    <li class="accordion-group <?php if($activo == 'administradores') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-administradores">
                            <span class="icon icone-user-md"></span>
                            <span class="text">Administradores</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>
                            
                        <ul id="sub-administradores" class="collapse">
                            <!-- Estado administradores: -->
                            <li><a href="<?php e(base_url()); ?>administradores/index"><span class="icon icone-angle-right"></span> Administradores</a></li>

                            <!-- Nuevo administrador: -->
                            <li><a href="<?php e(base_url()); ?>administradores/alta"><span class="icon icone-angle-right"></span> Alta administrador</a></li>

                            <!-- Editar administrador: -->
                            <li><a href="<?php e(base_url()); ?>administradores/editar"><span class="icon icone-angle-right"></span> Editar administrador</a></li>
                        </ul>
                    </li>
                    <!--/ ADMINISTRADORES -->

                    <!-- CLIENTES: -->
                    <li class="accordion-group <?php if($activo == 'clientes') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-clientes">
                            <span class="icon icone-group"></span>
                            <span class="text">Clientes</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>
                            
                        <ul id="sub-clientes" class="collapse">
                            <!-- Estado clientes: -->
                            <li><a href="<?php e(base_url()); ?>bk_clientes/index"><span class="icon icone-angle-right"></span> Clientes</a></li>

                            <!-- Nuevo cliente: -->
                            <li><a href="<?php e(base_url()); ?>bk_clientes/alta"><span class="icon icone-angle-right"></span> Alta cliente</a></li>

                            <!-- Editar cliente: -->
                            <li><a href="<?php e(base_url()); ?>bk_clientes/editar"><span class="icon icone-angle-right"></span> Editar cliente</a></li>

                            <!-- Propuestas clientes: -->
                            <li><a href="<?php e(base_url()); ?>bk_clientes/propuestas"><span class="icon icone-angle-right"></span> Propuestas clientes</a></li>
                        </ul>
                    </li>
                    <!--/ CLIENTES -->

                    <!-- COMERCIOS: -->
                    <li class="accordion-group <?php if($activo == 'comercios') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-comercios">
                            <span class="icon icone-pushpin"></span>
                            <span class="text">Comercios</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>

                        <ul id="sub-comercios" class="collapse">
                            <!-- Relación de comercios: -->
                            <li><a href="<?php e(base_url()); ?>bk_comercios/index"><span class="icon icone-angle-right"></span> Listados</a></li>

                            <!-- Categorías: -->
                            <li><a href="<?php e(base_url()); ?>bk_comercios/categorias"><span class="icon icone-angle-right"></span> Categorías</a></li>

                            <!-- Alta de comercios: -->
                            <li><a href="<?php e(base_url()); ?>bk_comercios/alta"><span class="icon icone-angle-right"></span> Nuevo comercio</a></li>

                            <!-- Edición de comercios: -->
                            <li><a href="<?php e(base_url()); ?>bk_comercios/editar"><span class="icon icone-angle-right"></span> Editar comercio</a></li>
                        </ul>
                    </li>
                    <!--/ COMERCIOS -->

                    <!-- PRODUCTOS: -->
                    <li class="accordion-group <?php if($activo == 'productos') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-productos">
                            <span class="icon icone-tags"></span>
                            <span class="text">Productos</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>

                        <ul id="sub-productos" class="collapse">
                            <!-- Estado: -->
                            <li><a href="<?php e(base_url()); ?>bk_productos/index"><span class="icon icone-angle-right"></span> Listados</a></li>

                            <!-- Tags: -->
                            <li><a href="<?php e(base_url()); ?>bk_productos/tags"><span class="icon icone-angle-right"></span> Tags</a></li>

                            <!-- Atributos: -->
                            <li><a href="<?php e(base_url()); ?>bk_productos/atributos"><span class="icon icone-angle-right"></span> Atributos</a></li>

                            <!-- Marcas: -->
                            <li><a href="<?php e(base_url()); ?>bk_productos/marcas"><span class="icon icone-angle-right"></span> Marcas</a></li>

                            <!-- Alta de producto: -->
                            <li><a href="<?php e(base_url()); ?>bk_productos/producto_alta"><span class="icon icone-angle-right"></span> Nuevo producto</a></li>

                            <!-- Editar producto: -->
                            <li><a href="<?php e(base_url()); ?>bk_productos/producto_editar"><span class="icon icone-angle-right"></span> Editar producto</a></li>

                            <!-- Importar CSV: -->
                            <li><a href="<?php e(base_url()); ?>bk_productos/importar_csv"><span class="icon icone-angle-right"></span> Importar CSV</a></li>

                            <!-- Unidades de venta: -->
                            <li><a href="<?php e(base_url()); ?>bk_productos/unidades_venta"><span class="icon icone-angle-right"></span> Unidades de venta</a></li>
                        </ul>
                    </li>
                    <!--/ PRODUCTOS -->

                    <!-- PEDIDOS: -->
                    <li class="accordion-group <?php if($activo == 'pedidos') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-pedidos">
                            <span class="icon icone-shopping-cart"></span>
                            <span class="text">Pedidos</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>

                        <ul id="sub-pedidos" class="collapse">
                            <!-- Estado: -->
                            <li><a href="<?php e(base_url()); ?>bk_pedidos"><span class="icon icone-angle-right"></span> Estado</a></li>

                            <!-- Detalle pedido: -->
                            <li><a href="<?php e(base_url()); ?>bk_pedidos/detalle_pedido"><span class="icon icone-angle-right"></span> Detalle pedido</a></li>

                            <!-- Histórico: -->
                            <li><a href="<?php e(base_url()); ?>bk_pedidos/historico"><span class="icon icone-angle-right"></span> Histórico</a></li>

                            <!-- Estadísticas: -->
                            <li><a href="<?php e(base_url()); ?>bk_pedidos/estadisticas"><span class="icon icone-angle-right"></span> Estadísticas</a></li>
                        </ul>
                    </li>
                    <!--/ PEDIDOS -->

                    <!-- CALIDAD: -->
                    <li class="accordion-group <?php if($activo == 'calidad') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-calidad">
                            <span class="icon icone-ok"></span>
                            <span class="text">Calidad</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>

                        <ul id="sub-calidad" class="collapse">
                            <!-- Incidencias: -->
                            <li><a href="<?php e(base_url()); ?>bk_calidad/index"><span class="icon icone-angle-right"></span> Incidencias</a></li>

                            <!-- Nueva incidencia: -->
                            <li><a href="<?php e(base_url()); ?>bk_calidad/incidencia_nueva"><span class="icon icone-angle-right"></span> Nueva incidencia</a></li>

                            <!-- Editar incidencia: -->
                            <li><a href="<?php e(base_url()); ?>bk_calidad/incidencia_editar"><span class="icon icone-angle-right"></span> Editar incidencia</a></li>

                            <!-- Motivos de incidencias: -->
                            <li><a href="<?php e(base_url()); ?>bk_calidad/incidencia_motivos"><span class="icon icone-angle-right"></span> Motivos incidencias</a></li>

                            <!-- Estados de incidencia: -->
                            <li><a href="<?php e(base_url()); ?>bk_calidad/incidencia_estados"><span class="icon icone-angle-right"></span> Estados incidencia</a></li>
                        </ul>
                    </li>
                    <!-- / CALIDAD: -->

                    <!-- LOGÍSTICA: -->
                    <li class="accordion-group <?php if($activo == 'logistica') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-logistica">
                            <span class="icon icone-truck"></span>
                            <span class="text">Logística</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>

                        <ul id="sub-logistica" class="collapse">
                            <!-- Estado: -->
                            <li><a href="<?php e(base_url()); ?>bk_logistica/index"><span class="icon icone-angle-right"></span> Estado</a></li>
                            <!-- Zonas de reparto: -->
                            <li><a href="<?php e(base_url()); ?>bk_logistica/zonas"><span class="icon icone-angle-right"></span> Zonas de reparto</a></li>
                            <!-- Repartidores: -->
                            <li><a href="<?php e(base_url()); ?>bk_logistica/repartidores"><span class="icon icone-angle-right"></span> Repartidores</a></li>
			                <!-- Alta repartidor: -->
                            <li><a href="<?php e(base_url()); ?>bk_logistica/repartidor_alta"><span class="icon icone-angle-right"></span> Alta repartidor</a></li>
			                <!-- Edición repartidor: -->
                            <li><a href="<?php e(base_url()); ?>bk_logistica/repartidor_editar"><span class="icon icone-angle-right"></span> Edición repartidor</a></li>
                        </ul>
                    </li>
                    <!-- / LOGÍSTICA: -->

                    <!-- COMERCIAL: -->
                    <li class="accordion-group <?php if($activo == 'comercial') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-comercial">
                            <span class="icon icone-bullhorn"></span>
                            <span class="text">Comercial</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>

                        <ul id="sub-comercial" class="collapse">
                            <!-- Estado: -->
                            <li><a href="<?php e(base_url()); ?>bk_comercial/index"><span class="icon icone-angle-right"></span> Estado</a></li>
                            <!-- Agente: -->
                            <li><a href="<?php e(base_url()); ?>bk_comercial/agente"><span class="icon icone-angle-right"></span> Agente</a></li>
                            <!-- Configuración: -->
                            <li><a href="<?php e(base_url()); ?>bk_comercial/configuracion"><span class="icon icone-angle-right"></span> Configuración</a></li>
                        </ul>
                    </li>
                    <!-- / COMERCIAL -->

                    <!-- CONTENIDOS: -->
                    <li class="accordion-group <?php if($activo == 'contenidos') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-contenidos">
                            <span class="icon icone-pencil"></span>
                            <span class="text">Contenidos</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>

                        <ul id="sub-contenidos" class="collapse">
                            <!-- Páginas: -->
                            <li><a href="<?php e(base_url()); ?>bk_contenidos/index"><span class="icon icone-angle-right"></span> Páginas</a></li>

                            <!-- Posiciones: -->
                            <li><a href="<?php e(base_url()); ?>bk_contenidos/posiciones"><span class="icon icone-angle-right"></span> Posiciones</a></li>

                            <!-- Nuevo contenido: -->
                            <li><a href="<?php e(base_url()); ?>bk_contenidos/nuevo"><span class="icon icone-angle-right"></span> Nuevo contenido</a></li>

                            <!-- Editar contenido: -->
                            <li><a href="<?php e(base_url()); ?>bk_contenidos/editar"><span class="icon icone-angle-right"></span> Editar contenido</a></li>
                        
                            <!-- Eliminar contenidos: -->
                            <li><a href="<?php e(base_url()); ?>bk_contenidos/eliminar_tipo_contenido"><span class="icon icone-angle-right"></span> Eliminar contenidos</a></li>

                            <!-- Galería de imágenes: -->
                            <li><a href="<?php e(base_url()); ?>bk_contenidos/galeria"><span class="icon icone-angle-right"></span> Imágenes</a></li>
                        </ul>
                    </li>
                    <!--/ CONTENIDOS -->

                    <!-- CONFIGURACIÓN: -->
                    <li class="accordion-group <?php if($activo == 'configuracion') e('active'); ?>">
                        <a data-toggle="collapse" data-parent="#navigation" href="#sub-configuracion">
                            <span class="icon icone-cog"></span>
                            <span class="text">Configuración</span>
                            <span class="arrow icone-caret-down"></span>
                        </a>

                        <ul id="sub-configuracion" class="collapse">
                            <!-- Idiomas: -->
                            <li><a href="<?php e(base_url()); ?>bk_configuracion/idiomas"><span class="icon icone-angle-right"></span> Idiomas</a></li>
                            <!-- Países: -->
                            <li><a href="<?php e(base_url()); ?>bk_configuracion/paises"><span class="icon icone-angle-right"></span> Paises</a></li>
                            <!-- Moneda: -->
                            <li><a href="<?php e(base_url()); ?>bk_configuracion/moneda"><span class="icon icone-angle-right"></span> Moneda</a></li>
                            <!-- Modalidades de pago: -->
                            <li><a href="<?php e(base_url()); ?>bk_configuracion/modalidades_pago"><span class="icon icone-angle-right"></span> Modalidades de pago</a></li>
                            <!-- Modalidades de cobro: -->
                            <li><a href="<?php e(base_url()); ?>bk_configuracion/modalidades_cobro"><span class="icon icone-angle-right"></span> Modalidades de cobro</a></li>
                            <!-- Modalidades de entrega: -->
                            <li><a href="<?php e(base_url()); ?>bk_configuracion/modalidades_entrega"><span class="icon icone-angle-right"></span> Modalidades de entrega</a></li>
                            <!-- Tarifas: -->
                            <li><a href="<?php e(base_url()); ?>bk_configuracion/tarifas"><span class="icon icone-angle-right"></span> Tarifas e impuestos</a></li>
                        </ul>
                    </li>
                    <!--/ CONFIGURACIÓN -->
                </ul>
            </nav>
            <!--/ END Sidebar Menu -->
        </div>
        <!--/ END Tab Pane(menu) -->
    </div>
    <!--/ END Tab Content -->
</aside>