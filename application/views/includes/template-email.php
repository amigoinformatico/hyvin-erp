<div style="box-sizing: border-box; padding: 25px; 6%; width:100%">
	<div style="background-color: #CB79E6; padding: 10px; width: 80%;">
		<h1 style="color: #FFF; font-size: 22px; font-weight: normal;">Coopmercio</h1>
	</div>

	<div style="padding: 30px 0;">

		<?php e($mensaje); ?>

	</div>

	<!-- Footer: -->
	<div style="background-color: #CCC; padding: 15px 10px; width: 80%;">
		Coopmercio, &copy; <?php e(date('Y')); ?>
	</div>
</div>
