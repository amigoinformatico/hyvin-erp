<!DOCTYPE html>
<html lang="es">
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->

<head>
    <!-- START META SECTION -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?= $title ?></title>
    <meta name="description" content="">
    <!--/ END META SECTION -->

    <!-- Inicio CSS: -->
    <link href="<?php e(base_url()); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php e(base_url()); ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="<?php e(base_url()); ?>css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="<?php e(base_url()); ?>css/plugins/fullcalendar/fullcalendar.print.css" media="print" rel="stylesheet">

    <!-- DataTables: -->
    <!--<link href="<?php e(base_url()); ?>css/datatables.min.css" rel="stylesheet"> -->

    <!-- Toastr style -->
    <link href="<?php e(base_url()); ?>css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php e(base_url()); ?>js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <!-- Dropzone css -->
    <link href="<?php e(base_url()); ?>js/plugins/dropzone/dropzone.css" rel="stylesheet">


    <!-- CSS exclusivos por página: -->
    <?php
    if (isset($arrcss)) {
        foreach ($arrcss as $css) {
            ?>

            <link rel="stylesheet" href="<?= base_url() . $css ?>">

            <?php
        }
    }
    ?>

    <link href="<?php e(base_url()); ?>css/animate.css" rel="stylesheet" type="text/css">
    <link href="<?php e(base_url()); ?>css/style.css" rel="stylesheet" type="text/css">
    <link href="<?php e(base_url()); ?>css/custom.css" rel="stylesheet" type="text/css">
    <link href="<?php e(base_url()); ?>css/custom-calculadora.css" rel="stylesheet" type="text/css">
    <!-- Fin CSS -->

    <!-- Javascript(Modernizr) -->
    <!--<script src="<?php e(base_url()); ?>js/modernizr.custom.28468.js"></script> -->

    <script>
        //Variables globales para .js:
        var base_url = '<?= base_url() ?>';
    </script>
</head>