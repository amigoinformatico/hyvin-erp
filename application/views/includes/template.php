<?php 
$this->load->view('includes/bk-head');

//Open body + Header:
$this->load->view('includes/bk-header');

//Contenido dinámico:
$this->load->view($contenido);

//Cargando javascript:
$this->load->view('includes/bk-load-js');

$this->load->view('includes/bk-body-close');