</div> <!-- / #wrapper -->

<!-- Mainly scripts -->
<script src="<?php e(base_url()); ?>js/jquery.min.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/fullcalendar/moment.min.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/fullcalendar/fullcalendar.js"></script>
<script src='<?php e(base_url()); ?>js/plugins/fullcalendar/es.js'></script>
<script src="<?php e(base_url()); ?>js/popper.min.js"></script>
<script src="<?php e(base_url()); ?>js/bootstrap.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- / #wrapper -->





<!-- Flot -->
<script src="<?php e(base_url()); ?>js/plugins/flot/jquery.flot.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="<?php e(base_url()); ?>js/plugins/peity/jquery.peity.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php e(base_url()); ?>js/inspinia.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/pace/pace.min.js"></script>

<!-- Dropzone -->
<script src="<?= base_url() ?>js/plugins/dropzone/dropzone.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php e(base_url()); ?>js/inspinia.js"></script>
<script src="<?php e(base_url()); ?>js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php e(base_url()); ?>js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="<?php e(base_url()); ?>js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="<?php e(base_url()); ?>js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<!--<script src="<?php e(base_url()); ?>js/demo/sparkline-demo.js"></script> -->

<!-- ChartJS-->
<script src="<?php e(base_url()); ?>js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<!--<script src="<?php e(base_url()); ?>js/plugins/toastr/toastr.min.js"></script> -->

<!-- JS exclusivos por página: -->
<?php
if(isset($arrjs)){
    foreach($arrjs as $js){
    ?>

        <script src="<?= base_url().$js ?>"></script>

    <?php
    }
}
?>

<!-- JS custom exclusivos por página: -->
<?php
if(isset($arrcustomjs)){
    foreach($arrcustomjs as $customjs){
        ?>

        <script src="<?= base_url().$customjs ?>"></script>

        <?php
    }
}
?>

<!-- Custom JS: -->
<script src="<?= base_url() ?>js/custom.js"></script>



<script>
    $(document).ready(function () {

        var date_last_clicked = null;

        $('#calendar').fullCalendar({
            lang: 'es',
            buttonText: {
                today: 'hoy'
            },
            eventSources: [
                {
                    color: '#18b9e6',
                    textColor: '#000000',
                    events: function (start, end, timezone, callback) {
                        $.ajax({
                            url: '/calendar/getEvents',
                            dataType: 'json',
                            data: {
                                // our hypothetical feed requires UNIX timestamps
                                start: start.unix(),
                                end: end.unix()
                            },
                            success: function (msg) {
                                var events = msg.events;
                                callback(events);
                            }
                        });
                    }
                },
            ],
            dayClick: function (date, jsEvent, view) {
                date_last_clicked = $(this);
                $('#star_date_create').val(moment(date).format('YYYY/MM/DD HH:mm'));
                $(this).css('background-color', '#bed7f3');
                $('#addModal').modal();
            },
            eventClick: function (event, jsEvent, view) {
                $('#name').val(event.title);
                $('#description').val(event.description);
                $('#start_date').val(moment(event.start).format('YYYY/MM/DD HH:mm'));
                if (event.end) {
                    $('#end_date').val(moment(event.end).format('YYYY/MM/DD HH:mm'));
                } else {
                    $('#end_date').val(moment(event.start).format('YYYY/MM/DD HH:mm'));
                }
                $('#event_id').val(event.id);
                $('#editModal').modal();
            },
        });






    });


</script>
