<div class="content-header">
	<span class="icon-sound">
        <img src="img/icon-sound.png" alt="">    
    </span>

    <h1>Añadir nuevo cliente</h1> 

    <?php 
    polizaLine(5);
    ?>

</div>

<div class="content-body">
	<h2><span class="paso">5</span> Resultados invalidez</h2>
	<br><br>

	<form action="<?php e(base_url()); ?>" method="post" accept-charset="utf-8" role="form">
		<input type="hidden" name="page" id="page" value="invalidez/report"/>
		<input type="hidden" name="current_page" id="current_page" value="5"/>
		<input type="hidden" name="tiene_conyugue" id="tiene_conyugue" value="<?php echo $tiene_conyugue; ?>"/>

		<?php 
		$class_col = ($tiene_conyugue == 0)? 'col-xs-12 col-md-6 col-lg-3':'col-xs-12 col-sm-4';
		$oculto = ($tiene_conyugue == 0)? 'hidden':'';
		?>

		<div class="row no-mb">
			<div class="form-group col-xs-12 col-sm-4 text-right">
        		<label for="" class="frm-label">Tipo de invalidez</label>
    		</div>
    		<div class="form-group col-xs-12 col-sm-8 text-right">
				<select class="form-control text-right" id="tipo_invalidez" name="tipo_invalidez" >
		            <option value="ipa" <?php if(isset($tipo_invalidez) && $tipo_invalidez=="ipa") echo "selected"; ?> >IPA</option>
		            <option value="ipt" <?php if(isset($tipo_invalidez) && $tipo_invalidez=="ipt") echo "selected"; ?> >IPT</option>
		        </select>
		    </div>
		</div>
		<br><br>

		<div class="row no-mb">
			<div class="head-frm form-group text-center <?php echo $class_col; ?>">
        		&nbsp;
    		</div>
		    <div class="head-frm form-group <?php echo $class_col; ?>">
		        Titular
		    </div>
		    <div class="head-frm form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        Cónyuge
		    </div>

			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Gastos mensuales familiares</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly placeholder="" value="<?php print_number($titular_gastos_mensuales_familiares); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly placeholder="" value="<?php print_number($conyugue_gastos_mensuales_familiares); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Ingresos cónyuge</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_ingresos_conyugue); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_ingresos_conyugue); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Pensión de invalidez</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_pension_invalidez); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_pension_invalidez); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Saldo mensual</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_saldo_mensual); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_saldo_mensual); ?>">
		    </div>
		</div>

		<div class="row no-mb">
			<div class="form-group col-sm-12">
				<h3>Cobertura propuesta: hasta la jubilación</h3>	
			</div>

			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Fecha de la jubilación</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_date($titular_fecha_jubilacion);?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_date($conyugue_fecha_jubilacion); ?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Edad jubilación</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly 
		        value="<?php print_number($titular_edad_jubilacion); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly 
		        value="<?php print_number($conyugue_edad_jubilacion); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Años de cobertura</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" name="invalidez_titular_anyos_cobertura" id="invalidez_titular_anyos_cobertura" value="<?php print_number($invalidez_titular_anyos_cobertura); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" 
		            name="invalidez_conyugue_anyos_cobertura" id="invalidez_conyugue_anyos_cobertura" value="<?php print_number($invalidez_conyugue_anyos_cobertura); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Cobertura necesaria</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_cobertura_necesaria); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_cobertura_necesaria); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Deudas pendientes</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_deudas_pendientes); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_deudas_pendientes); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Adaptación vivienda</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_adaptacion_vivienda); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_adaptacion_vivienda); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Adaptación vehículo y otros</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_adaptacion_vehiculos_otros); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_adaptacion_vehiculos_otros); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Capitales ahorro titular</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_capitales_ahorro_titular);?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_capitales_ahorro_conyugue);?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Capitales ahorro de ambos</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_capitales_ahorro_ambos); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_capitales_ahorro_ambos); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Saldo: seguro de invalidez necesario</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_saldo_seguro_vida_necesario); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_saldo_seguro_vida_necesario); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Seguros ya contratados</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_seguros_ya_contratados); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_seguros_ya_contratados); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Necesidad de seguro de invalidez</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_necesidad_seguro_vida); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_necesidad_seguro_vida); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Capital propuesto</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" id="invalidez_titular_capital_propuesto" name="invalidez_titular_capital_propuesto" value="<?php print_number($invalidez_titular_capital_propuesto); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" id="invalidez_conyugue_capital_propuesto" name="invalidez_conyugue_capital_propuesto" value="<?php print_number($invalidez_conyugue_capital_propuesto); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Prima aproximada</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" id="invalidez_titular_prima_aproximada" name="invalidez_titular_prima_aproximada" value="<?php print_number($invalidez_titular_prima_aproximada); ?>">       
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" id="invalidez_conyugue_prima_aproximada" name="invalidez_conyugue_prima_aproximada" value="<?php print_number($invalidez_conyugue_prima_aproximada); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Impuesto aproximado a pagar</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_total_cuota); ?>">      
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_total_cuota); ?>">
		    </div>
		</div>











		<br><br>
		<div class="row">
			<div class="form-group col-sm-12">
				<br>
				<input type="submit" name="submit_anterior" class="hyvin-boton" value="ANTERIOR">
				&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit" class="hyvin-boton" value="SIGUIENTE">
			</div>
		</div>
	</form>
</div>