<div class="content-header">
	<span class="icon-sound">
        <img src="img/icon-sound.png" alt="">    
    </span>

    <h1>Añadir nuevo cliente</h1> 

    <?php 
    polizaLine(7);
    ?>

</div>

<div class="content-body">
	<h2><span class="paso">7</span> Resultados dependencia</h2>
	<br><br>

	<form action="<?php e(base_url()); ?>" method="post" accept-charset="utf-8" role="form">
		<input type="hidden" name="page" id="page" value="fallecimiento/report"/>
		<input type="hidden" name="tiene_conyugue" id="tiene_conyugue" value="<?php echo $tiene_conyugue; ?>"/>

		<?php 
		$class_col = ($tiene_conyugue == 0)? 'col-xs-12 col-md-6 col-lg-3':'col-xs-12 col-sm-4';
		$oculto = ($tiene_conyugue == 0)? 'hidden':'';
		?>

		<div class="row no-mb">
			<div class="head-frm form-group <?php echo $class_col; ?> hidden-sm hidden-xs">
		        &nbsp;
		    </div>
		    <div class="head-frm form-group <?php echo $class_col; ?>">
		        Titular
		    </div>
		    <div class="head-frm form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        Cónyuge
		    </div>

			<?php 
			if($tiene_conyugue == 0){
			?>

		    	<div class="head-frm form-group col-md-3 hidden-md hidden-sm hidden-xs">
		        	&nbsp;
		    	</div>
			    <div class="head-frm form-group col-md-3 hidden-md hidden-sm hidden-xs">
			        Titular
			    </div>

			<?php 
			}
			?>

		    <div class="clearfix"></div>
			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Fecha de nacimiento</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly placeholder="" value="<?php print_date($titular_fecha_nacimiento);?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control text-right <?php hide_no_conyugue($tiene_conyugue);?>" readonly placeholder="" value="<?php print_date($conyugue_fecha_nacimiento);?>">    
		    </div>
		</div>
		<br>

		<div class="row no-mb">
			<div class="form-group col-sm-12">
				<h3>Escenario de dependencia calculado a los 80 años</h3>
			</div>
			<div class="clearfix"></div>

			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Fecha de su 80 cumpleaños</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_date($titular_fecha_80_cumpleanyos); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_date($conyugue_fecha_80_cumpleanyos); ?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Años hasta dependencia</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_anyos_hasta_dependencia); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_anyos_hasta_dependencia); ?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Fecha de su jubilación</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_date($titular_fecha_jubilacion);?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_date($conyugue_fecha_jubilacion);?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Edad a la jubilación</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_edad_jubilacion); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_edad_jubilacion); ?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Años desde la jubilación a dependencia</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_anyos_desde_jubilacion_a_dependencia); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_anyos_desde_jubilacion_a_dependencia); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Coste de la dependencia HOY</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" id="dependencia_titular_coste_dependencia_hoy" name="dependencia_titular_coste_dependencia_hoy" value="<?php print_number($dependencia_titular_coste_dependencia_hoy); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" id="dependencia_conyugue_coste_dependencia_hoy" name="dependencia_conyugue_coste_dependencia_hoy" value="<?php print_number($dependencia_conyugue_coste_dependencia_hoy); ?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Coste de la dependencia ACTUALIZADO</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly  value="<?php print_number($titular_coste_dependencia_actualizado); ?>">   
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_coste_dependencia_actualizado); ?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Años de dependencia</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" name="dependencia_titular_anyos_dependencia" id="dependencia_titular_anyos_dependencia"  placeholder="" value="<?php print_number($dependencia_titular_anyos_dependencia); ?>"> 
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" name="dependencia_conyugue_anyos_dependencia" id="dependencia_conyugue_anyos_dependencia" value="<?php print_number($dependencia_conyugue_anyos_dependencia); ?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Coste total de dependencia</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_coste_total_dependencia); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_coste_total_dependencia); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Porcentaje de cobertura</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" name="dependencia_titular_porcentaje_cobertura" id="dependencia_titular_porcentaje_cobertura" value="<?php print_number($dependencia_titular_porcentaje_cobertura); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" name="dependencia_conyugue_porcentaje_cobertura" id="dependencia_conyugue_porcentaje_cobertura" value="<?php print_number($dependencia_conyugue_porcentaje_cobertura); ?>">    
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Necesidad de dependencia</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_necesidad_dependencia); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_necesidad_dependencia); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="hidden">
		        <div class="form-group <?php echo $class_col; ?> text-right">
		            <label for="" class="frm-label">Contratante</label>
		        </div>
		        <div class="form-group <?php echo $class_col; ?>">
		            <input type="text" class="form-control" readonly value="<?php print_ifset($titular_contratante);?>">    
		        </div>
		        <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		            <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_ifset($conyugue_contratante);?>">    
		        </div>
		        <div class="clearfix visible-xs"></div>
		    </div>

		    <div class="hidden">
		        <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Modalidad</label>
		        </div>
		        <div class="form-group <?php echo $class_col; ?>">
		            <input type="text" class="form-control" readonly value="<?php print_ifset($titular_modalidad);?>"></td>
		                <td><input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_ifset($conyugue_modalidad);?>">   
		        </div>
		        <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		            <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_ifset($conyugue_modalidad);?>">
		        </div>  
		        <div class="clearfix visible-xs"></div>
		        <div class="clearfix hidden-xs"></div>  
		    </div>

		    <div class="hidden">
		        <div class="form-group <?php echo $class_col; ?> text-right">
		            <label for="" class="frm-label">Capital a la jubilación</label>
		        </div>
		        <div class="form-group <?php echo $class_col; ?>">
		            <input type="text" class="form-control" name="dependencia_titular_capital_jubilacion" id="dependencia_titular_capital_jubilacion" value="<?php print_number($dependencia_titular_capital_jubilacion); ?>">    
		        </div>
		        <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		            <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" name="dependencia_conyugue_capital_jubilacion" id="dependencia_conyugue_capital_jubilacion" value="<?php print_number($dependencia_conyugue_capital_jubilacion); ?>"> 
		        </div>
		        <div class="clearfix visible-xs"></div>
		        <div class="clearfix hidden-xs"></div>   
		    </div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Aportación anual</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" name="dependencia_titular_aportacion_anual" id="dependencia_titular_aportacion_anual" value="<?php print_number($dependencia_titular_aportacion_anual); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" name="dependencia_conyugue_aportacion_anual" id="dependencia_conyugue_aportacion_anual" value="<?php print_number($dependencia_conyugue_aportacion_anual); ?>">  
		    </div>
		</div>

		<div class="row no-mb">
			<div class="form-group col-sm-12">
				<h3>Beneficio fiscal de las aportaciones</h3>
			</div>
			<div class="clearfix"></div>

			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Importe a reducir la base liquidable</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_importe_reducir_base_liquidadora); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_importe_reducir_base_liquidadora); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Tipo marginal HOY</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_tipo_marginal_hoy); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_tipo_marginal_hoy); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Beneficio fiscal HOY</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_beneficio_fiscal); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_beneficio_fiscal); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Tipo marginal a la jubilación</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_tipo_marginal_jubilacion); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_tipo_marginal_jubilacion); ?>">   
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Beneficio fiscal a la jubilación</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_beneficio_fiscal_jubilacion); ?>">    
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_beneficio_fiscal_jubilacion); ?>">    
		    </div>
		</div>
		<br>

		<div class="row no-mb">
			<div class="form-group col-sm-12">
	        	<h3>Forma de cobro</h3>
	        </div>

			<div class="form-group col-sm-12">
				<div class="table-responsive">
		            <table class="tabla table table-bordered">
		                <tr class="tbl-head-primary">
		                    <th></th>
		                    <th colspan="2">Titular</th>                     
		                    <th colspan="2" class='<?php hide_no_conyugue($tiene_conyugue);?>'>Cónyuge</th>
		                </tr>

		                <tr class="tbl-head">
		                    <th width="<?php width_cobro_0($tiene_conyugue);?>"></th>
		                    <th width="<?php width_cobro_1($tiene_conyugue);?>">A Capital</th>
		                    <th width="<?php width_cobro_1($tiene_conyugue);?>">A Renta</th>
		                    <th width="<?php width_cobro_2($tiene_conyugue);?>" class='<?php hide_no_conyugue($tiene_conyugue);?>'>A Capital</th>
		                    <th width="<?php width_cobro_2($tiene_conyugue);?>" class='<?php hide_no_conyugue($tiene_conyugue);?>'>A Renta</th>
		                </tr>
		                <tr>
		                    <td>Reparto del Cobro del Capital Final</td>
		                    <td><input type="text" class="form-control" readonly value="<?php print_number($titular_reparto_cobro_capital_final_capital); ?>"></td>
		                    <td><input type="text" class="form-control" readonly value="<?php print_number($titular_reparto_cobro_capital_final_renta); ?>"></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'><input type="text" class="form-control" readonly value="<?php print_number($conyugue_reparto_cobro_capital_final_capital); ?>"></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'><input type="text" class="form-control" readonly value="<?php print_number($conyugue_reparto_cobro_capital_final_renta); ?>"></td>
		                </tr>
		                <tr>
		                    <td>Incremento de la BL</td>
		                    <td><input type="text" class="form-control" readonly value="<?php print_number($titular_incremento_bl); ?>"></td>
		                    <td></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'><input type="text" class="form-control" readonly value="<?php print_number($conyugue_incremento_bl); ?>"></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                </tr>
		                <tr>
		                    <td>Años de Renta</td>
		                    <td></td>
		                    <td><input type="text" class="form-control" 
		                               id="dependencia_titular_anyos_renta" name="dependencia_titular_anyos_renta"
		                               value="<?php print_number($dependencia_titular_anyos_renta); ?>"></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'><input type="text" class="form-control" 
		                            id="dependencia_conyugue_anyos_renta" name="dependencia_conyugue_anyos_renta"                                                                                  
		                            value="<?php print_number($dependencia_conyugue_anyos_renta); ?>"></td>
		                </tr>
		                <tr>
		                    <td>Renta Anual Obtenida</td>
		                    <td></td>
		                    <td><input type="text" class="form-control" readonly value="<?php print_number($titular_renta_anual_obtenida); ?>"></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'><input type="text" class="form-control" readonly value="<?php print_number($conyugue_renta_anual_obtenida); ?>"></td>
		                </tr>
		                <tr>
		                    <td>Renta Mensual Obtenida</td>
		                    <td></td>
		                    <td><input type="text" class="form-control" readonly value="<?php print_number($titular_renta_mensual_obtenida); ?>"></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'><input type="text" class="form-control" readonly value="<?php print_number($conyugue_renta_mensual_obtenida); ?>"></td>
		                </tr>
		            </table>
		        </div>
			</div>
		</div>
		<br>

		<div class="row no-mb">
			<div class="form-group col-sm-12">
	        	<h3>Comparativa Fiscal Prestaciones</h3>
	        </div>

			<div class="form-group col-sm-12">
				<div class="table-responsive">
		            <table class="tabla table table-bordered">
		                <tr class="tbl-head-primary">
		                    <th></th>
		                    <th colspan="3">Titular</td>                   h 
		                    <th colspan="3" class='<?php hide_no_conyugue($tiene_conyugue);?>'>Cónyuge</th>
		                </tr>
		                <tr class="tbl-head">
		                    <th width='16'></th>
		                    <th width='<?php width_compara_1($tiene_conyugue);?>'>Solo con SS</th>
		                    <th width='<?php width_compara_1($tiene_conyugue);?>'>SS+Capital</th>
		                    <th width='<?php width_compara_1($tiene_conyugue);?>'>SS+Renta</td>           h 
		                    <th width='<?php width_compara_2($tiene_conyugue);?>' class='<?php hide_no_conyugue($tiene_conyugue);?>'>Solo con SS</th>
		                    <th width='<?php width_compara_2($tiene_conyugue);?>' class='<?php hide_no_conyugue($tiene_conyugue);?>'>SS+Capital</th>
		                    <th width='<?php width_compara_2($tiene_conyugue);?>' class='<?php hide_no_conyugue($tiene_conyugue);?>'>SS+Renta</th>
		                </tr>
		                <tr>
		                    <td>Base Liquidable</td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_base_ss"
		                                name="titular_base_ss" placeholder="" value="<?php print_number($titular_base_ss);?>" >
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_base_ss_capital"
		                                name="titular_base_ss_capital" placeholder="" value="<?php print_number($titular_base_ss_capital); ?>">
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_base_ss_renta"
		                                name="titular_base_ss_renta" placeholder="" value="<?php print_number($titular_base_ss_renta); ?>">
		                        </div>
		                    </td>
		                                    
		                   <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_base_ss"
		                                name="conyugue_base_ss" placeholder="" value="<?php print_number($conyugue_base_ss); ?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_base_ss_capital"
		                                name="conyugue_base_ss_capital" placeholder="" value="<?php print_number($conyugue_base_ss_capital); ?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_base_ss_renta"
		                                name="conyugue_base_ss_renta" placeholder="" value="<?php print_number($conyugue_base_ss_renta); ?>">
		                        </div>
		                    </td>
		                    
		                </tr>
		                <tr>
		                    <td>Tipo Marginal Jubilación</td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_tipo_marginal_ss"
		                                name="titular_tipo_marginal_ss" placeholder="" value="<?php print_number($titular_tipo_marginal_ss); ?>">
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_tipo_marginal_ss_capital"
		                                name="titular_tipo_marginal_ss_capital" placeholder="" value="<?php print_number($titular_tipo_marginal_ss_capital);?>">
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_tipo_marginal_ss_renta"
		                                name="titular_tipo_marginal_ss_renta" placeholder="" value="<?php print_number($titular_tipo_marginal_ss_renta);?>">
		                        </div>
		                    </td>
		                                    
		                   <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_tipo_marginal_ss"
		                                name="conyugue_tipo_marginal_ss" placeholder="" value="<?php print_number($conyugue_tipo_marginal_ss);?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_tipo_marginal_ss_capital"
		                                name="conyugue_tipo_marginal_ss_capital" placeholder="" value="<?php print_number($conyugue_tipo_marginal_ss_capital);?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_tipo_marginal_ss_renta"
		                                name="conyugue_tipo_marginal_ss_renta" placeholder="" value="<?php print_number($conyugue_tipo_marginal_ss_renta);?>">
		                        </div>
		                    </td>
		                    
		                </tr>
		                <tr class='hidden'>
		                    <td>Total Cuota Estatal</td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_cuota_estatal_ss"
		                                name="titular_cuota_estatal_ss" placeholder="" value="<?php print_number($titular_cuota_estatal_ss); ?>">
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_cuota_estatal_ss_capital"
		                                name="titular_cuota_estatal_ss_capital" placeholder="" value="<?php print_number($titular_cuota_estatal_ss_capital);?>">
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_cuota_estatal_ss_renta"
		                                name="titular_cuota_estatal_ss_renta" placeholder="" value="<?php print_number($titular_cuota_estatal_ss_renta); ?>">
		                        </div>
		                    </td>
		                                    
		                   <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_cuota_estatal_ss"
		                                name="conyugue_cuota_estatal_ss" placeholder="" value="<?php print_number($conyugue_cuota_estatal_ss); ?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_cuota_estatal_ss_capital"
		                                name="conyugue_cuota_estatal_ss_capital" placeholder="" value="<?php print_number($conyugue_cuota_estatal_ss_capital); ?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_cuota_estatal_ss_renta"
		                                name="conyugue_cuota_estatal_ss_renta" placeholder="" value="<?php print_number($conyugue_cuota_estatal_ss_renta); ?>">
		                        </div>
		                    </td>
		                    
		                </tr>
		                <tr class='hidden'>
		                    <td>Total Cuota Autonómica</td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_cuota_autonomica_ss"
		                                name="titular_cuota_autonomica_ss" placeholder="" value="<?php print_number($titular_cuota_autonomica_ss); ?>">
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_cuota_autonomica_ss_capital"
		                                name="titular_cuota_autonomica_ss_capital" placeholder="" value="<?php print_number($titular_cuota_autonomica_ss_capital); ?>">
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_cuota_autonomica_ss_renta"
		                                name="titular_cuota_autonomica_ss_renta" placeholder="" value="<?php print_number($titular_cuota_autonomica_ss_renta); ?>">
		                        </div>
		                    </td>
		                                    
		                   <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_cuota_autonomica_ss"
		                                name="conyugue_cuota_autonomica_ss" placeholder="" value="<?php print_number($conyugue_cuota_autonomica_ss); ?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_cuota_autonomica_ss_capital"
		                                name="conyugue_cuota_autonomica_ss_capital" placeholder="" value="<?php print_number($conyugue_cuota_autonomica_ss_capital); ?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_cuota_autonomica_ss_renta"
		                                name="conyugue_cuota_autonomica_ss_renta" placeholder="" value="<?php print_number($conyugue_cuota_autonomica_ss_renta); ?>">
		                        </div>
		                    </td>
		                    
		                </tr>
		                <tr>
		                    <td>Total Cuota</td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_total_cuota_ss"
		                                name="titular_total_cuota_ss" placeholder="" value="<?php print_number($titular_total_cuota_ss); ?>">
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_total_cuota_ss_capital"
		                                name="titular_total_cuota_ss_capital" placeholder="" value="<?php print_number($titular_total_cuota_ss_capital); ?>">
		                        </div>
		                    </td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_total_cuota_ss_renta"
		                                name="titular_total_cuota_ss_renta" placeholder="" value="<?php print_number($titular_total_cuota_ss_renta); ?>">
		                        </div>
		                    </td>
		                                    
		                   <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_total_cuota_ss"
		                                name="conyugue_total_cuota_ss" placeholder="" value="<?php print_number($conyugue_total_cuota_ss); ?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_total_cuota_ss_capital"
		                                name="conyugue_total_cuota_ss_capital" placeholder="" value="<?php print_number($conyugue_total_cuota_ss_capital); ?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_total_cuota_ss_renta"
		                                name="conyugue_total_cuota_ss_renta" placeholder="" value="<?php print_number($conyugue_total_cuota_ss_renta); ?>">
		                        </div>
		                    </td>
		                    
		                </tr>
		                <tr>
		                    <td>Coste Fiscal Capital</td>
		                    <td></td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_coste_fiscal_capital"
		                                name="titular_coste_fiscal_capital" placeholder="" value="<?php print_number($titular_coste_fiscal_capital); ?>">
		                        </div>
		                    </td>
		                    <td></td>
		                                    
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_coste_fiscal_capital"
		                                name="conyugue_coste_fiscal_capital" placeholder="" value="<?php print_number($conyugue_coste_fiscal_capital); ?>">
		                        </div>
		                    </td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    
		                </tr>
		                <tr>
		                    <td>Coste Fiscal Renta 1 año</td>
		                    <td></td>
		                    <td></td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_coste_fiscal_renta_1"
		                                name="titular_coste_fiscal_renta_1" placeholder="" value="<?php print_number($titular_coste_fiscal_renta_1); ?>">
		                        </div>
		                    </td>
		                                    
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_coste_fiscal_renta_1"
		                                name="conyugue_coste_fiscal_renta_1" placeholder="" value="<?php print_number($conyugue_coste_fiscal_renta_1); ?>">
		                        </div>
		                    </td>
		                </tr>
		                <tr>
		                    <td>Coste Fiscal Renta x años de Renta</td>
		                    <td></td>
		                    <td></td>
		                    <td>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="titular_coste_fiscal_x"
		                                name="titular_coste_fiscal_x" placeholder="" value="<?php print_number($titular_coste_fiscal_x); ?>">
		                        </div>
		                    </td>
		                                    
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'></td>
		                    <td class='<?php hide_no_conyugue($tiene_conyugue);?>'>
		                         <div class="input-group">
		                            <input type="text" class="form-control" readonly id="conyugue_coste_fiscal_x"
		                                name="conyugue_coste_fiscal_x" placeholder="" value="<?php echo print_number($conyugue_coste_fiscal_x); ?>">
		                        </div>
		                    </td>
		                </tr>
		            </table>
		        </div>

			</div>
		</div>
		<br><br>

		<div class="row">
			<div class="form-group col-sm-12">
				<br>
				<input type="submit" name="submit_anterior" class="hyvin-boton" value="ANTERIOR">
			</div>
		</div>
	</form>
</div>