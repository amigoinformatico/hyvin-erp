<div class="content-header">
	<span class="icon-sound">
        <img src="img/icon-sound.png" alt="">    
    </span>

    <h1>Añadir nuevo cliente</h1> 

    <?php 
    polizaLine(3);
    ?>

</div>

<div class="content-body">
	<h2><span class="paso">3</span> Resultados Incapacidad Temporal (IT)</h2>
	<br><br>

	<form action="<?php e(base_url()); ?>" method="post" accept-charset="utf-8" role="form">
		<input type="hidden" name="page" id="page" value="fallecimiento/report"/>
		<input type="hidden" name="current_page" id="current_page" value="3"/>
		<input type="hidden" name="tiene_conyugue" id="tiene_conyugue" value="<?php echo $tiene_conyugue; ?>"/>

		<?php 
		$class_col = ($tiene_conyugue == 0)? 'col-xs-12 col-md-6 col-lg-3':'col-xs-12 col-sm-4';
		$oculto = ($tiene_conyugue == 0)? 'hidden':'';
		?>

		<div class="row no-mb">
			<div class="head-frm form-group <?php echo $class_col; ?> hidden-sm hidden-xs">
		        &nbsp;
		    </div>
		    <div class="head-frm form-group <?php echo $class_col; ?>">
		        Titular
		    </div>
		    <div class="head-frm form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        Cónyuge
		    </div>

			<?php 
			if($tiene_conyugue == 0){
			?>

		    	<div class="head-frm form-group col-md-3 hidden-md hidden-sm hidden-xs">
		        	&nbsp;
		    	</div>
			    <div class="head-frm form-group col-md-3 hidden-md hidden-sm hidden-xs">
			        Titular
			    </div>

			<?php 
			}
			?>

		    <div class="clearfix"></div>

			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">¿Es autónomo?</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly placeholder="" value="<?php print_ifset($titular_es_autonomo);?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control" readonly placeholder="" value="<?php print_ifset($conyugue_es_autonomo);?>">
		    </div>
		    <div class="clearfix visible-sm"></div>

			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Ingresos por la actividad autónomos</label>
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_ingresos_actividad_autonomos);?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?> text-right" readonly value="<?php print_number($conyugue_ingresos_actividad_autonomos);?>">
		    </div>
		    <div class="clearfix visible-xs visible-sm"></div>
		    <div class="clearfix hidden-xs"></div>

			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Base reguladora diaria</label>    
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_base_reguladora_diaria);?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?> text-right" readonly value="<?php print_number($conyugue_base_reguladora_diaria);?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Prestaciones de la SS (1er mes)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_prestaciones_ss_primer_mes); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?> text-right" readonly value="<?php print_number($conyugue_prestaciones_ss_primer_mes); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Prestaciones de la SS (2º y siguientes)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_prestaciones_ss_siguientes_meses); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?> text-right" readonly value="<?php print_number($conyugue_prestaciones_ss_siguientes_meses); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Pago cuota de autónomos</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_pago_cuota_autonomos); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?> text-right" readonly value="<?php print_number($conyugue_pago_cuota_autonomos); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Importes netos (1er mes)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control text-right" readonly value="<?php print_number($titular_importe_neto_primer_mes); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?> text-right" readonly value="<?php print_number($conyugue_importe_neto_primer_mes); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Importes netos (2º y siguientes)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_importe_neto_siguientes_meses); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_importe_neto_siguientes_meses); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Descobertura (1er mes)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_descobertura_primer_mes); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_descobertura_primer_mes); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Descobertura (2º y siguientes)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_descobertura_siguientes_meses); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_descobertura_siguientes_meses); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Porcentaje de cobertura</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" name="it_titular_porcentaje_cobertura" id="it_titular_porcentaje_cobertura" value="<?php print_number($it_titular_porcentaje_cobertura); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" 
		            name="it_conyugue_porcentaje_cobertura" id="it_conyugue_porcentaje_cobertura"
		            value="<?php print_number($it_conyugue_porcentaje_cobertura); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Necesidad de seguro de IT (calculado sobre el segundo mes)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_necesidad_seguro_it_segundo_mes); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_necesidad_seguro_it_segundo_mes); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Necesidad de seguro de IT (por día)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_necesidad_seguro_it_por_dia); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_necesidad_seguro_it_por_dia); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Capital propuesto por día de baja</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" id="titular_capital_propuesto_dia_baja" name="titular_capital_propuesto_dia_baja" 
		        value="<?php print_number($titular_capital_propuesto_dia_baja); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" 
		            id="conyugue_capital_propuesto_dia_baja" name="conyugue_capital_propuesto_dia_baja"
		            value="<?php print_number($conyugue_capital_propuesto_dia_baja); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Franquicia (días)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" id="titular_franquicia_dias" name="titular_franquicia_dias"
		        value="<?php print_number($titular_franquicia_dias); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" 
		        id="conyugue_franquicia_dias" name="conyugue_franquicia_dias"
		        value="<?php print_number($conyugue_franquicia_dias); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Duración máxima (días)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" id="titular_duracion_maxima_dias" name="titular_duracion_maxima_dias"
		        value="<?php print_number($titular_duracion_maxima_dias); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" 
		        id="conyugue_duracion_maxima_dias" name="conyugue_duracion_maxima_dias"
		        value="<?php print_number($conyugue_duracion_maxima_dias); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Prima anual</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" id="it_titular_prima_anual" name="it_titular_prima_anual"
		        value="<?php print_number($it_titular_prima_anual); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" 
		        id="it_conyugue_prima_anual" name="it_conyugue_prima_anual"
		        value="<?php print_number($it_conyugue_prima_anual); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		</div>
		<br>

		<div class="row no-mb">
			<div class="form-group col-sm-12">
				<h3>Fiscalidad de las aportaciones (aproximada)</h3>
			</div>
			<div class="clearfix"></div>

			<div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Importe deducible en el IRPF</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_importe_deducible_irpf); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($conyugue_importe_deducible_irpf); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Tipo marginal</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_tipo_marginal); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control  <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_tipo_marginal); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Posible beneficio fiscal</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_posible_beneficio_fiscal); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_posible_beneficio_fiscal); ?>">    
		    </div>
		</div>

		<div class="row no-mb">
			<div class="form-group col-sm-12">
				<h3>Ejemplo de fiscalidad de las prestaciones (aproximada)</h3>
		    </div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Días de indemnización percibida</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" 
		        id="titular_dias_indemnizacion_percibida" name="titular_dias_indemnizacion_percibida"
		        value="<?php print_number($titular_dias_indemnizacion_percibida); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control" 
		        id="conyugue_dias_indemnizacion_percibida" name="conyugue_dias_indemnizacion_percibida"     value="<?php print_number($conyugue_dias_indemnizacion_percibida); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Indemnización (€)</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_indemnizacion); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_indemnizacion); ?>">
		    </div>
		    <div class="clearfix visible-xs"></div>
		    <div class="clearfix hidden-xs"></div>

		    <div class="form-group <?php echo $class_col; ?> text-right">
		        <label for="" class="frm-label">Impuesto aproximado a pagar</label>   
		    </div>
		    <div class="form-group <?php echo $class_col; ?>">
		        <input type="text" class="form-control" readonly value="<?php print_number($titular_cuota_a_pagar); ?>">
		    </div>
		    <div class="form-group <?php echo $class_col; ?> <?php echo $oculto; ?>">
		        <input type="text" class="form-control <?php hide_no_conyugue($tiene_conyugue);?>" readonly value="<?php print_number($conyugue_cuota_a_pagar); ?>">
		    </div>
		</div>
		<br><br>

		<div class="row">
			<div class="form-group col-sm-12">
				<br>
				<input type="submit" name="submit_anterior" class="hyvin-boton" value="ANTERIOR">
				&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit" class="hyvin-boton" value="SIGUIENTE">
			</div>
		</div>
	</form>
</div>