<!-- Javascript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

<div class="content-header">
	<span class="icon-sound">
        <img src="img/icon-sound.png" alt="">    
    </span>

    <h1>¡Hola, Manuel! Así te está yendo.
        <a href="">Estadísticas avanzadas.</a>
    </h1> 
</div>

<ul class="nav nav-tabs hyvin-tabs" role="tablist">
	<li class="nav-item">
	    <a class="nav-link active" href="#resumen" role="tab" data-toggle="tab">Tu resumen</a>
	</li>
	
	<li class="nav-item">
	    <a class="nav-link" href="#polizas" role="tab" data-toggle="tab">Últimas pólizas</a>
	</li>
	
	<li class="nav-item">
	    <a class="nav-link" href="#clientes" role="tab" data-toggle="tab">Listado de clientes</a>
	</li>

	<!--<li class="nav-item">
	    <img class="icon-filtrar" src="img/icon-filtrar.png" alt="">
	</li> -->
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<!-- Resumen: -->
  	<div role="tabpanel" class="tab-pane fade in show active" id="resumen">
  		<div class="row">
	  		<div class="col-sm dsb-contador">
	  			<span>17</span>	
	  			<p>PÓLIZAS</p>
	  		</div>	

	  		<div class="col-sm dsb-contador">
	  			<span>12</span>	
	  			<p>CLIENTES NUEVOS</p>
	  		</div>	

	  		<div class="col-sm dsb-contador last">
	  			<span>45</span>	
	  			<p>MIL EUROS TOTALES</p>
	  		</div>	
	  		<div class="clearfix"></div>
  		</div>

  		<div class="col-sm dsb-grafic">
			<canvas id="comboBarLineChart"></canvas>
  		</div>
  	</div>
  	<!-- / Resumen -->

	<!-- Pólizas: -->
  	<div role="tabpanel" class="tab-pane fade" id="polizas">
		<div class="table-responsive">
			<table id="tblPolizas" class="table table-hover display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>CLIENTE</th>
						<th>DNI</th>
						<th>FECHA NACIMIENTO</th>
						<th>PÓLIZA</th>
					<!--	<th>FECHA</th>
						<th>IMPORTE</th>-->
					</tr>
				</thead>
			</table>
		</div> 
		<br>

		<div class="col-sm-12">
			<a href="" class="color_azul" style="font-weight: bold;">Cargar más</a>	
		</div>
  	</div>
  	<!-- / Pólizas -->

  	<!-- Clientes: -->
  	<div role="tabpanel" class="tab-pane fade" id="clientes">
		<div class="table-responsive">
			<table id="tblClientes" class="table table-hover display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>CLIENTE</th>
						<th>DNI</th>
						<th>FECHA NACIMIENTO</th>
						<th>PÓLIZA</th>

						<th>PRIMA TOTAL</th>
					</tr>
				</thead>
			</table>
		</div>	
		<br>

		<div class="col-sm-12">
			<a href="" class="color_azul" style="font-weight: bold;">Cargar más</a>	
		</div>
  	</div>
  	<!-- / Clientes -->
</div>

<script>
var ctx2 = document.getElementById("comboBarLineChart").getContext('2d');
var comboBarLineChart = new Chart(ctx2, {
    type: 'bar',
    data: {
        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        datasets: [{
                type: 'line',
                label: 'Dataset 1',
                borderColor: '#484c4f',
                borderWidth: 3,
                fill: false,
                data: [12, 19, 3, 5, 2, 3, 13, 17, 11, 8, 11, 9],
            }, {
                type: 'bar',
                label: 'Dataset 2',
                backgroundColor: '#FF6B8A',
                data: [10, 11, 7, 5, 9, 13, 10, 16, 7, 8, 12, 5],
                borderColor: 'white',
                borderWidth: 0
            }, {
                type: 'bar',
                label: 'Dataset 3',
                backgroundColor: '#059BFF',
                data: [10, 11, 7, 5, 9, 13, 10, 16, 7, 8, 12, 5],
            }], 
            borderWidth: 1
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>


