<div class="content-header">
    <span class="icon-sound">
        <img src="img/icon-sound.png" alt="">    
    </span>

    <h1>Enlaces recomendados</h1> 
</div>

<ul class="nav nav-tabs hyvin-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" href="#todos" role="tab" data-toggle="tab">Todos</a>
    </li>
    
    <li class="nav-item">
        <a class="nav-link" href="#favoritos" role="tab" data-toggle="tab">Favoritos</a>
    </li>

    <li style="margin-left: 52%;">
        <button type="button" class="hyvin-boton"  data-toggle="modal" data-target="#addLink">
            <i class="material-icons">Añadir</i>
        </button>   
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <!-- Resumen: -->
    <div role="tabpanel" class="tab-pane fade in show active" id="todos">
        <div class="table-responsive">
            <table class="table table-hover display">
                <thead>
                    <tr>
                        <th></th>
                        <th>ENLACE</th>
                        <th>AÑADIDO</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($links as $link): ?>
                        <tr>
                            <td>
                                <span class="fa fa-star-o"></span>
                            </th>
                            <td><strong class="color_azul"><?= $link->name?></strong></td>
                            <td>
                                <?= $link->created_at ?>
                            </td>
                            <td>
                                <strong><a href="/link/destroy/<?=$link->id?>" class="color_azul">Eliminar</a></strong>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Favoritos: -->
    <div role="tabpanel" class="tab-pane fade in show" id="favoritos">
        <div class="table-responsive">
            <table class="table table-hover display">
                <thead>
                    <tr>
                        <th></th>
                        <th>ENLACE</th>
                        <th>AÑADIDO</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($links as $link): ?>
                        <tr>
                            <td>
                                <span class="fa fa-star-o favorito"></span>
                            </th>
                            <td><strong class="color_azul"><?= $link->name?></strong></td>
                            <td>
                                <?= $link->created_at ?>
                            </td>
                            <td>
                                <strong><a href="/link/destroy/<?=$link->id?>" class="color_azul">Eliminar</a></strong>
                            </td>
                        </tr>
                    <?php endforeach; ?>    
                </tbody>
            </table>
        </div>
    </div>
</td>

<!-- Modals: -->
<div class="modal fade" id="addLink" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= form_open('link/store')?>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrar Enlaces</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="enlace" class="bmd-label-floating">Enlace</label>
                    <input type="text" name="enlace" class="form-control" id="enlace">
                    <span class="bmd-help">Añade un enlace</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
            <?= form_close()?>
        </div>
    </div>
</div>