<div class="content-header">
    <h1>Calendario</h1>
    
</div>

<div class="content-body">
    <div class="row">
        <div id="calendar"></div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Añadir evento de calendario</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <?php echo form_open('/calendar/add_event', array("class" => "form-horizontal")) ?>
                <div class="form-group">
                    <label for="p-in" class="col-sm-4 label-heading">Nombre del evento</label>
                    <div class="col-sm-12 ui-front">
                        <input type="text" class="form-control" name="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="p-in" class="col-sm-4 label-heading">Descripción</label>
                    <div class="col-sm-12 ui-front">
                        <input type="text" class="form-control" name="description">
                    </div>
                </div>
                <div class="form-group">
                    <label for="p-in" class="col-sm-4 label-heading">Fecha de inicio</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="start_date" id="star_date_create">
                    </div>
                </div>
                <div class="form-group">
                    <label for="p-in" class="col-sm-4 label-heading" >Fecha final</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control " name="end_date">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <input type="submit" class="btn btn-primary" value="Añadir Evento">
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Actualizar calendario de eventos</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <?php echo form_open(site_url("calendar/edit_event"), array("class" => "form-horizontal")) ?>
                <div class="form-group">
                    <label for="p-in" class="col-sm-4 label-heading">Nombre del evento</label>
                    <div class="col-sm-12 ui-front">
                        <input type="text" class="form-control" name="name" value="" id="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="p-in" class="col-sm-4 label-heading">Descripción</label>
                    <div class="col-sm-12 ui-front">
                        <input type="text" class="form-control" name="description" id="description">
                    </div>
                </div>
                <div class="form-group">
                    <label for="p-in" class="col-sm-4 label-heading">Fecha de inicio</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="start_date" id="start_date">
                    </div>
                </div>
                <div class="form-group">
                    <label for="p-in" class="col-sm-4 label-heading">Fecha final</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="end_date" id="end_date">
                    </div>
                </div>
                <div class="form-group">
                    <label for="p-in" class="col-sm-4 label-heading">Eliminar evento</label>
                    <div class="col-sm-12">
                        <input type="checkbox" name="delete" value="1">
                    </div>
                </div>
                <input type="hidden" name="eventid" id="event_id" value="0" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <input type="submit" class="btn btn-primary" value="Evento de actualización">
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>