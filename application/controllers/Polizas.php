<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Polizas extends MY_Controller{

	function __construct(){
		parent::__construct();

		//Models:
		$this->load->model('usuarios_model');
		$this->load->model('admin_model');

		//Librerías:
		$this->load->library('form_validation');
	}

	/* ***************** INDICE DE MÉTODOS *****************
	1. Pólizas.
	2. Datos económicos.
	3. Incapacidad temporal.
	4. Fallecimiento.
	5. Invalidez.
	6. Jubilación.
	7. Dependencia.
	
	******************************************************** */

	//1. Pólizas:
	public function index(){
		//Datos:
		$data['title'] = 'Pólizas';

		//CSS exclusivos:
		$data['arrcss'] = array(
			'js/plugins/datetimepicker/css/daterangepicker.css'
		);

		//JS exclusivos:
		$data['arrjs'] = array(
			'js/plugins/datetimepicker/js/moment.min.js',
			'js/plugins/datetimepicker/js/daterangepicker.js'		
		);

		//JS custom exclusivos:
		$data['arrcustomjs'] = array(
			'js/custom-datetimepicker.js'	
		);

		//Contenido:
		$data['contenido'] = 'polizas';

		//HTML:
		$this->load->view('includes/template', $data);
	}

	//2. Datos económicos:
	public function datos_economicos(){
		//Datos:
		$data['title'] = 'Pólizas - datos económicos';

		//CSS exclusivos:
		$data['arrcss'] = array(
			'js/plugins/datetimepicker/css/daterangepicker.css'
		);

		//JS exclusivos:
		$data['arrjs'] = array(
			'js/plugins/datetimepicker/js/moment.min.js',
			'js/plugins/datetimepicker/js/daterangepicker.js'		
		);

		//JS custom exclusivos:
		$data['arrcustomjs'] = array(
			'js/custom-datetimepicker.js'	
		);

		//Variables:
		$data['tiene_conyugue'] = '';
		$data['titular_regimen_ss'] = '';
		$data['titular_cotizado_ss_rg_anyos'] = '';
		$data['titular_cotizado_ss_rg_meses'] = '';
		$data['titular_cotizado_ss_aut_anyos'] = '';
		$data['titular_cotizado_ss_aut_meses'] = '';
		$data['conyugue_regimen_ss'] = '';
		$data['conyugue_cotizado_ss_rg_anyos'] = '';
		$data['conyugue_cotizado_ss_rg_meses'] = '';
		$data['conyugue_cotizado_ss_aut_anyos'] = '';
		$data['conyugue_cotizado_ss_aut_meses'] = '';
		
		$data['titular_distintos_periodicidad'] = '';
		$data['conyugue_distintos_periodicidad'] = '';
		$data['activos_ahorros_1_afectado'] = '';
		$data['activos_ahorros_1_tipo'] = '';
		$data['activos_ahorros_1_capital_hoy'] = '';
		$data['activos_ahorros_1_capital_jubilacion'] = '';
		$data['activos_ahorros_1_prima_mensual'] = '';
		$data['activos_ahorros_1_derechos_consolidados'] = '';
		$data['activos_ahorros_2_afectado'] = '';
		$data['activos_ahorros_2_tipo'] = '';
		$data['activos_ahorros_2_tipo'] = '';
		$data['activos_ahorros_2_capital_hoy'] = '';
		$data['activos_ahorros_2_capital_jubilacion'] = '';
		$data['activos_ahorros_2_prima_mensual'] = '';
		$data['$activos_ahorros_2_derechos_consolidados'] = '';
		$data['activos_sv_1_asegurado'] = '';
		$data['activos_sv_2_asegurado'] = '';

		//Contenido:
		$data['contenido'] = 'datos-economicos';

		//HTML:
		$this->load->view('includes/template', $data);	
	}

	//3. Incapacidad temporal:
	public function incapacidad_temporal(){
		//Datos:
		$data['title'] = 'Pólizas - incapacidad temporal';

		//CSS exclusivos:
		$data['arrcss'] = array(
			'js/plugins/datetimepicker/css/daterangepicker.css'
		);

		//JS exclusivos:
		$data['arrjs'] = array(
			'js/plugins/datetimepicker/js/moment.min.js',
			'js/plugins/datetimepicker/js/daterangepicker.js'		
		);

		//JS custom exclusivos:
		$data['arrcustomjs'] = array(
			'js/custom-datetimepicker.js'	
		);

		//Variables:
		$data['tiene_conyugue'] = 1;
		$data['titular_es_autonomo'] = '';
		$data['conyugue_es_autonomo'] = '';
		$data[''] = '';
		$data[''] = '';
		$data[''] = '';







		//Contenido:
		$data['contenido'] = 'incapacidad-temporal';

		//HTML:
		$this->load->view('includes/template', $data);	
	}

	//4. Fallecimiento:
	public function fallecimiento(){
		//Datos:
		$data['title'] = 'Pólizas - fallecimiento';

		//CSS exclusivos:
		$data['arrcss'] = array(
			'js/plugins/datetimepicker/css/daterangepicker.css'
		);

		//JS exclusivos:
		$data['arrjs'] = array(
			'js/plugins/datetimepicker/js/moment.min.js',
			'js/plugins/datetimepicker/js/daterangepicker.js'		
		);

		//JS custom exclusivos:
		$data['arrcustomjs'] = array(
			'js/custom-datetimepicker.js'	
		);

		//Variables:
		$data['tiene_conyugue'] = 1;
		$data['titular_es_autonomo'] = '';
		$data['conyugue_es_autonomo'] = '';
		$data[''] = '';
		$data[''] = '';
		$data[''] = '';

		//Contenido:
		$data['contenido'] = 'fallecimiento';

		//HTML:
		$this->load->view('includes/template', $data);	
	}

	//5. Invalidez:
	public function invalidez(){
		//Datos:
		$data['title'] = 'Pólizas - invalidez';

		//CSS exclusivos:
		$data['arrcss'] = array(
			'js/plugins/datetimepicker/css/daterangepicker.css'
		);

		//JS exclusivos:
		$data['arrjs'] = array(
			'js/plugins/datetimepicker/js/moment.min.js',
			'js/plugins/datetimepicker/js/daterangepicker.js'		
		);

		//JS custom exclusivos:
		$data['arrcustomjs'] = array(
			'js/custom-datetimepicker.js'	
		);

		//Variables:
		$data['tiene_conyugue'] = 1;
		$data['titular_es_autonomo'] = '';
		$data['conyugue_es_autonomo'] = '';
		$data[''] = '';
		$data[''] = '';
		$data[''] = '';

		//Contenido:
		$data['contenido'] = 'invalidez';

		//HTML:
		$this->load->view('includes/template', $data);	
	}

	//6. Jubilación:
	public function jubilacion(){
		//Datos:
		$data['title'] = 'Pólizas - jubilación';

		//CSS exclusivos:
		$data['arrcss'] = array(
			'js/plugins/datetimepicker/css/daterangepicker.css'
		);

		//JS exclusivos:
		$data['arrjs'] = array(
			'js/plugins/datetimepicker/js/moment.min.js',
			'js/plugins/datetimepicker/js/daterangepicker.js'		
		);

		//JS custom exclusivos:
		$data['arrcustomjs'] = array(
			'js/custom-datetimepicker.js'	
		);

		//Variables:
		$data['tiene_conyugue'] = 1;
		$data['titular_es_autonomo'] = '';
		$data['conyugue_es_autonomo'] = '';
		$data[''] = '';
		$data[''] = '';
		$data[''] = '';

		//Contenido:
		$data['contenido'] = 'jubilacion';

		//HTML:
		$this->load->view('includes/template', $data);	
	}

	//7. Dependencia:
	public function dependencia(){
		//Datos:
		$data['title'] = 'Pólizas - dependencia';

		//CSS exclusivos:
		$data['arrcss'] = array(
			'js/plugins/datetimepicker/css/daterangepicker.css'
		);

		//JS exclusivos:
		$data['arrjs'] = array(
			'js/plugins/datetimepicker/js/moment.min.js',
			'js/plugins/datetimepicker/js/daterangepicker.js'		
		);

		//JS custom exclusivos:
		$data['arrcustomjs'] = array(
			'js/custom-datetimepicker.js'	
		);

		//Variables:
		$data['tiene_conyugue'] = 1;
		$data['titular_es_autonomo'] = '';
		$data['conyugue_es_autonomo'] = '';
		$data[''] = '';
		$data[''] = '';
		$data[''] = '';

		//Contenido:
		$data['contenido'] = 'dependencia';

		//HTML:
		$this->load->view('includes/template', $data);	
	}

	

	

	

	

	

	

}