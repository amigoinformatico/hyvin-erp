<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends MY_Controller{

	function __construct(){
		parent::__construct();

		//Validamos que exista sesión:
		if(!$this->session->userdata('user_id')) redirect(base_url());
		
		//Models:
		$this->load->model('usuarios_model');
		$this->load->model('admin_model');

		//Librerías:
		$this->load->library('form_validation');
	}

	/* ***************** INDICE DE MÉTODOS *****************
	1. Clientes.
	
	******************************************************** */

	//1. Clientes: 
	public function clientes(){
		//Datos:
		$data['title'] = 'Clientes';

		//CSS exclusivos:
		$data['arrcss'] = array(
			
		);

		//JS exclusivos:
		$data['arrjs'] = array(
				
		);

		//JS custom exclusivos:
		$data['arrcustomjs'] = array(
			
		);

		//Contenido:
		$data['contenido'] = 'clientes';

		//HTML:
		$this->load->view('includes/template', $data);
	}

	
}
