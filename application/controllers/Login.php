<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Login extends MY_controller{

    public function __construct(){
        parent::__construct();

        //Models:
		$this->load->model('usuarios_model');

		//Librerías:
		$this->load->library('form_validation');
    }

    /* ***************** INDICE DE MÉTODOS *****************
	1. Login de usuario.
		1.1. Página de login de usuario.
	2. Logout de usuario.
	3. Recuperar password.
	4. Enviar nuevo password.
	******************************************************** */
 
 	//1. Login de usuario:
	public function login_user(){
		//Si no existe sesión abierta:
		if(!$this->session->userdata('user_id')){
			//Validamos formulario si se recibe el token:
			if($this->input->post('token') ){
				//&& $this->input->post('token') == $this->session->userdata('token')

				//Validación campos:
	            $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required|min_length[2]|max_length[75]');
	            $this->form_validation->set_rules('contrasena', 'Password', 'trim|required|min_length[6]|max_length[12]');
	 
	            //Si validación es errónea:
				if($this->form_validation->run() == FALSE){
					//Delimitador de errores:
					$this->form_validation->set_error_delimiters('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button>','</div>');

					//Token:
					$data['token'] = $this->token();

					redirect(base_url().'home');

				//Si validación OK, hacemos login:
				}else{
					$usuario = sanear($this->input->post('usuario'),TRUE);
					$pwd = sha1(sanear($this->input->post('contrasena')));
					$row = $this->usuarios_model->login_user($usuario,$pwd);

					//comprobamos que la ip no esté bloqueada:
					$ip = getRealIP();
					$rowip = $this->usuarios_model->getIp($ip);
					if($rowip != NULL){
						redirect(base_url()."infraccion", "refresh");
						exit;
					}

					//Si existe el usuario:
					if($row != null && $this->session->userdata('user_id') != NULL){
						//Actualizamos campo 'logins':
						$this->usuarios_model->setLogins($row->user_id);

						//Obtenemos avatar del administrador:
						$qavatar = $this->usuarios_model->get_user_images($row->user_id);
						foreach($qavatar as $row){
							$this->session->set_userdata('avatar',$row->foto);	
						}

						//Obtenemos notificaciones pendientes:
						$this->load->model('calidad_model');
						$nnotif = $this->calidad_model->getNotificacionesPdtes($this->session->userdata('user_id'));
						$this->session->set_userdata('nnotificaciones',$nnotif->Total);

						redirect(base_url().'home', 'refresh');
					}else{
						e('No existe usuario<br>');	
					}
				}

			//Sin validación de token:
			}else{
				e('sesión token: '.$this->session->userdata('token').'<br>');
				e('post token: '.$this->input->post('token').'<br>');
					exit;
				//Token:
				$data['token'] = $this->token();
				
				redirect(base_url());
			}

		}else{
			redirect(base_url(),'refresh');
		}
	}

	//1.1. Página de login de usuario:
	public function signin(){
		//Si no existe sesión abierta:
		if(!$this->session->userdata('user_id')){	
			//Datos:
			$pag = 'login';
			$data['pag'] = readPag($pag);
			$data['title'] = readContenido($pag.'-title', IDM_);
			$data['description'] = readContenido($pag.'-description', IDM_);
			$data['keywords'] = readContenido($pag.'-keywords', IDM_);

			//Token:
			if(!$this->session->userdata('user_id')){
				$data['token'] = $this->token();
			}
			
			//Idiomas:
			$data['idiomas'] = $this->getIdiomas();

			//HTML:
			$this->load->view(FRONTEND_.'/includes/head');

			$this->load->view(FRONTEND_.'/includes/body-open');

			//Navigation:
			$this->load->view(FRONTEND_.'/includes/navigation');

			//Contenido dinámico:
			$this->load->view(FRONTEND_.'/signin.php', $data);

			//Footer:
			$this->load->view(FRONTEND_.'/includes/footer');

			//Cargando javascript:
			$this->load->view(FRONTEND_.'/includes/load-js');

			$this->load->view(FRONTEND_.'/includes/body-close');

		}else{
			redirect(base_url().'home','refresh');
		}
	}
	
	//2. Logout de usuario:
	public function logout(){
		//$this->session->unset_userdata();
		$this->session->sess_destroy();
		redirect(base_url(),'refresh');
	}

	//3. Recuperar password:
	public function forgot_password(){
		//Si no existe sesión abierta:
		if(!$this->session->userdata('user_id')){	
			//Datos:
			$pag = 'recuperar-password';
			$data['pag'] = readPag($pag);
			$data['title'] = readContenido($pag.'-title', IDM_);
			$data['description'] = readContenido($pag.'-description', IDM_);
			$data['keywords'] = readContenido($pag.'-keywords', IDM_);

			//Token:
			if(!$this->session->userdata('user_id')){
				$data['token'] = $this->token();
			}
			
			//Idiomas:
			$data['idiomas'] = $this->getIdiomas();

			//HTML:
			$this->load->view(FRONTEND_.'/includes/head');

			$this->load->view(FRONTEND_.'/includes/body-open');

			//Navigation:
			$this->load->view(FRONTEND_.'/includes/navigation');

			//Contenido dinámico:
			$this->load->view(FRONTEND_.'/get-password.php', $data);

			//Footer:
			$this->load->view(FRONTEND_.'/includes/footer');

			//Cargando javascript:
			$this->load->view(FRONTEND_.'/includes/load-js');

			$this->load->view(FRONTEND_.'/includes/body-close');

		}else{
			redirect(base_url().'home','refresh');
		}	
	}

	//4. Enviar nuevo password:
	public function send_password(){
		//Si no existe sesión abierta:
		if(!$this->session->userdata('user_id')){
			//Validación campos:
            $this->form_validation->set_rules('correo', 'Email', 'trim|required|valid_email|min_length[2]|max_length[75]'); 

            //Si validación es errónea:
			if($this->form_validation->run() == FALSE){
				//Delimitador de errores:
				$this->form_validation->set_error_delimiters('<small class="alerta">','</small><br>');

				//HTML:
				$this->forgot_password();

			//Si validación OK:
			}else{
				//Variables:
				$correo = sanear($this->input->post('correo',TRUE));

				//Comprobamos existencia del correo:
				$row = $this->usuarios_model->getPwd($correo);

				//Si no existe correo:
				if($row == NULL){
					$this->session->set_flashdata('no-email','<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button>'.lang("fgt_no_email").'</div>');	

					//HTML:
					redirect(base_url().'recupera-password');

				//Si email ok:
				}else{
					//Generamos nueva contraseña:
					$user_pwd = auth_randomText(8);
					$db_pwd = sha1($user_pwd);

					//Actualizamos contraseña del usuario:
					$this->usuarios_model->update_pwd($row->user_id,$db_pwd);

					//Cargamos librería de email:
					$this->load->library('email');

					//Preparamos mensaje:
					$msg_data['mensaje'] = 'Hola '.$row->nombre.',<br><br>
						este es tu nuevo password para acceder a Coopmercio:<br><br>
						<strong>'.$user_pwd.'</strong>';
					$msg = $this->load->view(BACKEND_.'/includes/template-email', $msg_data, TRUE);

					//Configuración de email:
					$config['mailtype'] = 'html';
					$config['wordwrap'] = TRUE;

					//Envío de mail de confirmación:
					$this->email->initialize($config);
					$this->email->from(MAIL, 'Coopmercio Webmaster');
					$this->email->to($correo);
					$this->email->subject('Recuperación de password');
					$this->email->message($msg);
					if($this->email->send()){
						$this->session->set_flashdata('ok-send-email','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.lang("fgt_send_email").'</div>');
							
						redirect(base_url()."recupera-password","refresh");	
					}else{
						//Página de error.
					}
				}
			}
		}
	}
}
