<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        //Models:
        $this->load->model('usuarios_model');
        $this->load->model('admin_model');

        //Librerías:
        $this->load->library('form_validation');
    }

    /* ***************** INDICE DE MÉTODOS *****************
    1. Home + login Backend.
        1.1. Redireccionando a dashboard desde calendario.


    ******************************************************** */

    //1. Home + login Backend:
    public function index()
    {
        //Datos:
        $data['title'] = 'Dashboard';

        //CSS exclusivos:
        $data['arrcss'] = array(
            'css/plugins/dataTables/datatables.min.css',

        );

        //JS exclusivos:
        $data['arrjs'] = array(
            'js/plugins/dataTables/datatables.min.js',
            'js/plugins/dataTables/dataTables.bootstrap4.min.js'
        );

        //JS custom exclusivos:
        $data['arrcustomjs'] = array(
            'js/custom-datatables.js',

        );

        //Contenido:
        $data['contenido'] = 'dashboard';

        $data['usuarios'] = $this->usuarios_model->getUsers();

        //HTML:
        $this->load->view('includes/template', $data);
    }


    public function getUsuarios()
    {
        echo json_encode(array("data" =>$this->usuarios_model->getUsers()));
}


    //6. Colapsar menú;
    public function colapsar_menu()
    {
        if ($this->input->is_ajax_request()) {
            $valor = $this->security->xss_clean($this->input->post('valor'));

            if ($valor == 1) {
                $this->session->set_userdata('menu-colapse', 1);
            } else {
                $this->session->set_userdata('menu-colapse', '0');
            }
        }
    }


}