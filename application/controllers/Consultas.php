<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consultas extends MY_Controller{

	function __construct(){
		parent::__construct();

		//Models:
		$this->load->model('usuarios_model');
		$this->load->model('admin_model');
		$this->load->model('eloquent/Consulta');

		//Librerías:
		$this->load->library('form_validation');
	}

	/* ***************** INDICE DE MÉTODOS *****************
	1. Consultas.
		
	
	******************************************************** */

	//1. Consultas:
	public function index(){
		//Datos:
		$data['title'] = 'Consultas';

		//Contenido:
		$data['contenido'] = 'consultas';
		$data['consultas'] = Consulta::all();
//JS custom exclusivos:
        $data['arrcustomjs'] = array(
            'js/custom.js'
        );

		//HTML:
		$this->load->view('includes/template', $data);
	}




    public function upload() {
        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $fileName = $_FILES['file']['name'];
            $targetPath = getcwd() . '/uploads/';
            $targetFile = $targetPath . $fileName ;
            move_uploaded_file($tempFile, $targetFile);
            // if you want to save in db,where here
            // with out model just for example
            Consulta::create([
                'name'=>$fileName,
                'file_name'=>$fileName
            ]);
        }
    }
	

	public function delete(){
        $file_name = $this->input->post('file_name');
        $targetPath = getcwd() . '/uploads/';
        // Elimino el archivo fisicamente
        unlink($targetPath.$file_name);
	   Consulta::where('file_name',$file_name)->delete();
    }

	//6. Colapsar menú;
	public function colapsar_menu(){
		if($this->input->is_ajax_request()){	
			$valor = $this->security->xss_clean($this->input->post('valor'));

			if($valor == 1){
				$this->session->set_userdata('menu-colapse', 1);
			}else{
				$this->session->set_userdata('menu-colapse', '0');
			}
		}
	}

	

}