<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
 
class Login extends CI_Controller {

    function validate()
    {
        $this->load->library('form_validation');
        $this->load->helper('cookie');
        
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_user');
        
        if($this->form_validation->run() == FALSE)
        {
            delete_cookie('winp3_hello_id');
            redirect('welcome/index/errlogin');
        }
        else
        {
            $user = $this->User_model->get_user($this->session->userdata['logged_user']['id']);
            $this->input->set_cookie('winp3_hello_id', $user['id'], 86500);
            redirect('start/options');
        }

    }
    
    function check_user($password) {
        $this->load->model('User_model');
        $username = $this->input->post('username');

        $row = $this->User_model->login($username, $password);
        if($row) {
            $sess_array = array(
                'id' => $row->id,
                'administrador' => $row->administrador,
                'username' => $row->username,
                'nombre' => $row->nombre,
                'apellidos' => $row->apellidos
            );
            $this->session->set_userdata('logged_user', $sess_array);
            
            return TRUE;
        }
        else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }
    
    function logout() {
        $sess_array = array(
            'id' => '',
            'administrador' => '',
            'username' => '',
            'nombre' => '',
            'apellidos' => ''
        );
        $this->session->unset_userdata($ses_array);
        $this->session->sess_destroy();
        
        redirect('welcome/index/');
    }
}
 