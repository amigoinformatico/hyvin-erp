<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Welcome extends CI_Controller {

    
    public function index($parameter="") {
        $this->load->helper('cookie');
        
        $data=[];
        
        if($parameter == "errlogin") {
            $data['errlogin'] = 1;
            $hello_id = null;
        }
        else {
            $hello_id = get_cookie('winp3_hello_id'); 
        }

        if( !is_null($hello_id)) {
            $this->load->model('User_model');
            $user = $this->User_model->get_user($hello_id);
            if($user!='') {
                $data['hello_id'] = $hello_id;
                $data['hello_username'] = $user['user_name'];
                $data['hello_nombre'] = $user['nombre'];
                $this->load->view('pages/logged', $data);
                return;
            }
        }

        $this->load->view('pages/login', $data);
    }
    
}
