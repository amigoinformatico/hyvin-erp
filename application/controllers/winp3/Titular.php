<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Titular extends CI_Controller
{
    public $m_SimulacionRow;

    private function log_error($message)
    {
        if (strpos($this->config->item('winp3_missing_field'), $message) === FALSE) {
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field') . $message . " | ");
        }
    }

public function calculadora()
{
        $this->load->view('pages/calc');
}

public
function nueva()
{
    if (!isset($this->session->userdata['logged_user'])) {
        redirect('welcome/index/');
    }
    $this->simulacion_model->nueva_simulacion_usuario($this->session->userdata('logged_user')['id']);
    $this->datos();
}

public
function datos()
{
    if (!isset($this->session->userdata['logged_user'])) {
        redirect('welcome/index/');
    }

    // Librerias
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

    $data = [];
    $this->calculate($data);
    $data['missing_fields'] = $this->config->item('winp3_missing_field');
    $data['current_page'] = 1;
    $this->load->view('templates/header_nav', $data);
    $this->load->view('pages/titular', $data);
    $this->load->view('templates/footer_nav', $data);

}

public
function recalc()
{
    if (!isset($this->session->userdata['logged_user'])) {
        redirect('welcome/index/');
    }

    $this->load->library('session');
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

    $data = [];
    if ($this->save_data($data)) {
        $this->calculate($data);
    } else {
        $data['comunidades'] = $this->simulacion_model->get_comunidades_autonomas();
    }

    $data['winp3_from_recalc'] = 1;
    $data['missing_fields'] = $this->config->item('winp3_missing_field');
    $this->load->view('pages/titular', $data);
}


private
function calculate(&$data)
{
    $params[] = $this;
    $this->load->library('titular_lib', $params);
    $titular_lib = new Titular_Lib($params);

    // Los campos ya son validos para la vista tal y como vienen del modelo
    $this->m_SimulacionRow = $this->simulacion_model->get_all();
    $data = $this->m_SimulacionRow;
    if (!is_numeric($data['numero_hijos'])) {
        $data['numero_hijos'] = 0;
    }
    if (!is_numeric($data['comunidad_residencia'])) {
        $data['comunidad_residencia'] = 14;
    }
    $data['titular_edad'] = $titular_lib->titular_edad();
    $data['conyugue_edad'] = $titular_lib->conyugue_edad();
    $data['comunidades'] = $this->simulacion_model->get_comunidades_autonomas();
    $data['hijo_1_edad'] = $titular_lib->hijo_edad(1);
    $data['hijo_2_edad'] = $titular_lib->hijo_edad(2);
    $data['hijo_3_edad'] = $titular_lib->hijo_edad(3);
    $data['hijo_4_edad'] = $titular_lib->hijo_edad(4);
    $data['hijo_5_edad'] = $titular_lib->hijo_edad(5);
    $data['hijo_6_edad'] = $titular_lib->hijo_edad(6);
    $data['hijo_7_edad'] = $titular_lib->hijo_edad(7);

    for ($i = 1; $i < 8; $i++) {
        $data['hijo_' . $i . '_edad'] = $titular_lib->hijo_edad($i);

        $derecho_orfandad = $titular_lib->titular_hijo_derecho_orfandad($i);
        if ($derecho_orfandad == 1) {
            $data['hijo_' . $i . '_derecho_orfandad'] = 'Sí';
        } else {
            $data['hijo_' . $i . '_derecho_orfandad'] = 'No';
        }

        $derecho_orfandad = $titular_lib->conyugue_hijo_derecho_orfandad($i);
        if ($derecho_orfandad == 1) {
            $data['hijo_' . $i . '_derecho_orfandad_c'] = 'Sí';
        } else {
            $data['hijo_' . $i . '_derecho_orfandad_c'] = 'No';
        }

    }

}

public
function save()
{
    if (!isset($this->session->userdata['logged_user'])) {
        redirect('welcome/index/');
    }

    // Librerias
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

    $data;
    if (!$this->save_data($data)) {
        $data['comunidades'] = $this->simulacion_model->get_comunidades_autonomas();
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $data['current_page'] = 1;
        $this->load->view('templates/header_nav', $data);
        $this->load->view('pages/titular', $data);
        $this->load->view('templates/footer_nav', $data);
        return;
    }

    redirect($this->input->post('page'));
}

private
function save_data(&$data)
{

    $values_ok = $this->check_values($data);

    //Propios
    if ($this->input->post('titular_nombre')) {
        $data['titular_nombre'] = $this->input->post('titular_nombre');
    } else {
        $data['titular_nombre'] = null;
    }

    if ($this->input->post('titular_sexo')) {
        $data['titular_sexo'] = $this->input->post('titular_sexo');
    } else {
        $data['titular_sexo'] = null;
    }

    $data['tiene_conyugue'] = $this->input->post('tiene_conyugue');

    if ($this->input->post('titular_fecha_nacimiento')) {
        $data['titular_fecha_nacimiento'] = date("Y-m-d", strtotime($this->normalize_date($this->input->post('titular_fecha_nacimiento'))));
    } else {
        $data['titular_fecha_nacimiento'] = null;
    }

    if ($this->input->post('numero_hijos')) {
        $data['numero_hijos'] = $this->input->post('numero_hijos');
    } else {
        $data['numero_hijos'] = 0;
    }
    if ($data['numero_hijos'] > 7) {
        $data['numero_hijos'] = 0;
    }

    if ($this->input->post('conyugue_nombre') && $this->input->post('conyugue_nombre') != "") {
        $data['conyugue_nombre'] = $this->input->post('conyugue_nombre');
    } else {
        $data['conyugue_nombre'] = "Nombre";
    }

    if ($this->input->post('conyugue_sexo')) {
        $data['conyugue_sexo'] = $this->input->post('conyugue_sexo');
    } else {
        $data['conyugue_sexo'] = null;
    }

    if ($this->input->post('conyugue_fecha_nacimiento')) {
        $data['conyugue_fecha_nacimiento'] = date("Y-m-d", strtotime($this->normalize_date($this->input->post('conyugue_fecha_nacimiento'))));
    } else {
        $data['conyugue_fecha_nacimiento'] = null;
    }

    if ($this->input->post('comunidad_residencia')) {
        $data['comunidad_residencia'] = $this->input->post('comunidad_residencia');
    } else {
        $data['comunidad_residencia'] = 14;
    }

    $i;
    for ($i = 1; $i < 8; $i++) {
        if ($this->input->post('numero_hijos') >= $i) {
            $data['hijo_' . $i . '_nombre'] = $this->input->post('hijo_' . $i . '_nombre');
            $data['hijo_' . $i . '_sexo'] = $this->input->post('hijo_' . $i . '_sexo');
            $data['hijo_' . $i . '_fecha_nacimiento'] = date("Y-m-d", strtotime($this->normalize_date($this->input->post('hijo_' . $i . '_fecha_nacimiento'))));
            $data['hijo_' . $i . '_de'] = $this->input->post('hijo_' . $i . '_de');
        } else {
            $data['hijo_' . $i . '_nombre'] = null;
            $data['hijo_' . $i . '_sexo'] = null;
            $data['hijo_' . $i . '_fecha_nacimiento'] = null;
            $data['hijo_' . $i . '_de'] = null;
        }
    }

    if ($data['tiene_conyugue'] == 0) {

        $data['conyugue_nombre'] = "Nombre";
        $data['conyugue_sexo'] = null;
        $data['conyugue_fecha_nacimiento'] = null;
        $data['conyugue_regimen_ss'] = null;
        $data['conyugue_autonomos_ingreso_bruto_mensual'] = null;
        $data['conyugue_autonomos_cuota_mensual'] = null;
        $data['it_conyugue_porcentaje_cobertura'] = null;
        $data['conyugue_capital_propuesto_dia_baja'] = null;
        $data['conyugue_franquicia_dias'] = null;
        $data['conyugue_duracion_maxima_dias'] = null;
        $data['conyugue_prima_anual'] = null;
        $data['conyugue_autonomos_retencion_irpf'] = null;
        $data['conyugue_cotizado_ss_rg_anyos'] = 0;
        $data['conyugue_cotizado_ss_rg_meses'] = 0;
        $data['conyugue_cotizado_ss_aut_anyos'] = 0;
        $data['conyugue_cotizado_ss_aut_meses'] = 0;
        $data['conyugue_rg_neto_mes_ordinario'] = null;
        $data['conyugue_rg_paga_extra'] = null;
        $data['conyugue_rg_numero_pagas_extra'] = null;
        $data['conyugue_rg_retencion'] = null;
        $data['conyugue_distintos_ingresos'] = null;
        $data['conyugue_distintos_periodicidad'] = null;
        $data['conyugue_distintos_retencion'] = null;
        $data['conyugue_ahorro_mensual'] = null;
        $data['it_conyugue_prima_anual'] = null;
        $data['fallecimiento_conyugue_anyos_cobertura'] = null;
        $data['fallecimiento_conyugue_porcentaje_cobertura'] = null;
        $data['fallecimiento_conyugue_capital_propuesto'] = null;
        $data['fallecimiento_conyugue_modalidad'] = null;
        $data['fallecimiento_conyugue_prima_aproximada'] = null;
        $data['invalidez_conyugue_porcentaje_cobertura'] = null;
        $data['invalidez_conyugue_capital_propuesto'] = null;
        $data['invalidez_conyugue_modalidad'] = null;
        $data['invalidez_conyugue_prima_aproximada'] = null;
        $data['invalidez_conyugue_primas_incluidas'] = null;
        $data['invalidez_conyugue_anyos_cobertura'] = null;
        $data['jubilacion_conyugue_porcentaje_reduccion_gastos'] = null;
        $data['jubilacion_conyugue_anyos_cobertura'] = null;
        $data['dependencia_conyugue_coste_dependencia_hoy'] = null;
        $data['dependencia_conyugue_anyos_dependencia'] = null;
        $data['dependencia_conyugue_porcentaje_cobertura'] = null;
//            $data['dependencia_conyugue_capital_jubilacion'] = null;
        $data['dependencia_conyugue_aportacion_mensual'] = null;
        $data['dependencia_conyugue_anyos_renta'] = null;
        $data['conyugue_dias_indemnizacion_percibida'] = null;
        $data['jubilacion_conyugue_forma_calculo_pensiones'] = null;
        $data['jubilacion_conyugue_forma_calculo_pias'] = null;
        $data['jubilacion_conyugue_forma_calculo_seguro_vida'] = null;
        $data['jubilacion_conyugue_capital_pensiones'] = null;
        $data['jubilacion_conyugue_capital_pias'] = null;
        $data['jubilacion_conyugue_capital_seguro_vida'] = null;
        $data['jubilacion_conyugue_aportacion_pensiones'] = null;
        $data['jubilacion_conyugue_aportacion_pias'] = null;
        $data['jubilacion_conyugue_aportacion_seguro_vida'] = null;
        $data['jubilacion_conyugue_traspaso_plan_pensiones'] = null;
        $data['jubilacion_conyugue_anterior_2006'] = null;
        $data['jubilacion_conyugue_reparto_cobro_capital'] = null;
        $data['jubilacion_conyugue_anyos_renta'] = null;
        $data['jubilacion_conyugue_edad_inicio_renta'] = null;
    }

    if (!$values_ok) {
        $data['titular_fecha_nacimiento'] = $this->input->post('titular_fecha_nacimiento');
        $data['conyugue_fecha_nacimiento'] = $this->input->post('conyugue_fecha_nacimiento');
        return false;

    }

    if (!$this->simulacion_model->save($data)) {
        $this->log_error("Error guardando datos");
        return false;
    }

    return true;
}

private
function check_values(&$data)
{

    if ($this->input->post('titular_nombre') == "") {
        $this->log_error("Falta nombre Titular");
        return false;
    }

    if (!$this->validate_date($this->input->post('titular_fecha_nacimiento'))) {
        $this->log_error("Falta fecha nacimiento Titular");
        return false;
    }

    if (($this->input->post('tiene_conyugue')) == "1") {

        /*            if( $this->input->post('conyugue_nombre') == "" ) {
                        $this->log_error("Falta nombre Cónyuge");
                        return false;
                    }
        */
        if (!$this->validate_date($this->input->post('conyugue_fecha_nacimiento'))) {
            $this->log_error("Falta fecha nacimiento Cónyuge");
            return false;
        }
    }

    if (($this->input->post('comunidad_residencia')) == "") {
        $this->log_error("Falta Comunidad Residencia");
        return false;
    }

    return true;

}

private
function validate_date($date)
{
    $d = DateTime::createFromFormat('d-m-Y', $this->normalize_date($date));
    return $d && $d->format('d-m-Y') === $this->normalize_date($date);
}

private
function normalize_date($date)
{
    return str_replace("/", "-", $date);
}


}