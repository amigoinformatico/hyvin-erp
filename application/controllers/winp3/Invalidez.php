<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invalidez extends CI_Controller
{

    public $m_SimulacionRow;
    public $m_Bases;
    public $m_Informa_Bases_Pensiones;
    public $m_Titular_Lib;
    public $m_Invalidez_Lib;
    public $m_Economicos_Lib;
    public $m_Constantes;

    private function log_error($message)
    {
        if (strpos($this->config->item('winp3_missing_field'), $message) === FALSE) {
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field') . $message . " | ");
        }
    }

    public function report()
    {
        if (!isset($this->session->userdata['logged_user'])) {
            redirect('welcome/index/');
        }

        $data[] = '';
        $this->calculate($data);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $data['current_page'] = 5;
        $this->load->view('templates/header_nav', $data);
        $this->load->view('pages/invalidez', $data);
        $this->load->view('templates/footer_nav');
    }

    public function recalc()
    {
        if (!isset($this->session->userdata['logged_user'])) {
            redirect('welcome/index/');
        }

        $data = [];
        if ($this->save_data($data)) {
            $this->calculate($data);
        }

        $data['winp3_from_recalc'] = 1;
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/invalidez', $data);
    }

    public function dosier()
    {
        if (!isset($this->session->userdata['logged_user'])) {
            redirect('welcome/index/');
        }

        //Cargamos librería PDF:
        $this->load->library('mydompdf');

        $data = [];
        $this->calculate($data);
        $this->load->model('User_model');
        $user = $this->User_model->get_user($this->session->userdata['logged_user']['id']);

        $data['contacto_correduria'] = $user['contacto_correduria'];
        $data['contacto_nombre'] = $user['contacto_nombre'];
        $data['contacto_telefono_1'] = $user['contacto_telefono_1'];
        $data['contacto_telefono_2'] = $user['contacto_telefono_2'];
        $data['contacto_direccion_1'] = $user['contacto_direccion_1'];
        $data['contacto_direccion_2'] = $user['contacto_direccion_2'];
        $data['contacto_mail'] = $user['contacto_mail'];
        $data['contacto_final_1'] = $user['contacto_final_1'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];

        //$this->load->view('pages/print/invalidez', $data);

        //Generando pdf:
        $html = $this->load->view('pdf/pdf-invalidez', $data, true);
        $this->mydompdf->load_html($html);
        $this->mydompdf->render();
        $this->mydompdf->set_base_path('./assets/css/dompdf.css'); //agregar de nuevo el css
        $this->mydompdf->stream("invalidez.pdf", array(
            "Attachment" => false));
    }


    private function calculate(&$data)
    {
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params);
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params);
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('bases', $params);
        $this->m_Bases = new Bases($params);
        $this->load->library('invalidez_lib', $params);
        $this->m_Invalidez_Lib = new Invalidez_Lib($params);

        $this->m_SimulacionRow = $this->simulacion_model->get_all();

        //Propios
        $data['tipo_invalidez'] = $this->m_SimulacionRow['tipo_invalidez'];
        $data['invalidez_titular_porcentaje_cobertura'] = $this->m_SimulacionRow['invalidez_titular_porcentaje_cobertura'];
        $data['invalidez_titular_capital_propuesto'] = $this->m_SimulacionRow['invalidez_titular_capital_propuesto'];
        $data['invalidez_titular_modalidad'] = $this->m_SimulacionRow['invalidez_titular_modalidad'];
        $data['invalidez_titular_prima_aproximada'] = $this->m_SimulacionRow['invalidez_titular_prima_aproximada'];
        $data['invalidez_titular_primas_incluidas'] = $this->m_SimulacionRow['invalidez_titular_primas_incluidas'];
        $data['invalidez_titular_anyos_cobertura'] = $this->m_SimulacionRow['invalidez_titular_anyos_cobertura'];
        $data['invalidez_conyugue_porcentaje_cobertura'] = $this->m_SimulacionRow['invalidez_conyugue_porcentaje_cobertura'];
        $data['invalidez_conyugue_capital_propuesto'] = $this->m_SimulacionRow['invalidez_conyugue_capital_propuesto'];
        $data['invalidez_conyugue_modalidad'] = $this->m_SimulacionRow['invalidez_conyugue_modalidad'];
        $data['invalidez_conyugue_prima_aproximada'] = $this->m_SimulacionRow['invalidez_conyugue_prima_aproximada'];
        $data['invalidez_conyugue_primas_incluidas'] = $this->m_SimulacionRow['invalidez_conyugue_primas_incluidas'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];

        //Check fixed
        $data['invalidez_titular_porcentaje_cobertura'] = 100;
        $this->m_SimulacionRow['invalidez_titular_porcentaje_cobertura'] = $data['invalidez_titular_porcentaje_cobertura'];
        $data['invalidez_conyugue_porcentaje_cobertura'] = 100;
        $this->m_SimulacionRow['invalidez_conyugue_porcentaje_cobertura'] = $data['invalidez_conyugue_porcentaje_cobertura'];
        //

        //Calculados
        $data['titular_gastos_mensuales_familiares'] = -$this->m_Economicos_Lib->gastos_mensuales_media_familia();
        $data['titular_ingresos_conyugue'] = $this->m_Economicos_Lib->conyugue_total_ingresos_neto_mensual();
        if ($data['tipo_invalidez'] == 'ipt') {
            $data['titular_pension_invalidez'] = $this->m_Pensiones->titular_ipt_pension();
        } else {
            $data['titular_pension_invalidez'] = $this->m_Pensiones->titular_ipa_pension();
        }

        $data['titular_saldo_mensual'] = $data['titular_gastos_mensuales_familiares'] + $data['titular_ingresos_conyugue'] + $data['titular_pension_invalidez'];
        $data['titular_fecha_jubilacion'] = $this->m_Pensiones->titular_fecha_jubilacion();
        $data['titular_edad_jubilacion'] = $this->m_Pensiones->titular_edad_jubilacion();
        if ($data['titular_saldo_mensual'] < 0) {
            $data['titular_cobertura_necesaria'] = $data['titular_saldo_mensual'] * $data['invalidez_titular_anyos_cobertura'] * 12 * $data['invalidez_titular_porcentaje_cobertura'] / 100;
        } else {
            $data['titular_cobertura_necesaria'] = 0;
        }
        $data['titular_deudas_pendientes'] = -$this->m_Economicos_Lib->deudas_pendientes_familiares_hoy();
        $data['titular_adaptacion_vivienda'] = -$this->m_Constantes->adaptacion_vivienda();
        $data['titular_adaptacion_vehiculos_otros'] = -$this->m_Constantes->adaptacion_vehiculos();
        $data['titular_capitales_ahorro_titular'] = $this->m_Economicos_Lib->titular_capitales_ahorro_fallecimiento();
        $data['titular_capitales_ahorro_ambos'] = $this->m_Economicos_Lib->ambos_capitales_ahorro_fallecimiento();
        $data['titular_saldo_seguro_vida_necesario'] = $data['titular_cobertura_necesaria'] +
            $data['titular_deudas_pendientes'] + $data['titular_adaptacion_vivienda'] +
            $data['titular_adaptacion_vehiculos_otros'] + $data['titular_capitales_ahorro_titular'] +
            $data['titular_capitales_ahorro_ambos'];

        if ($data['tipo_invalidez'] == 'ipt') {
            $data['titular_seguros_ya_contratados'] = $this->m_Economicos_Lib->titular_suma_capitales_disponibles_seguro_vida_ipt();
        } else {
            $data['titular_seguros_ya_contratados'] = $this->m_Economicos_Lib->titular_suma_capitales_disponibles_seguro_vida_ipa();
        }

        $data['titular_necesidad_seguro_vida'] = -($data['titular_saldo_seguro_vida_necesario'] + $data['titular_seguros_ya_contratados']);

        $data['titular_total_cuota'] = $this->m_Fiscalidad->titular_invalidez_total_cuota();

        if ($this->m_SimulacionRow['tiene_conyugue'] == 1) {
            $data['conyugue_gastos_mensuales_familiares'] = -$this->m_Economicos_Lib->gastos_mensuales_media_familia();
            $data['conyugue_ingresos_conyugue'] = $this->m_Economicos_Lib->titular_total_ingresos_neto_mensual();
            if ($data['tipo_invalidez'] == 'ipt') {
                $data['conyugue_pension_invalidez'] = $this->m_Pensiones->conyugue_ipt_pension();
            } else {
                $data['conyugue_pension_invalidez'] = $this->m_Pensiones->conyugue_ipa_pension();
            }
            $data['conyugue_saldo_mensual'] = $data['conyugue_gastos_mensuales_familiares'] + $data['conyugue_ingresos_conyugue'] + $data['conyugue_pension_invalidez'];
            $data['conyugue_fecha_jubilacion'] = $this->m_Pensiones->conyugue_fecha_jubilacion();
            $data['conyugue_edad_jubilacion'] = $this->m_Pensiones->conyugue_edad_jubilacion();
            $data['invalidez_conyugue_anyos_cobertura'] = $this->m_Invalidez_Lib->conyugue_anyos_cobertura();

            if ($data['conyugue_saldo_mensual'] < 0) {
                $data['conyugue_cobertura_necesaria'] = $data['conyugue_saldo_mensual'] * $data['invalidez_conyugue_anyos_cobertura'] * 12 * $data['invalidez_conyugue_porcentaje_cobertura'] / 100;
            } else {
                $data['conyugue_cobertura_necesaria'] = 0;
            }
            $data['conyugue_deudas_pendientes'] = -$this->m_Economicos_Lib->deudas_pendientes_familiares_hoy();
            $data['conyugue_adaptacion_vivienda'] = -$this->m_Constantes->adaptacion_vivienda();
            $data['conyugue_adaptacion_vehiculos_otros'] = -$this->m_Constantes->adaptacion_vehiculos();
            $data['conyugue_capitales_ahorro_conyugue'] = $this->m_Economicos_Lib->conyugue_capitales_ahorro_fallecimiento();
            $data['conyugue_capitales_ahorro_ambos'] = $this->m_Economicos_Lib->ambos_capitales_ahorro_fallecimiento();
            $data['conyugue_saldo_seguro_vida_necesario'] = $data['conyugue_cobertura_necesaria'] +
                $data['conyugue_deudas_pendientes'] + $data['conyugue_adaptacion_vivienda'] +
                $data['conyugue_adaptacion_vehiculos_otros'] + $data['conyugue_capitales_ahorro_conyugue'] +
                $data['conyugue_capitales_ahorro_ambos'];
            if ($data['tipo_invalidez'] == 'ipt') {
                $data['conyugue_seguros_ya_contratados'] = $this->m_Economicos_Lib->conyugue_suma_capitales_disponibles_seguro_vida_ipt();
            } else {
                $data['conyugue_seguros_ya_contratados'] = $this->m_Economicos_Lib->conyugue_suma_capitales_disponibles_seguro_vida_ipa();
            }
            $data['conyugue_necesidad_seguro_vida'] = -($data['conyugue_saldo_seguro_vida_necesario'] + $data['conyugue_seguros_ya_contratados']);
            $data['conyugue_total_cuota'] = $this->m_Fiscalidad->conyugue_invalidez_total_cuota();
        }

    }

    public function save()
    {
        if (!isset($this->session->userdata['logged_user'])) {
            redirect('welcome/index/');
        }

        // Librerias
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $data = [];
        if (!$this->save_data($data)) {
            $this->load->view('templates/simulator_header');
            $data['missing_fields'] = $this->config->item('winp3_missing_field');
            $this->load->view('pages/invalidez', $data);
            $this->load->view('templates/footer');
            return;
        }


                 redirect($_POST['page']);


    }


    private function save_data(&$data)
    {
        $this->assign_field('tipo_invalidez', $data);
        $this->assign_field('invalidez_titular_porcentaje_cobertura', $data);
        $this->assign_field('invalidez_titular_capital_propuesto', $data);
        if ($data['invalidez_titular_capital_propuesto'] == '') {
            $data['invalidez_titular_capital_propuesto'] = 0;
        }
        $this->assign_field('invalidez_titular_modalidad', $data);
        $this->assign_field('invalidez_titular_prima_aproximada', $data);
        if ($data['invalidez_titular_prima_aproximada'] == '') {
            $data['invalidez_titular_prima_aproximada'] = 0;
        }
        $this->assign_field('invalidez_titular_primas_incluidas', $data);
        $this->assign_field('invalidez_titular_anyos_cobertura', $data);

        $this->assign_field('invalidez_conyugue_porcentaje_cobertura', $data);
        $this->assign_field('invalidez_conyugue_capital_propuesto', $data);
        if ($data['invalidez_conyugue_capital_propuesto'] == '') {
            $data['invalidez_conyugue_capital_propuesto'] = 0;
        }
        $this->assign_field('invalidez_conyugue_modalidad', $data);
        $this->assign_field('invalidez_conyugue_prima_aproximada', $data);
        if ($data['invalidez_conyugue_prima_aproximada'] == '') {
            $data['invalidez_conyugue_prima_aproximada'] = 0;
        }
        $this->assign_field('invalidez_conyugue_primas_incluidas', $data);
        $this->assign_field('invalidez_conyugue_anyos_cobertura', $data);

        if (!$this->simulacion_model->save($data)) {
            $this->log_error("Error guardando datos");
            return false;
        }

        return true;
    }

    private function assign_field($field_name, &$data)
    {
        if ($this->input->post($field_name) != null) {
            $data[$field_name] = str_replace(".", "", $this->input->post($field_name));
            $data[$field_name] = str_replace(",", ".", $data[$field_name]);
        } else {
            $data[$field_name] = null;
        }
    }


}
