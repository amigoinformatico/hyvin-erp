<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Parametros extends CI_Controller {
    
    private function log_error($message){
        if(strpos($this->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field').$message." | ");
        }
    }
    
    private function is_logged() {
        return isset($this->session->userdata['logged_user']);
    }
    private function is_admin() {
        if( !$this->is_logged()) {return false;}
        return $this->session->userdata['logged_user']['administrador'] == '1';
    }

    
    public function datos() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        if($this->session->userdata['logged_user']['administrador']!=1) {redirect('welcome/index/');}
        
        $data=[];
        $this->calculate($data);
        $data['comunidades'] = $this->simulacion_model->get_comunidades_autonomas();
        $data['tablas'] = $this->parametros_model->get_tablas_administrador_exportar();
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/configuracion-administrador', $data);
    }

    private function calculate(&$data) {
        $params[] = $this;
        
        $parametros = $this->parametros_model->get_all();
        $data['minimo_vital_irpf_hasta_64'] = $parametros->minimo_vital_irpf_hasta_64;
        $data['regimen_general_seguridad_social'] = $parametros->regimen_general_seguridad_social;
        
        $data['minimo_vital_irpf_65_74'] = $parametros->minimo_vital_irpf_65_74;
        $data['minimo_vital_irpf_75'] = $parametros->minimo_vital_irpf_75;
        $data['por_descendientes_primero'] = $parametros->por_descendientes_primero;
        $data['por_descendientes_segundo'] = $parametros->por_descendientes_segundo;
        $data['por_descendientes_tercero'] = $parametros->por_descendientes_tercero;
        $data['por_descendientes_cuarto'] = $parametros->por_descendientes_cuarto;
        $data['viudedad'] = $parametros->viudedad;
        $data['orfandad'] = $parametros->orfandad;
        $data['pension_maxima'] = $parametros->pension_maxima;
        $data['pension_minima_jubilacion_con_conyugue_a_cargo'] = $parametros->pension_minima_jubilacion_con_conyugue_a_cargo;
        $data['pension_minima_jubilacion_con_conyugue_a_no_cargo'] = $parametros->pension_minima_jubilacion_con_conyugue_a_no_cargo;
        $data['pension_minima_jubilacion_sin_conyugue'] = $parametros->pension_minima_jubilacion_sin_conyugue;
        $data['incapacidad_permanente_absoluta_con_conyugue_a_cargo'] = $parametros->incapacidad_permanente_absoluta_con_conyugue_a_cargo;
        $data['incapacidad_permanente_absoluta_con_conyugue_a_no_cargo'] = $parametros->incapacidad_permanente_absoluta_con_conyugue_a_no_cargo;
        $data['incapacidad_permanente_absoluta_sin_conyugue'] = $parametros->incapacidad_permanente_absoluta_sin_conyugue;
        $data['incapacidad_permanente_total_con_conyugue_a_cargo'] = $parametros->incapacidad_permanente_total_con_conyugue_a_cargo;
        $data['incapacidad_permanente_total_con_conyugue_a_no_cargo'] = $parametros->incapacidad_permanente_total_con_conyugue_a_no_cargo;
        $data['incapacidad_permanente_total_sin_conyugue'] = $parametros->incapacidad_permanente_total_sin_conyugue;
        $data['pensiones_titular_primer_tramo_hasta'] = $parametros->pensiones_titular_primer_tramo_hasta;
        $data['pensiones_conyugue_primer_tramo_hasta'] = $parametros->pensiones_conyugue_primer_tramo_hasta;
        $data['pensiones_titular_porcentaje_br_primer_tramo'] = $parametros->pensiones_titular_porcentaje_br_primer_tramo;
        $data['pensiones_conyugue_porcentaje_br_primer_tramo'] = $parametros->pensiones_conyugue_porcentaje_br_primer_tramo;
        $data['pensiones_titular_ipa_porcentaje'] = $parametros->pensiones_titular_ipa_porcentaje;
        $data['pensiones_conyugue_ipa_porcentaje'] = $parametros->pensiones_conyugue_ipa_porcentaje;
        $data['tipo_cotiz_auton'] = $parametros->tipo_cotiz_auton;
        
    }
    
    public function save() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        if($this->session->userdata['logged_user']['administrador']!=1) {redirect('welcome/index/');}

        // Librerias
        $data=[];
        $this->save_data($data);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $data['tablas'] = $this->parametros_model->get_tablas_administrador_exportar();
        $this->load->view('pages/configuracion-administrador', $data);
    }

    private function save_data(&$data) {
        $this->assign_field('minimo_vital_irpf_hasta_64', $data, "Mínimo Vital IRPF hasta 64 años");
        $this->assign_field('regimen_general_seguridad_social', $data, "Régimen General Seguridad Social");
        
        $this->assign_field('minimo_vital_irpf_65_74', $data,"Mínimo Vital IRPF de 65 a 74 años" );
        $this->assign_field('minimo_vital_irpf_75',$data, "Mínimo Vital IRPF a partir de 75 años");
        $this->assign_field('por_descendientes_primero',$data, "Por descendientes Primero");
        $this->assign_field('por_descendientes_segundo',$data, "Por descendientes Segundo");
        $this->assign_field('por_descendientes_tercero',$data, "Por descendientes Terdero");
        $this->assign_field('por_descendientes_cuarto',$data, "Por descendientes Cuarto");
        $this->assign_field('viudedad',$data, "Viudedad");
        $this->assign_field('orfandad',$data, "Orfandad");
        $this->assign_field('pension_maxima',$data, "Pensión Máxima");
        $this->assign_field('pension_minima_jubilacion_con_conyugue_a_cargo', $data,"Pensión mínima jubilación con cónyuge a cargo");
        $this->assign_field('pension_minima_jubilacion_con_conyugue_a_no_cargo',$data, "Pensión mínima jubilación con cónyuge a no cargo");
        $this->assign_field('pension_minima_jubilacion_sin_conyugue',$data, "Pensión mínima jubilación sin cónyuge");
        $this->assign_field('incapacidad_permanente_absoluta_con_conyugue_a_cargo',$data, "Incapacidad permanente absoluta con cónyuge a cargo");
        $this->assign_field('incapacidad_permanente_absoluta_con_conyugue_a_no_cargo', $data,"Incapacidad permanente absoluta con cónyuge a no cargo");
        $this->assign_field('incapacidad_permanente_absoluta_sin_conyugue', $data,"Incapacidad permanente absoluta sin cónyuge");
        $this->assign_field('incapacidad_permanente_total_con_conyugue_a_cargo', $data,"Incapacidad permanente total con cónyuge a cargo");
        $this->assign_field('incapacidad_permanente_total_con_conyugue_a_no_cargo',$data, "Incapacidad permanente total con cónyuge a no cargo");
        $this->assign_field('incapacidad_permanente_total_sin_conyugue', $data,"Incapacidad permanente total sin cónyuge");
        $this->assign_field('pensiones_titular_primer_tramo_hasta', $data,"Pensiones titular primer tramo hasta");
        $this->assign_field('pensiones_conyugue_primer_tramo_hasta', $data,"Pensiones cónyuge primer tramo hasta");
        $this->assign_field('pensiones_titular_porcentaje_br_primer_tramo', $data,"Pensiones titular porcentaje BR primer tramo");
        $this->assign_field('pensiones_conyugue_porcentaje_br_primer_tramo',$data, "Pensiones cónyuge porcentaje BR primer tramo");
        $this->assign_field('pensiones_titular_ipa_porcentaje', $data,"Pensiones titular IPA porcentaje");
        $this->assign_field('pensiones_conyugue_ipa_porcentaje', $data,"Pensiones cónyuge IPA porcentaje");
        $this->assign_field('tipo_cotiz_auton', $data, "Tipo Cotización Autónomo");
        
        if($this->config->item('winp3_missing_field')!='') {return;}

        if( !$this->parametros_model->save($data)) {
            $this->log_error("Error guardando datos");
        }
    }
    
    private function assign_field( $field_name, &$data, $message ) {
        if( $this->input->post($field_name) != null ) {
            $data[$field_name] = str_replace(".", "", $this->input->post($field_name)); 
            $data[$field_name] = str_replace(",", ".", $data[$field_name]); 
        }
        else {
            $data[$field_name] = null; 
            $this->log_error("Valor no válido en ".$message);
        }
    }
/*
    private function assign_field( $field_name, &$data, $message ) {
        if(!$this->check_numeric($this->input->post($field_name))) {
            $data[$field_name] = null; 
            $this->log_error("Valor no válido en ".$message);
            return;
        }
        
        $data[$field_name] = $this->input->post($field_name);
    }
*/
    private function check_numeric($value) {
        if( trim($value) == "" ) {return false;}
        return is_numeric(trim($value));
    }
    
    public function export(){
        if(!$this->is_admin())  {redirect('welcome/index/');}

        if($this->input->post('tabla') == null) {
            $this->datos();
            return;
        }

        $html = $this->parametros_model->export_table($this->input->post('tabla'), $this->input->post('comunidad_autonoma'));
        
        if($html=="") {
           $this->log_error("Error al exportar. ¿Ha indicado la Comunidad Autónoma?");
           $this->datos();
        }
        else{
            echo header('Content-type: text/plain');
            echo header('Content-Disposition: attachment; filename="'.$this->input->post('tabla').'.txt"');        

            foreach($html as $line){
                echo $line."\n";
            }
        }
    }
    
    public function import(){
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        if($this->session->userdata['logged_user']['administrador']!=1) {redirect('welcome/index/');}

        $now = new DateTime();
        $target_file = APPPATH.'/uploads/'.$now->getTimestamp().$_FILES["file_to_import"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION); 

        if(!move_uploaded_file($_FILES["file_to_import"]["tmp_name"], $target_file)){
            $this->log_error("Error al subir el fichero al servidor.");
            $this->datos();
            return;
        }
        
        $table = $this->input->post('import_tabla');
        $ccaa = $this->input->post('import_comunidad_autonoma');
        
        if($table==""){
            $this->log_error("Error al importar. Indique la tabla sobre la que importar");
            $this->datos();
            return;
        }
        
        $necesita_ccaa = ($table=='escala_gravamen_ahorro' || $table=="irpf_por_ccaa_autonomico" || $table=="irpf_por_ccaa_estatal" ||
           $table=="isd_por_ccaa" || $table=="reduccion_por_seguro_vida" || $table=="comunidades_autonomas");

        if($necesita_ccaa && $ccaa=='') {
            $this->log_error("Error al importar. Debe indicar la Comunidad Autónoma");
            $this->datos();
            return;
        }
        
        if(!$this->parametros_model->import_table($table, $ccaa, $target_file)){
           $this->log_error("Error al importar. Revise el formato del fichero");
           $this->datos();
           return;
        }

        //Ok
        $data['operation_ok']='';
        $this->calculate($data);
        $data['comunidades'] = $this->simulacion_model->get_comunidades_autonomas();
        $data['tablas'] = $this->parametros_model->get_tablas_administrador_exportar();
        $data['missing_fields'] = "";
        $this->load->view('pages/configuracion-administrador', $data);
    }
    
    public function index()
    {
                   $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

           $this->load->library('upload', $config);
        echo form_open_multipart('upload/do_upload');

echo '<input type="file" name="userfile" size="20" />';



echo '<input type="submit" value="upload" />';

echo '</form>';
        /*

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
echo"ERROR";
                     //   $this->load->view('upload_form', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
echo "OK";
                       // $this->load->view('upload_success', $data);
                }

         */
    }

    
}