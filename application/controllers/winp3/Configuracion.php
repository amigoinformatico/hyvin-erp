<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Configuracion extends CI_Controller {
    
    private function log_error($message){
        if(strpos($this->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field').$message." | ");
        }
    }

    private function is_logged() {
        return isset($this->session->userdata['logged_user']);
    }
    private function is_admin() {
        if( !$this->is_logged()) {return false;}
        return $this->session->userdata['logged_user']['administrador'] == '1';
    }
    
    public function datos() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        $data=[];
        $this->calculate($data);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $data['tablas'] = $this->parametros_model->get_tablas_usuario_exportar();
        $this->load->view('pages/configuracion-usuario', $data);
    }

    private function calculate(&$data) {
        $params[] = $this;
        $this->load->library('constantes', $params);
        $constantes = new Constantes($params);

        $data['ipc'] = $constantes->ipc();
        $data['gastos_entierro'] = $constantes->gastos_entierro();
        $data['costes_aceptacion_herencia'] = $constantes->costes_aceptacion_herencia();
        $data['rentabilidad_estimada'] = $constantes->rentabilidad_estimada();
        $data['edad_dependencia'] = $constantes->edad_dependencia();
        $data['edad_renta_pias'] = $constantes->edad_renta_pias();
        $data['adaptacion_vivienda'] = $constantes->adaptacion_vivienda();
        $data['adaptacion_vehiculos'] = $constantes->adaptacion_vehiculos();
       
    }
    
    public function save() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        // Librerias
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $data=[];
        $this->save_data($data);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $data['tablas'] = $this->parametros_model->get_tablas_usuario_exportar();
        $this->load->view('pages/configuracion-usuario', $data);
    }

    private function save_data(&$data) {
        $this->assign_field('ipc', $data, "IPC" );
        $this->assign_field('gastos_entierro', $data, "Gastos Entierro" );
        $this->assign_field('costes_aceptacion_herencia', $data, "Costes Aceptación Herencia" );
        $this->assign_field('rentabilidad_estimada', $data, "Rentabilidad Estimada");
        $this->assign_field('edad_dependencia', $data, "Edad Dependencia");
        $this->assign_field('edad_renta_pias', $data, "Edad Renta Pias");
        $this->assign_field('adaptacion_vivienda', $data, "Adaptación Vivienda");
        $this->assign_field('adaptacion_vehiculos', $data, "Adaptación Vehículos");

        
        if($this->config->item('winp3_missing_field')!='') {return;}

        if( !$this->constantes_model->save($data)) {
            $this->log_error("Error guardando datos");
        }

    }
    
    public function export(){
        if(!$this->is_logged())  {redirect('welcome/index/');}

        if($this->input->post('tabla') == null) {
            $this->datos();
            return;
        }

        $html = $this->parametros_model->export_table_user($this->session->userdata['logged_user']['id'],$this->input->post('tabla'));
        
        if($html=="") {
           $this->log_error("Error al exportar.");
           $this->datos();
        }
        else{
            echo header('Content-type: text/plain');
            echo header('Content-Disposition: attachment; filename="'.$this->input->post('tabla').'.txt"');        

            foreach($html as $line){
                echo $line."\n";
            }
        }
    }
    
    public function import(){
        if(!$this->is_logged())  {redirect('welcome/index/');}

        $now = new DateTime();
        $target_file = APPPATH.'/uploads/'.$now->getTimestamp().$_FILES["file_to_import"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION); 

        if(!move_uploaded_file($_FILES["file_to_import"]["tmp_name"], $target_file)){
            $this->log_error("Error al subir el fichero al servidor.");
            $this->datos();
            return;
        }
        
        $table = $this->input->post('import_tabla');
        
        if($table==""){
            $this->log_error("Error al importar. Indique la tabla sobre la que importar");
            $this->datos();
            return;
        }
        
        if(!$this->parametros_model->import_table_user($this->session->userdata['logged_user']['id'], $table, $target_file )){
           $this->log_error("Error al importar. Revise el formato del fichero");
           $this->datos();
           return;
        }

        //Ok
        $this->load->view('templates/header');
        $data['operation_ok']='';
        $this->calculate($data);
        $data['tablas'] = $this->parametros_model->get_tablas_usuario_exportar();
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/configuracion-usuario', $data);
        $this->load->view('templates/footer');
    }


    private function assign_field( $field_name, &$data ) {
        if( $this->input->post($field_name) != null ) {
            $data[$field_name] = str_replace(".", "", $this->input->post($field_name)); 
            $data[$field_name] = str_replace(",", ".", $data[$field_name]); 
        }
        else {
            $data[$field_name] = null;
            $message ='';
            $this->log_error("Valor no válido en ".$message);
        }
    }
    
}