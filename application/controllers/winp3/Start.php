<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Start extends CI_Controller {
    public $m_Bases;
    public $m_Constantes;
    public $m_Dependencia_Lib;
    public $m_Economicos_Lib;
    public $m_Fallecimiento_Lib;
    public $m_Fiscalidad;
    public $m_Informa_Bases_Pensiones;
    public $m_Invalidez_Lib;
    public $m_It_Lib;
    public $m_Jubilacion_Lib;
    public $m_Pensiones;
    public $m_SimulacionRow;
    public $m_Titular_Lib;
    
    private function log_error($message){
        if(strpos($this->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field').$message." | ");
        }
    }

    public function options() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        $data=[];
        $this->load->view('pages/start', $data);
    }
    
    
    public function export() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}

        $values = $this->simulacion_model->exporta_simulacion();
        
        if($values=="") {
            $this->options();
        }
        else{
            echo header('Content-type: text/plain');
            echo header('Content-Disposition: attachment; filename="simulacion.winp3"');        

            foreach($values as $value){
                echo $value."\n";
            }
        }
    }
    
    public function prepare_import() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        $data=[];
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/carga_simulacion', $data);
    }
    
    public function import(){
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}

        $now = new DateTime();
        $target_file = APPPATH.'/uploads/'.$now->getTimestamp().$_FILES["file_to_import"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION); 

        if(!move_uploaded_file($_FILES["file_to_import"]["tmp_name"], $target_file)){
            $this->log_error("Error al subir el fichero al servidor.");
            $this->prepare_import();
            return;
        }
        
        if(!$this->simulacion_model->importa_simulacion($target_file)){
           $this->log_error("Error al importar. El fichero debe haber sido creado por WinP3.");
           $this->prepare_import();
           return;
        }

        //Ok
        redirect('titular/datos');
        
    }

    public function dosier(){
        if(!isset($this->session->userdata['logged_user'])){
            redirect('welcome/index/');
        }

        //Cargamos librería PDF:
        $this->load->library('mydompdf');            
        
        $data = [];
        $this->calculateIncapacidadTemporal($data);
        $this->calculateFallecimiento($data);
        $this->calculateInvalidez($data);
        $this->calculateJubilacion($data);
        $this->calculateDependencia($data);
        $this->load->model('User_model');
        $user = $this->User_model->get_user($this->session->userdata['logged_user']['id']);

        $data['contacto_correduria'] = $user['contacto_correduria'];
        $data['contacto_nombre'] = $user['contacto_nombre'];
        $data['contacto_telefono_1'] = $user['contacto_telefono_1'];
        $data['contacto_telefono_2'] = $user['contacto_telefono_2'];
        $data['contacto_direccion_1'] = $user['contacto_direccion_1'];
        $data['contacto_direccion_2'] = $user['contacto_direccion_2'];
        $data['contacto_mail'] = $user['contacto_mail'];
        $data['contacto_final_1'] = $user['contacto_final_1'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];
        
        //$this->load->view('pages/print/incapacidadtemporal', $data);

        //Generando pdf:
        $html = $this->load->view('pdf/pdf-resumen-general', $data, true);
        $this->mydompdf->load_html($html);
        $this->mydompdf->render();
        $this->mydompdf->set_base_path('./assets/css/dompdf.css'); //agregar de nuevo el css
        $this->mydompdf->stream("resumen-general.pdf", array(
        "Attachment" => false));
    }

    //Cálculo Incapacidad Temporal:
    private function calculateIncapacidadTemporal(&$data) {
        
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params);
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('it_lib', $params);
        $this->m_It_Lib = new It_Lib($params);
        $this->m_SimulacionRow = $this->simulacion_model->get_all(); 
        
        // Propios
        $data['it_titular_porcentaje_cobertura'] = $this->m_SimulacionRow['it_titular_porcentaje_cobertura'];
        $data['titular_capital_propuesto_dia_baja'] = $this->m_SimulacionRow['titular_capital_propuesto_dia_baja'];
        $data['titular_franquicia_dias'] = $this->m_SimulacionRow['titular_franquicia_dias'];
        $data['titular_duracion_maxima_dias'] = $this->m_SimulacionRow['titular_duracion_maxima_dias'];
        $data['it_titular_prima_anual'] = $this->m_SimulacionRow['it_titular_prima_anual'];
        $data['titular_dias_indemnizacion_percibida'] = $this->m_SimulacionRow['titular_dias_indemnizacion_percibida'];
        
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['it_conyugue_porcentaje_cobertura'] = $this->m_SimulacionRow['it_conyugue_porcentaje_cobertura'];
            $data['conyugue_capital_propuesto_dia_baja'] = $this->m_SimulacionRow['conyugue_capital_propuesto_dia_baja'];
            $data['conyugue_franquicia_dias'] = $this->m_SimulacionRow['conyugue_franquicia_dias'];
            $data['conyugue_duracion_maxima_dias'] = $this->m_SimulacionRow['conyugue_duracion_maxima_dias'];
            $data['it_conyugue_prima_anual'] = $this->m_SimulacionRow['it_conyugue_prima_anual'];
            $data['conyugue_dias_indemnizacion_percibida'] = $this->m_SimulacionRow['conyugue_dias_indemnizacion_percibida'];
        }
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];
        
        //Check Defaults
        if( $data['it_titular_porcentaje_cobertura'] == null ){
            $data['it_titular_porcentaje_cobertura']=80;
            $this->m_SimulacionRow['it_titular_porcentaje_cobertura']=$data['it_titular_porcentaje_cobertura'];
        }
        if( $data['titular_duracion_maxima_dias'] == null ){
            $data['titular_duracion_maxima_dias']=360;
            $this->m_SimulacionRow['titular_duracion_maxima_dias']=$data['titular_duracion_maxima_dias'];
        }
        if( $data['titular_dias_indemnizacion_percibida'] == null ){
            $data['titular_dias_indemnizacion_percibida']=30;
            $this->m_SimulacionRow['titular_dias_indemnizacion_percibida']=$data['titular_dias_indemnizacion_percibida'];
        }

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            if( $data['it_conyugue_porcentaje_cobertura'] == null ){
                $data['it_conyugue_porcentaje_cobertura']=80;
                $this->m_SimulacionRow['it_conyugue_porcentaje_cobertura']=$data['it_conyugue_porcentaje_cobertura'];
            }
            if( $data['conyugue_duracion_maxima_dias'] == null ){
                $data['conyugue_duracion_maxima_dias']=360;
                $this->m_SimulacionRow['conyugue_duracion_maxima_dias']=$data['conyugue_duracion_maxima_dias'];
            }
            if( $data['conyugue_dias_indemnizacion_percibida'] == null ){
                $data['conyugue_dias_indemnizacion_percibida']=30;
                $this->m_SimulacionRow['conyugue_dias_indemnizacion_percibida']=$data['conyugue_dias_indemnizacion_percibida'];
            }
        }
        
        //Calculados
        if( $this->m_Pensiones->titular_es_autonomo() ) {
            $data['titular_es_autonomo'] = 'Sí';

          $this->assign_expected_numeric($data, 'titular_pago_cuota_autonomos', 'titular_autonomos_cuota_mensual', "Titular: Falta Cuota mensual autónomos");
            if( isset($data['titular_pago_cuota_autonomos'])) {$data['titular_pago_cuota_autonomos'] = -$data['titular_pago_cuota_autonomos'];}
            $data['titular_ingresos_actividad_autonomos'] = $this->m_It_Lib->titular_ingresos_actividad_autonomos();
            $data['titular_base_reguladora_diaria'] = $this->m_Pensiones->titular_br_diaria();//
            $data['titular_prestaciones_ss_primer_mes'] = $this->m_Pensiones->titular_prestacion_primer_mes();//
            $data['titular_prestaciones_ss_siguientes_meses'] = $this->m_It_Lib->titular_prestaciones_ss_siguientes_meses();
            $data['titular_importe_neto_primer_mes'] = $this->m_Pensiones->titular_prestacion_primer_mes() - $this->m_SimulacionRow['titular_autonomos_cuota_mensual'];//
            $data['titular_importe_neto_siguientes_meses'] = $data['titular_prestaciones_ss_siguientes_meses'] - $this->m_SimulacionRow['titular_autonomos_cuota_mensual'];//
            $data['titular_descobertura_primer_mes'] = $data['titular_importe_neto_primer_mes'] - $this->m_SimulacionRow['titular_autonomos_ingreso_bruto_mensual'];//
            $data['titular_descobertura_siguientes_meses'] = $data['titular_importe_neto_siguientes_meses'] - $this->m_SimulacionRow['titular_autonomos_ingreso_bruto_mensual'];
            $data['titular_necesidad_seguro_it_segundo_mes'] = $data['titular_descobertura_siguientes_meses']*$this->m_SimulacionRow['it_titular_porcentaje_cobertura']/100;//
            $data['titular_necesidad_seguro_it_por_dia'] = $data['titular_necesidad_seguro_it_segundo_mes'] / 30;//
            $data['titular_importe_deducible_irpf'] = $this->m_It_Lib->titular_importe_deducible_irpf();
            $data['titular_tipo_marginal'] = $this->m_It_Lib->titular_tipo_marginal();
            $data['titular_posible_beneficio_fiscal'] = $data['titular_importe_deducible_irpf']*$data['titular_tipo_marginal']/100;
            $data['titular_indemnizacion'] = $this->m_It_Lib->titular_indemnizacion();
            $titular_it_total_cuota = $this->m_Fiscalidad->titular_it_total_cuota();
            $data['titular_cuota_a_pagar'] = $titular_it_total_cuota;

        }
        else {
            $data['titular_es_autonomo'] = "No";
            $data['titular_pago_cuota_autonomos']="";
            $data['titular_ingresos_actividad_autonomos']="";
        }

        $data['conyugue_es_autonomo'] = 'No';
        if($this->m_SimulacionRow['tiene_conyugue']==1){ 
            if( $this->m_Pensiones->conyugue_es_autonomo() ) {
                $data['conyugue_es_autonomo'] = 'Sí';
               $this->assign_expected_numeric($data, 'conyugue_pago_cuota_autonomos', 'conyugue_autonomos_cuota_mensual', "Cónyuge: Falta Cuota mensual autónomos");
                if( isset($data['conyugue_pago_cuota_autonomos'])) {$data['conyugue_pago_cuota_autonomos'] = -$data['conyugue_pago_cuota_autonomos'];}
                $data['conyugue_ingresos_actividad_autonomos'] = $this->m_It_Lib->conyugue_ingresos_actividad_autonomos();
                $data['conyugue_base_reguladora_diaria'] = $this->m_Pensiones->conyugue_br_diaria();//
                $data['conyugue_prestaciones_ss_primer_mes'] = $this->m_Pensiones->conyugue_prestacion_primer_mes();//
                $data['conyugue_prestaciones_ss_siguientes_meses'] = $this->m_It_Lib->conyugue_prestaciones_ss_siguientes_meses();
                $data['conyugue_importe_neto_primer_mes'] = $this->m_Pensiones->conyugue_prestacion_primer_mes() - $this->m_SimulacionRow['conyugue_autonomos_cuota_mensual'];//
                $data['conyugue_importe_neto_siguientes_meses'] = $data['conyugue_prestaciones_ss_siguientes_meses'] - $this->m_SimulacionRow['conyugue_autonomos_cuota_mensual'];//
                $data['conyugue_descobertura_primer_mes'] = $data['conyugue_importe_neto_primer_mes'] - $this->m_SimulacionRow['conyugue_autonomos_ingreso_bruto_mensual'];//
                $data['conyugue_descobertura_siguientes_meses'] = $data['conyugue_importe_neto_siguientes_meses'] - $this->m_SimulacionRow['conyugue_autonomos_ingreso_bruto_mensual'];
                $data['conyugue_necesidad_seguro_it_segundo_mes'] = $data['conyugue_descobertura_siguientes_meses']*$this->m_SimulacionRow['it_conyugue_porcentaje_cobertura']/100;//
                $data['conyugue_necesidad_seguro_it_por_dia'] = $data['conyugue_necesidad_seguro_it_segundo_mes'] / 30;//
                $data['conyugue_importe_deducible_irpf'] = $this->m_It_Lib->conyugue_importe_deducible_irpf();
                $data['conyugue_tipo_marginal'] = $this->m_It_Lib->conyugue_tipo_marginal();
                $data['conyugue_posible_beneficio_fiscal'] = $data['conyugue_tipo_marginal']*$data['conyugue_importe_deducible_irpf']/100;
                $data['conyugue_indemnizacion'] = $this->m_It_Lib->conyugue_indemnizacion();
                $conyugue_it_total_cuota = $this->m_Fiscalidad->conyugue_it_total_cuota();
                $data['conyugue_cuota_a_pagar'] = $conyugue_it_total_cuota;
            }
            else {
                $data['conyugue_es_autonomo'] = "No";
                $data['conyugue_pago_cuota_autonomos'] = "";
                $data['conyugue_ingresos_actividad_autonomos'] = "";
            }
        }
        
        if( $this->config->item("winp3_missing_field")!="") {return;}
    }

    //Cálculo Fallecimiento:
    private function calculateFallecimiento(&$data) {
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params );
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('bases', $params);
        $this->m_Bases = new Bases($params);
        $this->load->library('fallecimiento_lib', $params );
        $this->m_Fallecimiento_Lib = new Fallecimiento_Lib($params);
        
        $this->m_SimulacionRow = $this->simulacion_model->get_all(); 
        
        // Propios
        $data['fallecimiento_titular_porcentaje_cobertura'] = $this->m_SimulacionRow['fallecimiento_titular_porcentaje_cobertura'];
        $data['fallecimiento_titular_anyos_cobertura'] = $this->m_SimulacionRow['fallecimiento_titular_anyos_cobertura'];
        $data['fallecimiento_titular_capital_propuesto'] = $this->m_SimulacionRow['fallecimiento_titular_capital_propuesto'];
        $data['fallecimiento_titular_modalidad'] = $this->m_SimulacionRow['fallecimiento_titular_modalidad'];
        $data['fallecimiento_titular_prima_aproximada'] = $this->m_SimulacionRow['fallecimiento_titular_prima_aproximada'];
        
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['fallecimiento_conyugue_porcentaje_cobertura'] = $this->m_SimulacionRow['fallecimiento_conyugue_porcentaje_cobertura'];
            $data['fallecimiento_conyugue_anyos_cobertura'] = $this->m_SimulacionRow['fallecimiento_conyugue_anyos_cobertura'];
            $data['fallecimiento_conyugue_capital_propuesto'] = $this->m_SimulacionRow['fallecimiento_conyugue_capital_propuesto'];
            $data['fallecimiento_conyugue_modalidad'] = $this->m_SimulacionRow['fallecimiento_conyugue_modalidad'];
            $data['fallecimiento_conyugue_prima_aproximada'] = $this->m_SimulacionRow['fallecimiento_conyugue_prima_aproximada'];
        }
        
        $data['fallecimiento_beneficiario_1_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_1_relacion'];
        $data['fallecimiento_beneficiario_1_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_1_edad_descendiente'];
        $data['fallecimiento_beneficiario_1_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_1_importe_cobrar'];
        $data['fallecimiento_beneficiario_2_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_2_relacion'];
        $data['fallecimiento_beneficiario_2_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_2_edad_descendiente'];
        $data['fallecimiento_beneficiario_2_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_2_importe_cobrar'];
        $data['fallecimiento_beneficiario_3_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_3_relacion'];
        $data['fallecimiento_beneficiario_3_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_3_edad_descendiente'];
        $data['fallecimiento_beneficiario_3_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_3_importe_cobrar'];
        $data['fallecimiento_beneficiario_4_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_4_relacion'];
        $data['fallecimiento_beneficiario_4_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_4_edad_descendiente'];
        $data['fallecimiento_beneficiario_4_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_4_importe_cobrar'];
        $data['fallecimiento_beneficiario_5_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_5_relacion'];
        $data['fallecimiento_beneficiario_5_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_5_edad_descendiente'];
        $data['fallecimiento_beneficiario_5_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_5_importe_cobrar'];
        $data['fallecimiento_beneficiario_6_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_6_relacion'];
        $data['fallecimiento_beneficiario_6_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_6_edad_descendiente'];
        $data['fallecimiento_beneficiario_6_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_6_importe_cobrar'];
        $data['fallecimiento_beneficiario_7_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_7_relacion'];
        $data['fallecimiento_beneficiario_7_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_7_edad_descendiente'];
        $data['fallecimiento_beneficiario_7_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_7_importe_cobrar'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];
        
        //Check Defaults
        if( $data['fallecimiento_titular_porcentaje_cobertura'] == null ){
            $data['fallecimiento_titular_porcentaje_cobertura']=80;
            $this->m_SimulacionRow['fallecimiento_titular_porcentaje_cobertura']=$data['fallecimiento_titular_porcentaje_cobertura'];
        }
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            if( $data['fallecimiento_conyugue_porcentaje_cobertura'] == null ){
                $data['fallecimiento_conyugue_porcentaje_cobertura']=80;
                $this->m_SimulacionRow['fallecimiento_conyugue_porcentaje_cobertura']=$data['fallecimiento_conyugue_porcentaje_cobertura'];
            }
        }
        //

        // Calculados 
        if( $this->config->item("winp3_missing_field")!="") {return;}
        
        $data['titular_gastos_mensuales_familiares'] = $this->m_Fallecimiento_Lib->titular_gastos_mensuales_familiares();
        $data['titular_ingresos_conyugue'] = $this->m_Fallecimiento_Lib->titular_ingresos_conyugue();
        $data['titular_pension_viudedad'] = $this->m_Fallecimiento_Lib->titular_pension_viudedad();
        $data['titular_pension_orfandad_todas'] = $this->m_Fallecimiento_Lib->titular_pension_orfandad();
        $data['titular_saldo_mensual'] = $this->m_Fallecimiento_Lib->titular_saldo_mensual();
        //        $data['titular_anyos_cobertura'] = $this->m_Fallecimiento_Lib->titular_anyos_cobertura();
        $data['titular_cobertura_necesaria'] = $this->m_Fallecimiento_Lib->titular_cobertura_necesaria();
        $data['titular_deudas_pendientes'] = $this->m_Fallecimiento_Lib->titular_deudas_pendientes();
        $data['titular_gastos_entierro'] = $this->m_Fallecimiento_Lib->titular_gastos_entierro();
        $data['titular_costes_aceptacion_herencia'] = $this->m_Fallecimiento_Lib->titular_costes_aceptacion_herencia();
        $data['titular_capitales_ahorro_titular'] = $this->m_Fallecimiento_Lib->titular_capitales_ahorro_titular();
        $data['titular_capitales_ahorro_ambos'] = $this->m_Fallecimiento_Lib->titular_capitales_ahorro_ambos();
        $data['titular_saldo_seguro_vida_necesario'] = $this->m_Fallecimiento_Lib->titular_saldo_seguro_vida_necesario();
        $data['titular_seguros_ya_contratados'] = $this->m_Fallecimiento_Lib->titular_seguros_ya_contratados();
        $data['titular_necesidad_seguro_vida'] = $this->m_Fallecimiento_Lib->titular_necesidad_seguro_vida();
        $data['texto_bonificaciones_cuota'] = $this->m_Fallecimiento_Lib->bonificaciones_cuota();
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['conyugue_gastos_mensuales_familiares'] = $this->m_Fallecimiento_Lib->conyugue_gastos_mensuales_familiares();
            $data['conyugue_ingresos_conyugue'] = $this->m_Fallecimiento_Lib->conyugue_ingresos_conyugue();
            $data['conyugue_pension_viudedad'] = $this->m_Fallecimiento_Lib->conyugue_pension_viudedad();
            $data['conyugue_pension_orfandad_todas'] = $this->m_Fallecimiento_Lib->conyugue_pension_orfandad();
            $data['conyugue_saldo_mensual'] = $this->m_Fallecimiento_Lib->conyugue_saldo_mensual();
     //       $data['conyugue_anyos_cobertura'] = $this->m_Fallecimiento_Lib->conyugue_anyos_cobertura();
            $data['conyugue_cobertura_necesaria'] = $this->m_Fallecimiento_Lib->conyugue_cobertura_necesaria();
            $data['conyugue_deudas_pendientes'] = $this->m_Fallecimiento_Lib->conyugue_deudas_pendientes();
            $data['conyugue_gastos_entierro'] = $this->m_Fallecimiento_Lib->conyugue_gastos_entierro();
            $data['conyugue_costes_aceptacion_herencia'] = $this->m_Fallecimiento_Lib->conyugue_costes_aceptacion_herencia();
            $data['conyugue_capitales_ahorro_conyugue'] = $this->m_Fallecimiento_Lib->conyugue_capitales_ahorro_conyugue();
            $data['conyugue_capitales_ahorro_ambos'] = $this->m_Fallecimiento_Lib->conyugue_capitales_ahorro_ambos();
            $data['conyugue_saldo_seguro_vida_necesario'] = $this->m_Fallecimiento_Lib->conyugue_saldo_seguro_vida_necesario();
            $data['conyugue_seguros_ya_contratados'] = $this->m_Fallecimiento_Lib->conyugue_seguros_ya_contratados();
            $data['conyugue_necesidad_seguro_vida'] = $this->m_Fallecimiento_Lib->conyugue_necesidad_seguro_vida();
        }
        $n_hijos=$this->m_SimulacionRow['numero_hijos'];
        $data['numero_hijos'] = $n_hijos;
        for($i = 1; $i <= 7; $i++){ 
            if($data['fallecimiento_beneficiario_'.$i.'_relacion']!=''){
                $data['fallecimiento_beneficiario_'.$i.'_cuota'] = $this->m_Fiscalidad->beneficiario_total_cuota($i);
            }
            else {
                $data['fallecimiento_beneficiario_'.$i.'_cuota'] = '';
            }
        }
    }

    //Cálculo invalidez:
    private function calculateInvalidez(&$data) {
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params );
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('bases', $params );
        $this->m_Bases = new Bases($params);
        $this->load->library('invalidez_lib', $params);
        $this->m_Invalidez_Lib = new Invalidez_Lib($params);
        
        $this->m_SimulacionRow = $this->simulacion_model->get_all(); 
        
        //Propios
        $data['tipo_invalidez'] = $this->m_SimulacionRow['tipo_invalidez'];
        $data['invalidez_titular_porcentaje_cobertura'] = $this->m_SimulacionRow['invalidez_titular_porcentaje_cobertura'];
        $data['invalidez_titular_capital_propuesto'] = $this->m_SimulacionRow['invalidez_titular_capital_propuesto'];
        $data['invalidez_titular_modalidad'] = $this->m_SimulacionRow['invalidez_titular_modalidad'];
        $data['invalidez_titular_prima_aproximada'] = $this->m_SimulacionRow['invalidez_titular_prima_aproximada'];
        $data['invalidez_titular_primas_incluidas'] = $this->m_SimulacionRow['invalidez_titular_primas_incluidas'];
        $data['invalidez_titular_anyos_cobertura'] = $this->m_SimulacionRow['invalidez_titular_anyos_cobertura'];
        $data['invalidez_conyugue_porcentaje_cobertura'] = $this->m_SimulacionRow['invalidez_conyugue_porcentaje_cobertura'];
        $data['invalidez_conyugue_capital_propuesto'] = $this->m_SimulacionRow['invalidez_conyugue_capital_propuesto'];
        $data['invalidez_conyugue_modalidad'] = $this->m_SimulacionRow['invalidez_conyugue_modalidad'];
        $data['invalidez_conyugue_prima_aproximada'] = $this->m_SimulacionRow['invalidez_conyugue_prima_aproximada'];
        $data['invalidez_conyugue_primas_incluidas'] = $this->m_SimulacionRow['invalidez_conyugue_primas_incluidas'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];

        //Check fixed
        $data['invalidez_titular_porcentaje_cobertura'] = 100;
        $this->m_SimulacionRow['invalidez_titular_porcentaje_cobertura']=$data['invalidez_titular_porcentaje_cobertura'];
        $data['invalidez_conyugue_porcentaje_cobertura'] = 100;
        $this->m_SimulacionRow['invalidez_conyugue_porcentaje_cobertura']=$data['invalidez_conyugue_porcentaje_cobertura'];
        //
        
        //Calculados
        $data['titular_gastos_mensuales_familiares'] = -$this->m_Economicos_Lib->gastos_mensuales_media_familia();
        $data['titular_ingresos_conyugue'] = $this->m_Economicos_Lib->conyugue_total_ingresos_neto_mensual();
        if( $data['tipo_invalidez'] == 'ipt') {
            $data['titular_pension_invalidez'] = $this->m_Pensiones->titular_ipt_pension();
        }
        else {
            $data['titular_pension_invalidez'] = $this->m_Pensiones->titular_ipa_pension();
        }
        
        $data['titular_saldo_mensual'] = $data['titular_gastos_mensuales_familiares']+$data['titular_ingresos_conyugue']+$data['titular_pension_invalidez'];
        $data['titular_fecha_jubilacion'] = $this->m_Pensiones->titular_fecha_jubilacion();
        $data['titular_edad_jubilacion'] = $this->m_Pensiones->titular_edad_jubilacion();
        if( $data['titular_saldo_mensual']<0) {
            $data['titular_cobertura_necesaria'] = $data['titular_saldo_mensual']*$data['invalidez_titular_anyos_cobertura']*12*$data['invalidez_titular_porcentaje_cobertura']/100;
        }
        else {
            $data['titular_cobertura_necesaria'] = 0;
        }
        $data['titular_deudas_pendientes'] = -$this->m_Economicos_Lib->deudas_pendientes_familiares_hoy();
        $data['titular_adaptacion_vivienda'] = -$this->m_Constantes->adaptacion_vivienda();
        $data['titular_adaptacion_vehiculos_otros'] = -$this->m_Constantes->adaptacion_vehiculos();
        $data['titular_capitales_ahorro_titular'] = $this->m_Economicos_Lib->titular_capitales_ahorro_fallecimiento();
        $data['titular_capitales_ahorro_ambos'] = $this->m_Economicos_Lib->ambos_capitales_ahorro_fallecimiento();
        $data['titular_saldo_seguro_vida_necesario'] = $data['titular_cobertura_necesaria']+
                $data['titular_deudas_pendientes'] + $data['titular_adaptacion_vivienda'] + 
                $data['titular_adaptacion_vehiculos_otros']+$data['titular_capitales_ahorro_titular']+
                $data['titular_capitales_ahorro_ambos'];
        
        if( $data['tipo_invalidez'] == 'ipt') {
            $data['titular_seguros_ya_contratados'] = $this->m_Economicos_Lib->titular_suma_capitales_disponibles_seguro_vida_ipt();
        }
        else {
            $data['titular_seguros_ya_contratados'] = $this->m_Economicos_Lib->titular_suma_capitales_disponibles_seguro_vida_ipa();
        }
        
        $data['titular_necesidad_seguro_vida'] = -($data['titular_saldo_seguro_vida_necesario'] + $data['titular_seguros_ya_contratados']);

        $data['titular_total_cuota'] = $this->m_Fiscalidad->titular_invalidez_total_cuota();

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['conyugue_gastos_mensuales_familiares'] = -$this->m_Economicos_Lib->gastos_mensuales_media_familia();
            $data['conyugue_ingresos_conyugue'] = $this->m_Economicos_Lib->titular_total_ingresos_neto_mensual();
            if( $data['tipo_invalidez'] == 'ipt') {
                $data['conyugue_pension_invalidez'] = $this->m_Pensiones->conyugue_ipt_pension();
            }
            else {
                $data['conyugue_pension_invalidez'] = $this->m_Pensiones->conyugue_ipa_pension();
            }
            $data['conyugue_saldo_mensual'] = $data['conyugue_gastos_mensuales_familiares']+$data['conyugue_ingresos_conyugue']+$data['conyugue_pension_invalidez'];
            $data['conyugue_fecha_jubilacion'] = $this->m_Pensiones->conyugue_fecha_jubilacion();
            $data['conyugue_edad_jubilacion'] = $this->m_Pensiones->conyugue_edad_jubilacion();
            $data['invalidez_conyugue_anyos_cobertura'] = $this->m_Invalidez_Lib->conyugue_anyos_cobertura();

            if( $data['conyugue_saldo_mensual']<0) {
                $data['conyugue_cobertura_necesaria'] = $data['conyugue_saldo_mensual']*$data['invalidez_conyugue_anyos_cobertura']*12*$data['invalidez_conyugue_porcentaje_cobertura']/100;
            }
            else {
                $data['conyugue_cobertura_necesaria'] = 0;
            }
            $data['conyugue_deudas_pendientes'] = -$this->m_Economicos_Lib->deudas_pendientes_familiares_hoy();
            $data['conyugue_adaptacion_vivienda'] = -$this->m_Constantes->adaptacion_vivienda();
            $data['conyugue_adaptacion_vehiculos_otros'] = -$this->m_Constantes->adaptacion_vehiculos();
            $data['conyugue_capitales_ahorro_conyugue'] = $this->m_Economicos_Lib->conyugue_capitales_ahorro_fallecimiento();
            $data['conyugue_capitales_ahorro_ambos'] = $this->m_Economicos_Lib->ambos_capitales_ahorro_fallecimiento();
            $data['conyugue_saldo_seguro_vida_necesario'] = $data['conyugue_cobertura_necesaria']+
                    $data['conyugue_deudas_pendientes'] + $data['conyugue_adaptacion_vivienda'] + 
                    $data['conyugue_adaptacion_vehiculos_otros']+$data['conyugue_capitales_ahorro_conyugue']+
                    $data['conyugue_capitales_ahorro_ambos'];
            if( $data['tipo_invalidez'] == 'ipt') {
                $data['conyugue_seguros_ya_contratados'] = $this->m_Economicos_Lib->conyugue_suma_capitales_disponibles_seguro_vida_ipt();
            }
            else {
                $data['conyugue_seguros_ya_contratados'] = $this->m_Economicos_Lib->conyugue_suma_capitales_disponibles_seguro_vida_ipa();
            }
            $data['conyugue_necesidad_seguro_vida'] = -($data['conyugue_saldo_seguro_vida_necesario'] + $data['conyugue_seguros_ya_contratados']);
            $data['conyugue_total_cuota'] = $this->m_Fiscalidad->conyugue_invalidez_total_cuota();
        }
    }  

    //Cálculo jubilación:
    private function calculateJubilacion(&$data) {
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params );
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('bases', $params );
        $this->m_Bases = new Bases($params);
        $this->load->library('jubilacion_lib', $params );
        $this->m_Jubilacion_Lib = new Jubilacion_Lib($params);
        $this->load->library('dependencia_lib', $params );
        $this->m_Dependencia_Lib = new Dependencia_Lib($params);

        
        $this->m_SimulacionRow = $this->simulacion_model->get_all(); 
   
        //Propios
        $data['jubilacion_titular_porcentaje_reduccion_gastos'] = 0; //$this->m_SimulacionRow['jubilacion_titular_porcentaje_reduccion_gastos'];
        $data['jubilacion_titular_anyos_cobertura'] = $this->m_SimulacionRow['jubilacion_titular_anyos_cobertura'];
        $data['jubilacion_conyugue_porcentaje_reduccion_gastos'] = 0; //$this->m_SimulacionRow['jubilacion_conyugue_porcentaje_reduccion_gastos'];
        $data['jubilacion_porcentaje_cobertura'] = $this->m_SimulacionRow['jubilacion_porcentaje_cobertura'];
        $data['jubilacion_deudas_previstas_jubilacion'] = $this->m_SimulacionRow['jubilacion_deudas_previstas_jubilacion'];
        $data['jubilacion_titular_forma_calculo_pensiones'] = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_pensiones'];
        if($data['jubilacion_titular_forma_calculo_pensiones']==null){$data['jubilacion_titular_forma_calculo_pensiones']='aportacion';}
        $data['jubilacion_titular_forma_calculo_pias'] = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_pias'];
        if($data['jubilacion_titular_forma_calculo_pias']==null){$data['jubilacion_titular_forma_calculo_pias']='aportacion';}
        $data['jubilacion_titular_forma_calculo_seguro_vida'] = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_seguro_vida'];
        if($data['jubilacion_titular_forma_calculo_seguro_vida']==null){$data['jubilacion_titular_forma_calculo_seguro_vida']='aportacion';}
        $data['jubilacion_conyugue_forma_calculo_pensiones'] = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_pensiones'];
        $data['jubilacion_conyugue_forma_calculo_pias'] = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_pias'];
        $data['jubilacion_conyugue_forma_calculo_seguro_vida'] = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_seguro_vida'];
        $data['jubilacion_titular_traspaso_plan_pensiones'] = $this->m_SimulacionRow['jubilacion_titular_traspaso_plan_pensiones'];
        $data['jubilacion_conyugue_traspaso_plan_pensiones'] = $this->m_SimulacionRow['jubilacion_conyugue_traspaso_plan_pensiones'];
        $data['jubilacion_titular_anterior_2006'] = $this->m_SimulacionRow['jubilacion_titular_anterior_2006'];
        $data['jubilacion_conyugue_anterior_2006'] = $this->m_SimulacionRow['jubilacion_conyugue_anterior_2006'];
        $data['jubilacion_titular_reparto_cobro_capital'] =$this->m_SimulacionRow['jubilacion_titular_reparto_cobro_capital'];
        $data['jubilacion_conyugue_reparto_cobro_capital'] =$this->m_SimulacionRow['jubilacion_conyugue_reparto_cobro_capital'];
        $data['jubilacion_titular_anyos_renta'] = $this->m_SimulacionRow['jubilacion_titular_anyos_renta'];
        $data['jubilacion_conyugue_anyos_renta'] = $this->m_SimulacionRow['jubilacion_conyugue_anyos_renta'];
        $data['jubilacion_titular_edad_inicio_renta'] = $this->m_SimulacionRow['jubilacion_titular_edad_inicio_renta'];
        $data['jubilacion_conyugue_edad_inicio_renta'] = $this->m_SimulacionRow['jubilacion_conyugue_edad_inicio_renta'];
        
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];

        //Check Defaults
        $data['jubilacion_titular_porcentaje_reduccion_gastos']=0;
        if( $data['jubilacion_titular_anyos_cobertura'] == null){
            $data['jubilacion_titular_anyos_cobertura'] = 25;
            $this->m_SimulacionRow['jubilacion_titular_anyos_cobertura']=$data['jubilacion_titular_anyos_cobertura'];
        }
        if( $data['jubilacion_porcentaje_cobertura']==null){
            $data['jubilacion_porcentaje_cobertura']=80;
            $this->m_SimulacionRow['jubilacion_porcentaje_cobertura']=$data['jubilacion_porcentaje_cobertura'];
        }
        if( $data['jubilacion_titular_reparto_cobro_capital']==null){
            $data['jubilacion_titular_reparto_cobro_capital']=$this->m_Jubilacion_Lib->titular_imputable_antes_2006();
            $this->m_SimulacionRow['jubilacion_titular_reparto_cobro_capital']=$data['jubilacion_titular_reparto_cobro_capital'];
        }
        if( $data['jubilacion_titular_anyos_renta']==null) {
            $data['jubilacion_titular_anyos_renta']=25;
            $this->m_SimulacionRow['jubilacion_titular_anyos_renta']=$data['jubilacion_titular_anyos_renta'];
        }
        if( $data['jubilacion_titular_edad_inicio_renta']==null) {
            $data['jubilacion_titular_edad_inicio_renta']=70;
            $this->m_SimulacionRow['jubilacion_titular_edad_inicio_renta']=$data['jubilacion_titular_edad_inicio_renta'];
        }
          
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['jubilacion_conyugue_porcentaje_reduccion_gastos']=0;
            if( $data['jubilacion_conyugue_reparto_cobro_capital']==null){
                $data['jubilacion_conyugue_reparto_cobro_capital']=$this->m_Jubilacion_Lib->conyugue_imputable_antes_2006();
                $this->m_SimulacionRow['jubilacion_conyugue_reparto_cobro_capital']=$data['jubilacion_conyugue_reparto_cobro_capital'];
            }
            if( $data['jubilacion_conyugue_anyos_renta']==null) {
                $data['jubilacion_conyugue_anyos_renta']=25;
                $this->m_SimulacionRow['jubilacion_conyugue_anyos_renta']=$data['jubilacion_conyugue_anyos_renta'];
            }
            if( $data['jubilacion_conyugue_edad_inicio_renta']==null) {
                $data['jubilacion_conyugue_edad_inicio_renta']=70;
                $this->m_SimulacionRow['jubilacion_conyugue_edad_inicio_renta']=$data['jubilacion_conyugue_edad_inicio_renta'];
            }            
        }

        //Calculados

        $data['titular_fecha_nacimiento'] = $this->m_Jubilacion_Lib->titular_fecha_nacimiento();
        $data['titular_fecha_jubilacion'] = $this->m_Pensiones->titular_fecha_jubilacion();
        $data['titular_edad_jubilacion'] = $this->m_Pensiones->titular_edad_jubilacion();
        $data['titular_anyos_hasta_jubilacion'] = $this->m_Jubilacion_Lib->titular_anyos_hasta_jubilacion();
        $data['titular_gastos_mensuales_familiares_hoy'] = $this->m_Economicos_Lib->gastos_mensuales_media_familia();
        $data['titular_gastos_reducidos_hoy'] = $data['titular_gastos_mensuales_familiares_hoy']-$data['titular_gastos_mensuales_familiares_hoy']*$data['jubilacion_titular_porcentaje_reduccion_gastos']/100;
        $data['titular_ipc_estimado'] = $this->m_Constantes->ipc();
        $data['titular_gastos_reducidos_actualizado'] = $this->m_Jubilacion_Lib->titular_gastos_reducidos_actualizado();
        $data['titular_pension_jubilacion_propia_actualizado'] = $this->m_Jubilacion_Lib->titular_pension_jubilacion_propia_actualizado();
        //

        $data['titular_saldo_diferencial_mensual_cubrir'] = $this->m_Jubilacion_Lib->titular_saldo_diferencial_mensual_cubrir();

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['titular_ingresos_conyugue_actualizado'] = $this->m_Jubilacion_Lib->titular_ingresos_conyugue_actualizado();
            $data['conyugue_fecha_nacimiento'] = $this->m_Jubilacion_Lib->conyugue_fecha_nacimiento();
            $data['conyugue_fecha_jubilacion'] = $this->m_Pensiones->conyugue_fecha_jubilacion();
            $data['conyugue_edad_jubilacion'] = $this->m_Pensiones->conyugue_edad_jubilacion();
            $data['conyugue_anyos_hasta_jubilacion'] = $this->m_Jubilacion_Lib->conyugue_anyos_hasta_jubilacion();
            $data['conyugue_gastos_mensuales_familiares_hoy'] = $this->m_Economicos_Lib->gastos_mensuales_media_familia();
            $data['conyugue_gastos_reducidos_hoy'] = $data['conyugue_gastos_mensuales_familiares_hoy']-$data['conyugue_gastos_mensuales_familiares_hoy']*$data['jubilacion_conyugue_porcentaje_reduccion_gastos']/100;
            $data['conyugue_ipc_estimado'] = $this->m_Constantes->ipc();
            $data['conyugue_gastos_reducidos_actualizado'] = $this->m_Jubilacion_Lib->conyugue_gastos_reducidos_actualizado();
            $data['conyugue_pension_jubilacion_propia_actualizado'] = $this->m_Jubilacion_Lib->conyugue_pension_jubilacion_propia_actualizado();
            $data['conyugue_ingresos_conyugue_actualizado'] = $this->m_Jubilacion_Lib->conyugue_ingresos_conyugue_actualizado();
            $data['conyugue_saldo_diferencial_mensual_cubrir'] = $this->m_Jubilacion_Lib->conyugue_saldo_diferencial_mensual_cubrir();
            $data['anyos_jubilacion_entre_conyugues'] = $this->m_Jubilacion_Lib->anyos_jubilacion_entre_conyugues();
              
             
         
            $conyugue_forma_calculo = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_pensiones'];
            if($conyugue_forma_calculo == 'capital'){
                $data['jubilacion_conyugue_capital_pensiones'] = $this->m_SimulacionRow['jubilacion_conyugue_capital_pensiones'];
                $data['jubilacion_conyugue_aportacion_pensiones'] = $this->m_Jubilacion_Lib->conyugue_si_capital_aportacion_pensiones();
                $this->m_SimulacionRow['jubilacion_conyugue_aportacion_pensiones'] = $data['jubilacion_conyugue_aportacion_pensiones']; //Para facilitar calculos
            }
            else if($conyugue_forma_calculo == 'aportacion'){
                $data['jubilacion_conyugue_aportacion_pensiones'] = $this->m_SimulacionRow['jubilacion_conyugue_aportacion_pensiones'];
                $data['jubilacion_conyugue_capital_pensiones'] = $this->m_Jubilacion_Lib->conyugue_capital_si_aportacion_pensiones();
                $this->m_SimulacionRow['jubilacion_conyugue_capital_pensiones'] = $data['jubilacion_conyugue_capital_pensiones']; //Para facilitar calculos
            }
            else {
                $data['jubilacion_conyugue_capital_pensiones'] = "";
                $data['jubilacion_conyugue_aportacion_pensiones'] = "";
            }
            $conyugue_forma_calculo = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_pias'];
            if($conyugue_forma_calculo == 'capital'){
                $data['jubilacion_conyugue_capital_pias'] = $this->m_SimulacionRow['jubilacion_conyugue_capital_pias'];
                $data['jubilacion_conyugue_aportacion_pias'] = $this->m_Jubilacion_Lib->conyugue_si_capital_aportacion_pias();
                $this->m_SimulacionRow['jubilacion_conyugue_aportacion_pias'] = $data['jubilacion_conyugue_aportacion_pias']; //Para facilitar calculos
            }
            else if($conyugue_forma_calculo == 'aportacion'){
                $data['jubilacion_conyugue_aportacion_pias'] = $this->m_SimulacionRow['jubilacion_conyugue_aportacion_pias'];
                $data['jubilacion_conyugue_capital_pias'] = $this->m_Jubilacion_Lib->conyugue_capital_si_aportacion_pias();
                $this->m_SimulacionRow['jubilacion_conyugue_capital_pias'] = $data['jubilacion_conyugue_capital_pias']; //Para facilitar calculos
            }
            else {
                $data['jubilacion_conyugue_capital_pias'] = "";
                $data['jubilacion_conyugue_aportacion_pias'] = "";
            }
            $conyugue_forma_calculo = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_seguro_vida'];
            if($conyugue_forma_calculo == 'capital'){
                $data['jubilacion_conyugue_capital_seguro_vida'] = $this->m_SimulacionRow['jubilacion_conyugue_capital_seguro_vida'];
                $data['jubilacion_conyugue_aportacion_seguro_vida'] = $this->m_Jubilacion_Lib->conyugue_si_capital_aportacion_seguro_vida();
                $this->m_SimulacionRow['jubilacion_conyugue_aportacion_seguro_vida'] = $data['jubilacion_conyugue_aportacion_seguro_vida']; //Para facilitar calculos
            }
            else if($conyugue_forma_calculo == 'aportacion'){
                $data['jubilacion_conyugue_aportacion_seguro_vida'] = $this->m_SimulacionRow['jubilacion_conyugue_aportacion_seguro_vida'];
                $data['jubilacion_conyugue_capital_seguro_vida'] = $this->m_Jubilacion_Lib->conyugue_capital_si_aportacion_seguro_vida();
                $this->m_SimulacionRow['jubilacion_conyugue_capital_seguro_vida'] = $data['jubilacion_conyugue_capital_seguro_vida']; //Para facilitar calculos
            }
            else {
                $data['jubilacion_conyugue_capital_seguro_vida'] = "";
                $data['jubilacion_conyugue_aportacion_seguro_vida'] = "";
            }
            
        }
        
        $titular_forma_calculo = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_pensiones'];
        if($titular_forma_calculo == 'capital'){
            $data['jubilacion_titular_capital_pensiones'] = $this->m_SimulacionRow['jubilacion_titular_capital_pensiones'];
            $data['jubilacion_titular_aportacion_pensiones'] = $this->m_Jubilacion_Lib->titular_si_capital_aportacion_pensiones();
            $this->m_SimulacionRow['jubilacion_titular_aportacion_pensiones'] = $data['jubilacion_titular_aportacion_pensiones']; //Para facilitar calculos
        }
        else if($titular_forma_calculo == 'aportacion'){
            $data['jubilacion_titular_aportacion_pensiones'] = $this->m_SimulacionRow['jubilacion_titular_aportacion_pensiones'];
            $data['jubilacion_titular_capital_pensiones'] = $this->m_Jubilacion_Lib->titular_capital_si_aportacion_pensiones();
            $this->m_SimulacionRow['jubilacion_titular_capital_pensiones'] = $data['jubilacion_titular_capital_pensiones']; //Para facilitar calculos
        }
        else {
            $data['jubilacion_titular_capital_pensiones'] = "";
            $data['jubilacion_titular_aportacion_pensiones'] = "";
        }
        $titular_forma_calculo = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_pias'];
        if($titular_forma_calculo == 'capital'){
            $data['jubilacion_titular_capital_pias'] = $this->m_SimulacionRow['jubilacion_titular_capital_pias'];
            $data['jubilacion_titular_aportacion_pias'] = $this->m_Jubilacion_Lib->titular_si_capital_aportacion_pias();
            $this->m_SimulacionRow['jubilacion_titular_aportacion_pias'] = $data['jubilacion_titular_aportacion_pias']; //Para facilitar calculos
        }
        else if($titular_forma_calculo == 'aportacion'){
            $data['jubilacion_titular_aportacion_pias'] = $this->m_SimulacionRow['jubilacion_titular_aportacion_pias'];
            $data['jubilacion_titular_capital_pias'] = $this->m_Jubilacion_Lib->titular_capital_si_aportacion_pias();
            $this->m_SimulacionRow['jubilacion_titular_capital_pias'] = $data['jubilacion_titular_capital_pias']; //Para facilitar calculos
        }
        else {
            $data['jubilacion_titular_capital_pias'] = "";
            $data['jubilacion_titular_aportacion_pias'] = "";
        }
        $titular_forma_calculo = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_seguro_vida'];
        if($titular_forma_calculo == 'capital'){
            $data['jubilacion_titular_capital_seguro_vida'] = $this->m_SimulacionRow['jubilacion_titular_capital_seguro_vida'];
            $data['jubilacion_titular_aportacion_seguro_vida'] = $this->m_Jubilacion_Lib->titular_si_capital_aportacion_seguro_vida();
            $this->m_SimulacionRow['jubilacion_titular_aportacion_seguro_vida'] = $data['jubilacion_titular_aportacion_seguro_vida']; //Para facilitar calculos
        }
        else if($titular_forma_calculo == 'aportacion'){
            $data['jubilacion_titular_aportacion_seguro_vida'] = $this->m_SimulacionRow['jubilacion_titular_aportacion_seguro_vida'];
            $data['jubilacion_titular_capital_seguro_vida'] = $this->m_Jubilacion_Lib->titular_capital_si_aportacion_seguro_vida();
            $this->m_SimulacionRow['jubilacion_titular_capital_seguro_vida'] = $data['jubilacion_titular_capital_seguro_vida']; //Para facilitar calculos
        }
        else {
            $data['jubilacion_titular_capital_seguro_vida'] = "";
            $data['jubilacion_titular_aportacion_seguro_vida'] = "";
        }

        $data['diferencial_generado_periodo_1'] = $this->m_Jubilacion_Lib->diferencial_generado_periodo_1();
        $data['resto_periodo_cobertura'] = $this->m_Jubilacion_Lib->resto_periodo_cobertura();
        $data['diferencial_generado_periodo_2'] =$this->m_Jubilacion_Lib->diferencial_generado_periodo_2();
        $data['diferencial_generado_periodo_1_mas_2'] = $this->m_Jubilacion_Lib->diferencial_generado_periodo_1_mas_2();
        $data['diferencial_cubrir'] = $this->m_Jubilacion_Lib->diferencial_cubrir();
        $data['titular_capital_necesario_jubilacion'] = -$this->m_Jubilacion_Lib->diferencial_cubrir();

        $data['deudas_familiares_hoy'] = -($this->m_Economicos_Lib->deudas_pendientes_familiares_hoy());
 
        $data['capital_necesario_jubilacion'] = $this->m_Jubilacion_Lib->capital_necesario_jubilacion();
        $data['ahorro_inversion_realizado'] = $this->m_Economicos_Lib->suma_capitales_disponibles_ahorro_jubilacion_total();
        $data['descobertura'] = $this->m_Jubilacion_Lib->descobertura();
        $data['suma_aportaciones_familiares'] = $this->m_Jubilacion_Lib->suma_aportaciones_familiares();
        $data['suma_capitales_obtenidos'] = $this->m_Jubilacion_Lib->suma_capitales_obtenidos();

        $data['titular_importe_reducir_base_liquidadora'] = $this->m_Jubilacion_Lib->titular_importe_reducir_base_liquidadora();
        $data['titular_tipo_marginal_hoy'] = $this->m_Jubilacion_Lib->titular_tipo_marginal_hoy();
        $data['titular_beneficio_fiscal_anual'] = $this->m_Jubilacion_Lib->titular_beneficio_fiscal_anual();
        $data['titular_beneficio_fiscal_acumulado'] = $this->m_Jubilacion_Lib->titular_beneficio_fiscal_acumulado();
        
        $data['titular_traspaso'] = $data['jubilacion_titular_traspaso_plan_pensiones'];
        $data['titular_aportaciones'] = $this->m_Jubilacion_Lib->titular_aportaciones();
        $data['titular_capital_final'] = $this->m_SimulacionRow['jubilacion_titular_capital_pensiones'];
        $data['titular_imputable_antes_2006'] = $this->m_Jubilacion_Lib->titular_imputable_antes_2006();

        $data['titular_reparto_cobro_renta'] = $this->m_Jubilacion_Lib->titular_reparto_cobro_capital_renta();
        $data['titular_prestaciones_anteriores_2006'] = $this->m_Jubilacion_Lib->titular_prestaciones_anteriores_2006();
        $data['titular_resto_percibir'] = $this->m_Jubilacion_Lib->titular_resto_percibir();
        $data['titular_incremento_bl'] = $this->m_Jubilacion_Lib->titular_incremento_bl();
        $data['titular_renta_anual'] = $this->m_Jubilacion_Lib->titular_renta_anual_obtenida();
        $data['titular_renta_mensual'] = $this->m_Jubilacion_Lib->titular_renta_mensual_obtenida();
        $data['titular_base_ss'] = $this->m_Jubilacion_Lib->titular_base_ss();
        $data['titular_base_ss_capital'] = $this->m_Jubilacion_Lib->titular_base_ss_capital();
        $data['titular_base_ss_renta'] = $this->m_Jubilacion_Lib->titular_base_ss_renta();
        $data['titular_tipo_marginal_ss'] = $this->m_Jubilacion_Lib->titular_tipo_marginal_ss();
        $data['titular_tipo_marginal_ss_capital'] = $this->m_Jubilacion_Lib->titular_tipo_marginal_ss_capital();
        $data['titular_tipo_marginal_ss_renta'] = $this->m_Jubilacion_Lib->titular_tipo_marginal_ss_renta();
        $data['titular_cuota_estatal_ss'] = $this->m_Jubilacion_Lib->titular_cuota_estatal_ss();
        $data['titular_cuota_estatal_ss_capital'] = $this->m_Jubilacion_Lib->titular_cuota_estatal_ss_capital();
        $data['titular_cuota_estatal_ss_renta'] = $this->m_Jubilacion_Lib->titular_cuota_estatal_ss_renta();
        $data['titular_cuota_autonomica_ss'] = $this->m_Jubilacion_Lib->titular_cuota_autonomica_ss();
        $data['titular_cuota_autonomica_ss_capital'] = $this->m_Jubilacion_Lib->titular_cuota_autonomica_ss_capital();
        $data['titular_cuota_autonomica_ss_renta'] = $this->m_Jubilacion_Lib->titular_cuota_autonomica_ss_renta();
        $data['titular_total_cuota_ss'] = $this->m_Jubilacion_Lib->titular_total_cuota_ss();
        $data['titular_total_cuota_ss_capital'] = $this->m_Jubilacion_Lib->titular_total_cuota_ss_capital();
        $data['titular_total_cuota_ss_renta'] = $this->m_Jubilacion_Lib->titular_total_cuota_ss_renta();

        $data['titular_coste_fiscal_capital'] = $this->m_Jubilacion_Lib->titular_coste_fiscal_capital();
        $data['titular_coste_fiscal_renta_1'] = $this->m_Jubilacion_Lib->titular_coste_fiscal_renta_1();
        $data['titular_coste_fiscal_x'] = $this->m_Jubilacion_Lib->titular_coste_fiscal_x();
        $data['titular_coste_fiscal_total'] = $this->m_Jubilacion_Lib->titular_coste_fiscal_total();
        $data['titular_capital_final_pias'] = $this->m_Jubilacion_Lib->titular_capital_final_pias();
        $data['titular_porcentaje_exencion'] = $this->m_Jubilacion_Lib->titular_porcentaje_exencion();
        $data['titular_pias_renta_anual_obtenida'] = $this->m_Jubilacion_Lib->titular_pias_renta_anual_obtenida();
        $data['titular_pias_renta_anual_exenta'] = $this->m_Jubilacion_Lib->titular_pias_renta_anual_exenta();
        $data['titular_pias_renta_anual_tributable'] = $this->m_Jubilacion_Lib->titular_pias_renta_anual_tributable();
        $data['titular_pias_total_cuota_tributaria_anual'] = $this->m_Jubilacion_Lib->titular_pias_total_cuota_tributaria_anual();
        $data['titular_pias_renta_anual_neta'] = $this->m_Jubilacion_Lib->titular_pias_renta_anual_neta();
        $data['titular_pias_renta_mensual_obtenida'] = $this->m_Jubilacion_Lib->titular_pias_renta_mensual_obtenida();
        $data['titular_pias_total_cuota_tributaria_mensual'] = $this->m_Jubilacion_Lib->titular_pias_total_cuota_tributaria_mensual();
        $data['titular_pias_renta_mensual_neta'] = $this->m_Jubilacion_Lib->titular_pias_renta_mensual_neta();
        $data['titular_pias_coste_fiscal_operacion'] = $this->m_Jubilacion_Lib->titular_pias_coste_fiscal_operacion();
        $data['titular_capital_final_sv'] = $this->m_Jubilacion_Lib->titular_capital_final_sv();
        $data['titular_aportaciones_sv'] = $this->m_Jubilacion_Lib->titular_aportaciones_sv();
        $data['titular_rendimiento_obtenido_sv'] = $this->m_Jubilacion_Lib->titular_rendimiento_obtenido_sv();
        $data['titular_total_cuota_operacion'] = $this->m_Jubilacion_Lib->titular_total_cuota_operacion();

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            
            $data['conyugue_importe_reducir_base_liquidadora'] = $this->m_Jubilacion_Lib->conyugue_importe_reducir_base_liquidadora();

            $data['conyugue_tipo_marginal_hoy'] = $this->m_Jubilacion_Lib->conyugue_tipo_marginal_hoy();
            $data['conyugue_beneficio_fiscal_anual'] = $this->m_Jubilacion_Lib->conyugue_beneficio_fiscal_anual();
            $data['conyugue_beneficio_fiscal_acumulado'] = $this->m_Jubilacion_Lib->conyugue_beneficio_fiscal_acumulado();

            $data['conyugue_traspaso'] = $data['jubilacion_conyugue_traspaso_plan_pensiones'];
            $data['conyugue_aportaciones'] = $this->m_Jubilacion_Lib->conyugue_aportaciones();
            $data['conyugue_capital_final'] = $this->m_SimulacionRow['jubilacion_conyugue_capital_pensiones'];
            $data['conyugue_imputable_antes_2006'] = $this->m_Jubilacion_Lib->conyugue_imputable_antes_2006();
            
            $data['conyugue_reparto_cobro_renta'] = $this->m_Jubilacion_Lib->conyugue_reparto_cobro_capital_renta();
            $data['conyugue_prestaciones_anteriores_2006'] = $this->m_Jubilacion_Lib->conyugue_prestaciones_anteriores_2006();
            $data['conyugue_resto_percibir'] = $this->m_Jubilacion_Lib->conyugue_resto_percibir();
            $data['conyugue_incremento_bl'] = $this->m_Jubilacion_Lib->conyugue_incremento_bl();

            $data['conyugue_renta_anual'] = $this->m_Jubilacion_Lib->conyugue_renta_anual_obtenida();
            $data['conyugue_renta_mensual'] = $this->m_Jubilacion_Lib->conyugue_renta_mensual_obtenida();
            $data['conyugue_base_ss'] = $this->m_Jubilacion_Lib->conyugue_base_ss();
            $data['conyugue_base_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_base_ss_capital();
            $data['conyugue_base_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_base_ss_renta();
            $data['conyugue_tipo_marginal_ss'] = $this->m_Jubilacion_Lib->conyugue_tipo_marginal_ss();
            $data['conyugue_tipo_marginal_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_tipo_marginal_ss_capital();
            $data['conyugue_tipo_marginal_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_tipo_marginal_ss_renta();
            $data['conyugue_cuota_estatal_ss'] = $this->m_Jubilacion_Lib->conyugue_cuota_estatal_ss();
            $data['conyugue_cuota_estatal_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_cuota_estatal_ss_capital();
            $data['conyugue_cuota_estatal_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_cuota_estatal_ss_renta();
            $data['conyugue_cuota_autonomica_ss'] = $this->m_Jubilacion_Lib->conyugue_cuota_autonomica_ss();
            $data['conyugue_cuota_autonomica_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_cuota_autonomica_ss_capital();
            $data['conyugue_cuota_autonomica_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_cuota_autonomica_ss_renta();
            $data['conyugue_total_cuota_ss'] = $this->m_Jubilacion_Lib->conyugue_total_cuota_ss();
            $data['conyugue_total_cuota_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_total_cuota_ss_capital();
            $data['conyugue_total_cuota_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_total_cuota_ss_renta();
            $data['conyugue_coste_fiscal_capital'] = $this->m_Jubilacion_Lib->conyugue_coste_fiscal_capital();
            $data['conyugue_coste_fiscal_renta_1'] = $this->m_Jubilacion_Lib->conyugue_coste_fiscal_renta_1();
            $data['conyugue_coste_fiscal_x'] = $this->m_Jubilacion_Lib->conyugue_coste_fiscal_x();
            $data['conyugue_coste_fiscal_total'] = $this->m_Jubilacion_Lib->conyugue_coste_fiscal_total();
            $data['conyugue_capital_final_pias'] = $this->m_Jubilacion_Lib->conyugue_capital_final_pias();
            $data['conyugue_porcentaje_exencion'] = $this->m_Jubilacion_Lib->conyugue_porcentaje_exencion();
            $data['conyugue_pias_renta_anual_obtenida'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_anual_obtenida();
            $data['conyugue_pias_renta_anual_exenta'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_anual_exenta();
            $data['conyugue_pias_renta_anual_tributable'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_anual_tributable();
            $data['conyugue_pias_total_cuota_tributaria_anual'] = $this->m_Jubilacion_Lib->conyugue_pias_total_cuota_tributaria_anual();
            $data['conyugue_pias_renta_anual_neta'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_anual_neta();
            $data['conyugue_pias_renta_mensual_obtenida'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_mensual_obtenida();
            $data['conyugue_pias_total_cuota_tributaria_mensual'] = $this->m_Jubilacion_Lib->conyugue_pias_total_cuota_tributaria_mensual();
            $data['conyugue_pias_renta_mensual_neta'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_mensual_neta();
            $data['conyugue_pias_coste_fiscal_operacion'] = $this->m_Jubilacion_Lib->conyugue_pias_coste_fiscal_operacion();
            $data['conyugue_capital_final_sv'] = $this->m_Jubilacion_Lib->conyugue_capital_final_sv();
            $data['conyugue_aportaciones_sv'] = $this->m_Jubilacion_Lib->conyugue_aportaciones_sv();
            $data['conyugue_rendimiento_obtenido_sv'] = $this->m_Jubilacion_Lib->conyugue_rendimiento_obtenido_sv();
            $data['conyugue_total_cuota_operacion'] = $this->m_Jubilacion_Lib->conyugue_total_cuota_operacion();


        }
    }

    //Cálculo dependencia:
    private function calculateDependencia(&$data) {
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params );
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('jubilacion_lib', $params );
        $this->m_Jubilacion_Lib = new Jubilacion_Lib($params);
        $this->load->library('dependencia_lib', $params );
        $this->m_Dependencia_Lib = new Dependencia_Lib($params);
        $this->load->library('bases', $params);
        $this->m_Bases = new Bases($params);
        
        $this->m_SimulacionRow = $this->simulacion_model->get_all(); 

        //Propios
        $data['dependencia_titular_coste_dependencia_hoy'] = $this->m_SimulacionRow['dependencia_titular_coste_dependencia_hoy'];
        $data['dependencia_titular_anyos_dependencia'] = $this->m_SimulacionRow['dependencia_titular_anyos_dependencia'];
        $data['dependencia_titular_porcentaje_cobertura'] = $this->m_SimulacionRow['dependencia_titular_porcentaje_cobertura'];
        $data['dependencia_titular_anyos_renta'] = $this->m_SimulacionRow['dependencia_titular_anyos_renta'];

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['dependencia_conyugue_coste_dependencia_hoy'] = $this->m_SimulacionRow['dependencia_conyugue_coste_dependencia_hoy'];
            $data['dependencia_conyugue_anyos_dependencia'] = $this->m_SimulacionRow['dependencia_conyugue_anyos_dependencia'];
            $data['dependencia_conyugue_porcentaje_cobertura'] = $this->m_SimulacionRow['dependencia_conyugue_porcentaje_cobertura'];
            $data['dependencia_conyugue_anyos_renta'] = $this->m_SimulacionRow['dependencia_conyugue_anyos_renta'];
        }
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];
        
        
        //Check Defaults
        if($data['dependencia_titular_coste_dependencia_hoy']==null){
            $data['dependencia_titular_coste_dependencia_hoy']=2500;
            $this->m_SimulacionRow['dependencia_titular_coste_dependencia_hoy']=$data['dependencia_titular_coste_dependencia_hoy'];
        }
        if($data['dependencia_titular_anyos_dependencia']==null){
            $data['dependencia_titular_anyos_dependencia']=7;
            $this->m_SimulacionRow['dependencia_titular_anyos_dependencia']=$data['dependencia_titular_anyos_dependencia'];
        }
        if($data['dependencia_titular_porcentaje_cobertura']==null) {
            $data['dependencia_titular_porcentaje_cobertura']=80;
            $this->m_SimulacionRow['dependencia_titular_porcentaje_cobertura']=$data['dependencia_titular_porcentaje_cobertura'];
        }
        if($data['dependencia_titular_anyos_renta']==null){
            $data['dependencia_titular_anyos_renta']=$data['dependencia_titular_anyos_dependencia'];
            $this->m_SimulacionRow['dependencia_titular_anyos_renta']=$data['dependencia_titular_anyos_renta'];
        }
        
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            if($data['dependencia_conyugue_coste_dependencia_hoy']==null){
                $data['dependencia_conyugue_coste_dependencia_hoy']=2500;
                $this->m_SimulacionRow['dependencia_conyugue_coste_dependencia_hoy']=$data['dependencia_conyugue_coste_dependencia_hoy'];
            }
            if($data['dependencia_conyugue_anyos_dependencia']==null){
                $data['dependencia_conyugue_anyos_dependencia']=7;
                $this->m_SimulacionRow['dependencia_conyugue_anyos_dependencia']=$data['dependencia_conyugue_anyos_dependencia'];
            }            
            if($data['dependencia_conyugue_porcentaje_cobertura']==null) {
                $data['dependencia_conyugue_porcentaje_cobertura']=80;
                $this->m_SimulacionRow['dependencia_conyugue_porcentaje_cobertura']=$data['dependencia_conyugue_porcentaje_cobertura'];
            }            
            if($data['dependencia_conyugue_anyos_renta']==null){
                $data['dependencia_conyugue_anyos_renta']=$data['dependencia_titular_anyos_dependencia'];
                $this->m_SimulacionRow['dependencia_conyugue_anyos_renta']=$data['dependencia_conyugue_anyos_renta'];
            }
        }
        
        //Calculados
        $data['titular_fecha_nacimiento'] = $this->m_Dependencia_Lib->titular_fecha_nacimiento()->format("d-m-Y");
        $data['titular_fecha_80_cumpleanyos'] = $this->m_Dependencia_Lib->titular_fecha_80_cumpleanyos()->format("d-m-Y");
        $data['titular_anyos_hasta_dependencia'] = $this->m_Dependencia_Lib->titular_anyos_hasta_dependencia();
        $data['titular_fecha_jubilacion'] = $this->m_Pensiones->titular_fecha_jubilacion();
        $data['titular_edad_jubilacion'] = $this->m_Pensiones->titular_edad_jubilacion();
        $data['titular_anyos_desde_jubilacion_a_dependencia'] = $this->m_Dependencia_Lib->titular_anyos_desde_jubilacion_a_dependencia();
        $data['titular_coste_dependencia_actualizado'] = $this->m_Dependencia_Lib->titular_coste_dependencia_actualizado();
        $data['titular_coste_total_dependencia'] = $this->m_Dependencia_Lib->titular_coste_total_dependencia();
        $data['titular_necesidad_dependencia'] = $this->m_Dependencia_Lib->titular_necesidad_dependencia();
        
        $data['titular_base_ss'] = $this->m_Dependencia_Lib->titular_base_ss();
        $data['titular_base_ss_capital'] = $this->m_Dependencia_Lib->titular_base_ss_capital();
        $data['titular_base_ss_renta'] = $this->m_Dependencia_Lib->titular_base_ss_renta();
        $data['titular_tipo_marginal_ss'] = $this->m_Dependencia_Lib->titular_tipo_marginal_ss();
        $data['titular_tipo_marginal_ss_capital'] = $this->m_Dependencia_Lib->titular_tipo_marginal_ss_capital();
        $data['titular_tipo_marginal_ss_renta'] = $this->m_Dependencia_Lib->titular_tipo_marginal_ss_renta();
        $data['titular_cuota_estatal_ss'] = $this->m_Dependencia_Lib->titular_cuota_estatal_ss();
        $data['titular_cuota_estatal_ss_capital'] = $this->m_Dependencia_Lib->titular_cuota_estatal_ss_capital();
        $data['titular_cuota_estatal_ss_renta'] = $this->m_Dependencia_Lib->titular_cuota_estatal_ss_renta();
        $data['titular_cuota_autonomica_ss'] = $this->m_Dependencia_Lib->titular_cuota_autonomica_ss();
        $data['titular_cuota_autonomica_ss_capital'] = $this->m_Dependencia_Lib->titular_cuota_autonomica_ss_capital();
        $data['titular_cuota_autonomica_ss_renta'] = $this->m_Dependencia_Lib->titular_cuota_autonomica_ss_renta();
        $data['titular_total_cuota_ss'] = $this->m_Dependencia_Lib->titular_total_cuota_ss();
        $data['titular_total_cuota_ss_capital'] = $this->m_Dependencia_Lib->titular_total_cuota_ss_capital();
        $data['titular_total_cuota_ss_renta'] = $this->m_Dependencia_Lib->titular_total_cuota_ss_renta();
        $data['titular_coste_fiscal_capital'] = $this->m_Dependencia_Lib->titular_coste_fiscal_capital();
        $data['titular_coste_fiscal_renta_1'] = $this->m_Dependencia_Lib->titular_coste_fiscal_renta_1();
        $data['titular_coste_fiscal_x'] = $this->m_Dependencia_Lib->titular_coste_fiscal_x();

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['conyugue_fecha_nacimiento'] = $this->m_Dependencia_Lib->conyugue_fecha_nacimiento()->format("d-m-Y");
            $data['conyugue_fecha_80_cumpleanyos'] = $this->m_Dependencia_Lib->conyugue_fecha_80_cumpleanyos()->format("d-m-Y");
            $data['conyugue_anyos_hasta_dependencia'] = $this->m_Dependencia_Lib->conyugue_anyos_hasta_dependencia();
            $data['conyugue_fecha_jubilacion'] = $this->m_Pensiones->conyugue_fecha_jubilacion();
            $data['conyugue_edad_jubilacion'] = $this->m_Pensiones->conyugue_edad_jubilacion();
            $data['conyugue_anyos_desde_jubilacion_a_dependencia'] = $this->m_Dependencia_Lib->conyugue_anyos_desde_jubilacion_a_dependencia();
            $data['conyugue_coste_dependencia_actualizado'] = $this->m_Dependencia_Lib->conyugue_coste_dependencia_actualizado();
            $data['conyugue_coste_total_dependencia'] = $this->m_Dependencia_Lib->conyugue_coste_total_dependencia();
            $data['conyugue_necesidad_dependencia'] = $this->m_Dependencia_Lib->conyugue_necesidad_dependencia();
        }

        //        $data['dependencia_titular_capital_jubilacion'] = $this->m_SimulacionRow['dependencia_titular_capital_jubilacion'];
        
        $data['dependencia_titular_aportacion_mensual'] = $this->m_SimulacionRow['dependencia_titular_aportacion_anual']/12;
        $data['dependencia_titular_aportacion_anual'] = $this->m_SimulacionRow['dependencia_titular_aportacion_anual'];
        $this->m_SimulacionRow['dependencia_titular_aportacion_mensual'] = $data['dependencia_titular_aportacion_mensual']; //Para facilitar calculos
        $this->m_SimulacionRow['dependencia_titular_aportacion_anual'] = $data['dependencia_titular_aportacion_anual']; //Para facilitar calculos
        
        if($this->m_SimulacionRow['tiene_conyugue']==1){
        //            $data['dependencia_conyugue_capital_jubilacion'] = $this->m_SimulacionRow['dependencia_conyugue_capital_jubilacion'];
            $data['dependencia_conyugue_aportacion_mensual'] = $this->m_SimulacionRow['dependencia_conyugue_aportacion_anual']/12;
            $data['dependencia_conyugue_aportacion_anual'] = $this->m_SimulacionRow['dependencia_conyugue_aportacion_anual'];
            $this->m_SimulacionRow['dependencia_conyugue_aportacion_mensual'] = $data['dependencia_conyugue_aportacion_mensual']; //Para facilitar calculos
            $this->m_SimulacionRow['dependencia_conyugue_aportacion_anual'] = $data['dependencia_conyugue_aportacion_anual']; //Para facilitar calculos
        }
        
        $data['titular_importe_reducir_base_liquidadora'] = $this->m_Dependencia_Lib->titular_importe_reducir_base_liquidadora();
        $data['titular_tipo_marginal_hoy'] = $this->m_Dependencia_Lib->titular_tipo_marginal_hoy();
        $data['titular_beneficio_fiscal'] = $this->m_Dependencia_Lib->titular_beneficio_fiscal();
        $data['titular_tipo_marginal_jubilacion'] = $this->m_Dependencia_Lib->titular_tipo_marginal_ss();
        $data['titular_beneficio_fiscal_jubilacion'] = $data['titular_importe_reducir_base_liquidadora']*$data['titular_tipo_marginal_jubilacion']/100;
        $data['titular_reparto_cobro_capital_final_capital']=$this->m_Dependencia_Lib->titular_reparto_cobro_capital_final_capital();
        $data['titular_reparto_cobro_capital_final_renta']=$this->m_Dependencia_Lib->titular_reparto_cobro_capital_final_renta();
        $data['titular_contratante'] = $this->m_Dependencia_Lib->titular_contratante();
        $data['titular_modalidad'] = $this->m_Dependencia_Lib->titular_modalidad();
        $data['titular_renta_anual_obtenida']=$this->m_Dependencia_Lib->titular_renta_anual_obtenida();
        $data['titular_renta_mensual_obtenida']=$this->m_Dependencia_Lib->titular_renta_mensual_obtenida();
        $data['titular_incremento_bl'] = $this->m_Dependencia_Lib->titular_incremento_bl();
        
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['conyugue_importe_reducir_base_liquidadora'] = $this->m_Dependencia_Lib->conyugue_importe_reducir_base_liquidadora();
            $data['conyugue_tipo_marginal_hoy'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_hoy();        
            $data['conyugue_beneficio_fiscal'] = $this->m_Dependencia_Lib->conyugue_beneficio_fiscal();
            $data['conyugue_tipo_marginal_jubilacion'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_ss();
            $data['conyugue_beneficio_fiscal_jubilacion'] = $data['conyugue_importe_reducir_base_liquidadora']*$data['conyugue_tipo_marginal_jubilacion']/100;
            $data['conyugue_reparto_cobro_capital_final_capital']=$this->m_Dependencia_Lib->conyugue_reparto_cobro_capital_final_capital();
            $data['conyugue_reparto_cobro_capital_final_renta']=$this->m_Dependencia_Lib->conyugue_reparto_cobro_capital_final_renta();
            $data['conyugue_contratante'] = $this->m_Dependencia_Lib->conyugue_contratante();
            $data['conyugue_modalidad'] = $this->m_Dependencia_Lib->conyugue_modalidad();
            $data['conyugue_renta_anual_obtenida']=$this->m_Dependencia_Lib->conyugue_renta_anual_obtenida();
            $data['conyugue_renta_mensual_obtenida']=$this->m_Dependencia_Lib->conyugue_renta_mensual_obtenida();
            $data['conyugue_incremento_bl'] = $this->m_Dependencia_Lib->conyugue_incremento_bl();
            $data['conyugue_base_ss'] = $this->m_Dependencia_Lib->conyugue_base_ss();
            $data['conyugue_base_ss_capital'] = $this->m_Dependencia_Lib->conyugue_base_ss_capital();
            $data['conyugue_base_ss_renta'] = $this->m_Dependencia_Lib->conyugue_base_ss_renta();
            $data['conyugue_tipo_marginal_ss'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_ss();
            $data['conyugue_tipo_marginal_ss_capital'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_ss_capital();
            $data['conyugue_tipo_marginal_ss_renta'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_ss_renta();
            $data['conyugue_cuota_estatal_ss'] = $this->m_Dependencia_Lib->conyugue_cuota_estatal_ss();
            $data['conyugue_cuota_estatal_ss_capital'] = $this->m_Dependencia_Lib->conyugue_cuota_estatal_ss_capital();
            $data['conyugue_cuota_estatal_ss_renta'] = $this->m_Dependencia_Lib->conyugue_cuota_estatal_ss_renta();
            $data['conyugue_cuota_autonomica_ss'] = $this->m_Dependencia_Lib->conyugue_cuota_autonomica_ss();
            $data['conyugue_cuota_autonomica_ss_capital'] = $this->m_Dependencia_Lib->conyugue_cuota_autonomica_ss_capital();
            $data['conyugue_cuota_autonomica_ss_renta'] = $this->m_Dependencia_Lib->conyugue_cuota_autonomica_ss_renta();
            $data['conyugue_total_cuota_ss'] = $this->m_Dependencia_Lib->conyugue_total_cuota_ss();
            $data['conyugue_total_cuota_ss_capital'] = $this->m_Dependencia_Lib->conyugue_total_cuota_ss_capital();
            $data['conyugue_total_cuota_ss_renta'] = $this->m_Dependencia_Lib->conyugue_total_cuota_ss_renta();
            $data['conyugue_coste_fiscal_capital'] = $this->m_Dependencia_Lib->conyugue_coste_fiscal_capital();
            $data['conyugue_coste_fiscal_renta_1'] = $this->m_Dependencia_Lib->conyugue_coste_fiscal_renta_1();
            $data['conyugue_coste_fiscal_x'] = $this->m_Dependencia_Lib->conyugue_coste_fiscal_x();
        }
        
    }

    private function assign_expected_numeric( &$data, $field_data, $field_simulacion, $message) {
        if( trim($this->m_SimulacionRow[$field_simulacion]) == "" ) {
            $this->log_error($message);
            return;
        }
        $data[$field_data] = $this->m_SimulacionRow[$field_simulacion];
    }
}