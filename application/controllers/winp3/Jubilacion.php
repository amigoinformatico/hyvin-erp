<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Jubilacion extends CI_Controller {
    
    public $m_SimulacionRow;
    public $m_Bases;
    public $m_Informa_Bases_Pensiones;
    public $m_Titular_Lib;
    public $m_Jubilacion_Lib;
    public $m_Economicos_Lib;
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }

    public function report() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
  
        //        $this->output->enable_profiler(TRUE);
                
        $data[]='';
        $this->calculate($data);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        
        $data['current_page'] = 6;
        $this->load->view('templates/header_nav', $data);
        $this->load->view('pages/jubilacion', $data);
        $this->load->view('templates/footer_nav', $data);
    }
    
    public function recalc() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        $data = [];
        if( $this->save_data($data) ) {
            $this->calculate($data);
        }

        $data['winp3_from_recalc'] = 1;
        $data['current_page'] = 6;
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/jubilacion', $data);
    }

    public function dosier() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}   

        //Cargamos librería PDF:
        $this->load->library('mydompdf');         
        
        $data = [];
        $this->calculate($data);
        $this->load->model('User_model');
        $user = $this->User_model->get_user($this->session->userdata['logged_user']['id']);

        $data['contacto_correduria'] = $user['contacto_correduria'];
        $data['contacto_nombre'] = $user['contacto_nombre'];
        $data['contacto_telefono_1'] = $user['contacto_telefono_1'];
        $data['contacto_telefono_2'] = $user['contacto_telefono_2'];
        $data['contacto_direccion_1'] = $user['contacto_direccion_1'];
        $data['contacto_direccion_2'] = $user['contacto_direccion_2'];
        $data['contacto_mail'] = $user['contacto_mail'];
        $data['contacto_final_1'] = $user['contacto_final_1'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];

        //$this->load->view('pages/print/jubilacion', $data);

        //Generando pdf:
        $html = $this->load->view('pdf/pdf-resultados-jubilacion', $data, true);
        $this->mydompdf->load_html($html);
        $this->mydompdf->render();
        $this->mydompdf->set_base_path('./assets/css/dompdf.css'); //agregar de nuevo el css
        $this->mydompdf->stream("resultados-jubilacion.pdf", array(
        "Attachment" => false));
    }

    
    private function calculate(&$data) {
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params );
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('bases', $params );
        $this->m_Bases = new Bases($params);
        $this->load->library('jubilacion_lib', $params );
        $this->m_Jubilacion_Lib = new Jubilacion_Lib($params);
        $this->load->library('dependencia_lib', $params );
        $this->m_Dependencia_Lib = new Dependencia_Lib($params);

        
        $this->m_SimulacionRow = $this->simulacion_model->get_all(); 
   
        //Propios
        $data['jubilacion_titular_porcentaje_reduccion_gastos'] = 0; //$this->m_SimulacionRow['jubilacion_titular_porcentaje_reduccion_gastos'];
        $data['jubilacion_titular_anyos_cobertura'] = $this->m_SimulacionRow['jubilacion_titular_anyos_cobertura'];
        $data['jubilacion_conyugue_porcentaje_reduccion_gastos'] = 0; //$this->m_SimulacionRow['jubilacion_conyugue_porcentaje_reduccion_gastos'];
        $data['jubilacion_porcentaje_cobertura'] = $this->m_SimulacionRow['jubilacion_porcentaje_cobertura'];
        $data['jubilacion_deudas_previstas_jubilacion'] = $this->m_SimulacionRow['jubilacion_deudas_previstas_jubilacion'];
        $data['jubilacion_titular_forma_calculo_pensiones'] = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_pensiones'];
        if($data['jubilacion_titular_forma_calculo_pensiones']==null){$data['jubilacion_titular_forma_calculo_pensiones']='aportacion';}
        $data['jubilacion_titular_forma_calculo_pias'] = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_pias'];
        if($data['jubilacion_titular_forma_calculo_pias']==null){$data['jubilacion_titular_forma_calculo_pias']='aportacion';}
        $data['jubilacion_titular_forma_calculo_seguro_vida'] = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_seguro_vida'];
        if($data['jubilacion_titular_forma_calculo_seguro_vida']==null){$data['jubilacion_titular_forma_calculo_seguro_vida']='aportacion';}
        $data['jubilacion_conyugue_forma_calculo_pensiones'] = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_pensiones'];
        $data['jubilacion_conyugue_forma_calculo_pias'] = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_pias'];
        $data['jubilacion_conyugue_forma_calculo_seguro_vida'] = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_seguro_vida'];
        $data['jubilacion_titular_traspaso_plan_pensiones'] = $this->m_SimulacionRow['jubilacion_titular_traspaso_plan_pensiones'];
        $data['jubilacion_conyugue_traspaso_plan_pensiones'] = $this->m_SimulacionRow['jubilacion_conyugue_traspaso_plan_pensiones'];
        $data['jubilacion_titular_anterior_2006'] = $this->m_SimulacionRow['jubilacion_titular_anterior_2006'];
        $data['jubilacion_conyugue_anterior_2006'] = $this->m_SimulacionRow['jubilacion_conyugue_anterior_2006'];
        $data['jubilacion_titular_reparto_cobro_capital'] =$this->m_SimulacionRow['jubilacion_titular_reparto_cobro_capital'];
        $data['jubilacion_conyugue_reparto_cobro_capital'] =$this->m_SimulacionRow['jubilacion_conyugue_reparto_cobro_capital'];
        $data['jubilacion_titular_anyos_renta'] = $this->m_SimulacionRow['jubilacion_titular_anyos_renta'];
        $data['jubilacion_conyugue_anyos_renta'] = $this->m_SimulacionRow['jubilacion_conyugue_anyos_renta'];
        $data['jubilacion_titular_edad_inicio_renta'] = $this->m_SimulacionRow['jubilacion_titular_edad_inicio_renta'];
        $data['jubilacion_conyugue_edad_inicio_renta'] = $this->m_SimulacionRow['jubilacion_conyugue_edad_inicio_renta'];
        
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];

        //Check Defaults
        $data['jubilacion_titular_porcentaje_reduccion_gastos']=0;
        if( $data['jubilacion_titular_anyos_cobertura'] == null){
            $data['jubilacion_titular_anyos_cobertura'] = 25;
            $this->m_SimulacionRow['jubilacion_titular_anyos_cobertura']=$data['jubilacion_titular_anyos_cobertura'];
        }
        if( $data['jubilacion_porcentaje_cobertura']==null){
            $data['jubilacion_porcentaje_cobertura']=80;
            $this->m_SimulacionRow['jubilacion_porcentaje_cobertura']=$data['jubilacion_porcentaje_cobertura'];
        }
        if( $data['jubilacion_titular_reparto_cobro_capital']==null){
            $data['jubilacion_titular_reparto_cobro_capital']=$this->m_Jubilacion_Lib->titular_imputable_antes_2006();
            $this->m_SimulacionRow['jubilacion_titular_reparto_cobro_capital']=$data['jubilacion_titular_reparto_cobro_capital'];
        }
        if( $data['jubilacion_titular_anyos_renta']==null) {
            $data['jubilacion_titular_anyos_renta']=25;
            $this->m_SimulacionRow['jubilacion_titular_anyos_renta']=$data['jubilacion_titular_anyos_renta'];
        }
        if( $data['jubilacion_titular_edad_inicio_renta']==null) {
            $data['jubilacion_titular_edad_inicio_renta']=70;
            $this->m_SimulacionRow['jubilacion_titular_edad_inicio_renta']=$data['jubilacion_titular_edad_inicio_renta'];
        }
          
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['jubilacion_conyugue_porcentaje_reduccion_gastos']=0;
            if( $data['jubilacion_conyugue_reparto_cobro_capital']==null){
                $data['jubilacion_conyugue_reparto_cobro_capital']=$this->m_Jubilacion_Lib->conyugue_imputable_antes_2006();
                $this->m_SimulacionRow['jubilacion_conyugue_reparto_cobro_capital']=$data['jubilacion_conyugue_reparto_cobro_capital'];
            }
            if( $data['jubilacion_conyugue_anyos_renta']==null) {
                $data['jubilacion_conyugue_anyos_renta']=25;
                $this->m_SimulacionRow['jubilacion_conyugue_anyos_renta']=$data['jubilacion_conyugue_anyos_renta'];
            }
            if( $data['jubilacion_conyugue_edad_inicio_renta']==null) {
                $data['jubilacion_conyugue_edad_inicio_renta']=70;
                $this->m_SimulacionRow['jubilacion_conyugue_edad_inicio_renta']=$data['jubilacion_conyugue_edad_inicio_renta'];
            }            
        }

        //Calculados

        $data['titular_fecha_nacimiento'] = $this->m_Jubilacion_Lib->titular_fecha_nacimiento();
        $data['titular_fecha_jubilacion'] = $this->m_Pensiones->titular_fecha_jubilacion();
        $data['titular_edad_jubilacion'] = $this->m_Pensiones->titular_edad_jubilacion();
        $data['titular_anyos_hasta_jubilacion'] = $this->m_Jubilacion_Lib->titular_anyos_hasta_jubilacion();
        $data['titular_gastos_mensuales_familiares_hoy'] = $this->m_Economicos_Lib->gastos_mensuales_media_familia();
        $data['titular_gastos_reducidos_hoy'] = $data['titular_gastos_mensuales_familiares_hoy']-$data['titular_gastos_mensuales_familiares_hoy']*$data['jubilacion_titular_porcentaje_reduccion_gastos']/100;
        $data['titular_ipc_estimado'] = $this->m_Constantes->ipc();
        $data['titular_gastos_reducidos_actualizado'] = $this->m_Jubilacion_Lib->titular_gastos_reducidos_actualizado();
        $data['titular_pension_jubilacion_propia_actualizado'] = $this->m_Jubilacion_Lib->titular_pension_jubilacion_propia_actualizado();
        //

        $data['titular_saldo_diferencial_mensual_cubrir'] = $this->m_Jubilacion_Lib->titular_saldo_diferencial_mensual_cubrir();

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['titular_ingresos_conyugue_actualizado'] = $this->m_Jubilacion_Lib->titular_ingresos_conyugue_actualizado();
            $data['conyugue_fecha_nacimiento'] = $this->m_Jubilacion_Lib->conyugue_fecha_nacimiento();
            $data['conyugue_fecha_jubilacion'] = $this->m_Pensiones->conyugue_fecha_jubilacion();
            $data['conyugue_edad_jubilacion'] = $this->m_Pensiones->conyugue_edad_jubilacion();
            $data['conyugue_anyos_hasta_jubilacion'] = $this->m_Jubilacion_Lib->conyugue_anyos_hasta_jubilacion();
            $data['conyugue_gastos_mensuales_familiares_hoy'] = $this->m_Economicos_Lib->gastos_mensuales_media_familia();
            $data['conyugue_gastos_reducidos_hoy'] = $data['conyugue_gastos_mensuales_familiares_hoy']-$data['conyugue_gastos_mensuales_familiares_hoy']*$data['jubilacion_conyugue_porcentaje_reduccion_gastos']/100;
            $data['conyugue_ipc_estimado'] = $this->m_Constantes->ipc();
            $data['conyugue_gastos_reducidos_actualizado'] = $this->m_Jubilacion_Lib->conyugue_gastos_reducidos_actualizado();
            $data['conyugue_pension_jubilacion_propia_actualizado'] = $this->m_Jubilacion_Lib->conyugue_pension_jubilacion_propia_actualizado();
            $data['conyugue_ingresos_conyugue_actualizado'] = $this->m_Jubilacion_Lib->conyugue_ingresos_conyugue_actualizado();
            $data['conyugue_saldo_diferencial_mensual_cubrir'] = $this->m_Jubilacion_Lib->conyugue_saldo_diferencial_mensual_cubrir();
            $data['anyos_jubilacion_entre_conyugues'] = $this->m_Jubilacion_Lib->anyos_jubilacion_entre_conyugues();
              
             
         
            $conyugue_forma_calculo = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_pensiones'];
            if($conyugue_forma_calculo == 'capital'){
                $data['jubilacion_conyugue_capital_pensiones'] = $this->m_SimulacionRow['jubilacion_conyugue_capital_pensiones'];
                $data['jubilacion_conyugue_aportacion_pensiones'] = $this->m_Jubilacion_Lib->conyugue_si_capital_aportacion_pensiones();
                $this->m_SimulacionRow['jubilacion_conyugue_aportacion_pensiones'] = $data['jubilacion_conyugue_aportacion_pensiones']; //Para facilitar calculos
            }
            else if($conyugue_forma_calculo == 'aportacion'){
                $data['jubilacion_conyugue_aportacion_pensiones'] = $this->m_SimulacionRow['jubilacion_conyugue_aportacion_pensiones'];
                $data['jubilacion_conyugue_capital_pensiones'] = $this->m_Jubilacion_Lib->conyugue_capital_si_aportacion_pensiones();
                $this->m_SimulacionRow['jubilacion_conyugue_capital_pensiones'] = $data['jubilacion_conyugue_capital_pensiones']; //Para facilitar calculos
            }
            else {
                $data['jubilacion_conyugue_capital_pensiones'] = "";
                $data['jubilacion_conyugue_aportacion_pensiones'] = "";
            }
            $conyugue_forma_calculo = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_pias'];
            if($conyugue_forma_calculo == 'capital'){
                $data['jubilacion_conyugue_capital_pias'] = $this->m_SimulacionRow['jubilacion_conyugue_capital_pias'];
                $data['jubilacion_conyugue_aportacion_pias'] = $this->m_Jubilacion_Lib->conyugue_si_capital_aportacion_pias();
                $this->m_SimulacionRow['jubilacion_conyugue_aportacion_pias'] = $data['jubilacion_conyugue_aportacion_pias']; //Para facilitar calculos
            }
            else if($conyugue_forma_calculo == 'aportacion'){
                $data['jubilacion_conyugue_aportacion_pias'] = $this->m_SimulacionRow['jubilacion_conyugue_aportacion_pias'];
                $data['jubilacion_conyugue_capital_pias'] = $this->m_Jubilacion_Lib->conyugue_capital_si_aportacion_pias();
                $this->m_SimulacionRow['jubilacion_conyugue_capital_pias'] = $data['jubilacion_conyugue_capital_pias']; //Para facilitar calculos
            }
            else {
                $data['jubilacion_conyugue_capital_pias'] = "";
                $data['jubilacion_conyugue_aportacion_pias'] = "";
            }
            $conyugue_forma_calculo = $this->m_SimulacionRow['jubilacion_conyugue_forma_calculo_seguro_vida'];
            if($conyugue_forma_calculo == 'capital'){
                $data['jubilacion_conyugue_capital_seguro_vida'] = $this->m_SimulacionRow['jubilacion_conyugue_capital_seguro_vida'];
                $data['jubilacion_conyugue_aportacion_seguro_vida'] = $this->m_Jubilacion_Lib->conyugue_si_capital_aportacion_seguro_vida();
                $this->m_SimulacionRow['jubilacion_conyugue_aportacion_seguro_vida'] = $data['jubilacion_conyugue_aportacion_seguro_vida']; //Para facilitar calculos
            }
            else if($conyugue_forma_calculo == 'aportacion'){
                $data['jubilacion_conyugue_aportacion_seguro_vida'] = $this->m_SimulacionRow['jubilacion_conyugue_aportacion_seguro_vida'];
                $data['jubilacion_conyugue_capital_seguro_vida'] = $this->m_Jubilacion_Lib->conyugue_capital_si_aportacion_seguro_vida();
                $this->m_SimulacionRow['jubilacion_conyugue_capital_seguro_vida'] = $data['jubilacion_conyugue_capital_seguro_vida']; //Para facilitar calculos
            }
            else {
                $data['jubilacion_conyugue_capital_seguro_vida'] = "";
                $data['jubilacion_conyugue_aportacion_seguro_vida'] = "";
            }
            
        }
        
        $titular_forma_calculo = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_pensiones'];
        if($titular_forma_calculo == 'capital'){
            $data['jubilacion_titular_capital_pensiones'] = $this->m_SimulacionRow['jubilacion_titular_capital_pensiones'];
            $data['jubilacion_titular_aportacion_pensiones'] = $this->m_Jubilacion_Lib->titular_si_capital_aportacion_pensiones();
            $this->m_SimulacionRow['jubilacion_titular_aportacion_pensiones'] = $data['jubilacion_titular_aportacion_pensiones']; //Para facilitar calculos
        }
        else if($titular_forma_calculo == 'aportacion'){
            $data['jubilacion_titular_aportacion_pensiones'] = $this->m_SimulacionRow['jubilacion_titular_aportacion_pensiones'];
            $data['jubilacion_titular_capital_pensiones'] = $this->m_Jubilacion_Lib->titular_capital_si_aportacion_pensiones();
            $this->m_SimulacionRow['jubilacion_titular_capital_pensiones'] = $data['jubilacion_titular_capital_pensiones']; //Para facilitar calculos
        }
        else {
            $data['jubilacion_titular_capital_pensiones'] = "";
            $data['jubilacion_titular_aportacion_pensiones'] = "";
        }
        $titular_forma_calculo = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_pias'];
        if($titular_forma_calculo == 'capital'){
            $data['jubilacion_titular_capital_pias'] = $this->m_SimulacionRow['jubilacion_titular_capital_pias'];
            $data['jubilacion_titular_aportacion_pias'] = $this->m_Jubilacion_Lib->titular_si_capital_aportacion_pias();
            $this->m_SimulacionRow['jubilacion_titular_aportacion_pias'] = $data['jubilacion_titular_aportacion_pias']; //Para facilitar calculos
        }
        else if($titular_forma_calculo == 'aportacion'){
            $data['jubilacion_titular_aportacion_pias'] = $this->m_SimulacionRow['jubilacion_titular_aportacion_pias'];
            $data['jubilacion_titular_capital_pias'] = $this->m_Jubilacion_Lib->titular_capital_si_aportacion_pias();
            $this->m_SimulacionRow['jubilacion_titular_capital_pias'] = $data['jubilacion_titular_capital_pias']; //Para facilitar calculos
        }
        else {
            $data['jubilacion_titular_capital_pias'] = "";
            $data['jubilacion_titular_aportacion_pias'] = "";
        }
        $titular_forma_calculo = $this->m_SimulacionRow['jubilacion_titular_forma_calculo_seguro_vida'];
        if($titular_forma_calculo == 'capital'){
            $data['jubilacion_titular_capital_seguro_vida'] = $this->m_SimulacionRow['jubilacion_titular_capital_seguro_vida'];
            $data['jubilacion_titular_aportacion_seguro_vida'] = $this->m_Jubilacion_Lib->titular_si_capital_aportacion_seguro_vida();
            $this->m_SimulacionRow['jubilacion_titular_aportacion_seguro_vida'] = $data['jubilacion_titular_aportacion_seguro_vida']; //Para facilitar calculos
        }
        else if($titular_forma_calculo == 'aportacion'){
            $data['jubilacion_titular_aportacion_seguro_vida'] = $this->m_SimulacionRow['jubilacion_titular_aportacion_seguro_vida'];
            $data['jubilacion_titular_capital_seguro_vida'] = $this->m_Jubilacion_Lib->titular_capital_si_aportacion_seguro_vida();
            $this->m_SimulacionRow['jubilacion_titular_capital_seguro_vida'] = $data['jubilacion_titular_capital_seguro_vida']; //Para facilitar calculos
        }
        else {
            $data['jubilacion_titular_capital_seguro_vida'] = "";
            $data['jubilacion_titular_aportacion_seguro_vida'] = "";
        }

        $data['diferencial_generado_periodo_1'] = $this->m_Jubilacion_Lib->diferencial_generado_periodo_1();
        $data['resto_periodo_cobertura'] = $this->m_Jubilacion_Lib->resto_periodo_cobertura();
        $data['diferencial_generado_periodo_2'] =$this->m_Jubilacion_Lib->diferencial_generado_periodo_2();
        $data['diferencial_generado_periodo_1_mas_2'] = $this->m_Jubilacion_Lib->diferencial_generado_periodo_1_mas_2();
        $data['diferencial_cubrir'] = $this->m_Jubilacion_Lib->diferencial_cubrir();
        $data['titular_capital_necesario_jubilacion'] = -$this->m_Jubilacion_Lib->diferencial_cubrir();

        $data['deudas_familiares_hoy'] = -($this->m_Economicos_Lib->deudas_pendientes_familiares_hoy());
 
        $data['capital_necesario_jubilacion'] = $this->m_Jubilacion_Lib->capital_necesario_jubilacion();
        $data['ahorro_inversion_realizado'] = $this->m_Economicos_Lib->suma_capitales_disponibles_ahorro_jubilacion_total();
        $data['descobertura'] = $this->m_Jubilacion_Lib->descobertura();
        $data['suma_aportaciones_familiares'] = $this->m_Jubilacion_Lib->suma_aportaciones_familiares();
        $data['suma_capitales_obtenidos'] = $this->m_Jubilacion_Lib->suma_capitales_obtenidos();

        $data['titular_importe_reducir_base_liquidadora'] = $this->m_Jubilacion_Lib->titular_importe_reducir_base_liquidadora();
        $data['titular_tipo_marginal_hoy'] = $this->m_Jubilacion_Lib->titular_tipo_marginal_hoy();
        $data['titular_beneficio_fiscal_anual'] = $this->m_Jubilacion_Lib->titular_beneficio_fiscal_anual();
        $data['titular_beneficio_fiscal_acumulado'] = $this->m_Jubilacion_Lib->titular_beneficio_fiscal_acumulado();
        
        $data['titular_traspaso'] = $data['jubilacion_titular_traspaso_plan_pensiones'];
        $data['titular_aportaciones'] = $this->m_Jubilacion_Lib->titular_aportaciones();
        $data['titular_capital_final'] = $this->m_SimulacionRow['jubilacion_titular_capital_pensiones'];
        $data['titular_imputable_antes_2006'] = $this->m_Jubilacion_Lib->titular_imputable_antes_2006();

        $data['titular_reparto_cobro_renta'] = $this->m_Jubilacion_Lib->titular_reparto_cobro_capital_renta();
        $data['titular_prestaciones_anteriores_2006'] = $this->m_Jubilacion_Lib->titular_prestaciones_anteriores_2006();
        $data['titular_resto_percibir'] = $this->m_Jubilacion_Lib->titular_resto_percibir();
        $data['titular_incremento_bl'] = $this->m_Jubilacion_Lib->titular_incremento_bl();
        $data['titular_renta_anual'] = $this->m_Jubilacion_Lib->titular_renta_anual_obtenida();
        $data['titular_renta_mensual'] = $this->m_Jubilacion_Lib->titular_renta_mensual_obtenida();
        $data['titular_base_ss'] = $this->m_Jubilacion_Lib->titular_base_ss();
        $data['titular_base_ss_capital'] = $this->m_Jubilacion_Lib->titular_base_ss_capital();
        $data['titular_base_ss_renta'] = $this->m_Jubilacion_Lib->titular_base_ss_renta();
        $data['titular_tipo_marginal_ss'] = $this->m_Jubilacion_Lib->titular_tipo_marginal_ss();
        $data['titular_tipo_marginal_ss_capital'] = $this->m_Jubilacion_Lib->titular_tipo_marginal_ss_capital();
        $data['titular_tipo_marginal_ss_renta'] = $this->m_Jubilacion_Lib->titular_tipo_marginal_ss_renta();
        $data['titular_cuota_estatal_ss'] = $this->m_Jubilacion_Lib->titular_cuota_estatal_ss();
        $data['titular_cuota_estatal_ss_capital'] = $this->m_Jubilacion_Lib->titular_cuota_estatal_ss_capital();
        $data['titular_cuota_estatal_ss_renta'] = $this->m_Jubilacion_Lib->titular_cuota_estatal_ss_renta();
        $data['titular_cuota_autonomica_ss'] = $this->m_Jubilacion_Lib->titular_cuota_autonomica_ss();
        $data['titular_cuota_autonomica_ss_capital'] = $this->m_Jubilacion_Lib->titular_cuota_autonomica_ss_capital();
        $data['titular_cuota_autonomica_ss_renta'] = $this->m_Jubilacion_Lib->titular_cuota_autonomica_ss_renta();
        $data['titular_total_cuota_ss'] = $this->m_Jubilacion_Lib->titular_total_cuota_ss();
        $data['titular_total_cuota_ss_capital'] = $this->m_Jubilacion_Lib->titular_total_cuota_ss_capital();
        $data['titular_total_cuota_ss_renta'] = $this->m_Jubilacion_Lib->titular_total_cuota_ss_renta();

        $data['titular_coste_fiscal_capital'] = $this->m_Jubilacion_Lib->titular_coste_fiscal_capital();
        $data['titular_coste_fiscal_renta_1'] = $this->m_Jubilacion_Lib->titular_coste_fiscal_renta_1();
        $data['titular_coste_fiscal_x'] = $this->m_Jubilacion_Lib->titular_coste_fiscal_x();
        $data['titular_coste_fiscal_total'] = $this->m_Jubilacion_Lib->titular_coste_fiscal_total();
        $data['titular_capital_final_pias'] = $this->m_Jubilacion_Lib->titular_capital_final_pias();
        $data['titular_porcentaje_exencion'] = $this->m_Jubilacion_Lib->titular_porcentaje_exencion();
        $data['titular_pias_renta_anual_obtenida'] = $this->m_Jubilacion_Lib->titular_pias_renta_anual_obtenida();
        $data['titular_pias_renta_anual_exenta'] = $this->m_Jubilacion_Lib->titular_pias_renta_anual_exenta();
        $data['titular_pias_renta_anual_tributable'] = $this->m_Jubilacion_Lib->titular_pias_renta_anual_tributable();
        $data['titular_pias_total_cuota_tributaria_anual'] = $this->m_Jubilacion_Lib->titular_pias_total_cuota_tributaria_anual();
        $data['titular_pias_renta_anual_neta'] = $this->m_Jubilacion_Lib->titular_pias_renta_anual_neta();
        $data['titular_pias_renta_mensual_obtenida'] = $this->m_Jubilacion_Lib->titular_pias_renta_mensual_obtenida();
        $data['titular_pias_total_cuota_tributaria_mensual'] = $this->m_Jubilacion_Lib->titular_pias_total_cuota_tributaria_mensual();
        $data['titular_pias_renta_mensual_neta'] = $this->m_Jubilacion_Lib->titular_pias_renta_mensual_neta();
        $data['titular_pias_coste_fiscal_operacion'] = $this->m_Jubilacion_Lib->titular_pias_coste_fiscal_operacion();
        $data['titular_capital_final_sv'] = $this->m_Jubilacion_Lib->titular_capital_final_sv();
        $data['titular_aportaciones_sv'] = $this->m_Jubilacion_Lib->titular_aportaciones_sv();
        $data['titular_rendimiento_obtenido_sv'] = $this->m_Jubilacion_Lib->titular_rendimiento_obtenido_sv();
        $data['titular_total_cuota_operacion'] = $this->m_Jubilacion_Lib->titular_total_cuota_operacion();

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            
            $data['conyugue_importe_reducir_base_liquidadora'] = $this->m_Jubilacion_Lib->conyugue_importe_reducir_base_liquidadora();

            $data['conyugue_tipo_marginal_hoy'] = $this->m_Jubilacion_Lib->conyugue_tipo_marginal_hoy();
            $data['conyugue_beneficio_fiscal_anual'] = $this->m_Jubilacion_Lib->conyugue_beneficio_fiscal_anual();
            $data['conyugue_beneficio_fiscal_acumulado'] = $this->m_Jubilacion_Lib->conyugue_beneficio_fiscal_acumulado();

            $data['conyugue_traspaso'] = $data['jubilacion_conyugue_traspaso_plan_pensiones'];
            $data['conyugue_aportaciones'] = $this->m_Jubilacion_Lib->conyugue_aportaciones();
            $data['conyugue_capital_final'] = $this->m_SimulacionRow['jubilacion_conyugue_capital_pensiones'];
            $data['conyugue_imputable_antes_2006'] = $this->m_Jubilacion_Lib->conyugue_imputable_antes_2006();
            
            $data['conyugue_reparto_cobro_renta'] = $this->m_Jubilacion_Lib->conyugue_reparto_cobro_capital_renta();
            $data['conyugue_prestaciones_anteriores_2006'] = $this->m_Jubilacion_Lib->conyugue_prestaciones_anteriores_2006();
            $data['conyugue_resto_percibir'] = $this->m_Jubilacion_Lib->conyugue_resto_percibir();
            $data['conyugue_incremento_bl'] = $this->m_Jubilacion_Lib->conyugue_incremento_bl();

            $data['conyugue_renta_anual'] = $this->m_Jubilacion_Lib->conyugue_renta_anual_obtenida();
            $data['conyugue_renta_mensual'] = $this->m_Jubilacion_Lib->conyugue_renta_mensual_obtenida();
            $data['conyugue_base_ss'] = $this->m_Jubilacion_Lib->conyugue_base_ss();
            $data['conyugue_base_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_base_ss_capital();
            $data['conyugue_base_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_base_ss_renta();
            $data['conyugue_tipo_marginal_ss'] = $this->m_Jubilacion_Lib->conyugue_tipo_marginal_ss();
            $data['conyugue_tipo_marginal_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_tipo_marginal_ss_capital();
            $data['conyugue_tipo_marginal_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_tipo_marginal_ss_renta();
            $data['conyugue_cuota_estatal_ss'] = $this->m_Jubilacion_Lib->conyugue_cuota_estatal_ss();
            $data['conyugue_cuota_estatal_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_cuota_estatal_ss_capital();
            $data['conyugue_cuota_estatal_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_cuota_estatal_ss_renta();
            $data['conyugue_cuota_autonomica_ss'] = $this->m_Jubilacion_Lib->conyugue_cuota_autonomica_ss();
            $data['conyugue_cuota_autonomica_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_cuota_autonomica_ss_capital();
            $data['conyugue_cuota_autonomica_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_cuota_autonomica_ss_renta();
            $data['conyugue_total_cuota_ss'] = $this->m_Jubilacion_Lib->conyugue_total_cuota_ss();
            $data['conyugue_total_cuota_ss_capital'] = $this->m_Jubilacion_Lib->conyugue_total_cuota_ss_capital();
            $data['conyugue_total_cuota_ss_renta'] = $this->m_Jubilacion_Lib->conyugue_total_cuota_ss_renta();
            $data['conyugue_coste_fiscal_capital'] = $this->m_Jubilacion_Lib->conyugue_coste_fiscal_capital();
            $data['conyugue_coste_fiscal_renta_1'] = $this->m_Jubilacion_Lib->conyugue_coste_fiscal_renta_1();
            $data['conyugue_coste_fiscal_x'] = $this->m_Jubilacion_Lib->conyugue_coste_fiscal_x();
            $data['conyugue_coste_fiscal_total'] = $this->m_Jubilacion_Lib->conyugue_coste_fiscal_total();
            $data['conyugue_capital_final_pias'] = $this->m_Jubilacion_Lib->conyugue_capital_final_pias();
            $data['conyugue_porcentaje_exencion'] = $this->m_Jubilacion_Lib->conyugue_porcentaje_exencion();
            $data['conyugue_pias_renta_anual_obtenida'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_anual_obtenida();
            $data['conyugue_pias_renta_anual_exenta'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_anual_exenta();
            $data['conyugue_pias_renta_anual_tributable'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_anual_tributable();
            $data['conyugue_pias_total_cuota_tributaria_anual'] = $this->m_Jubilacion_Lib->conyugue_pias_total_cuota_tributaria_anual();
            $data['conyugue_pias_renta_anual_neta'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_anual_neta();
            $data['conyugue_pias_renta_mensual_obtenida'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_mensual_obtenida();
            $data['conyugue_pias_total_cuota_tributaria_mensual'] = $this->m_Jubilacion_Lib->conyugue_pias_total_cuota_tributaria_mensual();
            $data['conyugue_pias_renta_mensual_neta'] = $this->m_Jubilacion_Lib->conyugue_pias_renta_mensual_neta();
            $data['conyugue_pias_coste_fiscal_operacion'] = $this->m_Jubilacion_Lib->conyugue_pias_coste_fiscal_operacion();
            $data['conyugue_capital_final_sv'] = $this->m_Jubilacion_Lib->conyugue_capital_final_sv();
            $data['conyugue_aportaciones_sv'] = $this->m_Jubilacion_Lib->conyugue_aportaciones_sv();
            $data['conyugue_rendimiento_obtenido_sv'] = $this->m_Jubilacion_Lib->conyugue_rendimiento_obtenido_sv();
            $data['conyugue_total_cuota_operacion'] = $this->m_Jubilacion_Lib->conyugue_total_cuota_operacion();


        }

         
    }

    public function save() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        // Librerias
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $data=[];
        if( !$this->save_data($data) ) {
            $this->load->view('templates/simulator_header');
            $data['missing_fields'] = $this->config->item('winp3_missing_field');
            $this->load->view('pages/jubilacion', $data);
            $this->load->view('templates/footer');
            return;
        }
             
        redirect($this->input->post('page') );
    }

    
    private function save_data(&$data) {
        $data['jubilacion_titular_porcentaje_reduccion_gastos']=0;
        $data['jubilacion_conyugue_porcentaje_reduccion_gastos']=0;
        $this->assign_field('jubilacion_titular_anyos_cobertura', $data );
        $this->assign_field('jubilacion_porcentaje_cobertura', $data );
        if( $this->input->post('jubilacion_deudas_previstas_jubilacion') != null ) {
            $deudas_tmp = str_replace('.', '', $this->input->post('jubilacion_deudas_previstas_jubilacion'));
            $deudas_tmp = str_replace(',', '.', $deudas_tmp);
            if( $deudas_tmp > 0){$data['jubilacion_deudas_previstas_jubilacion'] = -$deudas_tmp;}
            else {$data['jubilacion_deudas_previstas_jubilacion'] = $deudas_tmp;}
        }
        else {$data['jubilacion_deudas_previstas_jubilacion'] = null; }
        
        $this->assign_field('jubilacion_titular_forma_calculo_pensiones', $data );
        $this->assign_field('jubilacion_titular_forma_calculo_pias', $data );
        $this->assign_field('jubilacion_titular_forma_calculo_seguro_vida', $data );
        $this->assign_field('jubilacion_conyugue_forma_calculo_pensiones', $data );
        $this->assign_field('jubilacion_conyugue_forma_calculo_pias', $data );
        $this->assign_field('jubilacion_conyugue_forma_calculo_seguro_vida', $data );
        
        $this->assign_field('jubilacion_titular_aportacion_pensiones', $data );
        $this->assign_field('jubilacion_titular_aportacion_pias', $data );
        $this->assign_field('jubilacion_titular_aportacion_seguro_vida', $data );
        $this->assign_field('jubilacion_conyugue_aportacion_pensiones', $data );
        $this->assign_field('jubilacion_conyugue_aportacion_pias', $data );
        $this->assign_field('jubilacion_conyugue_aportacion_seguro_vida', $data );
        $this->assign_field('jubilacion_titular_capital_pensiones', $data );
        $this->assign_field('jubilacion_titular_capital_pias', $data );
        $this->assign_field('jubilacion_titular_capital_seguro_vida', $data );
        $this->assign_field('jubilacion_conyugue_capital_pensiones', $data );
        $this->assign_field('jubilacion_conyugue_capital_pias', $data );
        $this->assign_field('jubilacion_conyugue_capital_seguro_vida', $data );
        $this->assign_field('jubilacion_conyugue_aportacion_pensiones', $data );
        $this->assign_field('jubilacion_conyugue_aportacion_pias', $data );
        $this->assign_field('jubilacion_conyugue_aportacion_seguro_vida', $data );
        $this->assign_field('jubilacion_titular_traspaso_plan_pensiones', $data );
        if( $data['jubilacion_titular_traspaso_plan_pensiones'] == ""){
            $data['jubilacion_titular_traspaso_plan_pensiones'] = 0;
        }
        
        $this->assign_field('jubilacion_conyugue_traspaso_plan_pensiones', $data );
        if( $data['jubilacion_conyugue_traspaso_plan_pensiones']==""){
            $data['jubilacion_conyugue_traspaso_plan_pensiones'] = 0;
        }
        $this->assign_field('jubilacion_titular_anterior_2006', $data);
        $this->assign_field('jubilacion_conyugue_anterior_2006', $data);
        $this->assign_field('jubilacion_titular_reparto_cobro_capital', $data );
        $this->assign_field('jubilacion_conyugue_reparto_cobro_capital', $data );
        $this->assign_field('jubilacion_titular_anyos_renta', $data);
        $this->assign_field('jubilacion_conyugue_anyos_renta', $data);
        $this->assign_field('jubilacion_titular_edad_inicio_renta', $data );
        $this->assign_field('jubilacion_conyugue_edad_inicio_renta', $data );

        //$data['jubilacion_titular_anterior_2006'] = '';
        //$data['jubilacion_titular_aportacion_pensiones'] = '';
        //$data['jubilacion_titular_capital_pensiones'] = '';

        if( (!is_numeric($data['jubilacion_titular_aportacion_pias']) || 
                $data['jubilacion_titular_aportacion_pias'] == 0 ) &&
            (!is_numeric($data['jubilacion_titular_capital_pias']) || 
                $data['jubilacion_titular_capital_pias'] == 0 )) {
            
            $data['jubilacion_titular_aportacion_pias'] = '';
            $data['jubilacion_titular_capital_pias'] = '';
        }
        
        if( (!is_numeric($data['jubilacion_titular_aportacion_seguro_vida']) || 
                $data['jubilacion_titular_aportacion_seguro_vida'] == 0 ) &&
            (!is_numeric($data['jubilacion_titular_capital_seguro_vida']) || 
                $data['jubilacion_titular_capital_seguro_vida'] == 0 )) {
            
            $data['jubilacion_titular_aportacion_seguro_vida'] = '';
            $data['jubilacion_titular_capital_seguro_vida'] = '';
        }
/*
        if( $this->input->post('jubilacion_conyugue_traspaso_plan_pensiones') == ""){
            $data['jubilacion_conyugue_anterior_2006'] = '';
            $data['jubilacion_conyugue_aportacion_pensiones'] = '';
            $data['jubilacion_conyugue_capital_pensiones'] = '';
        }
*/
        if( (!is_numeric($data['jubilacion_conyugue_aportacion_pias']) || 
                $data['jubilacion_conyugue_aportacion_pias'] == 0 ) &&
            (!is_numeric($data['jubilacion_conyugue_capital_pias']) || 
                $data['jubilacion_conyugue_capital_pias'] == 0 )) {
            
            $data['jubilacion_conyugue_aportacion_pias'] = '';
            $data['jubilacion_conyugue_capital_pias'] = '';
        }
        
        if( (!is_numeric($data['jubilacion_conyugue_aportacion_seguro_vida']) || 
                $data['jubilacion_conyugue_aportacion_seguro_vida'] == 0 ) &&
            (!is_numeric($data['jubilacion_conyugue_capital_seguro_vida']) || 
                $data['jubilacion_conyugue_capital_seguro_vida'] == 0 )) {
            
            $data['jubilacion_conyugue_aportacion_seguro_vida'] = '';
            $data['jubilacion_conyugue_capital_seguro_vida'] = '';
        }
        
        if( !$this->simulacion_model->save($data)) {
            $this->log_error("Error guardando datos");
            return false;
        }
        
        return true;
    }
    
    private function assign_field( $field_name, &$data ) {
        if( $this->input->post($field_name) != null ) {
            $data[$field_name] = str_replace(".", "", $this->input->post($field_name)); 
            $data[$field_name] = str_replace(",", ".", $data[$field_name]); 
        }
        else {
            $data[$field_name] = null; 
        }
    }
    

}