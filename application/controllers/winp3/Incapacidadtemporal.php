<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Incapacidadtemporal extends CI_Controller {

    public $m_Fiscalidad;
    public $m_Economicos_Lib;
    public $m_Pensiones;
    public $m_SimulacionRow;
    public $m_Constantes;
    public $m_Informa_Bases_Pensiones;
    public $m_Titular_Lib;
    public $m_It_Lib;

    private function log_error($message){
        if(strpos($this->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field').$message." | ");
        }
    }
    
    public function report() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        $data[]='';
        $this->calculate($data);
        if( !isset($data['missing_fields'])) {$data['missing_fields'] = $this->config->item('winp3_missing_field');}
        $data['current_page'] = 3;
        $this->load->view('templates/header_nav', $data);
        $this->load->view('pages/incapacidad_temporal', $data);
        $this->load->view('templates/footer_nav');
    }
    
    public function dosier() {
        if(!isset($this->session->userdata['logged_user'])){
            redirect('welcome/index/');
        }

        //Cargamos librería PDF:
        $this->load->library('mydompdf');            
        
        $data = [];
        $this->calculate($data);
        $this->load->model('User_model');
        $user = $this->User_model->get_user($this->session->userdata['logged_user']['id']);

        $data['contacto_correduria'] = $user['contacto_correduria'];
        $data['contacto_nombre'] = $user['contacto_nombre'];
        $data['contacto_telefono_1'] = $user['contacto_telefono_1'];
        $data['contacto_telefono_2'] = $user['contacto_telefono_2'];
        $data['contacto_direccion_1'] = $user['contacto_direccion_1'];
        $data['contacto_direccion_2'] = $user['contacto_direccion_2'];
        $data['contacto_mail'] = $user['contacto_mail'];
        $data['contacto_final_1'] = $user['contacto_final_1'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];
        
        //$this->load->view('pages/print/incapacidadtemporal', $data);

        //Generando pdf:
        $html = $this->load->view('pdf/pdf-incapacidad-temporal', $data, true);
        $this->mydompdf->load_html($html);
        $this->mydompdf->render();
        $this->mydompdf->set_base_path('./assets/css/dompdf.css'); //agregar de nuevo el css
        $this->mydompdf->stream("incapacidad-temporal.pdf", array(
        "Attachment" => false));
        
        //$this->load->library('pdf_it', $data);
        //echo $this->pdf_it->print_document();
    }
    
    public function recalc() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}            
        
        $data = [];
        if( $this->save_data($data) ) {
            $this->calculate($data);
        }

        $data['winp3_from_recalc'] = 1;
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/incapacidad_temporal', $data);
    }

    public function save() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        // Librerias
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $data=[];
        if( !$this->save_data($data) ) {
            $this->load->view('templates/simulator_header');
            if( !isset($data['missing_fields'])) {$data['missing_fields'] = $this->config->item('winp3_missing_field');}
            $this->load->view('pages/incapacidad_temporal', $data);
            $this->load->view('templates/footer');
            return;
        }
             
        redirect($this->input->post('page') );
    }

    
    private function save_data(&$data) {
        $this->assign_field('titular_capital_propuesto_dia_baja', $data );
        $this->assign_field('titular_franquicia_dias', $data );
        $this->assign_field('titular_duracion_maxima_dias', $data );
        $this->assign_field('it_titular_prima_anual', $data );
        $this->assign_field('titular_dias_indemnizacion_percibida', $data );   
        $this->assign_field('it_titular_porcentaje_cobertura', $data );

        $this->assign_field('conyugue_capital_propuesto_dia_baja', $data );
        $this->assign_field('conyugue_franquicia_dias', $data );
        $this->assign_field('conyugue_duracion_maxima_dias', $data );
        $this->assign_field('it_conyugue_prima_anual', $data );
        $this->assign_field('conyugue_dias_indemnizacion_percibida', $data );   
        $this->assign_field('it_conyugue_porcentaje_cobertura', $data );

        if( !$this->simulacion_model->save($data)) {
            $this->log_error("Error guardando datos");
            return false;
        }
        
        return true;
    }

    private function assign_field( $field_name, &$data ) {
        if( $this->input->post($field_name) != null ) {
            $data[$field_name] = str_replace(".", "", $this->input->post($field_name)); 
            $data[$field_name] = str_replace(",", ".", $data[$field_name]); 
        }
        else {
            $data[$field_name] = null; 
        }
    }

    private function calculate(&$data) {
        
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params);
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('it_lib', $params);
        $this->m_It_Lib = new It_Lib($params);
        $this->m_SimulacionRow = $this->simulacion_model->get_all(); 
        
        // Propios
        $data['it_titular_porcentaje_cobertura'] = $this->m_SimulacionRow['it_titular_porcentaje_cobertura'];
        $data['titular_capital_propuesto_dia_baja'] = $this->m_SimulacionRow['titular_capital_propuesto_dia_baja'];
        $data['titular_franquicia_dias'] = $this->m_SimulacionRow['titular_franquicia_dias'];
        $data['titular_duracion_maxima_dias'] = $this->m_SimulacionRow['titular_duracion_maxima_dias'];
        $data['it_titular_prima_anual'] = $this->m_SimulacionRow['it_titular_prima_anual'];
        $data['titular_dias_indemnizacion_percibida'] = $this->m_SimulacionRow['titular_dias_indemnizacion_percibida'];
        
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['it_conyugue_porcentaje_cobertura'] = $this->m_SimulacionRow['it_conyugue_porcentaje_cobertura'];
            $data['conyugue_capital_propuesto_dia_baja'] = $this->m_SimulacionRow['conyugue_capital_propuesto_dia_baja'];
            $data['conyugue_franquicia_dias'] = $this->m_SimulacionRow['conyugue_franquicia_dias'];
            $data['conyugue_duracion_maxima_dias'] = $this->m_SimulacionRow['conyugue_duracion_maxima_dias'];
            $data['it_conyugue_prima_anual'] = $this->m_SimulacionRow['it_conyugue_prima_anual'];
            $data['conyugue_dias_indemnizacion_percibida'] = $this->m_SimulacionRow['conyugue_dias_indemnizacion_percibida'];
        }
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];
        
        //Check Defaults
        if( $data['it_titular_porcentaje_cobertura'] == null ){
            $data['it_titular_porcentaje_cobertura']=80;
            $this->m_SimulacionRow['it_titular_porcentaje_cobertura']=$data['it_titular_porcentaje_cobertura'];
        }
        if( $data['titular_duracion_maxima_dias'] == null ){
            $data['titular_duracion_maxima_dias']=360;
            $this->m_SimulacionRow['titular_duracion_maxima_dias']=$data['titular_duracion_maxima_dias'];
        }
        if( $data['titular_dias_indemnizacion_percibida'] == null ){
            $data['titular_dias_indemnizacion_percibida']=30;
            $this->m_SimulacionRow['titular_dias_indemnizacion_percibida']=$data['titular_dias_indemnizacion_percibida'];
        }

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            if( $data['it_conyugue_porcentaje_cobertura'] == null ){
                $data['it_conyugue_porcentaje_cobertura']=80;
                $this->m_SimulacionRow['it_conyugue_porcentaje_cobertura']=$data['it_conyugue_porcentaje_cobertura'];
            }
            if( $data['conyugue_duracion_maxima_dias'] == null ){
                $data['conyugue_duracion_maxima_dias']=360;
                $this->m_SimulacionRow['conyugue_duracion_maxima_dias']=$data['conyugue_duracion_maxima_dias'];
            }
            if( $data['conyugue_dias_indemnizacion_percibida'] == null ){
                $data['conyugue_dias_indemnizacion_percibida']=30;
                $this->m_SimulacionRow['conyugue_dias_indemnizacion_percibida']=$data['conyugue_dias_indemnizacion_percibida'];
            }
        }
        
        //Calculados
        if( $this->m_Pensiones->titular_es_autonomo() ) {
            $data['titular_es_autonomo'] = 'Sí';
            $this->assign_expected_numeric($data, 'titular_pago_cuota_autonomos', 'titular_autonomos_cuota_mensual', "Titular: Falta Cuota mensual autónomos");
            if( isset($data['titular_pago_cuota_autonomos'])) {$data['titular_pago_cuota_autonomos'] = -$data['titular_pago_cuota_autonomos'];}
            $data['titular_ingresos_actividad_autonomos'] = $this->m_It_Lib->titular_ingresos_actividad_autonomos();
            $data['titular_base_reguladora_diaria'] = $this->m_Pensiones->titular_br_diaria();//
            $data['titular_prestaciones_ss_primer_mes'] = $this->m_Pensiones->titular_prestacion_primer_mes();//
            $data['titular_prestaciones_ss_siguientes_meses'] = $this->m_It_Lib->titular_prestaciones_ss_siguientes_meses();
            $data['titular_importe_neto_primer_mes'] = $this->m_Pensiones->titular_prestacion_primer_mes() - $this->m_SimulacionRow['titular_autonomos_cuota_mensual'];//
            $data['titular_importe_neto_siguientes_meses'] = $data['titular_prestaciones_ss_siguientes_meses'] - $this->m_SimulacionRow['titular_autonomos_cuota_mensual'];//
            $data['titular_descobertura_primer_mes'] = $data['titular_importe_neto_primer_mes'] - $this->m_SimulacionRow['titular_autonomos_ingreso_bruto_mensual'];//
            $data['titular_descobertura_siguientes_meses'] = $data['titular_importe_neto_siguientes_meses'] - $this->m_SimulacionRow['titular_autonomos_ingreso_bruto_mensual'];
            $data['titular_necesidad_seguro_it_segundo_mes'] = $data['titular_descobertura_siguientes_meses']*$this->m_SimulacionRow['it_titular_porcentaje_cobertura']/100;//
            $data['titular_necesidad_seguro_it_por_dia'] = $data['titular_necesidad_seguro_it_segundo_mes'] / 30;//
            $data['titular_importe_deducible_irpf'] = $this->m_It_Lib->titular_importe_deducible_irpf();
            $data['titular_tipo_marginal'] = $this->m_It_Lib->titular_tipo_marginal();
            $data['titular_posible_beneficio_fiscal'] = $data['titular_importe_deducible_irpf']*$data['titular_tipo_marginal']/100;
            $data['titular_indemnizacion'] = $this->m_It_Lib->titular_indemnizacion();
            $titular_it_total_cuota = $this->m_Fiscalidad->titular_it_total_cuota();
            $data['titular_cuota_a_pagar'] = $titular_it_total_cuota;

        }
        else {
            $data['titular_es_autonomo'] = "No";
            $data['titular_pago_cuota_autonomos']="";
            $data['titular_ingresos_actividad_autonomos']="";
        }

        $data['conyugue_es_autonomo'] = 'No';
        if($this->m_SimulacionRow['tiene_conyugue']==1){ 
            if( $this->m_Pensiones->conyugue_es_autonomo() ) {
                $data['conyugue_es_autonomo'] = 'Sí';
                $this->assign_expected_numeric($data, 'conyugue_pago_cuota_autonomos', 'conyugue_autonomos_cuota_mensual', "Cónyuge: Falta Cuota mensual autónomos");
                if( isset($data['conyugue_pago_cuota_autonomos'])) {$data['conyugue_pago_cuota_autonomos'] = -$data['conyugue_pago_cuota_autonomos'];}
                $data['conyugue_ingresos_actividad_autonomos'] = $this->m_It_Lib->conyugue_ingresos_actividad_autonomos();
                $data['conyugue_base_reguladora_diaria'] = $this->m_Pensiones->conyugue_br_diaria();//
                $data['conyugue_prestaciones_ss_primer_mes'] = $this->m_Pensiones->conyugue_prestacion_primer_mes();//
                $data['conyugue_prestaciones_ss_siguientes_meses'] = $this->m_It_Lib->conyugue_prestaciones_ss_siguientes_meses();
                $data['conyugue_importe_neto_primer_mes'] = $this->m_Pensiones->conyugue_prestacion_primer_mes() - $this->m_SimulacionRow['conyugue_autonomos_cuota_mensual'];//
                $data['conyugue_importe_neto_siguientes_meses'] = $data['conyugue_prestaciones_ss_siguientes_meses'] - $this->m_SimulacionRow['conyugue_autonomos_cuota_mensual'];//
                $data['conyugue_descobertura_primer_mes'] = $data['conyugue_importe_neto_primer_mes'] - $this->m_SimulacionRow['conyugue_autonomos_ingreso_bruto_mensual'];//
                $data['conyugue_descobertura_siguientes_meses'] = $data['conyugue_importe_neto_siguientes_meses'] - $this->m_SimulacionRow['conyugue_autonomos_ingreso_bruto_mensual'];
                $data['conyugue_necesidad_seguro_it_segundo_mes'] = $data['conyugue_descobertura_siguientes_meses']*$this->m_SimulacionRow['it_conyugue_porcentaje_cobertura']/100;//
                $data['conyugue_necesidad_seguro_it_por_dia'] = $data['conyugue_necesidad_seguro_it_segundo_mes'] / 30;//
                $data['conyugue_importe_deducible_irpf'] = $this->m_It_Lib->conyugue_importe_deducible_irpf();
                $data['conyugue_tipo_marginal'] = $this->m_It_Lib->conyugue_tipo_marginal();
                $data['conyugue_posible_beneficio_fiscal'] = $data['conyugue_tipo_marginal']*$data['conyugue_importe_deducible_irpf']/100;
                $data['conyugue_indemnizacion'] = $this->m_It_Lib->conyugue_indemnizacion();
                $conyugue_it_total_cuota = $this->m_Fiscalidad->conyugue_it_total_cuota();
                $data['conyugue_cuota_a_pagar'] = $conyugue_it_total_cuota;
            }
            else {
                $data['conyugue_es_autonomo'] = "No";
                $data['conyugue_pago_cuota_autonomos'] = "";
                $data['conyugue_ingresos_actividad_autonomos'] = "";
            }
        }
        
        if( $this->config->item("winp3_missing_field")!="") {return;}
    }
    
    private function assign_expected_numeric( &$data, $field_data, $field_simulacion, $message) {
        if( trim($this->m_SimulacionRow[$field_simulacion]) == "" ) {
            $this->log_error($message);
            return;
        }
        $data[$field_data] = $this->m_SimulacionRow[$field_simulacion];
    }

}
