<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fallecimiento extends CI_Controller {

    public $m_SimulacionRow;
    public $m_Titular_Lib;
    public $m_Bases;
    public $m_Informa_Bases_Pensiones;
    public $m_Fallecimiento_Lib;
    public $m_Economicos_Lib;

    private function log_error($message){
        if(strpos($this->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field').$message." | ");
        }
    }

    public function report() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}

        $data[]='';
        $this->calculate($data);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $data['current_page'] = 4;
        $this->load->view('templates/header_nav', $data);
        $this->load->view('pages/fallecimiento', $data);
        $this->load->view('templates/footer_nav');
    }

    public function dosier() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}

        //Cargamos librería PDF:
        $this->load->library('mydompdf');

        $data = [];
        $this->calculate($data);
        $this->load->model('User_model');
        $user = $this->User_model->get_user($this->session->userdata['logged_user']['id']);

        $data['contacto_correduria'] = $user['contacto_correduria'];
        $data['contacto_nombre'] = $user['contacto_nombre'];
        $data['contacto_telefono_1'] = $user['contacto_telefono_1'];
        $data['contacto_telefono_2'] = $user['contacto_telefono_2'];
        $data['contacto_direccion_1'] = $user['contacto_direccion_1'];
        $data['contacto_direccion_2'] = $user['contacto_direccion_2'];
        $data['contacto_mail'] = $user['contacto_mail'];
        $data['contacto_final_1'] = $user['contacto_final_1'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];

        //$this->load->view('pages/print/fallecimiento', $data);

        //Generando pdf:
        $html = $this->load->view('pdf/pdf-resultados-fallecimiento', $data, true);
        $this->mydompdf->load_html($html);
        $this->mydompdf->render();
        $this->mydompdf->set_base_path('./assets/css/dompdf.css'); //agregar de nuevo el css
        $this->mydompdf->stream("resultados-fallecimiento.pdf", array(
        "Attachment" => false));

        //$this->load->library('pdf_fallecimiento', $data);
        //echo $this->pdf_fallecimiento->print_document();
    }

    public function recalc() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}

        $data = [];
        if( $this->save_data($data) ) {
            $this->calculate($data);
        }

        $data['winp3_from_recalc'] = 1;
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        //var_dump($data);
        $this->load->view('pages/fallecimiento', $data);
    }

    private function calculate(&$data) {
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params );
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('bases', $params);
        $this->m_Bases = new Bases($params);
        $this->load->library('fallecimiento_lib', $params );
        $this->m_Fallecimiento_Lib = new Fallecimiento_Lib($params);

        $this->m_SimulacionRow = $this->simulacion_model->get_all();

        // Propios
        $data['fallecimiento_titular_porcentaje_cobertura'] = $this->m_SimulacionRow['fallecimiento_titular_porcentaje_cobertura'];
        $data['fallecimiento_titular_anyos_cobertura'] = $this->m_SimulacionRow['fallecimiento_titular_anyos_cobertura'];
        $data['fallecimiento_titular_capital_propuesto'] = $this->m_SimulacionRow['fallecimiento_titular_capital_propuesto'];
        $data['fallecimiento_titular_modalidad'] = $this->m_SimulacionRow['fallecimiento_titular_modalidad'];
        $data['fallecimiento_titular_prima_aproximada'] = $this->m_SimulacionRow['fallecimiento_titular_prima_aproximada'];

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['fallecimiento_conyugue_porcentaje_cobertura'] = $this->m_SimulacionRow['fallecimiento_conyugue_porcentaje_cobertura'];
            $data['fallecimiento_conyugue_anyos_cobertura'] = $this->m_SimulacionRow['fallecimiento_conyugue_anyos_cobertura'];
            $data['fallecimiento_conyugue_capital_propuesto'] = $this->m_SimulacionRow['fallecimiento_conyugue_capital_propuesto'];
            $data['fallecimiento_conyugue_modalidad'] = $this->m_SimulacionRow['fallecimiento_conyugue_modalidad'];
            $data['fallecimiento_conyugue_prima_aproximada'] = $this->m_SimulacionRow['fallecimiento_conyugue_prima_aproximada'];
        }

        $data['fallecimiento_beneficiario_1_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_1_relacion'];
        $data['fallecimiento_beneficiario_1_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_1_edad_descendiente'];
        $data['fallecimiento_beneficiario_1_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_1_importe_cobrar'];
        $data['fallecimiento_beneficiario_2_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_2_relacion'];
        $data['fallecimiento_beneficiario_2_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_2_edad_descendiente'];
        $data['fallecimiento_beneficiario_2_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_2_importe_cobrar'];
        $data['fallecimiento_beneficiario_3_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_3_relacion'];
        $data['fallecimiento_beneficiario_3_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_3_edad_descendiente'];
        $data['fallecimiento_beneficiario_3_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_3_importe_cobrar'];
        $data['fallecimiento_beneficiario_4_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_4_relacion'];
        $data['fallecimiento_beneficiario_4_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_4_edad_descendiente'];
        $data['fallecimiento_beneficiario_4_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_4_importe_cobrar'];
        $data['fallecimiento_beneficiario_5_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_5_relacion'];
        $data['fallecimiento_beneficiario_5_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_5_edad_descendiente'];
        $data['fallecimiento_beneficiario_5_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_5_importe_cobrar'];
        $data['fallecimiento_beneficiario_6_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_6_relacion'];
        $data['fallecimiento_beneficiario_6_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_6_edad_descendiente'];
        $data['fallecimiento_beneficiario_6_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_6_importe_cobrar'];
        $data['fallecimiento_beneficiario_7_relacion'] = $this->m_SimulacionRow['fallecimiento_beneficiario_7_relacion'];
        $data['fallecimiento_beneficiario_7_edad_descendiente'] = $this->m_SimulacionRow['fallecimiento_beneficiario_7_edad_descendiente'];
        $data['fallecimiento_beneficiario_7_importe_cobrar'] = $this->m_SimulacionRow['fallecimiento_beneficiario_7_importe_cobrar'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];

        //Check Defaults
        if( $data['fallecimiento_titular_porcentaje_cobertura'] == null ){
            $data['fallecimiento_titular_porcentaje_cobertura']=80;
            $this->m_SimulacionRow['fallecimiento_titular_porcentaje_cobertura']=$data['fallecimiento_titular_porcentaje_cobertura'];
        }
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            if( $data['fallecimiento_conyugue_porcentaje_cobertura'] == null ){
                $data['fallecimiento_conyugue_porcentaje_cobertura']=80;
                $this->m_SimulacionRow['fallecimiento_conyugue_porcentaje_cobertura']=$data['fallecimiento_conyugue_porcentaje_cobertura'];
            }
        }
        //

        // Calculados 
        if( $this->config->item("winp3_missing_field")!="") {return;}

        $data['titular_gastos_mensuales_familiares'] = $this->m_Fallecimiento_Lib->titular_gastos_mensuales_familiares();
        $data['titular_ingresos_conyugue'] = $this->m_Fallecimiento_Lib->titular_ingresos_conyugue();
        $data['titular_pension_viudedad'] = $this->m_Fallecimiento_Lib->titular_pension_viudedad();
        $data['titular_pension_orfandad_todas'] = $this->m_Fallecimiento_Lib->titular_pension_orfandad();
        $data['titular_saldo_mensual'] = $this->m_Fallecimiento_Lib->titular_saldo_mensual();
//        $data['titular_anyos_cobertura'] = $this->m_Fallecimiento_Lib->titular_anyos_cobertura();
        $data['titular_cobertura_necesaria'] = $this->m_Fallecimiento_Lib->titular_cobertura_necesaria();
        $data['titular_deudas_pendientes'] = $this->m_Fallecimiento_Lib->titular_deudas_pendientes();
        $data['titular_gastos_entierro'] = $this->m_Fallecimiento_Lib->titular_gastos_entierro();
        $data['titular_costes_aceptacion_herencia'] = $this->m_Fallecimiento_Lib->titular_costes_aceptacion_herencia();
        $data['titular_capitales_ahorro_titular'] = $this->m_Fallecimiento_Lib->titular_capitales_ahorro_titular();
        $data['titular_capitales_ahorro_ambos'] = $this->m_Fallecimiento_Lib->titular_capitales_ahorro_ambos();
        $data['titular_saldo_seguro_vida_necesario'] = $this->m_Fallecimiento_Lib->titular_saldo_seguro_vida_necesario();
        $data['titular_seguros_ya_contratados'] = $this->m_Fallecimiento_Lib->titular_seguros_ya_contratados();
        $data['titular_necesidad_seguro_vida'] = $this->m_Fallecimiento_Lib->titular_necesidad_seguro_vida();
        $data['texto_bonificaciones_cuota'] = $this->m_Fallecimiento_Lib->bonificaciones_cuota();
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['conyugue_gastos_mensuales_familiares'] = $this->m_Fallecimiento_Lib->conyugue_gastos_mensuales_familiares();
            $data['conyugue_ingresos_conyugue'] = $this->m_Fallecimiento_Lib->conyugue_ingresos_conyugue();
            $data['conyugue_pension_viudedad'] = $this->m_Fallecimiento_Lib->conyugue_pension_viudedad();
            $data['conyugue_pension_orfandad_todas'] = $this->m_Fallecimiento_Lib->conyugue_pension_orfandad();
            $data['conyugue_saldo_mensual'] = $this->m_Fallecimiento_Lib->conyugue_saldo_mensual();
     //       $data['conyugue_anyos_cobertura'] = $this->m_Fallecimiento_Lib->conyugue_anyos_cobertura();
            $data['conyugue_cobertura_necesaria'] = $this->m_Fallecimiento_Lib->conyugue_cobertura_necesaria();
            $data['conyugue_deudas_pendientes'] = $this->m_Fallecimiento_Lib->conyugue_deudas_pendientes();
            $data['conyugue_gastos_entierro'] = $this->m_Fallecimiento_Lib->conyugue_gastos_entierro();
            $data['conyugue_costes_aceptacion_herencia'] = $this->m_Fallecimiento_Lib->conyugue_costes_aceptacion_herencia();
            $data['conyugue_capitales_ahorro_conyugue'] = $this->m_Fallecimiento_Lib->conyugue_capitales_ahorro_conyugue();
            $data['conyugue_capitales_ahorro_ambos'] = $this->m_Fallecimiento_Lib->conyugue_capitales_ahorro_ambos();
            $data['conyugue_saldo_seguro_vida_necesario'] = $this->m_Fallecimiento_Lib->conyugue_saldo_seguro_vida_necesario();
            $data['conyugue_seguros_ya_contratados'] = $this->m_Fallecimiento_Lib->conyugue_seguros_ya_contratados();
            $data['conyugue_necesidad_seguro_vida'] = $this->m_Fallecimiento_Lib->conyugue_necesidad_seguro_vida();
        }
        $n_hijos=$this->m_SimulacionRow['numero_hijos'];
        $data['numero_hijos'] = $n_hijos;
        for($i = 1; $i <= 7; $i++){
            if($data['fallecimiento_beneficiario_'.$i.'_relacion']!=''){
                $data['fallecimiento_beneficiario_'.$i.'_cuota'] = $this->m_Fiscalidad->beneficiario_total_cuota($i);
            }
            else {
                $data['fallecimiento_beneficiario_'.$i.'_cuota'] = '';
            }
        }
    }


    public function save() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}

        // Librerias
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $data=[];
        if( !$this->save_data($data) ) {
            $this->load->view('templates/simulator_header');
            $data['missing_fields'] = $this->config->item('winp3_missing_field');
            $this->load->view('pages/fallecimiento', $data);
            $this->load->view('templates/footer');
            return;
        }

        redirect($this->input->post('page') );
    }


    private function save_data(&$data) {
        $this->assign_field('fallecimiento_titular_porcentaje_cobertura', $data );
        $this->assign_field('fallecimiento_titular_anyos_cobertura', $data );
        $this->assign_field('fallecimiento_titular_capital_propuesto', $data );
        $this->assign_field('fallecimiento_titular_modalidad', $data );
        $this->assign_field('fallecimiento_titular_prima_aproximada', $data );

        $this->assign_field('fallecimiento_conyugue_porcentaje_cobertura', $data );
        $this->assign_field('fallecimiento_conyugue_anyos_cobertura', $data );
        $this->assign_field('fallecimiento_conyugue_capital_propuesto', $data );
        $this->assign_field('fallecimiento_conyugue_modalidad', $data );
        $this->assign_field('fallecimiento_conyugue_prima_aproximada', $data );

        $this->assign_field('fallecimiento_beneficiario_1_relacion', $data );
        $this->assign_field('fallecimiento_beneficiario_1_edad_descendiente', $data );
        $this->assign_field('fallecimiento_beneficiario_1_importe_cobrar', $data );
        $this->assign_field('fallecimiento_beneficiario_2_relacion', $data );
        $this->assign_field('fallecimiento_beneficiario_2_edad_descendiente', $data );
        $this->assign_field('fallecimiento_beneficiario_2_importe_cobrar', $data );
        $this->assign_field('fallecimiento_beneficiario_3_relacion', $data );
        $this->assign_field('fallecimiento_beneficiario_3_edad_descendiente', $data );
        $this->assign_field('fallecimiento_beneficiario_3_importe_cobrar', $data );
        $this->assign_field('fallecimiento_beneficiario_4_relacion', $data );
        $this->assign_field('fallecimiento_beneficiario_4_edad_descendiente', $data );
        $this->assign_field('fallecimiento_beneficiario_4_importe_cobrar', $data );
        $this->assign_field('fallecimiento_beneficiario_5_relacion', $data );
        $this->assign_field('fallecimiento_beneficiario_5_edad_descendiente', $data );
        $this->assign_field('fallecimiento_beneficiario_5_importe_cobrar', $data );
        $this->assign_field('fallecimiento_beneficiario_6_relacion', $data );
        $this->assign_field('fallecimiento_beneficiario_6_edad_descendiente', $data );
        $this->assign_field('fallecimiento_beneficiario_6_importe_cobrar', $data );
        $this->assign_field('fallecimiento_beneficiario_7_relacion', $data );
        $this->assign_field('fallecimiento_beneficiario_7_edad_descendiente', $data );
        $this->assign_field('fallecimiento_beneficiario_7_importe_cobrar', $data );

        if( !$this->simulacion_model->save($data)) {
            $this->log_error("Error guardando datos");
            return false;
        }

        return true;
    }

    private function assign_field( $field_name, &$data ) {
        if( $this->input->post($field_name) != null ) {
            $data[$field_name] = str_replace(".", "", $this->input->post($field_name));
            $data[$field_name] = str_replace(",", ".", $data[$field_name]);
        }
        else {
            $data[$field_name] = null;
        }
    }


}
