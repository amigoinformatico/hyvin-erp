<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Economicos extends CI_Controller {

    public $m_SimulacionRow;
    public $m_Economicos_Lib;
    public $m_Constantes;
    public $m_Informa_Bases_Pensiones;
    
    private function log_error($message){
        if(strpos($this->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field').$message." | ");
        }
    }

    public function datos() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}

        // Librerias
        $params[0] = $this;
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);

        // Prepara datos para la vista y la llama
        $data[]='';
        $this->calculate($data);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $data['current_page'] = 2;
        $this->load->view('templates/header_nav', $data);
        $this->load->view('pages/economicos', $data);
        $this->load->view('templates/footer_nav', $data);
    }

    public function recalc() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}

        $params[0] = $this;
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        
        $data=[];
        $data['tiene_conyugue'] = $this->input->post("tiene_conyugue");
        
        if( $this->save_data($data) ) {
            $this->calculate($data);
        }
        
        $data['winp3_from_recalc'] = 1;
        $data['current_page'] = 2;
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/economicos', $data);
    }
    
    public function save() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}

        // Librerias
        $params[0] = $this;
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);

        $data=[];
        $data['tiene_conyugue'] = $this->input->post("tiene_conyugue");
        if( !$this->save_data($data) ) {

            $data['missing_fields'] = $this->config->item('winp3_missing_field');
            $data['current_page'] = 2;
            $this->load->view('templates/header_nav', $data);
            $this->load->view('pages/economicos', $data);
            $this->load->view('templates/footer_nav');
            return;
        }

        redirect($this->input->post('page') );
    }
    
    private function save_data(&$data) {
        
        //Assign
        $this->assign_field('titular_regimen_ss', $data );
        
        if($data['titular_regimen_ss'] == "autonomo" || $data['titular_regimen_ss']=="ambos"){
            $this->assign_field('titular_autonomos_ingreso_bruto_mensual', $data);
            $this->assign_field('titular_autonomos_cuota_mensual', $data );
            $this->assign_field('titular_autonomos_retencion_irpf', $data);
        }
        else {
            $data['titular_autonomos_ingreso_bruto_mensual'] = 0;
            $data['titular_autonomos_cuota_mensual'] = 0;
            $data['titular_autonomos_retencion_irpf'] = 0;
        }

        if($data['titular_regimen_ss'] == "rg" || $data['titular_regimen_ss']=="ambos"){
            $this->assign_field('titular_rg_neto_mes_ordinario', $data);
            $this->assign_field('titular_rg_paga_extra', $data);
            $this->assign_field('titular_rg_numero_pagas_extra', $data);
        }
        else {
            $data['titular_rg_neto_mes_ordinario'] = 0;
            $data['titular_rg_paga_extra'] = 0;
            $data['titular_rg_numero_pagas_extra'] = 0;
        }
       
        $this->assign_field('titular_cotizado_ss_rg_anyos', $data);
        $this->assign_field('titular_cotizado_ss_rg_meses', $data);
        $this->assign_field('titular_cotizado_ss_aut_anyos', $data);
        $this->assign_field('titular_cotizado_ss_aut_meses', $data);
        
        if( $data['titular_cotizado_ss_rg_anyos']==''){$data['titular_cotizado_ss_rg_anyos']=0;}
        if( $data['titular_cotizado_ss_rg_meses']==''){$data['titular_cotizado_ss_rg_meses']=0;}
        if( $data['titular_cotizado_ss_aut_anyos']==''){$data['titular_cotizado_ss_aut_anyos']=0;}
        if( $data['titular_cotizado_ss_aut_meses']==''){$data['titular_cotizado_ss_aut_meses']=0;}
        
        $this->assign_field('titular_rg_retencion', $data);
        $this->assign_field('titular_distintos_ingresos', $data);
        $this->assign_field('titular_distintos_periodicidad', $data);
        $this->assign_field('titular_distintos_retencion', $data );
        $this->assign_field('titular_ahorro_mensual', $data);
        
        $this->assign_field('conyugue_regimen_ss', $data);
        if($data['conyugue_regimen_ss'] == "autonomo" || $data['conyugue_regimen_ss']=="ambos"){
            $this->assign_field('conyugue_autonomos_ingreso_bruto_mensual', $data);
            $this->assign_field('conyugue_autonomos_cuota_mensual', $data );
            $this->assign_field('conyugue_autonomos_retencion_irpf', $data);
        }
        else {
            $data['conyugue_autonomos_ingreso_bruto_mensual'] = 0;
            $data['conyugue_autonomos_cuota_mensual'] = 0;
            $data['conyugue_autonomos_retencion_irpf'] = 0;
        }

        if($data['conyugue_regimen_ss'] == "rg" || $data['conyugue_regimen_ss']=="ambos"){
            $this->assign_field('conyugue_rg_neto_mes_ordinario', $data);
            $this->assign_field('conyugue_rg_paga_extra', $data);
            $this->assign_field('conyugue_rg_numero_pagas_extra', $data);
        }
        else {
            $data['conyugue_rg_neto_mes_ordinario'] = 0;
            $data['conyugue_rg_paga_extra'] = 0;
            $data['conyugue_rg_numero_pagas_extra'] = 0;
        }

        $this->assign_field('conyugue_cotizado_ss_rg_anyos', $data);
        $this->assign_field('conyugue_cotizado_ss_rg_meses', $data);
        $this->assign_field('conyugue_cotizado_ss_aut_anyos', $data);
        $this->assign_field('conyugue_cotizado_ss_aut_meses', $data);
        
        if( $data['conyugue_cotizado_ss_rg_anyos']==''){$data['conyugue_cotizado_ss_rg_anyos']=0;}
        if( $data['conyugue_cotizado_ss_rg_meses']==''){$data['conyugue_cotizado_ss_rg_meses']=0;}
        if( $data['conyugue_cotizado_ss_aut_anyos']==''){$data['conyugue_cotizado_ss_aut_anyos']=0;}
        if( $data['conyugue_cotizado_ss_aut_meses']==''){$data['conyugue_cotizado_ss_aut_meses']=0;}
        
        $this->assign_field('conyugue_rg_retencion', $data);
        $this->assign_field('conyugue_distintos_ingresos', $data);
        $this->assign_field('conyugue_distintos_periodicidad', $data);
        $this->assign_field('conyugue_distintos_retencion', $data );
        $this->assign_field('conyugue_ahorro_mensual', $data);
        $this->assign_field('deuda1_nombre', $data);
        $this->assign_field('deuda1_capital_pendiente', $data);
        $this->assign_field('deuda1_anyos_pendientes', $data );
        $this->assign_field('deuda1_cuota', $data);
        $this->assign_field('deuda2_nombre', $data);
        $this->assign_field('deuda2_capital_pendiente', $data);
        $this->assign_field('deuda2_anyos_pendientes', $data );
        $this->assign_field('deuda2_cuota', $data);
        $this->assign_field('deuda3_nombre', $data);
        $this->assign_field('deuda3_capital_pendiente', $data);
        $this->assign_field('deuda3_anyos_pendientes', $data );
        $this->assign_field('deuda3_cuota', $data);
        $this->assign_field('activos_ahorros_1_afectado', $data);
        $this->assign_field('activos_ahorros_1_tipo', $data);
        $this->assign_field('activos_ahorros_1_capital_hoy', $data);
        $this->assign_field('activos_ahorros_1_capital_jubilacion', $data);
        $this->assign_field('activos_ahorros_1_prima_mensual', $data);
        $this->assign_field('activos_ahorros_1_derechos_consolidados', $data);
        $this->assign_field('activos_ahorros_2_afectado', $data);
        $this->assign_field('activos_ahorros_2_tipo', $data);
        $this->assign_field('activos_ahorros_2_capital_hoy', $data);
        $this->assign_field('activos_ahorros_2_capital_jubilacion', $data);
        $this->assign_field('activos_ahorros_2_prima_mensual', $data);
        $this->assign_field('activos_ahorros_2_derechos_consolidados', $data);
        $this->assign_field('activos_ahorros_3_afectado', $data);
        $this->assign_field('activos_ahorros_3_tipo', $data);
        $this->assign_field('activos_ahorros_3_capital_hoy', $data);
        $this->assign_field('activos_ahorros_3_capital_jubilacion', $data);
        $this->assign_field('activos_ahorros_3_prima_mensual', $data);
        $this->assign_field('activos_ahorros_3_derechos_consolidados', $data);
        $this->assign_field('activos_ahorros_4_afectado', $data);
        $this->assign_field('activos_ahorros_4_tipo', $data);
        $this->assign_field('activos_ahorros_4_capital_hoy', $data);
        $this->assign_field('activos_ahorros_4_capital_jubilacion', $data);
        $this->assign_field('activos_ahorros_4_prima_mensual', $data);
        $this->assign_field('activos_ahorros_4_derechos_consolidados', $data);
        $this->assign_field('activos_ahorros_5_afectado', $data);
        $this->assign_field('activos_ahorros_5_tipo', $data);
        $this->assign_field('activos_ahorros_5_capital_hoy', $data);
        $this->assign_field('activos_ahorros_5_capital_jubilacion', $data);
        $this->assign_field('activos_ahorros_5_prima_mensual', $data);
        $this->assign_field('activos_ahorros_5_derechos_consolidados', $data);
        $this->assign_field('activos_ahorros_6_afectado', $data);
        $this->assign_field('activos_ahorros_6_tipo', $data);
        $this->assign_field('activos_ahorros_6_capital_hoy', $data);
        $this->assign_field('activos_ahorros_6_capital_jubilacion', $data);
        $this->assign_field('activos_ahorros_6_prima_mensual', $data);
        $this->assign_field('activos_ahorros_6_derechos_consolidados', $data);
        $this->assign_field('activos_sv_1_asegurado', $data );
        $this->assign_field('activos_sv_1_fallecimiento', $data );
        $this->assign_field('activos_sv_1_ipa', $data );
        $this->assign_field('activos_sv_1_ipt', $data );
        $this->assign_field('activos_sv_1_prima_anual', $data );
        $this->assign_field('activos_sv_2_asegurado', $data );
        $this->assign_field('activos_sv_2_fallecimiento', $data );
        $this->assign_field('activos_sv_2_ipa', $data );
        $this->assign_field('activos_sv_2_ipt', $data );
        $this->assign_field('activos_sv_2_prima_anual', $data );
        $this->assign_field('activos_sv_3_asegurado', $data );
        $this->assign_field('activos_sv_3_fallecimiento', $data );
        $this->assign_field('activos_sv_3_ipa', $data );
        $this->assign_field('activos_sv_3_ipt', $data );
        $this->assign_field('activos_sv_3_prima_anual', $data );
        $this->assign_field('activos_sv_4_asegurado', $data );
        $this->assign_field('activos_sv_4_fallecimiento', $data );
        $this->assign_field('activos_sv_4_ipa', $data );
        $this->assign_field('activos_sv_4_ipt', $data );
        $this->assign_field('activos_sv_4_prima_anual', $data );
        $this->assign_field('activos_sv_5_asegurado', $data );
        $this->assign_field('activos_sv_5_fallecimiento', $data );
        $this->assign_field('activos_sv_5_ipa', $data );
        $this->assign_field('activos_sv_5_ipt', $data );
        $this->assign_field('activos_sv_5_prima_anual', $data );
       
        if(!$this->check_values($data) ) {return false; }
        
        if( !$this->simulacion_model->save($data)) {
            $this->log_error("Error guardando datos");
            return false;
        }
        
        return true;
    }
    
    private function assign_field( $field_name, &$data ) {
        if( $this->input->post($field_name) != null ) {
            $data[$field_name] = str_replace(".", "", $this->input->post($field_name)); 
            $data[$field_name] = str_replace(",", ".", $data[$field_name]); 
        }
        else {
            $data[$field_name] = null; 
        }
    }

    
    // Llena y calcula los campos de la vista
    private function calculate( &$data ) {
        $row = $this->m_SimulacionRow = $this->simulacion_model->get_all();
        
        $data['tiene_conyugue'] = $row['tiene_conyugue'];
        
        //Propios
        $data['titular_regimen_ss'] = $row['titular_regimen_ss'];
        $data['conyugue_regimen_ss'] = $row['conyugue_regimen_ss'];
        $data['titular_cotizado_ss_rg_anyos'] = $row['titular_cotizado_ss_rg_anyos'];
        $data['titular_cotizado_ss_rg_meses'] = $row['titular_cotizado_ss_rg_meses'];
        $data['titular_cotizado_ss_aut_anyos'] = $row['titular_cotizado_ss_aut_anyos'];
        $data['titular_cotizado_ss_aut_meses'] = $row['titular_cotizado_ss_aut_meses'];
        $data['conyugue_cotizado_ss_rg_anyos'] = $row['conyugue_cotizado_ss_rg_anyos'];
        $data['conyugue_cotizado_ss_rg_meses'] = $row['conyugue_cotizado_ss_rg_meses'];
        $data['conyugue_cotizado_ss_aut_anyos'] = $row['conyugue_cotizado_ss_aut_anyos'];
        $data['conyugue_cotizado_ss_aut_meses'] = $row['conyugue_cotizado_ss_aut_meses'];
        $data['titular_autonomos_cuota_mensual'] = $row['titular_autonomos_cuota_mensual'];
        $data['titular_autonomos_ingreso_bruto_mensual'] = $row['titular_autonomos_ingreso_bruto_mensual'];
        $data['titular_autonomos_retencion_irpf'] = $row['titular_autonomos_retencion_irpf'];
        $data['tiene_conyugue'] = $row['tiene_conyugue'];
        $data['titular_rg_neto_mes_ordinario'] = $row['titular_rg_neto_mes_ordinario'];
        $data['titular_rg_paga_extra'] = $row['titular_rg_paga_extra'];
        $data['titular_rg_numero_pagas_extra'] = $row['titular_rg_numero_pagas_extra'];
        $data['titular_rg_retencion'] = $row['titular_rg_retencion'];
        
        $data['titular_distintos_ingresos'] = $row['titular_distintos_ingresos'];
        $data['titular_distintos_periodicidad'] =$row['titular_distintos_periodicidad'];
        $data['titular_distintos_retencion'] = $row['titular_distintos_retencion'];
        
        $data['titular_ahorro_mensual'] = $row['titular_ahorro_mensual'];
        
        $data['deuda1_nombre'] = $row['deuda1_nombre'];
        $data['deuda1_capital_pendiente'] = $row['deuda1_capital_pendiente'];
        $data['deuda1_anyos_pendientes'] = $row['deuda1_anyos_pendientes'];
        $data['deuda1_cuota'] = $row['deuda1_cuota'];
        $data['deuda2_nombre'] = $row['deuda2_nombre'];
        $data['deuda2_capital_pendiente'] = $row['deuda2_capital_pendiente'];
        $data['deuda2_anyos_pendientes'] = $row['deuda2_anyos_pendientes'];
        $data['deuda2_cuota'] = $row['deuda2_cuota'];
        $data['deuda3_nombre'] = $row['deuda3_nombre'];
        $data['deuda3_capital_pendiente'] = $row['deuda3_capital_pendiente'];
        $data['deuda3_anyos_pendientes'] = $row['deuda3_anyos_pendientes'];
        $data['deuda3_cuota'] = $row['deuda3_cuota'];

        $data['activos_ahorros_1_afectado'] = $row['activos_ahorros_1_afectado'];
        $data['activos_ahorros_1_tipo'] = $row['activos_ahorros_1_tipo'];
        $data['activos_ahorros_1_capital_hoy'] = $row['activos_ahorros_1_capital_hoy'];
        $data['activos_ahorros_1_capital_jubilacion'] = $row['activos_ahorros_1_capital_jubilacion'];
        $data['activos_ahorros_1_prima_mensual'] = $row['activos_ahorros_1_prima_mensual'];
        $data['activos_ahorros_1_derechos_consolidados'] = $row['activos_ahorros_1_derechos_consolidados'];
        $data['activos_ahorros_2_afectado'] = $row['activos_ahorros_2_afectado'];
        $data['activos_ahorros_2_tipo'] = $row['activos_ahorros_2_tipo'];
        $data['activos_ahorros_2_capital_hoy'] = $row['activos_ahorros_2_capital_hoy'];
        $data['activos_ahorros_2_capital_jubilacion'] = $row['activos_ahorros_2_capital_jubilacion'];
        $data['activos_ahorros_2_prima_mensual'] = $row['activos_ahorros_2_prima_mensual'];
        $data['activos_ahorros_2_derechos_consolidados'] = $row['activos_ahorros_2_derechos_consolidados'];
        $data['activos_ahorros_3_afectado'] = $row['activos_ahorros_3_afectado'];
        $data['activos_ahorros_3_tipo'] = $row['activos_ahorros_3_tipo'];
        $data['activos_ahorros_3_capital_hoy'] = $row['activos_ahorros_3_capital_hoy'];
        $data['activos_ahorros_3_capital_jubilacion'] = $row['activos_ahorros_3_capital_jubilacion'];
        $data['activos_ahorros_3_prima_mensual'] = $row['activos_ahorros_3_prima_mensual'];
        $data['activos_ahorros_3_derechos_consolidados'] = $row['activos_ahorros_3_derechos_consolidados'];
        $data['activos_ahorros_4_afectado'] = $row['activos_ahorros_4_afectado'];
        $data['activos_ahorros_4_tipo'] = $row['activos_ahorros_4_tipo'];
        $data['activos_ahorros_4_capital_hoy'] = $row['activos_ahorros_4_capital_hoy'];
        $data['activos_ahorros_4_capital_jubilacion'] = $row['activos_ahorros_4_capital_jubilacion'];
        $data['activos_ahorros_4_prima_mensual'] = $row['activos_ahorros_4_prima_mensual'];
        $data['activos_ahorros_4_derechos_consolidados'] = $row['activos_ahorros_4_derechos_consolidados'];
        $data['activos_ahorros_5_afectado'] = $row['activos_ahorros_5_afectado'];
        $data['activos_ahorros_5_tipo'] = $row['activos_ahorros_5_tipo'];
        $data['activos_ahorros_5_capital_hoy'] = $row['activos_ahorros_5_capital_hoy'];
        $data['activos_ahorros_5_capital_jubilacion'] = $row['activos_ahorros_5_capital_jubilacion'];
        $data['activos_ahorros_5_prima_mensual'] = $row['activos_ahorros_5_prima_mensual'];
        $data['activos_ahorros_5_derechos_consolidados'] = $row['activos_ahorros_5_derechos_consolidados'];
        $data['activos_ahorros_6_afectado'] = $row['activos_ahorros_6_afectado'];
        $data['activos_ahorros_6_tipo'] = $row['activos_ahorros_6_tipo'];
        $data['activos_ahorros_6_capital_hoy'] = $row['activos_ahorros_6_capital_hoy'];
        $data['activos_ahorros_6_capital_jubilacion'] = $row['activos_ahorros_6_capital_jubilacion'];
        $data['activos_ahorros_6_prima_mensual'] = $row['activos_ahorros_6_prima_mensual'];
        $data['activos_ahorros_6_derechos_consolidados'] = $row['activos_ahorros_6_derechos_consolidados'];
        $data['activos_sv_1_asegurado'] = $row['activos_sv_1_asegurado'];
        $data['activos_sv_1_fallecimiento'] = $row['activos_sv_1_fallecimiento'];
        $data['activos_sv_1_ipa'] = $row['activos_sv_1_ipa'];
        $data['activos_sv_1_ipt'] = $row['activos_sv_1_ipt'];
        $data['activos_sv_1_prima_anual'] = $row['activos_sv_1_prima_anual'];
        $data['activos_sv_2_asegurado'] = $row['activos_sv_2_asegurado'];
        $data['activos_sv_2_fallecimiento'] = $row['activos_sv_2_fallecimiento'];
        $data['activos_sv_2_ipa'] = $row['activos_sv_2_ipa'];
        $data['activos_sv_2_ipt'] = $row['activos_sv_2_ipt'];
        $data['activos_sv_2_prima_anual'] = $row['activos_sv_2_prima_anual'];
        $data['activos_sv_3_asegurado'] = $row['activos_sv_3_asegurado'];
        $data['activos_sv_3_fallecimiento'] = $row['activos_sv_3_fallecimiento'];
        $data['activos_sv_3_ipa'] = $row['activos_sv_3_ipa'];
        $data['activos_sv_3_ipt'] = $row['activos_sv_3_ipt'];
        $data['activos_sv_3_prima_anual'] = $row['activos_sv_3_prima_anual'];
        $data['activos_sv_4_asegurado'] = $row['activos_sv_4_asegurado'];
        $data['activos_sv_4_fallecimiento'] = $row['activos_sv_4_fallecimiento'];
        $data['activos_sv_4_ipa'] = $row['activos_sv_4_ipa'];
        $data['activos_sv_4_ipt'] = $row['activos_sv_4_ipt'];
        $data['activos_sv_4_prima_anual'] = $row['activos_sv_4_prima_anual'];
        $data['activos_sv_5_asegurado'] = $row['activos_sv_5_asegurado'];
        $data['activos_sv_5_fallecimiento'] = $row['activos_sv_5_fallecimiento'];
        $data['activos_sv_5_ipa'] = $row['activos_sv_5_ipa'];
        $data['activos_sv_5_ipt'] = $row['activos_sv_5_ipt'];
        $data['activos_sv_5_prima_anual'] = $row['activos_sv_5_prima_anual'];
        $data['conyugue_autonomos_cuota_mensual'] = $row['conyugue_autonomos_cuota_mensual'];
        $data['conyugue_autonomos_ingreso_bruto_mensual'] = $row['conyugue_autonomos_ingreso_bruto_mensual'];
        $data['conyugue_autonomos_retencion_irpf'] = $row['conyugue_autonomos_retencion_irpf'];
        $data['conyugue_rg_neto_mes_ordinario'] = $row['conyugue_rg_neto_mes_ordinario'];
        $data['conyugue_rg_paga_extra'] = $row['conyugue_rg_paga_extra'];
        $data['conyugue_rg_numero_pagas_extra'] = $row['conyugue_rg_numero_pagas_extra'];
        $data['conyugue_rg_retencion'] = $row['conyugue_rg_retencion'];
        $data['conyugue_distintos_ingresos'] = $row['conyugue_distintos_ingresos'];
        $data['conyugue_distintos_periodicidad'] =$row['conyugue_distintos_periodicidad'];
        $data['conyugue_distintos_retencion'] = $row['conyugue_distintos_retencion'];
        $data['conyugue_ahorro_mensual'] =$row['conyugue_ahorro_mensual'];
        
        //Check Defaults
        if( $data['titular_regimen_ss']=='autonomo' || $data['titular_regimen_ss']=='ambos') {
            if($data['titular_autonomos_retencion_irpf']==null){
                $data['titular_autonomos_retencion_irpf'] = 15;
                $this->m_SimulacionRow['titular_autonomos_retencion_irpf']=$data['titular_autonomos_retencion_irpf'];
            }
        }
        if( $data['conyugue_regimen_ss']=='autonomo' || $data['conyugue_regimen_ss']=='ambos') {
            if($data['conyugue_autonomos_retencion_irpf']==null){
                $data['conyugue_autonomos_retencion_irpf'] = 15;
                $this->m_SimulacionRow['conyugue_autonomos_retencion_irpf']=$data['conyugue_autonomos_retencion_irpf'];
            }
        }
        if( $data['titular_regimen_ss']=='rg' || $data['titular_regimen_ss']=='ambos') {
            if($data['titular_rg_retencion']==null){
                $data['titular_rg_retencion'] = 15;
                $this->m_SimulacionRow['titular_rg_retencion']=$data['titular_rg_retencion'];
            }
        }
        if( $data['conyugue_regimen_ss']=='rg' || $data['conyugue_regimen_ss']=='ambos') {
            if($data['conyugue_rg_retencion']==null){
                $data['conyugue_rg_retencion'] = 15;
                $this->m_SimulacionRow['conyugue_rg_retencion']=$data['conyugue_rg_retencion'];
            }
        }
        if( $data['titular_distintos_ingresos']!=null && $data['titular_distintos_retencion']==null){
            $data['titular_distintos_retencion']=15;
            $this->m_SimulacionRow['titular_distintos_retencion']=$data['titular_distintos_retencion'];
        }
        if( $data['conyugue_distintos_ingresos']!=null && $data['conyugue_distintos_retencion']==null){
            $data['conyugue_distintos_retencion']=15;
            $this->m_SimulacionRow['conyugue_distintos_retencion']=$data['conyugue_distintos_retencion'];
        }
        //

        
        //Calculados
        $data['titular_autonomos_neto_mensual_media'] = $this->m_Economicos_Lib->titular_autonomos_neto_mensual_media();
        $data['titular_autonomos_base_cotizacion'] = $this->m_Economicos_Lib->titular_autonomos_base_cotizacion();
        
        if( $this->m_SimulacionRow['tiene_conyugue'] == '1' && $data['conyugue_regimen_ss'] == 'autonomo' || $data['conyugue_regimen_ss'] == 'ambos' ) {
            $data['conyugue_autonomos_neto_mensual_media'] = $this->m_Economicos_Lib->conyugue_autonomos_neto_mensual_media();
            $data['conyugue_autonomos_base_cotizacion'] = $this->m_Economicos_Lib->conyugue_autonomos_base_cotizacion();
        }
    }
    
    private function check_values(&$data) {
        $this->check_numeric($data['titular_cotizado_ss_rg_anyos'], "Titular: Tiempo Cotizado régimen general" );
        $this->check_numeric($data['titular_cotizado_ss_rg_meses'], "Titular: Tiempo Cotizado régimen general" );
        $this->check_numeric($data['titular_cotizado_ss_aut_anyos'], "Titular: Tiempo Cotizado autónomos" );
        $this->check_numeric($data['titular_cotizado_ss_aut_anyos'], "Titular: Tiempo Cotizado autónomos" );

        if( $data['titular_regimen_ss']=='autonomo' || $data['titular_regimen_ss']=='ambos') {
            $this->check_numeric($data['titular_autonomos_cuota_mensual'], "Titular: Cuota mensual autónomos" );
            if( $data['titular_autonomos_cuota_mensual'] < 50 || $data['titular_autonomos_cuota_mensual'] > 1118) {
                $this->log_error("La cuota de autónomos del titular debe estar entre 50€ y 1.118€");            
                return false;
            }
            $this->check_numeric($data['titular_autonomos_ingreso_bruto_mensual'], "Titular: Ingreso bruto mensual autónomos");
            $this->check_numeric($data['titular_autonomos_retencion_irpf'], "Titular: Retención IRPF");
        }        
        
        $this->check_numeric($data['titular_rg_neto_mes_ordinario'], "Titular: Régimen general neto mes ordinario");
        $this->check_numeric($data['titular_rg_paga_extra'], "Titular: Régimen general paga extra");
        $this->check_numeric($data['titular_rg_numero_pagas_extra'], "Titular: Régimen general número de pagas extra");
        $this->check_numeric($data['titular_rg_retencion'], "Titular: Régimen general retención" );
        $this->check_numeric($data['titular_distintos_ingresos'], "Titular: Ingresos distintos al trabajo");
        $this->check_numeric($data['titular_distintos_retencion'], "Titular: Ingresos distintos retención" );
        $this->check_numeric($data['titular_ahorro_mensual'], "Titular: Ahorro mensual");
        $this->check_numeric($data['deuda1_capital_pendiente'], "Deudas: Capital pendiente" );
        $this->check_numeric($data['deuda1_anyos_pendientes'], "Deuddas: Años pendientes");
        $this->check_numeric($data['deuda1_cuota'], "Deudas: Cuota" );
        $this->check_numeric($data['deuda2_capital_pendiente'], "Deudas: Capital pediente" );
        $this->check_numeric($data['deuda2_anyos_pendientes'], "Deudas: Años pendientes" );
        $this->check_numeric($data['deuda2_cuota'], "Deudas: Cuota" );
        $this->check_numeric($data['deuda3_capital_pendiente'], "Deudas: Capital pendiente");
        $this->check_numeric($data['deuda3_anyos_pendientes'], "Deudas: Años pendientes" );
        $this->check_numeric($data['deuda3_cuota'], "Deudas: Cuota");
        $this->check_numeric($data['activos_ahorros_1_capital_hoy'], "Activos: Capital hoy");
        $this->check_numeric($data['activos_ahorros_1_capital_jubilacion'], "Activos: Capital jubilación");
        $this->check_numeric($data['activos_ahorros_1_prima_mensual'], "Activos: Prima mensual" );
        $this->check_numeric($data['activos_ahorros_1_derechos_consolidados'], "Activos: Derechos consolidados" );
        $this->check_numeric($data['activos_ahorros_2_capital_hoy'], "Activos: Capital hoy");
        $this->check_numeric($data['activos_ahorros_2_capital_jubilacion'], "Activos: Capital jubilación");
        $this->check_numeric($data['activos_ahorros_2_prima_mensual'], "Activos: Prima mensual" );
        $this->check_numeric($data['activos_ahorros_2_derechos_consolidados'], "Activos: Derechos consolidados" );
        $this->check_numeric($data['activos_ahorros_3_capital_hoy'], "Activos: Capital hoy");
        $this->check_numeric($data['activos_ahorros_3_capital_jubilacion'], "Activos: Capital jubilación");
        $this->check_numeric($data['activos_ahorros_3_prima_mensual'], "Activos: Prima mensual" );
        $this->check_numeric($data['activos_ahorros_3_derechos_consolidados'], "Activos: Derechos consolidados" );
        $this->check_numeric($data['activos_ahorros_4_capital_hoy'], "Activos: Capital hoy");
        $this->check_numeric($data['activos_ahorros_4_capital_jubilacion'], "Activos: Capital jubilación");
        $this->check_numeric($data['activos_ahorros_4_prima_mensual'], "Activos: Prima mensual" );
        $this->check_numeric($data['activos_ahorros_4_derechos_consolidados'], "Activos: Derechos consolidados" );
        $this->check_numeric($data['activos_ahorros_5_capital_hoy'], "Activos: Capital hoy");
        $this->check_numeric($data['activos_ahorros_5_capital_jubilacion'], "Activos: Capital jubilación");
        $this->check_numeric($data['activos_ahorros_5_prima_mensual'], "Activos: Prima mensual" );
        $this->check_numeric($data['activos_ahorros_5_derechos_consolidados'], "Activos: Derechos consolidados" );
        $this->check_numeric($data['activos_ahorros_6_capital_hoy'], "Activos: Capital hoy");
        $this->check_numeric($data['activos_ahorros_6_capital_jubilacion'], "Activos: Capital jubilación");
        $this->check_numeric($data['activos_ahorros_6_prima_mensual'], "Activos: Prima mensual" );
        $this->check_numeric($data['activos_ahorros_6_derechos_consolidados'], "Activos: Derechos consolidados" );
        $this->check_numeric($data['activos_sv_1_fallecimiento'], "Activos: Fallecimiento" );
        $this->check_numeric($data['activos_sv_1_ipa'], "Activos: IPA" );
        $this->check_numeric($data['activos_sv_1_ipt'], "Activos: IPT" );
        $this->check_numeric($data['activos_sv_1_prima_anual'], "Activos: Prima anual");
        $this->check_numeric($data['activos_sv_2_fallecimiento'], "Activos: Fallecimiento" );
        $this->check_numeric($data['activos_sv_2_ipa'], "Activos: IPA" );
        $this->check_numeric($data['activos_sv_2_ipt'], "Activos: IPT" );
        $this->check_numeric($data['activos_sv_2_prima_anual'], "Activos: Prima anual");
        $this->check_numeric($data['activos_sv_3_fallecimiento'], "Activos: Fallecimiento" );
        $this->check_numeric($data['activos_sv_3_ipa'], "Activos: IPA" );
        $this->check_numeric($data['activos_sv_3_ipt'], "Activos: IPT" );
        $this->check_numeric($data['activos_sv_3_prima_anual'], "Activos: Prima anual");
        $this->check_numeric($data['activos_sv_4_fallecimiento'], "Activos: Fallecimiento" );
        $this->check_numeric($data['activos_sv_4_ipa'], "Activos: IPA" );
        $this->check_numeric($data['activos_sv_4_ipt'], "Activos: IPT" );
        $this->check_numeric($data['activos_sv_4_prima_anual'], "Activos: Prima anual");
        $this->check_numeric($data['activos_sv_5_fallecimiento'], "Activos: Fallecimiento" );
        $this->check_numeric($data['activos_sv_5_ipa'], "Activos: IPA" );
        $this->check_numeric($data['activos_sv_5_ipt'], "Activos: IPT" );
        $this->check_numeric($data['activos_sv_5_prima_anual'], "Activos: Prima anual");
        
        //Conyugue
        if( $data['tiene_conyugue'] == 1) {
            $this->check_numeric($data['conyugue_cotizado_ss_rg_anyos'], "Cónyuge: Tiempo Cotizado régimen general" );
            $this->check_numeric($data['conyugue_cotizado_ss_rg_meses'], "Cónyuge: Tiempo Cotizado régimen general" );
            $this->check_numeric($data['conyugue_cotizado_ss_aut_anyos'], "Cónyuge: Tiempo Cotizado autónomos" );
            $this->check_numeric($data['conyugue_cotizado_ss_aut_anyos'], "Cónyuge: Tiempo Cotizado autónomos" );

            if( $data['conyugue_regimen_ss']=='autonomo' || $data['conyugue_regimen_ss']=='ambos') {
                $this->check_numeric($data['conyugue_autonomos_cuota_mensual'], "Cónyuge: Cuota mensual autónomos" );
                if( $data['conyugue_autonomos_cuota_mensual'] < 50 || $data['conyugue_autonomos_cuota_mensual'] > 1118) {
                    $this->log_error("La cuota de autónomos del cónyuge debe estar entre 50€ y 1.118€");            
                    return false;
                }
                $this->check_numeric($data['conyugue_autonomos_ingreso_bruto_mensual'], "Cónyuge: Ingreso bruto mensual autónomos");
                $this->check_numeric($data['conyugue_autonomos_retencion_irpf'], "Cónyuge: Retención IRPF");
            }        

            $this->check_numeric($data['conyugue_rg_neto_mes_ordinario'], "Cónyuge: Régimen general neto mes ordinario");
            $this->check_numeric($data['conyugue_rg_paga_extra'], "Cónyuge: Régimen general paga extra");
            $this->check_numeric($data['conyugue_rg_numero_pagas_extra'], "Cónyuge: Régimen general número de pagas extra");
            $this->check_numeric($data['conyugue_rg_retencion'], "Cónyuge: Régimen general retención" );
            $this->check_numeric($data['conyugue_distintos_ingresos'], "Cónyuge: Ingresos distintos al trabajo");
            $this->check_numeric($data['conyugue_distintos_retencion'], "Cónyuge: Ingresos distintos retención" );
            $this->check_numeric($data['conyugue_ahorro_mensual'], "Cónyuge: Ahorro mensual");
        }
        
        if(strlen($this->config->item('winp3_missing_field'))>0) {
            return false;
        }
  
        return true;
    }
    
    private function check_numeric($value, $message) {
        if( strlen(trim($value))==0){return true;}
        if( !is_numeric(trim($value))) {
            $this->log_error("Valor incorrecto en ".$message);
            return false;
        }
        return true;
    }
}