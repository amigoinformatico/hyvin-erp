<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Entry extends CI_Controller {

    public function welcome() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        $data=[];
        $this->load->view('templates/header');
        $this->load->view('pages/entry', $data);
        $this->load->view('templates/footer');
    }
    
}