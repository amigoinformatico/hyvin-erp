<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Usuarios extends CI_Controller {
    
    private function is_logged() {
        return isset($this->session->userdata['logged_user']);
    }
    private function is_admin() {
        if( !$this->is_logged()) {return false;}
        return $this->session->userdata['logged_user']['administrador'] == '1';
    }
    

    public function lista($params='') {
        if( !$this->is_admin()) {redirect('welcome/index/');}
        
        $this->load->model('User_model');
        $users = $this->User_model->get_all();
        $data=[];        
        $data['users'] = $users;
        if($params == 'ok') {$data['user_added']='';}
        if($params == 'updated') {$data['user_updated']='';}

        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/usuarios', $data);
    }
    
    public function nuevo() {
        if( !$this->is_admin()) {redirect('welcome/index/');}
        $data=[];
        $data['missing_fields'] = "";
        $data['nuevo'] = "";
        $this->load->view('pages/nuevousuario', $data);
    }
    
    public function crear() { 
        if( !$this->is_admin()) {redirect('welcome/index/');}
        $this->load->model('User_model');
        $user_id = $this->User_model->add_user($this);
        
        if($user_id!=0){
            //$this->parametros_model->import_table_user($user_id, "bases_sumas", APPPATH.'/uploads/bases_sumas.txt');
            $this->parametros_model->import_table_user($user_id, "constantes", APPPATH.'/uploads/constantes.txt');
            $this->simulacion_model->nueva_simulacion_usuario($user_id);
            redirect('usuarios/lista/ok');
        }
        else {
            $data['add_error']='';

            $data['user_name'] = $this->input->post('user_name');
            $data['nombre'] = $this->input->post('nombre');
            $data['apellidos'] = $this->input->post('apellidos');
            $data['email'] = $this->input->post('email');
            $data['repetir_email'] = $this->input->post('repetir_email');
            if($this->input->post('tipo_usuario')=='admin'){$data['administrador']=1;}
            else {$data['administrador']=0;}
            $data['contacto_correduria']=$this->input->post('contacto_correduria');
            $data['contacto_nombre'] = $this->input->post('contacto_nombre');
            $data['contacto_telefono_1'] = $this->input->post('contacto_telefono_1');
            $data['contacto_telefono_2'] = $this->input->post('contacto_telefono_2');
            $data['contacto_direccion_1'] = $this->input->post('contacto_direccion_1');
            $data['contacto_direccion_2'] = $this->input->post('contacto_direccion_2');
            $data['contacto_mail'] = $this->input->post('contacto_mail');
            $data['contacto_final_1'] = $this->input->post('contacto_final_1');
            
            $data['missing_fields'] = $this->config->item('winp3_missing_field');    
            $this->load->view('pages/nuevousuario', $data);
        }
    }
    
    public function modificar() {
        if( !$this->is_logged())  {redirect('welcome/index/');}
        if( $this->input->post('user_id') != $this->session->userdata('logged_user')['id'] && !$this->is_admin()) {redirect('welcome/index/');} 

        $this->load->model('User_model');
        
        if($this->User_model->update_user($this)){
            if($this->is_admin()){
                redirect('usuarios/lista/updated');
            }
            else {
                redirect('start/options');
            }
        }
        else {
            $data['edit_error']='';
            $data['editing_user']='1';
            $data['user_id'] = $this->input->post('user_id');
            $data['user_name'] = $this->input->post('user_name');
            $data['nombre'] = $this->input->post('nombre');
            $data['apellidos'] = $this->input->post('apellidos');
            $data['email'] = $this->input->post('email');
            $data['repetir_email'] = $this->input->post('repetir_email');
            if($this->input->post('tipo_usuario')=='admin'){$data['administrador']=1;}
            else {$data['administrador']=0;}
            $data['contacto_correduria']=$this->input->post('contacto_correduria');
            $data['contacto_nombre'] = $this->input->post('contacto_nombre');
            $data['contacto_telefono_1'] = $this->input->post('contacto_telefono_1');
            $data['contacto_telefono_2'] = $this->input->post('contacto_telefono_2');
            $data['contacto_direccion_1'] = $this->input->post('contacto_direccion_1');
            $data['contacto_direccion_2'] = $this->input->post('contacto_direccion_2');
            $data['contacto_mail'] = $this->input->post('contacto_mail');
            $data['contacto_final_1'] = $this->input->post('contacto_final_1');
            
            $data['missing_fields'] = $this->config->item('winp3_missing_field');
            $this->load->view('pages/nuevousuario', $data);
        }
    }
    
    public function edit($user_id=''){
        if( !$this->is_logged()) {redirect('welcome/index/');} 
        if( $user_id=='' && !$this->is_admin()) {redirect('welcome/index/');} 
        if( $user_id != $this->session->userdata('logged_user')['id'] && !$this->is_admin()) {redirect('welcome/index/');} 
        
        if($user_id==''){redirect('/welcome/index/');} 
        
        $this->load->model('User_model');

        $data=$this->User_model->get_user($user_id);
        if($data==='') {redirect('/welcome/index/');} 
        $data['editing_user']='1';
        $data['user_id'] = $user_id;
 
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/nuevousuario', $data);
    }

    public function deactivate($user_id=''){
        if( !$this->is_admin()) {redirect('welcome/index/');}
        
        if($user_id==''){redirect('/index.php/usuarios/lista');}
        $this->load->model('User_model');

        if($this->session->userdata('logged_user')['administrador']!=1) {
            redirect('/index.php/usuarios/lista');
        }

        $data=$this->User_model->deactivate_user($user_id);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        redirect('usuarios/lista/updated');
    }
    
    public function activate($user_id=''){
        if( !$this->is_admin()) {redirect('welcome/index/');}

        if($user_id==''){redirect('/index.php/usuarios/lista');}
        $this->load->model('User_model');

        $data=$this->User_model->activate_user($user_id);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        redirect('usuarios/lista/updated');
    }

}