<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Dependencia extends CI_Controller {
    
    public $m_SimulacionRow;
    public $m_Bases;
    public $m_Titular_Lib;
    public $m_Jubilacion_Lib;
    public $m_Economicos_Lib;
    public $m_Dependencia_Lib;
    public $m_Informa_Bases_Pensiones;
    
    private function log_error($message){
        if(strpos($this->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field').$message." | ");
        }
    }

    public function report() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}       
        
        $data[]='';
        $this->calculate($data);
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $data['current_page'] = 7;
        $this->load->view('templates/header_nav', $data);
        $this->load->view('pages/dependencia', $data);
        $this->load->view('templates/footer_nav', $data);
    }

    public function recalc() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        $data = [];
        if( $this->save_data($data) ) {
            $this->calculate($data);
        }

        $data['winp3_from_recalc'] = 1;
        $data['current_page'] = 7;
        $data['missing_fields'] = $this->config->item('winp3_missing_field');
        $this->load->view('pages/dependencia', $data);
    }

    public function dosier() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}    

        //Cargamos librería PDF:
        $this->load->library('mydompdf');          
        
        $data = [];
        $this->calculate($data);
        $this->load->model('User_model');
        $user = $this->User_model->get_user($this->session->userdata['logged_user']['id']);

        $data['contacto_correduria'] = $user['contacto_correduria'];
        $data['contacto_nombre'] = $user['contacto_nombre'];
        $data['contacto_telefono_1'] = $user['contacto_telefono_1'];
        $data['contacto_telefono_2'] = $user['contacto_telefono_2'];
        $data['contacto_direccion_1'] = $user['contacto_direccion_1'];
        $data['contacto_direccion_2'] = $user['contacto_direccion_2'];
        $data['contacto_mail'] = $user['contacto_mail'];
        $data['contacto_final_1'] = $user['contacto_final_1'];
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];

        //$this->load->view('pages/print/dependencia', $data);

        //Generando pdf:
        $html = $this->load->view('pdf/pdf-resultados-dependencia', $data, true);
        $this->mydompdf->load_html($html);
        $this->mydompdf->render();
        $this->mydompdf->set_base_path('./assets/css/dompdf.css'); //agregar de nuevo el css
        $this->mydompdf->stream("resultados-dependencia.pdf", array(
        "Attachment" => false));
    }

    
    private function calculate(&$data) {
        $params[] = $this;
        $this->load->library('fiscalidad', $params);
        $this->m_Fiscalidad = new Fiscalidad($params);
        $this->load->library('economicos_lib', $params);
        $this->m_Economicos_Lib = new Economicos_Lib($params);
        $this->load->library('pensiones', $params);
        $this->m_Pensiones = new Pensiones($params);
        $this->load->library('constantes', $params);
        $this->m_Constantes = new Constantes($params);
        $this->load->library('informa_bases_pensiones', $params );
        $this->m_Informa_Bases_Pensiones = new Informa_Bases_Pensiones($params);
        $this->load->library('titular_lib', $params );
        $this->m_Titular_Lib = new Titular_Lib($params);
        $this->load->library('jubilacion_lib', $params );
        $this->m_Jubilacion_Lib = new Jubilacion_Lib($params);
        $this->load->library('dependencia_lib', $params );
        $this->m_Dependencia_Lib = new Dependencia_Lib($params);
        $this->load->library('bases', $params);
        $this->m_Bases = new Bases($params);
        
        $this->m_SimulacionRow = $this->simulacion_model->get_all(); 

        //Propios
        $data['dependencia_titular_coste_dependencia_hoy'] = $this->m_SimulacionRow['dependencia_titular_coste_dependencia_hoy'];
        $data['dependencia_titular_anyos_dependencia'] = $this->m_SimulacionRow['dependencia_titular_anyos_dependencia'];
        $data['dependencia_titular_porcentaje_cobertura'] = $this->m_SimulacionRow['dependencia_titular_porcentaje_cobertura'];
        $data['dependencia_titular_anyos_renta'] = $this->m_SimulacionRow['dependencia_titular_anyos_renta'];

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['dependencia_conyugue_coste_dependencia_hoy'] = $this->m_SimulacionRow['dependencia_conyugue_coste_dependencia_hoy'];
            $data['dependencia_conyugue_anyos_dependencia'] = $this->m_SimulacionRow['dependencia_conyugue_anyos_dependencia'];
            $data['dependencia_conyugue_porcentaje_cobertura'] = $this->m_SimulacionRow['dependencia_conyugue_porcentaje_cobertura'];
            $data['dependencia_conyugue_anyos_renta'] = $this->m_SimulacionRow['dependencia_conyugue_anyos_renta'];
        }
        $data['tiene_conyugue'] = $this->m_SimulacionRow['tiene_conyugue'];
        
        
        //Check Defaults
        if($data['dependencia_titular_coste_dependencia_hoy']==null){
            $data['dependencia_titular_coste_dependencia_hoy']=2500;
            $this->m_SimulacionRow['dependencia_titular_coste_dependencia_hoy']=$data['dependencia_titular_coste_dependencia_hoy'];
        }
        if($data['dependencia_titular_anyos_dependencia']==null){
            $data['dependencia_titular_anyos_dependencia']=7;
            $this->m_SimulacionRow['dependencia_titular_anyos_dependencia']=$data['dependencia_titular_anyos_dependencia'];
        }
        if($data['dependencia_titular_porcentaje_cobertura']==null) {
            $data['dependencia_titular_porcentaje_cobertura']=80;
            $this->m_SimulacionRow['dependencia_titular_porcentaje_cobertura']=$data['dependencia_titular_porcentaje_cobertura'];
        }
        if($data['dependencia_titular_anyos_renta']==null){
            $data['dependencia_titular_anyos_renta']=$data['dependencia_titular_anyos_dependencia'];
            $this->m_SimulacionRow['dependencia_titular_anyos_renta']=$data['dependencia_titular_anyos_renta'];
        }
        
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            if($data['dependencia_conyugue_coste_dependencia_hoy']==null){
                $data['dependencia_conyugue_coste_dependencia_hoy']=2500;
                $this->m_SimulacionRow['dependencia_conyugue_coste_dependencia_hoy']=$data['dependencia_conyugue_coste_dependencia_hoy'];
            }
            if($data['dependencia_conyugue_anyos_dependencia']==null){
                $data['dependencia_conyugue_anyos_dependencia']=7;
                $this->m_SimulacionRow['dependencia_conyugue_anyos_dependencia']=$data['dependencia_conyugue_anyos_dependencia'];
            }            
            if($data['dependencia_conyugue_porcentaje_cobertura']==null) {
                $data['dependencia_conyugue_porcentaje_cobertura']=80;
                $this->m_SimulacionRow['dependencia_conyugue_porcentaje_cobertura']=$data['dependencia_conyugue_porcentaje_cobertura'];
            }            
            if($data['dependencia_conyugue_anyos_renta']==null){
                $data['dependencia_conyugue_anyos_renta']=$data['dependencia_titular_anyos_dependencia'];
                $this->m_SimulacionRow['dependencia_conyugue_anyos_renta']=$data['dependencia_conyugue_anyos_renta'];
            }
        }
        
        //Calculados
        $data['titular_fecha_nacimiento'] = $this->m_Dependencia_Lib->titular_fecha_nacimiento()->format("d-m-Y");
        $data['titular_fecha_80_cumpleanyos'] = $this->m_Dependencia_Lib->titular_fecha_80_cumpleanyos()->format("d-m-Y");
        $data['titular_anyos_hasta_dependencia'] = $this->m_Dependencia_Lib->titular_anyos_hasta_dependencia();
        $data['titular_fecha_jubilacion'] = $this->m_Pensiones->titular_fecha_jubilacion();
        $data['titular_edad_jubilacion'] = $this->m_Pensiones->titular_edad_jubilacion();
        $data['titular_anyos_desde_jubilacion_a_dependencia'] = $this->m_Dependencia_Lib->titular_anyos_desde_jubilacion_a_dependencia();
        $data['titular_coste_dependencia_actualizado'] = $this->m_Dependencia_Lib->titular_coste_dependencia_actualizado();
        $data['titular_coste_total_dependencia'] = $this->m_Dependencia_Lib->titular_coste_total_dependencia();
        $data['titular_necesidad_dependencia'] = $this->m_Dependencia_Lib->titular_necesidad_dependencia();
        
        $data['titular_base_ss'] = $this->m_Dependencia_Lib->titular_base_ss();
        $data['titular_base_ss_capital'] = $this->m_Dependencia_Lib->titular_base_ss_capital();
        $data['titular_base_ss_renta'] = $this->m_Dependencia_Lib->titular_base_ss_renta();
        $data['titular_tipo_marginal_ss'] = $this->m_Dependencia_Lib->titular_tipo_marginal_ss();
        $data['titular_tipo_marginal_ss_capital'] = $this->m_Dependencia_Lib->titular_tipo_marginal_ss_capital();
        $data['titular_tipo_marginal_ss_renta'] = $this->m_Dependencia_Lib->titular_tipo_marginal_ss_renta();
        $data['titular_cuota_estatal_ss'] = $this->m_Dependencia_Lib->titular_cuota_estatal_ss();
        $data['titular_cuota_estatal_ss_capital'] = $this->m_Dependencia_Lib->titular_cuota_estatal_ss_capital();
        $data['titular_cuota_estatal_ss_renta'] = $this->m_Dependencia_Lib->titular_cuota_estatal_ss_renta();
        $data['titular_cuota_autonomica_ss'] = $this->m_Dependencia_Lib->titular_cuota_autonomica_ss();
        $data['titular_cuota_autonomica_ss_capital'] = $this->m_Dependencia_Lib->titular_cuota_autonomica_ss_capital();
        $data['titular_cuota_autonomica_ss_renta'] = $this->m_Dependencia_Lib->titular_cuota_autonomica_ss_renta();
        $data['titular_total_cuota_ss'] = $this->m_Dependencia_Lib->titular_total_cuota_ss();
        $data['titular_total_cuota_ss_capital'] = $this->m_Dependencia_Lib->titular_total_cuota_ss_capital();
        $data['titular_total_cuota_ss_renta'] = $this->m_Dependencia_Lib->titular_total_cuota_ss_renta();
        $data['titular_coste_fiscal_capital'] = $this->m_Dependencia_Lib->titular_coste_fiscal_capital();
        $data['titular_coste_fiscal_renta_1'] = $this->m_Dependencia_Lib->titular_coste_fiscal_renta_1();
        $data['titular_coste_fiscal_x'] = $this->m_Dependencia_Lib->titular_coste_fiscal_x();

        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['conyugue_fecha_nacimiento'] = $this->m_Dependencia_Lib->conyugue_fecha_nacimiento()->format("d-m-Y");
            $data['conyugue_fecha_80_cumpleanyos'] = $this->m_Dependencia_Lib->conyugue_fecha_80_cumpleanyos()->format("d-m-Y");
            $data['conyugue_anyos_hasta_dependencia'] = $this->m_Dependencia_Lib->conyugue_anyos_hasta_dependencia();
            $data['conyugue_fecha_jubilacion'] = $this->m_Pensiones->conyugue_fecha_jubilacion();
            $data['conyugue_edad_jubilacion'] = $this->m_Pensiones->conyugue_edad_jubilacion();
            $data['conyugue_anyos_desde_jubilacion_a_dependencia'] = $this->m_Dependencia_Lib->conyugue_anyos_desde_jubilacion_a_dependencia();
            $data['conyugue_coste_dependencia_actualizado'] = $this->m_Dependencia_Lib->conyugue_coste_dependencia_actualizado();
            $data['conyugue_coste_total_dependencia'] = $this->m_Dependencia_Lib->conyugue_coste_total_dependencia();
            $data['conyugue_necesidad_dependencia'] = $this->m_Dependencia_Lib->conyugue_necesidad_dependencia();
        }

//        $data['dependencia_titular_capital_jubilacion'] = $this->m_SimulacionRow['dependencia_titular_capital_jubilacion'];
        
        $data['dependencia_titular_aportacion_mensual'] = $this->m_SimulacionRow['dependencia_titular_aportacion_anual']/12;
        $data['dependencia_titular_aportacion_anual'] = $this->m_SimulacionRow['dependencia_titular_aportacion_anual'];
        $this->m_SimulacionRow['dependencia_titular_aportacion_mensual'] = $data['dependencia_titular_aportacion_mensual']; //Para facilitar calculos
        $this->m_SimulacionRow['dependencia_titular_aportacion_anual'] = $data['dependencia_titular_aportacion_anual']; //Para facilitar calculos
        
        if($this->m_SimulacionRow['tiene_conyugue']==1){
//            $data['dependencia_conyugue_capital_jubilacion'] = $this->m_SimulacionRow['dependencia_conyugue_capital_jubilacion'];
            $data['dependencia_conyugue_aportacion_mensual'] = $this->m_SimulacionRow['dependencia_conyugue_aportacion_anual']/12;
            $data['dependencia_conyugue_aportacion_anual'] = $this->m_SimulacionRow['dependencia_conyugue_aportacion_anual'];
            $this->m_SimulacionRow['dependencia_conyugue_aportacion_mensual'] = $data['dependencia_conyugue_aportacion_mensual']; //Para facilitar calculos
            $this->m_SimulacionRow['dependencia_conyugue_aportacion_anual'] = $data['dependencia_conyugue_aportacion_anual']; //Para facilitar calculos
        }
        
        $data['titular_importe_reducir_base_liquidadora'] = $this->m_Dependencia_Lib->titular_importe_reducir_base_liquidadora();
        $data['titular_tipo_marginal_hoy'] = $this->m_Dependencia_Lib->titular_tipo_marginal_hoy();
        $data['titular_beneficio_fiscal'] = $this->m_Dependencia_Lib->titular_beneficio_fiscal();
        $data['titular_tipo_marginal_jubilacion'] = $this->m_Dependencia_Lib->titular_tipo_marginal_ss();
        $data['titular_beneficio_fiscal_jubilacion'] = $data['titular_importe_reducir_base_liquidadora']*$data['titular_tipo_marginal_jubilacion']/100;
        $data['titular_reparto_cobro_capital_final_capital']=$this->m_Dependencia_Lib->titular_reparto_cobro_capital_final_capital();
        $data['titular_reparto_cobro_capital_final_renta']=$this->m_Dependencia_Lib->titular_reparto_cobro_capital_final_renta();
        $data['titular_contratante'] = $this->m_Dependencia_Lib->titular_contratante();
        $data['titular_modalidad'] = $this->m_Dependencia_Lib->titular_modalidad();
        $data['titular_renta_anual_obtenida']=$this->m_Dependencia_Lib->titular_renta_anual_obtenida();
        $data['titular_renta_mensual_obtenida']=$this->m_Dependencia_Lib->titular_renta_mensual_obtenida();
        $data['titular_incremento_bl'] = $this->m_Dependencia_Lib->titular_incremento_bl();
        
        if($this->m_SimulacionRow['tiene_conyugue']==1){
            $data['conyugue_importe_reducir_base_liquidadora'] = $this->m_Dependencia_Lib->conyugue_importe_reducir_base_liquidadora();
            $data['conyugue_tipo_marginal_hoy'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_hoy();        
            $data['conyugue_beneficio_fiscal'] = $this->m_Dependencia_Lib->conyugue_beneficio_fiscal();
            $data['conyugue_tipo_marginal_jubilacion'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_ss();
            $data['conyugue_beneficio_fiscal_jubilacion'] = $data['conyugue_importe_reducir_base_liquidadora']*$data['conyugue_tipo_marginal_jubilacion']/100;
            $data['conyugue_reparto_cobro_capital_final_capital']=$this->m_Dependencia_Lib->conyugue_reparto_cobro_capital_final_capital();
            $data['conyugue_reparto_cobro_capital_final_renta']=$this->m_Dependencia_Lib->conyugue_reparto_cobro_capital_final_renta();
            $data['conyugue_contratante'] = $this->m_Dependencia_Lib->conyugue_contratante();
            $data['conyugue_modalidad'] = $this->m_Dependencia_Lib->conyugue_modalidad();
            $data['conyugue_renta_anual_obtenida']=$this->m_Dependencia_Lib->conyugue_renta_anual_obtenida();
            $data['conyugue_renta_mensual_obtenida']=$this->m_Dependencia_Lib->conyugue_renta_mensual_obtenida();
            $data['conyugue_incremento_bl'] = $this->m_Dependencia_Lib->conyugue_incremento_bl();
            $data['conyugue_base_ss'] = $this->m_Dependencia_Lib->conyugue_base_ss();
            $data['conyugue_base_ss_capital'] = $this->m_Dependencia_Lib->conyugue_base_ss_capital();
            $data['conyugue_base_ss_renta'] = $this->m_Dependencia_Lib->conyugue_base_ss_renta();
            $data['conyugue_tipo_marginal_ss'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_ss();
            $data['conyugue_tipo_marginal_ss_capital'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_ss_capital();
            $data['conyugue_tipo_marginal_ss_renta'] = $this->m_Dependencia_Lib->conyugue_tipo_marginal_ss_renta();
            $data['conyugue_cuota_estatal_ss'] = $this->m_Dependencia_Lib->conyugue_cuota_estatal_ss();
            $data['conyugue_cuota_estatal_ss_capital'] = $this->m_Dependencia_Lib->conyugue_cuota_estatal_ss_capital();
            $data['conyugue_cuota_estatal_ss_renta'] = $this->m_Dependencia_Lib->conyugue_cuota_estatal_ss_renta();
            $data['conyugue_cuota_autonomica_ss'] = $this->m_Dependencia_Lib->conyugue_cuota_autonomica_ss();
            $data['conyugue_cuota_autonomica_ss_capital'] = $this->m_Dependencia_Lib->conyugue_cuota_autonomica_ss_capital();
            $data['conyugue_cuota_autonomica_ss_renta'] = $this->m_Dependencia_Lib->conyugue_cuota_autonomica_ss_renta();
            $data['conyugue_total_cuota_ss'] = $this->m_Dependencia_Lib->conyugue_total_cuota_ss();
            $data['conyugue_total_cuota_ss_capital'] = $this->m_Dependencia_Lib->conyugue_total_cuota_ss_capital();
            $data['conyugue_total_cuota_ss_renta'] = $this->m_Dependencia_Lib->conyugue_total_cuota_ss_renta();
            $data['conyugue_coste_fiscal_capital'] = $this->m_Dependencia_Lib->conyugue_coste_fiscal_capital();
            $data['conyugue_coste_fiscal_renta_1'] = $this->m_Dependencia_Lib->conyugue_coste_fiscal_renta_1();
            $data['conyugue_coste_fiscal_x'] = $this->m_Dependencia_Lib->conyugue_coste_fiscal_x();
        }
        
    }
    
    public function save() {
        if(!isset($this->session->userdata['logged_user'])) {redirect('welcome/index/');}
        
        // Librerias
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $data = [];
        if( !$this->save_data($data) ) {
            $this->load->view('templates/simulator_header');
            $data['missing_fields'] = $this->config->item('winp3_missing_field');
            $this->load->view('pages/dependencia', $data);
            $this->load->view('templates/footer');
            return;
        }
             
        redirect($this->input->post('page') );
    }

    
    private function save_data(&$data) {
        $this->assign_field('dependencia_titular_coste_dependencia_hoy', $data );
        $this->assign_field('dependencia_titular_anyos_dependencia', $data );
        $this->assign_field('dependencia_titular_porcentaje_cobertura', $data );
        $this->assign_field('dependencia_conyugue_coste_dependencia_hoy', $data );
        $this->assign_field('dependencia_conyugue_anyos_dependencia', $data );
        $this->assign_field('dependencia_conyugue_porcentaje_cobertura', $data );
        $this->assign_field('dependencia_titular_anyos_renta', $data);
        $this->assign_field('dependencia_conyugue_anyos_renta', $data);
//        $this->assign_field('dependencia_titular_capital_jubilacion', $data);
//        $this->assign_field('dependencia_conyugue_capital_jubilacion', $data);
        
        $this->assign_field_titular_aportacion_mensual($data);
        $this->assign_field_conyugue_aportacion_mensual($data);
            
        if( !$this->simulacion_model->save($data)) {
            $this->log_error("Error guardando datos");
            return false;
        }
        
        return true;
    }
    
    private function assign_field( $field_name, &$data ) {
        if( $this->input->post($field_name) != null ) {
            $data[$field_name] = str_replace(".", "", $this->input->post($field_name)); 
            $data[$field_name] = str_replace(",", ".", $data[$field_name]); 
        }
        else {
            $data[$field_name] = null; 
        }
    }
    
    // El Excel calcula con aportacion mensual. Han cambiado despues a aportacion anual pero
    // el Excel sigue suponiendo mensual.
    private function assign_field_titular_aportacion_mensual( &$data ) {
        $field_name_mensual = 'dependencia_titular_aportacion_mensual';
        $field_name_anual = 'dependencia_titular_aportacion_anual';

        if( $this->input->post($field_name_anual) != null) {
            $data[$field_name_anual] = str_replace(".", "", $this->input->post($field_name_anual)); 
            $data[$field_name_anual] = str_replace(",", ".", $data[$field_name_anual]); 
            $data[$field_name_mensual] = $data[$field_name_anual]/12;
        }
        else {
            $data[$field_name_mensual] = null;
            $data[$field_name_anual] = null;
        }
    }

    private function assign_field_conyugue_aportacion_mensual( &$data ) {
        $field_name_mensual = 'dependencia_conyugue_aportacion_mensual';
        $field_name_anual = 'dependencia_conyugue_aportacion_anual';
        if( $this->input->post($field_name_anual) != null) {
            $data[$field_name_anual] = str_replace(".", "", $this->input->post($field_name_anual)); 
            $data[$field_name_anual] = str_replace(",", ".", $data[$field_name_anual]); 
            $data[$field_name_mensual] = $data[$field_name_anual]/12;
        }
        else {
            $data[$field_name_mensual] = null;
            $data[$field_name_anual] = null;
        }
    }

}
