<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: zerocool
 * Date: 09/10/2018
 * Time: 22:41
 */
class Link extends MY_controller
{
    /**
     * Link constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('eloquent/Links');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $data['title'] = 'Enlaces';

        //CSS exclusivos:
        $data['arrcss'] = array(
            'js/plugins/datetimepicker/css/daterangepicker.css'
        );

        //Contenido:
        $data['contenido'] = 'links';
        $data['links'] = Links::all();

        //HTML:
        $this->load->view('includes/template', $data);
    }


    /**
     * GUardamos el enlace
     */
    public function store()
    {
        Links::create([
            'name'=>$this->input->post('enlace')
        ]);

        return redirect('/link');
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
       Links::destroy($id);
       return redirect('link');
    }
}
