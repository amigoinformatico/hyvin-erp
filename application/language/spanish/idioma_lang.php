<?php 
//Castellano:

//Textos genéricos:
$lang['lblusuario'] = "usuario";
$lang['lblnombre'] = "nombre";
$lang['lbltu_nombre'] = "tu nombre";
$lang['lblapellidos'] = "apellidos";
$lang['lbltus_apellidos'] = "tus apellidos";
$lang['lblcontrasena'] = "contraseña";
$lang['lblpassword'] = "password";
$lang['lblenviar'] = "enviar";
$lang['lblcrear'] = "crear";
$lang['lblaceptar'] = "aceptar";
$lang['lblacceder'] = "acceder";
$lang['lblbuscar'] = "buscar";
$lang['lblguardar'] = "guardar";
$lang['lblagregar'] = "agregar";
$lang['lblasociar'] = "asociar";
$lang['lblactualizar'] = "actualizar";
$lang['lblcancelar'] = "cancelar";
$lang['lblsuscribete'] = "suscríbete";
$lang['lblregistrate'] = "regístrate";
$lang['lbldescubre_registro'] = "Descubre un nuevo modo de comprar en internet";
$lang['lblcontactar'] = "contactar";
$lang['lblver'] = "ver";
$lang['lblleer'] = "leer";
$lang['lblcontactanos'] = "contáctanos";
$lang['lblhorario'] = "horario";
$lang['lblfecha'] = "fecha";
$lang['lblautor'] = "autor";
$lang['lblmensaje'] = "mensaje";
$lang['lblestado'] = "estado";
$lang['lblpais'] = "país";
$lang['lblprovincia'] = "provincia";
$lang['lblpoblacion'] = "población";
$lang['lblcp'] = "CP";
$lang['lblcdo_postal'] = "código postal";
$lang['lbldireccion'] = "dirección";
$lang['lbllat'] = "latitud";
$lang['lbllng'] = "longitud";
$lang['lbltelf'] = "teléfono";
$lang['lbltelf_otro'] = "otro teléfono";
$lang['lblhola'] = "hola";
$lang['lblcerrar_sesion'] = "cerrar sesión";
$lang['lblcerrar'] = "cerrar";
$lang['lblpedido'] = "pedido";
$lang['lblcomercio'] = "comercio";
$lang['lblnombre_comercio'] = "nombre del comercio";
$lang['lblcerrado'] = "cerrado";
$lang['lblabierto'] = "abierto";
$lang['lblnuevo_comercio'] = "Crear nuevo comercio";
$lang['lblcomercio_status'] = "Comercio abierto/cerrado";
$lang['lblcomercio_abierto'] = "Comercio abierto";
$lang['lblcomercio_cerrado'] = "Comercio cerrado";
$lang['lblstatus'] = "abierto/cerrado";
$lang['lblcerrado_desde'] = 'Cerrado desde';
$lang['lblcerrado_hasta'] = 'Cerrado hasta';
$lang['lblnuevo_producto'] = "Crear nuevo producto";
$lang['lblproducto'] = "producto";
$lang['lblservicio'] = "servicio";
$lang['lblacepta_las'] = "Acepta las ";
$lang['lblcondiciones_legales'] = "condiciones legales";
$lang['lblcopia_email'] = "copiar mi email personal";
$lang['lblfotos'] = "fotos";
$lang['lblproyecto_coopmercio'] = "Proyecto Coopmercio";
$lang['lbldescarga_pdf_coopmercio'] = "Descárgate y conoce el proyecto Coopmercio";
$lang['lbldescarga_pdf'] = "Descargar PDF";
$lang['lblcondicion'] = "condición";
$lang['lblmodalidades_pago'] = "Modalidades de pago";
$lang['lblmodalidades_entrega'] = "Modalidades de entrega";
$lang['lbldesde'] = "desde";
$lang['lblhasta'] = "hasta";
$lang['lblcuota'] = "cuota";
$lang['lblforma_pago'] = "forma pago";
$lang['lblfecha_pago'] = "fecha pago";
$lang['lblprincipal'] = "principal";
$lang['lblconfirmado'] = "confirmado";
$lang['lbleliminar'] = "eliminar";
$lang['lbltipo'] = "tipo";
$lang['lblver_todos'] = "ver todos";
$lang['administrador'] = "administrador";
$lang['lblimagenes'] = "imágenes";

$lang['lbldelete_imagen'] = "eliminar imagen";
$lang['lblsin_resultados'] = "sin resultados que mostrar";
$lang['lblinicia_sesion_comercio'] = "Debes tener la sesión abierta como usuario para poder crear una tienda.<br><br>
	Si acabas de suscribirte recuerda que primero debes confirmar tus datos a través del email que habrás recibido en tu bandeja de correo.";
$lang['lblinicia_sesion_repartidor'] = "Debes tener la sesión abierta como usuario para poder suscribirte como repartidor.<br><br>
	Si acabas de suscribirte recuerda que primero debes confirmar tus datos a través del email que habrás recibido en tu bandeja de correo.";
$lang['lblsituacion'] = "situación";
$lang['lbleditar'] = "editar";
$lang['lblactivo'] = "activo";
$lang['lblinactivo'] = "inactivo";
$lang['lblpendiente_pago'] = "pendiente pago";
$lang['lblvencimiento'] = "vencimiento";

//Mensajes de validación de formularios:
$lang['warning_eliminar'] = "Sí, eliminar";
$lang['warning_cancelar'] = "No, cancelar";
$lang['valid_nombre'] = "Introduce tu nombre";
$lang['valid_apellidos'] = "Introduce tus apellidos";
$lang['valid_correo'] = "Introduce un email válido";
$lang['valid_pwd'] = "Introduce tu password";
$lang['valid_old_pwd'] = "Introduce tu anterior password";
$lang['valid_min_pwd'] = "Mínimo 6 caracteres";
$lang['valid_max_pwd'] = "Máximo 12 caracteres";
$lang['valid_equal_pwd'] = "Los passwords no coinciden";
$lang['valid_permisos'] = "Indica los permisos";
$lang['valid_min2'] = "Mínimo 2 caracteres";
$lang['valid_min50'] = "Mínimo 50 caracteres";
$lang['valid_pais'] = "Selecciona país";
$lang['valid_provincia'] = "Selecciona provincia";
$lang['valid_poblacion'] = "Selecciona población";
$lang['valid_telf'] = "Introduce un número de teléfono válido";
$lang['valid_nombre_comercio'] = "Introduce nombre del comercio";
$lang['valid_direccion'] = "Indica la dirección";
$lang['valid_cp'] = "Indica el Código Postal";
$lang['valid_tarifa'] = "Indica el concepto de la tarifa";
$lang['valid_tarifa_importe'] = "Debes indicar el importe en la tarifa";
$lang['valid_tarifa_numbers'] = "Sólo se permiten números";
$lang['valid_tarifa_periodo'] = "Indica el período de validez de la tarifa";
$lang['valid_forma_pago'] = "Selecciona la forma de pago";
$lang['valid_nombre_producto'] = "Indica el nombre del producto";
$lang['valid_tipo_producto'] = "Indica el tipo de producto";
$lang['valid_precio_oferta'] = "El precio de oferta no puede ser superior o igual al precio ordinario";
$lang['valid_notif_tipo'] = "Indica el tipo de notificación";
$lang['valid_notif'] = "El campo para la notificación no puede quedar vacío";
$lang['msg_notificacion_ok'] = "Tu notificación se ha enviado correctamente, te responderemos a la mayor brevedad";
$lang['valid_almacen'] = "El almacén debe tener nombre o numeración";
$lang['valid_almacen_tipo'] = "Debes indicar el tipo de almacén";
$lang['valid_categoria_comercio'] = "Indica las categorías del comercio";
$lang['valid_1_opcion'] = "Debes seleccionar al menos 1 opción";
$lang['valid_modalidad_pago'] = "Debes indicar al menos una modalidad de pago";
$lang['valid_modalidad_entrega'] = "Debes indicar al menos una modalidad de entrega";
$lang['valid_proveedor'] = "Debes indicar el proveedor";

//Mensajes Ok y error:
$lang['okuser_updated'] = "Tus datos de cliente se han actualizado correctamente.";
$lang['okpwd_updated'] = "Tu contraseña ha sido modificada correctamente.";
$lang['okmsg_created'] = "Tu mensaje se ha enviado correctamente";
$lang['okproducto_created'] = "El producto se ha creado correctamente";

//Tooltips:
$lang['tip_otro_telf'] = "Otro teléfono ayuda a tus clientes a contactarte";
$lang['tip_whatsapp'] = "Utiliza tu número de whatsapp para recibir mensajes de tus clientes";
$lang['tip_website'] = "Si tienes website o tienda electrónica propia, hazlo saber a los usuarios";
$lang['tip_fecha_apertura'] = "Programa la fecha de apertura de la tienda.  Por defecto es la fecha actual";
$lang['tip_cif'] = "Da confianza y seguridad a tus clientes";
$lang['tip_titulo_tienda_info'] = "Da un título que te caracterice como tienda";
$lang['tip_tags_tienda_info'] = "Escribe las etiquetas que mejor definan tu tienda.  Recuerda que debes separarlas con comas";
$lang['tip_destacado_tienda_info'] = "Escribe un texto breve sobre tu tienda. Lo usaremos para darte a conocer a los clientes";
$lang['tip_texto_tienda_info'] = "Describe cómo es tu tienda, cómo trabajáis, qué ofrecéis a vuestros clientes,...";
$lang['tip_abierto_cerrado'] = "Informa a tus clientes si cierras temporalmente tu e-commerce, de las fechas y el motivo del cierre (vacaciones, inventario,...)";
$lang['tip_modalidades_pago'] = "Selecciona los métodos de pago que vas a habilitar a tus clientes";
$lang['tip_modalidades_entrega'] = "Indica los métodos de entrega de los pedidos a tus clientes.";
$lang['tip_renovar_contrato'] = "Contrato pendiente de renovación";
$lang['tip_categorias_comercio'] = "Indica al menos una categoría para tu comercio. Puedes seleccionar hasta 5";

//Menú:
$lang['menuinicio'] = "inicio";
$lang['menuproyecto'] = "el proyecto";
$lang['menucomercios'] = "comercios";
$lang['menucategorias'] = "categorías";
$lang['menuproductos'] = "productos";
$lang['menucontacto'] = "contacto";
$lang['que_quieres'] = "¿Qué quieres?";

//Footer:
$lang['lblpolitica_privacidad'] = "política de privacidad";
$lang['lbl_politica_cookies'] = "política de cookies";
$lang['lblaviso_legal'] = "aviso legal";

//Registro de usuario:
$lang['regapuntate'] = "Apúntate a Coopmercio";
$lang['regtu_pwd'] = "Tu contraseña";
$lang['regrepite_pwd'] = "Repite contraseña";

//Login:
$lang['lgn_abre_sesion'] = "Abre tu sesión";
$lang['forgot_pwd'] = "¿Olvidaste tu contraseña?";

//Forgot password:
$lang['fgt_recupera'] = "Recupera tu contraseña";
$lang['fgt_no_email'] = "El email introducido no existe en nuestro sistema";
$lang['fgt_send_email'] = "Hemos enviado la contraseña a tu dirección de e-mail";

//Notificaciones:
$lang['ntf_notificanos'] = "Notifica a Coopmercio";
$lang['ntf_notificanos_text'] = "Notifícanos cualquier duda, consulta, sugerencia que tengas o error que hayas detectado en la página en la que te encuentras. Te responderemos a la mayor brevedad. Gracias por tu colaboración.";
$lang['ntf_tipo'] = "Tipo de notificación";
$lang['ntf_qme_cuentas'] = "¡Qué me estás contando!";

//Alta comercio:
$lang['ac_crea_tienda'] = "Crea tu tienda";
$lang['ac_nombre_tienda'] = "Nombre de la tienda";
$lang['ac_tu_tienda'] = "Tu tienda";
$lang['ac_condiciones_legales_comercio'] = "condiciones legales para las tiendas"; 
$lang['ac_fecha_apertura'] = "Fecha de apertura";
$lang['ac_descrip_tienda'] = "Descripción de tu tienda";
$lang['ac_categorias_small'] = "Marca hasta 5 categorías para clasificar tu tienda";
$lang['ac_propon_categoria'] = "Sugiérenos otras categorías que no hayas encontrado en el listado";
$lang['ac_propon_placeholder'] = "Separa tus propuestas con comas";
$lang['ac_descrip_small'] = "Háblanos de tu tienda y sobre todo de vosotros: cómo vendéis, cómo atendéis a vuestros clientes,... Los clientes ya saben qué vendéis, pero les gustaría saber cómo lo hacéis.";
$lang['ac_titulo_tienda'] = "Título";
$lang['ac_separar_comas'] = "separar con comas";
$lang['ac_destacado_tienda'] = "Destacado";
$lang['ac_texto_tienda'] = "Descripción";
$lang['ac_fotos_small'] = "Sube hasta 4 imágenes de tu tienda. Max. peso foto: 500 bytes. Ancho máximo: 840px";
$lang['ac_transf_banca'] = "Transferencia bancaria";
$lang['ac_periodo_gratis'] = "Período gratuito";

//Área privada:
$lang['ap_area_privada'] = "mi área privada";
$lang['ap_menu_usuario'] = "Menú usuario";
$lang['ap_menu_comercio'] = "Menú comercio";
$lang['ap_menu_repartidor'] = "Menú repartidor";
$lang['ap_perfil'] = "mi perfil";
$lang['ap_clave'] = 'mi clave';
$lang['ap_pedidos'] = "mis pedidos";
$lang['ap_notificaciones'] = "mis notificaciones";
$lang['ap_mensajes'] = "mis mensajes";
$lang['ap_incidencias'] = "mis incidencias";
$lang['ap_datos_personales'] = "mis datos personales";
$lang['ap_comercios'] = "mis comercios";
$lang['ap_comercios_alta'] = "nuevo comercio";
$lang['ap_add_administrador'] = "agregar administrador";
$lang['ap_add_almacen'] = "agregar almacén";
$lang['ap_edit_almacen'] = "editar almacén";
$lang['ap_fecha_alta'] = "fecha alta";
$lang['ap_contrato_vigente'] = "contrato vigente hasta";
$lang['ap_contrato_pdte_pago'] = "contrato pendiente de pago";
$lang['ap_contrato_vencido'] = "contrato vencido el ";
$lang['ap_fotos'] = "fotos comercio";
$lang['ap_ventas'] = "mis ventas";
$lang['ap_comercios_pedidos_venta'] = "pedidos de venta";
$lang['ap_comercios_pedidos_compra'] = "pedidos de compra";
$lang['ap_productos'] = "mis productos";
$lang['ap_nproductos'] = "nº productos";
$lang['ap_clave_titulo'] = "Tu clave te permite ganar dinero con Coopmercio";
$lang['ap_clave_texto'] = "Tu <b>clave</b> te permite lo siguiente: <br>
	· Obtener comisión fija por cada comercio que se suscriba a nuestro site que venga recomendado por ti.<br>
	· Premios en metálico a nuestros mejores comerciales.<br>
	· Obtener descuentos especiales en nuestros comercios";
$lang['ap_modificar_pwd'] = "modifica tu password";
$lang['ap_pwd_actual'] = "password actual";
$lang['ap_pwd_nuevo'] = "nuevo password";
$lang['ap_pwd_repetir'] = "repetir nuevo password";
$lang['ap_foto_personal'] = "mi foto personal";
$lang['ap_foto_eliminar'] = "¿Estás seguro de eliminar esta imagen?";
$lang['ap_de'] = "de";
$lang['ap_a'] = "a";
$lang['ap_ver'] = "ver mensajes";
$lang['ap_ver_mensajes'] = "ver todo el hilo de mensajes";
$lang['ap_responder'] = "responder";
$lang['ap_escribe_respuesta'] = "Escribe aquí tu respuesta";
$lang['ap_msg_eliminar'] = "Eliminar mensaje";
$lang['ap_msg_eliminando'] = "¿Estás seguro de eliminar este mensaje?";
$lang['ap_msg_nuevos'] = " mensajes nuevos";
$lang['ap_describe_comercio'] = "Describe tu comercio";
$lang['ap_clientes'] = "clientes";
$lang['ap_abre_tienda'] = "Abre tu tienda";
$lang['ap_abre_tienda_texto'] = "Empieza a vender tus productos o servicios en Coopmercio.";
$lang['ap_abrir_tienda'] = "Abrir tienda";
$lang['ap_hazte_repartidor'] = "Hazte repartidor";
$lang['ap_hazte_repartidor_texto'] = "Trabaja con nosotros repartiendo los productos vendidos.";
$lang['ap_sobre_comercio'] = "Sobre tu comercio";
$lang['ap_medios_pago_aceptados'] = "Métodos de pago aceptados";
$lang['ap_comercio_fotos'] = "Fotos comercio";
$lang['ap_almacenes'] = "Almacenes";
$lang['ap_almacen'] = "almacén";
$lang['ap_nombre_almacen'] = "nombre almacén";
$lang['ap_tipo_almacen'] = "tipo de almacén";
$lang['ap_nivel'] = "nivel";
$lang['ap_comercio_contratos'] = "Contratos";
$lang['ap_comercio_administradores'] = "Administradores";
$lang['ap_datos_generales'] = "Datos generales";
$lang['ap_vista_previa'] = "Vista previa";
$lang['ap_cierre_temporal'] = "Cierre temporal";
$lang['ap_reabrir_comercio'] = "Reabrir comercio";
$lang['ap_reabrir_comercio_aviso'] = "Vas a reabrir la tienda. Esta acción anula el cierre temporal programado";
$lang['ap_ref_venta'] = "ref. venta";
$lang['ap_precio_base'] = "precio base";
$lang['ap_precio_und'] = "precio/un.";
$lang['ap_producto_activo'] = "producto activo";
$lang['ap_producto_inactivo'] = "producto inactivo";
$lang['ap_traspaso_almacenes'] = "Traspaso entre almacenes";
$lang['ap_leido'] = "leído";
$lang['ap_pdte_leer'] = "pendiente leer";
$lang['ap_add_respuesta'] = "Añadir respuesta";
$lang['ap_administrador_dice'] = "administrador dice";
$lang['ap_tu_dices'] = "tu dices";
$lang['ap_respondido'] = "respondido";
$lang['ap_sin_responder'] = "sin responder";
$lang['ap_producto_eliminar'] = "¿Estás seguro de eliminar este producto?";
$lang['ap_datos_basicos'] = "Datos básicos";
$lang['ap_datos_basicos_text'] = "por defecto el producto está desactivado. Recuerda activarlo al completar todos los datos.";
$lang['ap_producto_activo'] = "producto activo";
$lang['ap_producto_nombre'] = "nombre producto";
$lang['ap_producto_tipo'] = "tipo de producto";
$lang['ap_producto_ref'] = "referencia de venta";
$lang['ap_producto_ref_text'] = "referencia interna del vendedor";
$lang['ap_descrip_text'] = "en los distintos idiomas";
$lang['ap_descrip_corta'] = "descripción corta";
$lang['ap_descrip_extensa'] = "descripción extensa";
$lang['ap_precio_ofertas'] = "precio + ofertas";
$lang['ap_atributos'] = "atributos";
$lang['ap_atributos_text'] = "relación de características del producto";
$lang['ap_var_logisticas'] = "variables logísticas";
$lang['ap_proveedores'] = "proveedores";
$lang['ap_proveedor'] = "proveedor";
$lang['ap_marcas'] = "marcas";
$lang['ap_marca'] = "marca";
$lang['ap_ventas'] = "ventas";
$lang['ap_compras'] = "compras";
$lang['ap_lotes'] = "lotes";
$lang['ap_lotes_text'] = "Marcar si el producto lleva gestión de lotes";
$lang['incidencias'] = "incidencias";
$lang['ap_subir_imagen'] = "Subir imagen";
$lang['ap_imagen'] = "imagen";
$lang['ap_medidas_max'] = "medidas máx.";
$lang['ap_peso_max'] = "peso máx.";
$lang['ap_obsv_entrega_pedidos'] = "Observaciones entrega de pedidos";
$lang['ap_subir_avatar'] = "subir avatar";
$lang['ap_precio'] = "precio";
$lang['ap_precio_text'] = "precio unitario. Programar ofertas.";
$lang['ap_unidad_venta'] = "unidad de venta";
$lang['ap_unidad_venta_text'] = "Selecciona la unidad de comercialización del producto";
$lang['ap_venta_granel'] = "Venta a granel";
$lang['ap_venta_granel_text'] = "Indica si el producto admite venta a granel";
$lang['ap_producto_oferta'] = "Producto en oferta";
$lang['ap_precio_oferta'] = "precio oferta";
$lang['ap_cancelar_oferta'] = "cancelar oferta";
$lang['ap_caracteristicas_fisicas'] = "características físicas";
$lang['ap_peso'] = "peso";
$lang['ap_dimensiones'] = "dimensiones";
$lang['ap_alto'] = "alto";
$lang['ap_largo'] = "largo";
$lang['ap_ancho'] = "ancho";
$lang['ap_volumen'] = "volumen";
$lang['ap_ubicacion'] = "ubicación";
$lang['ap_ubicaciones'] = "ubicaciones";
$lang['ap_ubicaciones_text'] = "Relación de ubicaciones posibles para el producto. Separa por comas.";
$lang['ap_cantidad'] = "cantidad";
$lang['ap_movs_stock'] = "movimientos de stock";
$lang['ap_mov'] = "movimiento";
$lang['ap_select_mov_stock'] = "selecciona movimiento de stock";
$lang['ap_num_pedido'] = "nº pedido";
$lang['ap_para_este_art'] = "para este artículo";
$lang['ap_busca_proveedor'] = "busca proveedor";
$lang['ap_nuevo_proveedor'] = "nuevo proveedor";
$lang['ap_add_proveedor'] = "agrega proveedor";
$lang['ap_proveedor_producto'] = "Proveedor del producto";
$lang['ap_proveedor_producto_text'] = "Asocia proveedor y ref. de proveedor a tu producto. En caso de no encontrar el proveedor, créalo.";
$lang['ap_relacion_proveedores'] = "relación de proveedores";
$lang['ap_referencia'] = "referencia";
$lang['ap_referencia_proveedor'] = "referencia proveedor";
$lang['ap_nuevo_proveedor_producto'] = "Crear nuevo proveedor del producto";
$lang['ap_nuevo_proveedor_producto_text'] = "Completa todos los campos para poder realizar pedidos desde la plataforma.";
$lang['ap_escribe_url_completa'] = "escribe la url completa";
$lang['ap_marcas_text'] = "marcas o fabricantes del artículo";
$lang['ap_busca_marca'] = "busca marca";
$lang['ap_relacion_marcas_producto'] = "relación de marcas del producto";
$lang['ap_nueva_marca'] = "nueva marca";
$lang['ap_marca_producto'] = "marca del producto";
$lang['ap_marca_producto_text'] = "Asocia marca o fabricante a tu producto. En caso de no encontrar la marca, créala.";
$lang['ap_crear_nueva_marca'] = "Crear nueva marca para el producto";
$lang['ap_asociar_categorias'] = "Puedes asociar múltiples categorías a cada marca";
$lang['ap_asocia_categorias'] = "Asocia categorías";
$lang['ap_ventas_producto'] = "Ventas del producto";
$lang['ap_comprador'] = "comprador";
$lang['ap_importe'] = "importe";
$lang['ap_compras_producto'] = "compras del producto";
$lang['ap_activar_desactivar_lotes'] = "Activar / desactivar gestión de lotes";

//proyecto.php:
$lang['pyt_sobre_proyecto'] = "Sobre el proyecto";
$lang['pyt_sobre_comercios'] = "Para tu tienda";
$lang['pyt_sobre_clientes'] = "Para todo el mundo";

//contacto.php:
$lang['lblllamanos'] = "Llámanos";
$lang['lblescribenos'] = "Escríbenos";
$lang['lblform_contacto'] = "Formulario de contacto";
$lang['lblnombre_apellidos'] = "Tu nombre y apellidos";
$lang['lbltu_email'] = "Tu email";
$lang['lblasunto'] = "asunto";
$lang['lblescribe_mensaje'] = "Escribe tu mensaje";
?>