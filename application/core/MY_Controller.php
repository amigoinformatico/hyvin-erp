<?php 
if( ! defined('BASEPATH')) exit('No direct script access allowed');

//Super-controlador para extender las funciones comunes a todas las páginas:
class MY_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();

		//Idiomas:
		$this->lang->load('idioma', 'spanish');

		//$this->session->set_userdata('idm',$this->lang->lang());
	}

	/* ***************** INDICE DE MÉTODOS *****************
	1. Token sesión.
	2. Idiomas del site.
	3. Equivalencia de contraseñas.
	4. Comprobar contraseña antigua.
	5. Obtener provincias por país.
	6. Obtener poblaciones por provincia.
	7. Comprobación existencia nick.
	8. Comprobación existencia email.
	9. Obtener latitud y longitud a partir de dirección.
	10. Crear thumbnail de imagen.
	11. Array de estados de incidencias.
	12. Tratamiento imagen documentos. Parámetro: user_id, redireccionamiento y tipo de documento.
	13. Redimensionar imagen.
	14. Inserción de propuestas de usuarios.
	15. Eliminar caché.
	16. Obtener centros por grupo.
	17. Tratamiento .pdf.
		17.1. Tratamiento informe auditoría.
	18. Tratamiento .xls.
	******************************************************** */

	//1. Token sesión:
	public function token(){
		$token = sha1(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}

	//2. Idiomas del site:
	public function getIdiomas(){
		if(!$this->session->userdata('idiomas')){
			//Models:
			$this->load->model('contenidos_model');

			$this->session->set_userdata('idiomas', $this->contenidos_model->getIdiomas(1)); 
			//Parámetro del método getIdiomas: 1 = idiomas activos.
		}
		$idiomas = $this->session->userdata('idiomas');
		return $idiomas;
	}
	
	//3. Equivalencia de contraseñas:
	public function epwd($pwd){
		$pwd2 = $this->input->post('contrasena_bis');
		if($pwd === $pwd2){
			return TRUE;
		}else{
			$this->form_validation->set_message('epwd', 'Las contraseñas no coinciden');
			return FALSE;
		}
	}

	//4. Comprobar contraseña antigua:
	public function dpwd($pwd){
		$pwd2 = $this->input->post('contrasena_orig');
		if(sha1($pwd) === $pwd2){
			return TRUE;
		}else{
			$this->form_validation->set_message('dpwd', 'La contraseña antigua no es correcta');
			return FALSE;
		}
	}	

	//5. Obtener provincias por país:
	public function get_provincias(){
		//Comprobamos que se trata de una llamada ajax:
		if($this->input->is_ajax_request() && $this->input->post('id')){	
			//Limpiamos las variables recibidas
			$id = $this->security->xss_clean($this->input->post('id'));
			$provincia_id = $this->security->xss_clean($this->input->post('provinciaId'));

			//Consulta:
			$q = $this->usuarios_model->getProvincias($id);
			if($q !== FALSE){
				e("<option value=\"\">Selecciona provincia</option>");
				foreach($q as $row){
					e("<option value=\"".$row->provincia_id."\" ".set_select('provincia_id', $row->provincia_id)." ");
					if($row->provincia_id == $provincia_id) e("selected");
					e(">".$row->provincia."</option>");
				}
			}
		}
	}

	//6. Obtener poblaciones por provincia:
	public function get_poblaciones(){
		//Comprobamos que se trata de una llamada ajax:
		if($this->input->is_ajax_request() && $this->input->post('id')){	
			//Limpiamos las variables recibidas
			$id = $this->security->xss_clean($this->input->post('id'));
			$poblacion_id = $this->security->xss_clean($this->input->post('poblacionId'));

			//Consulta:
			$q = $this->usuarios_model->getPoblaciones($id);
			if($q !== FALSE){
				e("<option value=\"\">Selecciona población</option>");
				foreach($q as $row){
					e("<option value=\"".$row->poblacion_id."\" ");
					if($row->poblacion_id == $poblacion_id) e("selected");
					e(">".$row->poblacion."</option>");
				}
			}
		}
	}

	//7. Comprobación existencia nick:
	public function check_nick(){
		//Comprobamos que se trata de una llamada ajax:
		if($this->input->is_ajax_request() && $this->input->post('nick')){
			//Limpiamos las variables recibidas:
			$nick = $this->security->xss_clean($this->input->post('nick'));
			$orig = $this->security->xss_clean($this->input->post('orig'));

			//Consulta:
			$result = $this->usuarios_model->checkNick($nick,$orig);
			if($result == TRUE) e(1);		
		}
	}

	//8. Comprobación existencia email:
	public function check_email(){
		//Comprobamos que se trata de una llamada ajax:
		if($this->input->is_ajax_request() && $this->input->post('correo')){
			//Limpiamos las variables recibidas:
			$correo = $this->security->xss_clean($this->input->post('correo'));
			$orig = $this->security->xss_clean($this->input->post('orig'));

			//Consulta:
			$result = $this->usuarios_model->checkEmail($correo,$orig);
			if($result == TRUE) e(1);			
		}
	}
	
	//9. Obtener latitud y longitud a partir de dirección:
	public function getLatLng($direccion){
		$dir = urlencode($direccion);
		//buscamos la dirección en el servicio de Google:
		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$dir.'&sensor=false');
		//Decodificamos el .json recibido:
		$output = json_decode($geocode);
		if(!empty($output->results)){
			$lat = $output->results[0]->geometry->location->lat;
			$lng = $output->results[0]->geometry->location->lng;

			$arr = array('lat'=>$lat, 'lng'=>$lng);
		}else{
			$arr = array('lat'=>'', 'lng'=>'');
		}
		return $arr;
	}

	//10. Crear thumbnail de imagen:
	public function create_thumbnail($filename,$directorio){
		$config['image_library'] = 'gd2';
		//Recuperamos imagen del directorio:
		$config['source_image'] = 'img/'.$directorio.'/'.$filename;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		//Redimensionamos y guardamos el thumbnail:
		$config['new_image'] = 'img/'.$directorio.'/thumbs/'.$filename;
		$config['width'] = 100;
		$config['height'] = 100;
		$this->load->library('image_lib',$config);
		$this->image_lib->resize();
	}

	//11. Array de estados de incidencias:
	public function arrIncidenciasEstados(){
		$arr = array('ab'=>'abierta','at'=>'atendida','rc'=>'respuesta cliente');
		return $arr;
	}

	//12. Tratamiento imagen documentos. Parámetro: user_id, redireccionamiento y tipo de documento:
	public function imagen_documento($id, $redireccion, $tipo){
		if(isset($_FILES['imagen']) && $_FILES['imagen']['error'] != 4){
			//Obtenemos la extensión del fichero:
			$p = explode('.', $_FILES['imagen']['name']);
			$ext = array_pop($p);

			//1. Configuración parámetros:
			$config['upload_path'] = './img/docs/';
			$config['allowed_types'] = '*';
			//|jpeg|jpg|png|gif|JPEG|JPG|PNG|GIF
			$sufijo = auth_randomText(6);
			$config['file_name'] = 'doc-'.$id.$sufijo.'.'.$ext;
			$config['overwrite'] = TRUE;
			$config['max_size'] = 10000;
			$config['max_width'] = 10000;
			$config['max_height'] = 10000;
			//2. Cargamos librería y le pasamos configuración:
			$this->load->library('upload', $config);
			//3. Error durante la subida:
			if(!$this->upload->do_upload('imagen')){
				$error = $this->upload->display_errors();

				$this->session->set_flashdata('msg_ko','<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$error.'</div>');
					
				redirect("/usuarios/".$redireccion."/".$id."/documentacion","refresh");

			//4. Éxito en la subida:
			}else{
				//Guardamos en tbl_docs:
				$this->usuarios_model->insert_img($id,$config['file_name'],$tipo);
			}
		}		
	}

	//13. Redimensionar imagen:
	public function resizeImg($archivo, $ancho, $alto){
		$config['source_image'] = $archivo;
		$config['width'] = $ancho;
		$config['height'] = $alto;
		$this->load->library('image_lib',$config);
		if(!$this->image_lib->resize()){
			return false;
		}else{
			return true;
		}
	}

	//14. Inserción de propuestas de usuarios:
	public function propuestas(){
		//Comprobamos que se trata de una llamada ajax:
		if($this->input->is_ajax_request() && $this->input->post('propuesta')){
			//Limpiamos las variables recibidas:
			$propuesta = $this->security->xss_clean($this->input->post('propuesta'));
			$tipo = $this->security->xss_clean($this->input->post('tipo'));

			//Array de datos:
			$data = array(
				'user_id' => $this->session->userdata('user_id'),
				'propuesta' => $propuesta,
				'tipo' => $tipo
			);

			//Consulta:
			$result = $this->usuarios_model->insertPropuesta($data);
			if($result == TRUE) e(1);		
		}
	}

	//15. Eliminar caché:
	public function removeCache(){
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		$this->output->set_header('Pragma: no-cache');
	}

	//16. Obtener centros por grupo:
	public function get_centros(){
		//Comprobamos que se trata de una llamada ajax:
		if($this->input->is_ajax_request() && $this->input->post('id')){	
			//Limpiamos las variables recibidas
			$id = $this->security->xss_clean($this->input->post('id'));
			$centro_id = $this->security->xss_clean($this->input->post('centroId'));

			//Consulta:
			$q = $this->admin_model->getCentrosGrupo($id);
			if($q !== FALSE){
				e("<option value=\"\">Selecciona centro</option>");
				foreach($q as $row){
					e("<option value=\"".$row->centro_id."\" ");
					if($row->centro_id == $centro_id) e("selected");
					e(">".$row->centro."</option>");
				}
			}
		}	
	}

	//17. Tratamiento .pdf:
	public function loadpdf($id, $directorio, $redireccion){
		if(isset($_FILES['doc']) && $_FILES['doc']['error'] != 4){
			//1. Configuración parámetros:
			$config['upload_path'] = './img/'.$directorio.'/';
			$config['allowed_types'] = '*';
			//|jpeg|jpg|png|gif|JPEG|JPG|PNG|GIF
			$sufijo = auth_randomText(6);
			$config['file_name'] = 'doc-'.$id.'.pdf';
			$config['overwrite'] = TRUE;
			$config['max_size'] = 80000;
			$config['max_width'] = 10000;
			$config['max_height'] = 10000;
			//2. Cargamos librería y le pasamos configuración:
			$this->load->library('upload', $config);
			//3. Error durante la subida:
			if(!$this->upload->do_upload('doc')){
				$error = $this->upload->display_errors();

				$this->session->set_flashdata('msg_ko','<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$error.'</div>');
					
				redirect("/usuarios/".$redireccion."/".$id."/","refresh");

			//4. Éxito en la subida:
			}else{
				//Guardamos en tbl_users_contratos:
				$this->usuarios_model->insert_doc_contrato($id,$config['file_name']);
			}
		}	
	}

	//17.1. Tratamiento informe auditoría:
	public function load_informe($id){
		if(isset($_FILES['doc']) && $_FILES['doc']['error'] != 4){
			//1. Configuración parámetros:
			$config['upload_path'] = './img/informes/';
			$config['allowed_types'] = '*';
			//|jpeg|jpg|png|gif|JPEG|JPG|PNG|GIF
			$sufijo = auth_randomText(6);
			$config['file_name'] = 'auditoria'.$id.'.pdf';
			$config['overwrite'] = TRUE;
			$config['max_size'] = 800;
			$config['max_width'] = 1000;
			$config['max_height'] = 1000;
			//2. Cargamos librería y le pasamos configuración:
			$this->load->library('upload', $config);
			//3. Error durante la subida:
			if(!$this->upload->do_upload('doc')){
				$error = $this->upload->display_errors();

				$this->session->set_flashdata('msg_ko','<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$error.'</div>');
					
				redirect("/restaurantes/auditorias","refresh");

			//4. Éxito en la subida:
			}else{
				//Actualizamos en tbl_auditorias:
				$this->admin_model->update_doc_auditoria($id,$config['file_name']);
			}
		}			
	}

	//18. Tratamiento .xls:
	public function load_xls($file, $tabla, $fields, $pk){
		if(isset($_FILES['excel']) && $_FILES['excel']['error'] != 4){
			//Verificamos directorio para guardar fichero:
			if(!is_dir("./excel_files/")){
				mkdir("./excel_files/", 0777);
			}

			//comprobamos si el archivo ha subido para poder utilizarlo
    		if($file && copy($_FILES['excel']['tmp_name'],"./excel_files/".$file)){

			    //obtenemos la extensión del archivo
			    $trozos = explode(".", $file);

      			//solo queremos archivos excel
      			if($trozos[1] != "xlsx" && $trozos[1] != "xls") return;

      			//Librerías necesarias:
      			require_once APPPATH . 'libraries/Classes/PHPExcel.php';
      			require_once APPPATH . 'libraries/Classes/PHPExcel/Reader/Excel2007.php';

      			//require_once APPPATH . 'libraries/spreadsheet-reader/php-excel-reader/excel_reader2.php';
      			//require_once APPPATH . 'libraries/spreadsheet-reader/SpreadsheetReader.php';

      			//creamos el objeto que debe leer el excel
     			$objReader = new PHPExcel_Reader_Excel2007();
     			$ruta = "./excel_files/".$file;
      			$objPHPExcel = $objReader->load($ruta);

      			//número de filas del archivo excel
      			$rows = $objPHPExcel->getActiveSheet()->getHighestRow(); 

      			//obtenemos los nombres que el usuario ha introducido en el campo text del formulario,
      			//se supone que deben ser los campos de la tabla de la base de datos.
      			$fields_table = explode(",", $fields);

  				//inicializamos sql como un array
      			$sql = array();

			    //array con las letras de la cabecera de un archivo excel
			    $letras = array(
			        "A","B","C","D","E","F","G",
			        "H","I","J","Q","L","M","N",
			        "O","P","Q","R","S","T","U",
			        "V","W","X","Y","Z"
			    );

      			//recorremos el excel y creamos un array para después insertarlo en la base de datos
      			for($i=1; $i<=$rows; $i++){
        			//ahora recorremos los campos del formulario para ir creando el array de forma dinámica
        			for($z=0; $z<count($fields_table); $z++){
          				$sql[$i][trim($fields_table[$z])] = $objPHPExcel->getActiveSheet()->getCell($letras[$z].$i)->getCalculatedValue();
        			}
      			}   


      			//insertamos los datos del excel en la base de datos
      			$import_excel = $this->admin_model->updFromXls($tabla,$sql,$pk);

      			//finalmente, eliminamos el archivo pase lo que pase
      			unlink("./excel_files/".$file);

      			//comprobamos si se ha guardado bien
      			if($import_excel == TRUE){
        			return TRUE;
      			}else{
        			return FALSE;
      			}
      		}
		}	
	}



}
 