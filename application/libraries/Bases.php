<?php
class Bases {
    private $m_Parent;
    private $m_BR_fallecimiento_incapacidad_revalorizacion;
    
    public function __construct($params) {
        $this->m_Parent = $params[0];
        $this->m_BR_fallecimiento_incapacidad_revalorizacion = array(
            '2017' => 100, '2016' =>100, '2015'=>100, '2014'=>99, '2013'=>99.3,'2012'=>102.18,'2011'=>104.63,'2010'=>107.77
        );
    }

    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }

    //C32
    private function titular_base_cotizacion_autonomo() {
        return $this->m_Parent->m_Economicos_Lib->titular_autonomos_base_cotizacion();
    }
    
    //D32
    private function titular_base_cotizacion_regimen_general() {
        return $this->m_Parent->m_Economicos_Lib->titular_regimen_general_base_cotizacion();
    }
    
    //P34
    public function titular_fallecimiento_base_reguladora() {
        $titular;
        $conyugue;
        $anyo = $this->m_Parent->m_Constantes->anyo_estudio(); 
        $ipc = $this->m_Parent->m_Constantes->ipc();
        $titular_autonomo_base_cotizacion = $this->m_Parent->m_Economicos_Lib->titular_autonomos_base_cotizacion();
        $titular_rg_base_cotizacion = $this->m_Parent->m_Economicos_Lib->titular_regimen_general_base_cotizacion();        
        $conyugue_autonomo_base_cotizacion = $this->m_Parent->m_Economicos_Lib->conyugue_autonomos_base_cotizacion();
        $conyugue_rg_base_cotizacion = $this->m_Parent->m_Economicos_Lib->conyugue_regimen_general_base_cotizacion();        

        $this->m_Parent->simulacion_model->br_fallecimiento_incapacidad($anyo, $ipc, 
                $titular_autonomo_base_cotizacion, $titular_rg_base_cotizacion, 
                $conyugue_autonomo_base_cotizacion, $conyugue_rg_base_cotizacion, 
                $titular, $conyugue);
        
        return $titular*12/14/8;
    }
    
    //Q34
    public function conyugue_fallecimiento_base_reguladora() {
        $titular;
        $conyugue;
        $anyo = $this->m_Parent->m_Constantes->anyo_estudio(); 
        $anyo = $this->m_Parent->m_Constantes->anyo_estudio(); 
        $ipc = $this->m_Parent->m_Constantes->ipc();
        $titular_autonomo_base_cotizacion = $this->m_Parent->m_Economicos_Lib->titular_autonomos_base_cotizacion();
        $titular_rg_base_cotizacion = $this->m_Parent->m_Economicos_Lib->titular_regimen_general_base_cotizacion();        
        $conyugue_autonomo_base_cotizacion = $this->m_Parent->m_Economicos_Lib->conyugue_autonomos_base_cotizacion();
        $conyugue_rg_base_cotizacion = $this->m_Parent->m_Economicos_Lib->conyugue_regimen_general_base_cotizacion();        

        $this->m_Parent->simulacion_model->br_fallecimiento_incapacidad(
                $anyo, 
                $ipc, 
                $titular_autonomo_base_cotizacion, $titular_rg_base_cotizacion,
                $conyugue_autonomo_base_cotizacion, $conyugue_rg_base_cotizacion, 
                $titular, 
                $conyugue);

        return $conyugue*12/14/8;
    }

    //T5
    private function titular_anyo_jubilacion() {
        return $this->m_Parent->m_Pensiones->titular_anyo_jubilacion();
    }
    
    //T6
    private function titular_jubilacion_anyos_para_br() {
        return $this->m_Parent->m_Pensiones->titular_anyos_para_br();
    }

    //Construye la tabla 3Bases.T9
    //X35 
    private function titular_jubilacion_sumas() {
        $titular_autonomo_base_cotizacion = $this->m_Parent->m_Economicos_Lib->titular_autonomos_base_cotizacion();
        $titular_rg_base_cotizacion = $this->m_Parent->m_Economicos_Lib->titular_regimen_general_base_cotizacion();        
        $ipc = $this->m_Parent->m_Constantes->ipc();

        $anyo_jubilacion = $this->titular_anyo_jubilacion();
        $anyos_para_jubilacion = $this->titular_jubilacion_anyos_para_br(); 
        $hasta_anyo = $anyo_jubilacion - $anyos_para_jubilacion;
        
        $sumas_bc_titular = [];
        
        $anyo = $this->m_Parent->m_Constantes->anyo_estudio();
        for( $i = $anyo; $i >= $hasta_anyo; $i--) {
           
            if( $i == $anyo){
                $titular_autonomo = $titular_autonomo_base_cotizacion;
                $titular_rg = $titular_rg_base_cotizacion;
            }
            else {
                $titular_autonomo = $titular_autonomo - ($titular_autonomo*$ipc/100);
                $titular_rg = $titular_rg - ($titular_rg*$ipc/100);
            }

            $b_maxima;
            $historico_ipc;
            $no_minimo;
            $this->m_Parent->simulacion_model->historico_bases_ipc( $i, $no_minimo, $b_maxima, $historico_ipc );
            $sumas_bc_titular[$i] = min($titular_autonomo + $titular_rg, $b_maxima);
        }

        for( $i = $anyo; $i < $anyo+67; $i++) {
            if( $i == $anyo){
                $titular_autonomo = $titular_autonomo_base_cotizacion;
                $titular_rg = $titular_rg_base_cotizacion;
            }
            else {
                $titular_autonomo = $titular_autonomo + ($titular_autonomo*$ipc/100);
                $titular_rg = $titular_rg + ($titular_rg*$ipc/100);
            }
            $b_maxima;
            $no_ipc;
            $no_minimo;
            $this->m_Parent->simulacion_model->historico_bases_ipc( $i, $no_minimo, $b_maxima, $no_ipc );
            $sumas_bc_titular[$i] = min($titular_autonomo + $titular_rg, $b_maxima);
        }

//        $anyo_jubilacion = $this->titular_anyo_jubilacion();
        
        if( $anyo_jubilacion >= $i ) { 
            $this->log_error("El año de jubilación del titular calculado (".$anyo_jubilacion.") debe ser menor que ".($anyo+67));
            return;
        }
        
  //      $anyos_para_jubilacion = $this->titular_jubilacion_anyos_para_br(); 
        $revaloriz=100;
        $suma_bc_reval = 0;
        
        for( $i = $anyo_jubilacion; $i > ($anyo_jubilacion-$anyos_para_jubilacion); $i--){
            $no_b_maxima;
            $no_ipc;
            $no_minimo;
            $this->m_Parent->simulacion_model->historico_bases_ipc( $i, $no_minimo, $no_b_maxima, $historico_ipc );
            
            if( $i > ($anyo_jubilacion-2)){
                $revaloriz = 1;
            }
            else {
                $revaloriz = (1+$historico_ipc/100)*$revaloriz;
            }
            
            $suma_bc_reval = $suma_bc_reval + $sumas_bc_titular[$i]*$revaloriz;
        }
        
        return $suma_bc_reval;
    }

    //X36
    public function titular_jubilacion_base_reguladora() {
        $titular_sumas = $this->titular_jubilacion_sumas();
        $titular_anyos_para_br = $this->titular_jubilacion_anyos_para_br();

        if( $titular_anyos_para_br == 0) {
            $this->log_error("Titular años para BR=0. No se puede calcular Jubilación base reguladora");
            return;
        }
        
        return $titular_sumas/$titular_anyos_para_br*12/14;        
    }

    //AA5
    private function conyugue_anyo_jubilacion() {
        return $this->m_Parent->m_Pensiones->conyugue_anyo_jubilacion();
    }

    //AA6
    private function conyugue_jubilacion_anyos_para_br() {
        return $this->m_Parent->m_Pensiones->conyugue_anyos_para_br();
    }

    //AE35
    private function conyugue_jubilacion_sumas() {
        $conyugue_autonomo_base_cotizacion = $this->m_Parent->m_Economicos_Lib->conyugue_autonomos_base_cotizacion();
        $conyugue_rg_base_cotizacion = $this->m_Parent->m_Economicos_Lib->conyugue_regimen_general_base_cotizacion();        
        $ipc = $this->m_Parent->m_Constantes->ipc();

        $anyo_jubilacion = $this->conyugue_anyo_jubilacion();
        $anyos_para_jubilacion = $this->conyugue_jubilacion_anyos_para_br(); 
        $hasta_anyo = $anyo_jubilacion - $anyos_para_jubilacion;

        $sumas_bc_conyugue = [];
        
        $anyo = $this->m_Parent->m_Constantes->anyo_estudio();

        for( $i = $anyo; $i >= $hasta_anyo; $i--) {
           
            if( $i == $anyo){
                $conyugue_autonomo = $conyugue_autonomo_base_cotizacion;
                $conyugue_rg = $conyugue_rg_base_cotizacion;
            }
            else {
                $conyugue_autonomo = $conyugue_autonomo - ($conyugue_autonomo*$ipc/100);
                $conyugue_rg = $conyugue_rg - ($conyugue_rg*$ipc/100);
            }

            $b_maxima;
            $historico_ipc;
            $no_minimo;
            $this->m_Parent->simulacion_model->historico_bases_ipc( $i, $no_minimo, $b_maxima, $historico_ipc );
            $sumas_bc_conyugue[$i] = min($conyugue_autonomo + $conyugue_rg, $b_maxima);
        }
        
        for( $i = $anyo; $i < $anyo+67; $i++) {
            if( $i == $anyo){
                $conyugue_autonomo = $conyugue_autonomo_base_cotizacion;
                $conyugue_rg = $conyugue_rg_base_cotizacion;
            }
            else {
                $conyugue_autonomo = $conyugue_autonomo + ($conyugue_autonomo*$ipc/100);
                $conyugue_rg = $conyugue_rg + ($conyugue_rg*$ipc/100);
            }
            $b_maxima;
            $no_ipc;
            $no_minimo;
            $this->m_Parent->simulacion_model->historico_bases_ipc( $i, $no_minimo, $b_maxima, $no_ipc );
            $sumas_bc_conyugue[$i] = min($conyugue_autonomo + $conyugue_rg, $b_maxima);
        }
        
        if( $anyo_jubilacion >= $i ) { 
            $this->log_error("El año de jubilación del cónyuge calculado (".$anyo_jubilacion.") debe ser menor que ".($anyo+67));
            return;
        }

        $revaloriz=100;
        $suma_bc_reval = 0;
        for( $i = $anyo_jubilacion; $i > ($anyo_jubilacion-$anyos_para_jubilacion); $i--){
            $no_b_maxima;
            $no_ipc;
            $no_minimo;
            $this->m_Parent->simulacion_model->historico_bases_ipc( $i, $no_minimo, $no_b_maxima, $historico_ipc );
            
            if( $i > ($anyo_jubilacion-2)){
                $revaloriz = 1;
            }
            else {
                $revaloriz = (1+$historico_ipc/100)*$revaloriz;
            }
            
            $suma_bc_reval = $suma_bc_reval + $sumas_bc_conyugue[$i]*$revaloriz;
        }
        
        return $suma_bc_reval;
    }

    
    
    //AE36
    public function conyugue_jubilacion_base_reguladora() {
        $conyugue_sumas = $this->conyugue_jubilacion_sumas();
        $conyugue_anyos_para_br = $this->conyugue_jubilacion_anyos_para_br();
        
        if( $conyugue_anyos_para_br == 0 ) {return 0;}
        
        return $conyugue_sumas/$conyugue_anyos_para_br*12/14;        
    }
    
}    
?>