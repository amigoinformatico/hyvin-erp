<?php
class Fiscalidad {
    
    private $m_Parent;
    
    public function __construct($params) {
        $this->m_Parent = $params[0];
    }
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }

    
    //B69
    private function minimo_vital_irpf_hasta_64() {
        return $this->m_Parent->parametros_model->get_all()->minimo_vital_irpf_hasta_64;
    }
    
    //B70
    private function minimo_vital_irpf_65_74() {
        return $this->m_Parent->parametros_model->get_all()->minimo_vital_irpf_hasta_65_74;
    }
    
    //B71
    private function minimo_vital_75() {
        return $this->m_Parent->parametros_model->get_all()->minimo_vital_irpf_75;
    }
    
    //B73
    private function por_descendientes_primero() {
        return $this->m_Parent->parametros_model->get_all()->por_descendientes_primero;
    }
    
    //B74
    private function por_descendientes_segundo() {
        return $this->m_Parent->parametros_model->get_all()->por_descendientes_segundo;
    }
    
    //B75
    private function por_descendientes_tercero() {
        return $this->m_Parent->parametros_model->get_all()->por_descendientes_tercero;
    }
    
    //B76
    private function por_descendientes_cuarto() {
        return $this->m_Parent->parametros_model->get_all()->por_descendientes_cuarto;
    }
    
    //B80
    private function aplicamos_hijos_1() {
        return $this->por_descendientes_primero()/2;
    }

    //B81
    private function aplicamos_hijos_2() {
        return $this->aplicamos_hijos_1()+$this->por_descendientes_segundo()/2;
    }
    
    //B82
    private function aplicamos_hijos_3() {
        return $this->aplicamos_hijos_2()+$this->por_descendientes_tercero()/2;
    }

    //B84
    private function aplicamos_hijos_4() {
        return $this->aplicamos_hijos_3()+$this->por_descendientes_cuarto()/2;
    }

    //B85
    private function aplicamos_hijos_5() {
        return $this->aplicamos_hijos_4()+$this->por_descendientes_cuarto()/2;
    }

    //B86
    private function aplicamos_hijos_6() {
        return $this->aplicamos_hijos_5()+$this->por_descendientes_cuarto()/2;
    }

    //B87
    private function aplicamos_hijos_7() {
        return $this->aplicamos_hijos_6()+$this->por_descendientes_cuarto()/2;
    }
    
    //K72
    private function titular_ingresos_brutos_anuales() {
        return $this->m_Parent->m_Economicos_Lib->titular_total_ingresos_bruto_anual();
    }
    
    //K74
    private function titular_min_vit_personal() {
        return $this->minimo_vital_irpf_hasta_64();
    }
    
    //K75
    private function titular_personas_derecho_orfandad() {
        return $this->m_Parent->m_Titular_Lib->titular_hijos_derecho_orfandad();
    }
    
    //K76
    private function titular_min_vit_x_desc() {
        $orfandad = $this->titular_personas_derecho_orfandad();
        switch($orfandad) {
            case 0: return 0;
            case 1: return $this->aplicamos_hijos_1();
            case 2: return $this->aplicamos_hijos_2();
            case 3: return $this->aplicamos_hijos_3();
            case 4: return $this->aplicamos_hijos_4();
            case 5: return $this->aplicamos_hijos_5();
            default:$this->log_error("Fiscalidad. Titular min vit x desc: Número de hijos no tratado=".$orfandad);
        }
    }
    
    
    //K78
    private function calculo_generales_hoy_titular_base_liquidable() {
        $titular_ingresos_brutos_anuales = $this->titular_ingresos_brutos_anuales();
        $titular_min_vit_personal = $this->titular_min_vit_personal();
        $titular_min_vit_x_desc = $this->titular_min_vit_x_desc();

        if($this->m_Parent->config->item("winp3_missing_field") != "") {return; }
        
        return max(0,$titular_ingresos_brutos_anuales-$titular_min_vit_personal-$titular_min_vit_x_desc);
    }
    
    //K80
    private function calculo_generales_hoy_titular_tipo_marginal_estatal() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->calculo_generales_hoy_titular_base_liquidable();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;
        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);

        
        /*
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_base_liquidable();
        $columna = 1;
        return $this->m_Parent->fiscalidad_model->get_irpf($comunidad_autonoma, $fila, $columna, $base_liquidable);
         */
    }

    //K81
    private function calculo_generales_hoy_titular_tipo_marginal_autonomico() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->calculo_generales_hoy_titular_base_liquidable();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }

        $columna = 4;
        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //K82
    public function calculo_generales_hoy_titular_tipo_marginal_total() { 
        $titular_tipo_marginal_estatal = $this->calculo_generales_hoy_titular_tipo_marginal_estatal();
        $titular_tipo_marginal_autonomico = $this->calculo_generales_hoy_titular_tipo_marginal_autonomico();
        
        if( $this->m_Parent->config->item('winp3_missing_field') != "" ) {return;}
        
        return $titular_tipo_marginal_estatal + $titular_tipo_marginal_autonomico;
    }
    
    //K85
    public function calculo_generales_titular_ingresos_brutos_anuales() {
        return $this->m_Parent->m_Pensiones->titular_jubilacion_pension()*14;
    }

    //L6
    private function beneficiario_relacion($beneficiario) {
        return $this->m_Parent->m_SimulacionRow['fallecimiento_beneficiario_'.$beneficiario.'_relacion'];
    }

    //L7
    private function beneficiario_edad_descendiente($beneficiario) {
        return min( $this->m_Parent->m_Fallecimiento_Lib->beneficiario_edad($beneficiario), 25);
    }
    
    //L8
    private function beneficiario_importe_cobrar($beneficiario) {
        return $this->m_Parent->m_Fallecimiento_Lib->beneficiario_importe_cobrar_seguro_vida($beneficiario);
    }

    //L10
    private function beneficiario_reduccion_seguro_vida($beneficiario) {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $titular;
        $conyugue;
        $edad_beneficiario=0;
        $beneficiario;
        $this->m_Parent->fiscalidad_model->reduccion_por_seguro_vida($comunidad_autonoma, $titular, $conyugue, $edad_beneficiario, $beneficiario);
        return $titular;
    }
    
    //L11
    private function beneficiario_reduccion_conyugue($beneficiario){
        if($this->beneficiario_relacion($beneficiario)!='conyugue'){return 0;}
        
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $titular;
        $conyugue;
        $edad_beneficiario=0;
        $beneficiario;
        $this->m_Parent->fiscalidad_model->reduccion_por_seguro_vida($comunidad_autonoma, $titular, $conyugue, $edad_beneficiario, $beneficiario);
        return $conyugue;
    }
    
    //L12
    private function beneficiario_reduccion_descendiente($beneficiario){
        if($this->beneficiario_relacion($beneficiario)!='descendiente'){return 0;}
        
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $titular;
        $conyugue;
        $edad_beneficiario=$this->beneficiario_edad_descendiente($beneficiario);
        $beneficiario;
        $this->m_Parent->fiscalidad_model->reduccion_por_seguro_vida($comunidad_autonoma, $titular, $conyugue, $edad_beneficiario, $beneficiario);
        return $beneficiario;
    }
    
    //L14
    private function beneficiario_importe_menos_reducciones($beneficiario) {
        $importe_cobrar = $this->beneficiario_importe_cobrar($beneficiario); //L8
        $reduccion_sv = $this->beneficiario_reduccion_seguro_vida($beneficiario); //L10
        $reduccion_conyugue = $this->beneficiario_reduccion_conyugue($beneficiario); //L11
        $reduccion_descendiente = $this->beneficiario_reduccion_descendiente($beneficiario); //L12
        
        return $importe_cobrar-$reduccion_sv-$reduccion_conyugue-$reduccion_descendiente;
    }
    
    //L16
    private function calculos_beneficiario_base_liquidable($beneficiario) {
        return max($this->beneficiario_importe_menos_reducciones($beneficiario), 0);
    }
    
    //L18
    private function beneficiario_base_liquidable_hasta($beneficiario) {
        $base_liquidable = $this->calculos_beneficiario_base_liquidable($beneficiario); //L16
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        return $this->m_Parent->fiscalidad_model->isd_por_ccaa($comunidad_autonoma, $base_liquidable, 1);
    }
    
    //L19
    private function beneficiario_cuota_i($beneficiario) {
        $base_liquidable = $this->calculos_beneficiario_base_liquidable($beneficiario); //L16
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        return $this->m_Parent->fiscalidad_model->isd_por_ccaa($comunidad_autonoma, $base_liquidable, 2);
    }
    
    //L20
    private function beneficiario_resto_base_liquidable($beneficiario) {
        $base_liquidable = $this->calculos_beneficiario_base_liquidable($beneficiario); //L16
        $base_liquidable_hasta = $this->beneficiario_base_liquidable_hasta($beneficiario); //L18

        return $base_liquidable - $base_liquidable_hasta;
    }
    
    //L21
    private function beneficiario_tipo($beneficiario) {
        $base_liquidable = $this->calculos_beneficiario_base_liquidable($beneficiario);
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        
        return $this->m_Parent->fiscalidad_model->isd_por_ccaa($comunidad_autonoma, $base_liquidable, 4);
    }
    
    //L22
    private function beneficiario_cuota_ii($beneficiario) {
        return $this->beneficiario_resto_base_liquidable($beneficiario)*$this->beneficiario_tipo($beneficiario)/100;
    }
    
    //L23
    public function beneficiario_total_cuota($beneficiario) {
        $beneficiario_cuota_i = $this->beneficiario_cuota_i($beneficiario);//L19
        $beneficiario_cuota_ii = $this->beneficiario_cuota_ii($beneficiario);//L22
        
        if( $this->m_Parent->config->item('winp3_missing_field') != "" ) {return;}
        
        return $beneficiario_cuota_i + $beneficiario_cuota_ii;
    }
    

    //L72
    private function conyugue_ingresos_brutos_anuales() {
        return $this->m_Parent->m_Economicos_Lib->conyugue_total_ingresos_bruto_anual();
    }
    
    //L74
    private function conyugue_min_vit_personal() {
        return $this->minimo_vital_irpf_hasta_64();
    }

    //L75
    private function conyugue_personas_derecho_orfandad() {
        return $this->m_Parent->m_Titular_Lib->conyugue_hijos_derecho_orfandad();
    }
   
    //L76
    private function conyugue_min_vit_x_desc() {
        $orfandad = $this->conyugue_personas_derecho_orfandad();

        switch($orfandad) {
            case 0: return 0;
            case 1: return $this->aplicamos_hijos_1();
            case 2: return $this->aplicamos_hijos_2();
            case 3: return $this->aplicamos_hijos_3();
            case 4: return $this->aplicamos_hijos_4();
            case 5: return $this->aplicamos_hijos_5();
            default:$this->log_error("Fiscalidad. Titular min vit x desc: Número de hijos no tratado=".$orfandad);
        }

    }
    
    
    //L78 
    private function calculo_generales_hoy_conyugue_base_liquidable() {
        $conyugue_ingresos_brutos_anuales = $this->conyugue_ingresos_brutos_anuales();
        $conyugue_min_vit_personal = $this->conyugue_min_vit_personal();
        $conyugue_min_vit_x_desc = $this->conyugue_min_vit_x_desc();

        if($this->m_Parent->config->item("winp3_missing_field") != "") {return 0; }
        
        return max(0, $conyugue_ingresos_brutos_anuales-$conyugue_min_vit_personal-$conyugue_min_vit_x_desc );
    }
    
    //L80
    private function calculo_generales_hoy_conyugue_tipo_marginal_estatal() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->calculo_generales_hoy_conyugue_base_liquidable();

        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;
        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //L81
    private function calculo_generales_hoy_conyugue_tipo_marginal_autonomico() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->calculo_generales_hoy_conyugue_base_liquidable();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }

        $columna = 4;
        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //L82
    public function calculo_generales_hoy_conyugue_tipo_marginal_total() { 
        $conyugue_tipo_marginal_estatal = $this->calculo_generales_hoy_conyugue_tipo_marginal_estatal();
        $conyugue_tipo_marginal_autonomico = $this->calculo_generales_hoy_conyugue_tipo_marginal_autonomico();

        if( $this->m_Parent->config->item('winp3_missing_field') != "" ) {return;}
        
        return $conyugue_tipo_marginal_estatal + $conyugue_tipo_marginal_autonomico;
    }
    
    //L85
    public function calculo_generales_conyugue_ingresos_brutos_anuales() {
        return $this->m_Parent->m_Pensiones->conyugue_jubilacion_pension()*14;
    }


    //P70
    private function titular_it_indemnizacion() {
        return $this->m_Parent->m_It_Lib->titular_indemnizacion();
    }
    
    //P72
    private function titular_it_base_liquidable_hasta() {
        $titular_it_indemnizacion = $this->titular_it_indemnizacion();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $titular_it_indemnizacion, $columna);
    }
    
    //P73
    private function titular_it_cuota_i() {
        $titular_it_indemnizacion = $this->titular_it_indemnizacion();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $titular_it_indemnizacion, $columna);
    }
    
    //P74
    private function titular_it_resto_base_liquidable() {
       return $this->titular_it_indemnizacion()-$this->titular_it_base_liquidable_hasta();
    }
    
    //P75
    private function titular_it_tipo() {
        $titular_it_indemnizacion = $this->titular_it_indemnizacion();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $titular_it_indemnizacion, $columna);
    }
    
    //P76
    private function titular_it_cuota_ii() {
        $titular_it_tipo = $this->titular_it_tipo();
        $titular_it_resto_base_liquidable = $this->titular_it_resto_base_liquidable();
        if( $this->m_Parent->config->item('winp3_missing_field') != "" ) {return;}

        return $titular_it_tipo*$titular_it_resto_base_liquidable/100;
    }
    
    //
    //P77
    public function titular_it_total_cuota() {
        $titular_it_cuota_i = $this->titular_it_cuota_i();
        $titular_it_cuota_ii = $this->titular_it_cuota_ii();
        if( $this->m_Parent->config->item('winp3_missing_field') != "" ) {return;}
        
        return $titular_it_cuota_i + $titular_it_cuota_ii;
    }
    
    //P81
    private function titular_invalidez_capital_prima() {
        $titular_invalidez_capital_propuesto = $this->m_Parent->m_Invalidez_Lib->titular_capital_propuesto();
        $titular_invalidez_prima_aproximada = $this->m_Parent->m_Invalidez_Lib->titular_prima_aproximada();

        return $titular_invalidez_capital_propuesto-$titular_invalidez_prima_aproximada;
    }
    
    //P83
    private function titular_invalidez_base_liquidable_hasta() {
        $titular_invalidez_capital_prima = $this->titular_invalidez_capital_prima();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 1;
        
        if($titular_invalidez_capital_prima===null) {
            $this->log_error(" Titular invalidez base liquidable hasta: Falta capital prima");
            return;
        }

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $titular_invalidez_capital_prima, $columna);
    }
    
    //P84
    private function titular_invalidez_cuota_i() {
        $titular_invalidez_capital_prima = $this->titular_invalidez_capital_prima();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $titular_invalidez_capital_prima, $columna);
    }
    
    //P85
    private function titular_invalidez_resto_base_liquidable() {
        $titular_invalidez_capital_prima = $this->titular_invalidez_capital_prima();
        $titular_invalidez_base_liquidable_hasta = $this->titular_invalidez_base_liquidable_hasta();

        if( $this->m_Parent->config->item("win3p_missing_field")!="") {return;}

        return $titular_invalidez_capital_prima-$titular_invalidez_base_liquidable_hasta;
    }
    
    //P86
    private function titular_invalidez_tipo() {
        $titular_invalidez_capital_prima = $this->titular_invalidez_capital_prima();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 4;
        
        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $titular_invalidez_capital_prima, $columna);
    }
    
    //P87
    private function titular_invalidez_cuota_ii() {
        $titular_invalidez_tipo = $this->titular_invalidez_tipo();
        $titular_invalidez_resto_base_liquidable = $this->titular_invalidez_resto_base_liquidable();
        
        return $titular_invalidez_resto_base_liquidable*$titular_invalidez_tipo/100;
    }
    
    //P88
    public function titular_invalidez_total_cuota() {
        $titular_invalidez_cuota_i = $this->titular_invalidez_cuota_i();
        $titular_invalidez_cuota_ii = $this->titular_invalidez_cuota_ii();
        
        return $titular_invalidez_cuota_i + $titular_invalidez_cuota_ii;
    }

    //Q70
    private function conyugue_it_indemnizacion() {
        return $this->m_Parent->m_It_Lib->conyugue_indemnizacion();
    }
    
    //Q72
    private function conyugue_it_base_liquidable_hasta() {
        $conyugue_it_indemnizacion = $this->conyugue_it_indemnizacion();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $conyugue_it_indemnizacion, $columna);
    }
    
    //Q73
    private function conyugue_it_cuota_i() {
        $conyugue_it_indemnizacion = $this->conyugue_it_indemnizacion();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $conyugue_it_indemnizacion, $columna);
    }
    
    //Q74
    private function conyugue_it_resto_base_liquidable() {
       return $this->conyugue_it_indemnizacion()-$this->conyugue_it_base_liquidable_hasta();
    }
    
    //Q75
    private function conyugue_it_tipo() {
        $conyugue_it_indemnizacion = $this->conyugue_it_indemnizacion();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $conyugue_it_indemnizacion, $columna);
    }
    
    //Q76
    private function conyugue_it_cuota_ii() {
        $conyugue_it_tipo = $this->conyugue_it_tipo();
        $conyugue_it_resto_base_liquidable = $this->conyugue_it_resto_base_liquidable();
        if( $this->m_Parent->config->item('winp3_missing_field') != "" ) {return;}

        return $conyugue_it_tipo*$conyugue_it_resto_base_liquidable/100;
    }
    
    //
    //Q77
    public function conyugue_it_total_cuota() {
        $conyugue_it_cuota_i = $this->conyugue_it_cuota_i();
        $conyugue_it_cuota_ii = $this->conyugue_it_cuota_ii();
        if( $this->m_Parent->config->item('winp3_missing_field') != "" ) {return;}
        
        return $conyugue_it_cuota_i + $conyugue_it_cuota_ii;
    }
    
    //Q81
    private function conyugue_invalidez_capital_prima() {
        $conyugue_invalidez_capital_propuesto = $this->m_Parent->m_Invalidez_Lib->conyugue_capital_propuesto();
        $conyugue_invalidez_prima_aproximada = $this->m_Parent->m_Invalidez_Lib->conyugue_prima_aproximada();

        return $conyugue_invalidez_capital_propuesto-$conyugue_invalidez_prima_aproximada;
    }
    
    //Q83
    private function conyugue_invalidez_base_liquidable_hasta() {
        $conyugue_invalidez_capital_prima = $this->conyugue_invalidez_capital_prima();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 1;
        
        if($conyugue_invalidez_capital_prima===null) {
            $this->log_error("Cónyuge invalidez base liquidable hasta: Falta capital prima");
            return;
        }
        
        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $conyugue_invalidez_capital_prima, $columna);
    }
    
    //Q84
    private function conyugue_invalidez_cuota_i() {
        $conyugue_invalidez_capital_prima = $this->conyugue_invalidez_capital_prima();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 2;
        
        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $conyugue_invalidez_capital_prima, $columna);
    }
    
    //Q85
    private function conyugue_invalidez_resto_base_liquidable() {
        $conyugue_invalidez_capital_prima = $this->conyugue_invalidez_capital_prima();
        $conyugue_invalidez_base_liquidable_hasta = $this->conyugue_invalidez_base_liquidable_hasta();

        if( $this->m_Parent->config->item("win3p_missing_field")!="") {return;}

        return $conyugue_invalidez_capital_prima-$conyugue_invalidez_base_liquidable_hasta;
    }
    
    //Q86
    private function conyugue_invalidez_tipo() {
        $conyugue_invalidez_capital_prima = $this->conyugue_invalidez_capital_prima();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 4;
        
        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $conyugue_invalidez_capital_prima, $columna);
    }
    
    //Q87
    private function conyugue_invalidez_cuota_ii() {
        $conyugue_invalidez_tipo = $this->conyugue_invalidez_tipo();
        $conyugue_invalidez_resto_base_liquidable = $this->conyugue_invalidez_resto_base_liquidable();
        
        return $conyugue_invalidez_resto_base_liquidable*$conyugue_invalidez_tipo/100;
    }
    
    //Q88
    public function conyugue_invalidez_total_cuota() {
        $conyugue_invalidez_cuota_i = $this->conyugue_invalidez_cuota_i();
        $conyugue_invalidez_cuota_ii = $this->conyugue_invalidez_cuota_ii();
        
        return $conyugue_invalidez_cuota_i + $conyugue_invalidez_cuota_ii;
    }
    
    //U74
    public function titular_jubilacion_bl_solo_ss() {
        return $this->calculo_generales_titular_ingresos_brutos_anuales();
    }
    
    //U76
    public function titular_jubilacion_bl_hasta_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //U77
    public function titular_jubilacion_bl_arriba_cuota_i_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //U78
    public function titular_jubilacion_bl_arriba_resto_bl_solo_ss() {
        return $this->titular_jubilacion_bl_solo_ss()-$this->titular_jubilacion_bl_hasta_solo_ss();
    }
    
    //U79
    public function titular_jubilacion_bl_arriba_tipo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //U80
    public function titular_jubilacion_bl_arriba_cuota_ii_solo_ss() {
        return $this->titular_jubilacion_bl_arriba_tipo_solo_ss()*$this->titular_jubilacion_bl_arriba_resto_bl_solo_ss()/100;
    }
    
    //U81
    public function titular_jubilacion_arriba_total_cuota_solo_ss() {
        return $this->titular_jubilacion_bl_arriba_cuota_i_solo_ss()+$this->titular_jubilacion_bl_arriba_cuota_ii_solo_ss();
    }
    
    //U83
    public function titular_jubilacion_bl_hasta_abajo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //U84
    public function titular_jubilacion_bl_abajo_cuota_i_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //U85
    public function titular_jubilacion_bl_abajo_resto_bl_solo_ss() {
        return $this->titular_jubilacion_bl_solo_ss()-$this->titular_jubilacion_bl_hasta_abajo_solo_ss();
    }

    
    //U86
    public function titular_jubilacion_bl_abajo_tipo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //U87
    public function titular_jubilacion_bl_abajo_cuota_ii_solo_ss() {
        return $this->titular_jubilacion_bl_abajo_tipo_solo_ss()*$this->titular_jubilacion_bl_abajo_resto_bl_solo_ss()/100;
    }
    
    //U88
    public function titular_jubilacion_abajo_total_cuota_solo_ss() {
        return $this->titular_jubilacion_bl_abajo_cuota_i_solo_ss()+$this->titular_jubilacion_bl_abajo_cuota_ii_solo_ss();
    }


    //V74
    public function titular_jubilacion_bl_ss_capital() {
        return $this->calculo_generales_titular_ingresos_brutos_anuales()+$this->m_Parent->m_Jubilacion_Lib->titular_incremento_bl();
    }

    //V76
    public function titular_jubilacion_bl_hasta_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    
    //V77
    public function titular_jubilacion_bl_arriba_cuota_i_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //V78
    public function titular_jubilacion_bl_arriba_resto_bl_ss_capital() {
        return $this->titular_jubilacion_bl_ss_capital()-$this->titular_jubilacion_bl_hasta_ss_capital();
    }


    //V79
    public function titular_jubilacion_bl_arriba_tipo_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //V80
    public function titular_jubilacion_bl_arriba_cuota_ii_ss_capital() {
        return $this->titular_jubilacion_bl_arriba_tipo_ss_capital()*$this->titular_jubilacion_bl_arriba_resto_bl_ss_capital()/100;
    }

    
    //V81
    public function titular_jubilacion_arriba_total_cuota_ss_capital() {
        return $this->titular_jubilacion_bl_arriba_cuota_i_ss_capital()+$this->titular_jubilacion_bl_arriba_cuota_ii_ss_capital();
    }

    //V83
    public function titular_jubilacion_bl_hasta_abajo_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //V84
    public function titular_jubilacion_bl_abajo_cuota_i_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //V85
    public function titular_jubilacion_bl_abajo_resto_bl_ss_capital() {
        return $this->titular_jubilacion_bl_ss_capital()-$this->titular_jubilacion_bl_hasta_abajo_ss_capital();
    }

    //V86
    public function titular_jubilacion_bl_ss_capital_abajo_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //V87
    public function titular_jubilacion_bl_abajo_cuota_ii_ss_capital() {
        return $this->titular_jubilacion_bl_ss_capital_abajo_tipo()*$this->titular_jubilacion_bl_abajo_resto_bl_ss_capital()/100;
    }
    
    //V88
    public function titular_jubilacion_abajo_total_cuota_ss_capital() {
        return $this->titular_jubilacion_bl_abajo_cuota_i_ss_capital()+$this->titular_jubilacion_bl_abajo_cuota_ii_ss_capital();
    }


    //W74
    public function titular_jubilacion_bl_ss_renta() {
        return $this->calculo_generales_titular_ingresos_brutos_anuales()+$this->m_Parent->m_Jubilacion_Lib->titular_renta_anual_obtenida();
    }
    
    //W76
    public function titular_jubilacion_bl_hasta_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //W77
    public function titular_jubilacion_bl_arriba_cuota_i_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //W78
    public function titular_jubilacion_bl_arriba_resto_bl_ss_renta() {
        return $this->titular_jubilacion_bl_ss_renta()-$this->titular_jubilacion_bl_hasta_ss_renta();
    }

    //W79
    public function titular_jubilacion_bl_ss_renta_arriba_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //W80
    public function titular_jubilacion_bl_arriba_cuota_ii_ss_renta() {
        return $this->titular_jubilacion_bl_ss_renta_arriba_tipo()*$this->titular_jubilacion_bl_arriba_resto_bl_ss_renta()/100;
    }

    
    //W81
    public function titular_jubilacion_arriba_total_cuota_ss_renta() {
        return $this->titular_jubilacion_bl_arriba_cuota_i_ss_renta()+$this->titular_jubilacion_bl_arriba_cuota_ii_ss_renta();
    }

    //W83
    public function titular_jubilacion_bl_hasta_abajo_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //W84
    public function titular_jubilacion_bl_abajo_cuota_i_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //W85
    public function titular_jubilacion_bl_abajo_resto_bl_ss_renta() {
        return $this->titular_jubilacion_bl_ss_renta()-$this->titular_jubilacion_bl_hasta_abajo_ss_renta();
    }

    //W86
    public function titular_jubilacion_bl_ss_renta_abajo_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //W87
    public function titular_jubilacion_bl_abajo_cuota_ii_ss_renta() {
        return $this->titular_jubilacion_bl_ss_renta_abajo_tipo()*$this->titular_jubilacion_bl_abajo_resto_bl_ss_renta()/100;
    }
    
    //W88
    public function titular_jubilacion_abajo_total_cuota_ss_renta() {
        return $this->titular_jubilacion_bl_abajo_cuota_i_ss_renta()+$this->titular_jubilacion_bl_abajo_cuota_ii_ss_renta();
    }

    
    //Y74
    public function conyugue_jubilacion_bl_solo_ss() {
        return $this->calculo_generales_conyugue_ingresos_brutos_anuales();
    }

    //Y76
    public function conyugue_jubilacion_bl_hasta_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //Y77
    public function conyugue_jubilacion_bl_arriba_cuota_i_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //Y78
    public function conyugue_jubilacion_bl_arriba_resto_bl_solo_ss() {
        return $this->conyugue_jubilacion_bl_solo_ss()-$this->conyugue_jubilacion_bl_hasta_solo_ss();
    }
    
    //Y79
    public function conyugue_jubilacion_bl_arriba_tipo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //Y80
    public function conyugue_jubilacion_bl_arriba_cuota_ii_solo_ss() {
        return $this->conyugue_jubilacion_bl_arriba_tipo_solo_ss()*$this->conyugue_jubilacion_bl_arriba_resto_bl_solo_ss()/100;
    }
    
    //Y81
    public function conyugue_jubilacion_arriba_total_cuota_solo_ss() {
        return $this->conyugue_jubilacion_bl_arriba_cuota_i_solo_ss()+$this->conyugue_jubilacion_bl_arriba_cuota_ii_solo_ss();
    }
    
    //Y83
    public function conyugue_jubilacion_bl_hasta_abajo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //Y84
    public function conyugue_jubilacion_bl_abajo_cuota_i_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //Y85
    public function conyugue_jubilacion_bl_abajo_resto_bl_solo_ss() {
        return $this->conyugue_jubilacion_bl_solo_ss()-$this->conyugue_jubilacion_bl_hasta_abajo_solo_ss();
    }

    
    //Y86
    public function conyugue_jubilacion_bl_abajo_tipo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //Y87
    public function conyugue_jubilacion_bl_abajo_cuota_ii_solo_ss() {
        return $this->conyugue_jubilacion_bl_abajo_tipo_solo_ss()*$this->conyugue_jubilacion_bl_abajo_resto_bl_solo_ss()/100;
    }
    
    //Y88
    public function conyugue_jubilacion_abajo_total_cuota_solo_ss() {
        return $this->conyugue_jubilacion_bl_abajo_cuota_i_solo_ss()+$this->conyugue_jubilacion_bl_abajo_cuota_ii_solo_ss();
    }
    
    //Z74
    public function conyugue_jubilacion_bl_ss_capital() {
        return $this->calculo_generales_conyugue_ingresos_brutos_anuales()+$this->m_Parent->m_Jubilacion_Lib->conyugue_incremento_bl();
    }

        //Z76
    public function conyugue_jubilacion_bl_hasta_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    
    //Z77
    public function conyugue_jubilacion_bl_arriba_cuota_i_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //Z78
    public function conyugue_jubilacion_bl_arriba_resto_bl_ss_capital() {
        return $this->conyugue_jubilacion_bl_ss_capital()-$this->conyugue_jubilacion_bl_hasta_ss_capital();
    }


    //Z79
    public function conyugue_jubilacion_bl_arriba_tipo_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //Z80
    public function conyugue_jubilacion_bl_arriba_cuota_ii_ss_capital() {
        return $this->conyugue_jubilacion_bl_arriba_tipo_ss_capital()*$this->conyugue_jubilacion_bl_arriba_resto_bl_ss_capital()/100;
    }

    
    //Z81
    public function conyugue_jubilacion_arriba_total_cuota_ss_capital() {
        return $this->conyugue_jubilacion_bl_arriba_cuota_i_ss_capital()+$this->conyugue_jubilacion_bl_arriba_cuota_ii_ss_capital();
    }

    //Z83
    public function conyugue_jubilacion_bl_hasta_abajo_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //Z84
    public function conyugue_jubilacion_bl_abajo_cuota_i_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //Z85
    public function conyugue_jubilacion_bl_abajo_resto_bl_ss_capital() {
        return $this->conyugue_jubilacion_bl_ss_capital()-$this->conyugue_jubilacion_bl_hasta_abajo_ss_capital();
    }

    //Z86
    public function conyugue_jubilacion_bl_ss_capital_abajo_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //Z87
    public function conyugue_jubilacion_bl_abajo_cuota_ii_ss_capital() {
        return $this->conyugue_jubilacion_bl_ss_capital_abajo_tipo()*$this->conyugue_jubilacion_bl_abajo_resto_bl_ss_capital()/100;
    }
    
    //Z88
    public function conyugue_jubilacion_abajo_total_cuota_ss_capital() {
        return $this->conyugue_jubilacion_bl_abajo_cuota_i_ss_capital()+$this->conyugue_jubilacion_bl_abajo_cuota_ii_ss_capital();
    }

    //AA74
    public function conyugue_jubilacion_bl_ss_renta() {
        return $this->calculo_generales_conyugue_ingresos_brutos_anuales()+$this->m_Parent->m_Jubilacion_Lib->conyugue_renta_anual_obtenida();
    }

        //AA76
    public function conyugue_jubilacion_bl_hasta_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AA77
    public function conyugue_jubilacion_bl_arriba_cuota_i_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AA78
    public function conyugue_jubilacion_bl_arriba_resto_bl_ss_renta() {
        return $this->conyugue_jubilacion_bl_ss_renta()-$this->conyugue_jubilacion_bl_hasta_ss_renta();
    }

    //AA79
    public function conyugue_jubilacion_bl_ss_renta_arriba_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AA80
    public function conyugue_jubilacion_bl_arriba_cuota_ii_ss_renta() {
        return $this->conyugue_jubilacion_bl_ss_renta_arriba_tipo()*$this->conyugue_jubilacion_bl_arriba_resto_bl_ss_renta()/100;
    }

    
    //AA81
    public function conyugue_jubilacion_arriba_total_cuota_ss_renta() {
        return $this->conyugue_jubilacion_bl_arriba_cuota_i_ss_renta()+$this->conyugue_jubilacion_bl_arriba_cuota_ii_ss_renta();
    }

    //AA83
    public function conyugue_jubilacion_bl_hasta_abajo_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AA84
    public function conyugue_jubilacion_bl_abajo_cuota_i_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AA85
    public function conyugue_jubilacion_bl_abajo_resto_bl_ss_renta() {
        return $this->conyugue_jubilacion_bl_ss_renta()-$this->conyugue_jubilacion_bl_hasta_abajo_ss_renta();
    }

    //AA86
    public function conyugue_jubilacion_bl_ss_renta_abajo_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_jubilacion_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AA87
    public function conyugue_jubilacion_bl_abajo_cuota_ii_ss_renta() {
        return $this->conyugue_jubilacion_bl_ss_renta_abajo_tipo()*$this->conyugue_jubilacion_bl_abajo_resto_bl_ss_renta()/100;
    }
    
    //AA88
    public function conyugue_jubilacion_abajo_total_cuota_ss_renta() {
        return $this->conyugue_jubilacion_bl_abajo_cuota_i_ss_renta()+$this->conyugue_jubilacion_bl_abajo_cuota_ii_ss_renta();
    }

    
    //AE72
    private function titular_pias_renta_vitalicia_anual() {
        return $this->m_Parent->m_Jubilacion_Lib->titular_pias_renta_anual_obtenida();
    }
    
    //AE73
    private function titular_pias_edad_constituir() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_titular_edad_inicio_renta'];
    }
    
    //AE74
    public function titular_pias_porcentaje_exencion() {
        return $this->reducciones_renta_vitalicia($this->titular_pias_edad_constituir());
    }
    
    //AE75
    public function titular_pias_renta_exenta() {
        return $this->titular_pias_renta_vitalicia_anual()*$this->titular_pias_porcentaje_exencion()/100;
    }
    
    //AE76
    public function titular_pias_renta_tributable() {
        return $this->titular_pias_renta_vitalicia_anual()-$this->titular_pias_renta_exenta();
    }
    
    //AE78
    private function titular_pias_bl_hasta() {
        $cantidad = $this->titular_pias_renta_tributable();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AE79
    private function titular_pias_cuota_i(){
        $cantidad = $this->titular_pias_renta_tributable();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
            
    //AE80
    private function titular_pias_resto_bl() {
        return $this->titular_pias_renta_tributable()-$this->titular_pias_bl_hasta();
    }
    
    //AE81
    private function titular_pias_tipo() {
        $cantidad = $this->titular_pias_renta_tributable();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AE82
    private function titular_pias_cuota_ii() {
        return $this->titular_pias_tipo()*$this->titular_pias_resto_bl()/100;
    }
    
    //AE83
    public function titular_pias_total_cuota() {
        return $this->titular_pias_cuota_i() + $this->titular_pias_cuota_ii();
    }
    
    private function reducciones_renta_vitalicia( $edad ){
        if( $edad >= 70) return 92;
        if( $edad >= 66) return 80;
        if( $edad >= 60) return 76;
        if( $edad >= 50) return 72;
        if( $edad >= 40) return 65;
        return 0;
    }

    //AF72
    private function conyugue_pias_renta_vitalicia_anual() {
        return $this->m_Parent->m_Jubilacion_Lib->conyugue_pias_renta_anual_obtenida();
    }
    
    //AF73
    private function conyugue_pias_edad_constituir() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_edad_inicio_renta'];
    }
    
    //AF74
    public function conyugue_pias_porcentaje_exencion() {
        return $this->reducciones_renta_vitalicia($this->conyugue_pias_edad_constituir());
    }
    
    //AF75
    public function conyugue_pias_renta_exenta() {
        return $this->conyugue_pias_renta_vitalicia_anual()*$this->conyugue_pias_porcentaje_exencion()/100;
    }
    
    //AF76
    public function conyugue_pias_renta_tributable() {
        return $this->conyugue_pias_renta_vitalicia_anual()-$this->conyugue_pias_renta_exenta();
    }
    
    //AF78
    private function conyugue_pias_bl_hasta() {
        $cantidad = $this->conyugue_pias_renta_tributable();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AF79
    private function conyugue_pias_cuota_i(){
        $cantidad = $this->conyugue_pias_renta_tributable();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
            
    //AF80
    private function conyugue_pias_resto_bl() {
        return $this->conyugue_pias_renta_tributable()-$this->conyugue_pias_bl_hasta();
    }
    
    //AF81
    private function conyugue_pias_tipo() {
        $cantidad = $this->conyugue_pias_renta_tributable();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AF82
    private function conyugue_pias_cuota_ii() {
        return $this->conyugue_pias_tipo()*$this->conyugue_pias_resto_bl()/100;
    }
    
    //AF83
    public function conyugue_pias_total_cuota() {
        return $this->conyugue_pias_cuota_i() + $this->conyugue_pias_cuota_ii();
    }
    
    //AJ72
    private function titular_sv_rendimiento_obtenido() {
        return $this->m_Parent->m_Jubilacion_Lib->titular_rendimiento_obtenido_sv();
    }
    
    //AJ74
    private function titular_sv_bl_hasta() {
        $cantidad = $this->titular_sv_rendimiento_obtenido();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AJ75
    private function titular_sv_cuota_i() {
        $cantidad = $this->titular_sv_rendimiento_obtenido();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AJ76
    private function titular_sv_tipo() {
        $cantidad = $this->titular_sv_rendimiento_obtenido();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AJ77
    private function titular_sv_resto_bl() {
        return $this->titular_sv_rendimiento_obtenido()-$this->titular_sv_bl_hasta();
    }
    
    //AJ78
    public function titular_sv_cuota_ii() {
        return $this->titular_sv_tipo()*$this->titular_sv_resto_bl()/100;
    }
            
            
    //AJ79
    public function titular_sv_total_cuota() {
        return $this->titular_sv_cuota_i()+$this->titular_sv_cuota_ii();
    }
    
    //AK72
    private function conyugue_sv_rendimiento_obtenido() {
        return $this->m_Parent->m_Jubilacion_Lib->conyugue_rendimiento_obtenido_sv();
    }
    
    //AK74
    private function conyugue_sv_bl_hasta() {
        $cantidad = $this->conyugue_sv_rendimiento_obtenido();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AK75
    private function conyugue_sv_cuota_i() {
        $cantidad = $this->conyugue_sv_rendimiento_obtenido();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AK76
    private function conyugue_sv_tipo() {
        $cantidad = $this->conyugue_sv_rendimiento_obtenido();
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_escala_gravamen_ahorro($comunidad_autonoma, $cantidad, $columna);
    }
    
    //AK77
    private function conyugue_sv_resto_bl() {
        return $this->conyugue_sv_rendimiento_obtenido()-$this->conyugue_sv_bl_hasta();
    }
    
    //AK78
    public function conyugue_sv_cuota_ii() {
        return $this->conyugue_sv_tipo()*$this->conyugue_sv_resto_bl()/100;
    }
            
            
    //AK79
    public function conyugue_sv_total_cuota() {
        return $this->conyugue_sv_cuota_i()+$this->conyugue_sv_cuota_ii();
    }
    
    //AO74
    public function titular_dependencia_bl_solo_ss() {
        return $this->calculo_generales_titular_ingresos_brutos_anuales();
    }

    //AO76
    public function titular_dependencia_bl_hasta_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    

    //AO77
    public function titular_dependencia_bl_arriba_cuota_i_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AO78
    public function titular_dependencia_bl_arriba_resto_bl_solo_ss() {
        return $this->titular_dependencia_bl_solo_ss()-$this->titular_dependencia_bl_hasta_solo_ss();
    }

    //AO79
    public function titular_dependencia_bl_arriba_tipo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AO80
    public function titular_dependencia_bl_arriba_cuota_ii_solo_ss() {
        return $this->titular_dependencia_bl_arriba_tipo_solo_ss()*$this->titular_dependencia_bl_arriba_resto_bl_solo_ss()/100;
    }

    
    //AO81
    public function titular_dependencia_arriba_total_cuota_solo_ss() {
        return $this->titular_dependencia_bl_arriba_cuota_i_solo_ss()+$this->titular_dependencia_bl_arriba_cuota_ii_solo_ss();
    }

        //AO83
    public function titular_dependencia_bl_hasta_abajo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AO84
    public function titular_dependencia_bl_abajo_cuota_i_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AO85
    public function titular_dependencia_bl_abajo_resto_bl_solo_ss() {
        return $this->titular_dependencia_bl_solo_ss()-$this->titular_dependencia_bl_hasta_abajo_solo_ss();
    }

    //AO86
    public function titular_dependencia_bl_abajo_tipo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AO87
    public function titular_dependencia_bl_abajo_cuota_ii_solo_ss() {
        return $this->titular_dependencia_bl_abajo_tipo_solo_ss()*$this->titular_dependencia_bl_abajo_resto_bl_solo_ss()/100;
    }
    
    //AO88
    public function titular_dependencia_abajo_total_cuota_solo_ss() {
        return $this->titular_dependencia_bl_abajo_cuota_i_solo_ss()+$this->titular_dependencia_bl_abajo_cuota_ii_solo_ss();
    }
    
    //AP74
    public function titular_dependencia_bl_ss_capital() {
        return $this->titular_dependencia_bl_solo_ss()+$this->m_Parent->m_Dependencia_Lib->titular_incremento_bl();
    }

    //AP76
    public function titular_dependencia_bl_hasta_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    
    //AP77
    public function titular_dependencia_bl_arriba_cuota_i_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AP78
    public function titular_dependencia_bl_arriba_resto_bl_ss_capital() {
        return $this->titular_dependencia_bl_ss_capital()-$this->titular_dependencia_bl_hasta_ss_capital();
    }

    //AP79
    public function titular_dependencia_bl_arriba_tipo_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AP80
    public function titular_dependencia_bl_arriba_cuota_ii_ss_capital() {
        return $this->titular_dependencia_bl_arriba_tipo_ss_capital()*$this->titular_dependencia_bl_arriba_resto_bl_ss_capital()/100;
    }

    //AP81
    public function titular_dependencia_arriba_total_cuota_ss_capital() {
        return $this->titular_dependencia_bl_arriba_cuota_i_ss_capital()+$this->titular_dependencia_bl_arriba_cuota_ii_ss_capital();
    }

    //AP83
    public function titular_dependencia_bl_hasta_abajo_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AP84
    public function titular_dependencia_bl_abajo_cuota_i_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AP85
    public function titular_dependencia_bl_abajo_resto_bl_ss_capital() {
        return $this->titular_dependencia_bl_ss_capital()-$this->titular_dependencia_bl_hasta_abajo_ss_capital();
    }

    //AP86
    public function titular_dependencia_bl_ss_capital_abajo_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AP87
    public function titular_dependencia_bl_abajo_cuota_ii_ss_capital() {
        return $this->titular_dependencia_bl_ss_capital_abajo_tipo()*$this->titular_dependencia_bl_abajo_resto_bl_ss_capital()/100;
    }
    
    //AP88
    public function titular_dependencia_abajo_total_cuota_ss_capital() {
        return $this->titular_dependencia_bl_abajo_cuota_i_ss_capital()+$this->titular_dependencia_bl_abajo_cuota_ii_ss_capital();
    }


    //AQ74
    public function titular_dependencia_bl_ss_renta() {
        return $this->titular_dependencia_bl_solo_ss()+$this->m_Parent->m_Dependencia_Lib->titular_renta_anual_obtenida();
    }

    //AQ76
    public function titular_dependencia_bl_hasta_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AQ77
    public function titular_dependencia_bl_arriba_cuota_i_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AQ78
    public function titular_dependencia_bl_arriba_resto_bl_ss_renta() {
        return $this->titular_dependencia_bl_ss_renta()-$this->titular_dependencia_bl_hasta_ss_renta();
    }

    //AQ79
    public function titular_dependencia_bl_ss_renta_arriba_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AQ80
    public function titular_dependencia_bl_arriba_cuota_ii_ss_renta() {
        return $this->titular_dependencia_bl_ss_renta_arriba_tipo()*$this->titular_dependencia_bl_arriba_resto_bl_ss_renta()/100;
    }

    
    //AQ81
    public function titular_dependencia_arriba_total_cuota_ss_renta() {
        return $this->titular_dependencia_bl_arriba_cuota_i_ss_renta()+$this->titular_dependencia_bl_arriba_cuota_ii_ss_renta();
    }

    //AQ83
    public function titular_dependencia_bl_hasta_abajo_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AQ84
    public function titular_dependencia_bl_abajo_cuota_i_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AQ85
    public function titular_dependencia_bl_abajo_resto_bl_ss_renta() {
        return $this->titular_dependencia_bl_ss_renta()-$this->titular_dependencia_bl_hasta_abajo_ss_renta();
    }

    //AQ86
    public function titular_dependencia_bl_ss_renta_abajo_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->titular_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AQ87
    public function titular_dependencia_bl_abajo_cuota_ii_ss_renta() {
        return $this->titular_dependencia_bl_ss_renta_abajo_tipo()*$this->titular_dependencia_bl_abajo_resto_bl_ss_renta()/100;
    }
    
    //AQ88
    public function titular_dependencia_abajo_total_cuota_ss_renta() {
        return $this->titular_dependencia_bl_abajo_cuota_i_ss_renta()+$this->titular_dependencia_bl_abajo_cuota_ii_ss_renta();
    }

    //AS74
    public function conyugue_dependencia_bl_solo_ss() {
        return $this->calculo_generales_conyugue_ingresos_brutos_anuales();
    }

    //AS76
    public function conyugue_dependencia_bl_hasta_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    

    //AS77
    public function conyugue_dependencia_bl_arriba_cuota_i_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AS78
    public function conyugue_dependencia_bl_arriba_resto_bl_solo_ss() {
        return $this->conyugue_dependencia_bl_solo_ss()-$this->conyugue_dependencia_bl_hasta_solo_ss();
    }

    //AS79
    public function conyugue_dependencia_bl_arriba_tipo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AS80
    public function conyugue_dependencia_bl_arriba_cuota_ii_solo_ss() {
        return $this->conyugue_dependencia_bl_arriba_tipo_solo_ss()*$this->conyugue_dependencia_bl_arriba_resto_bl_solo_ss()/100;
    }

    
    //AS81
    public function conyugue_dependencia_arriba_total_cuota_solo_ss() {
        return $this->conyugue_dependencia_bl_arriba_cuota_i_solo_ss()+$this->conyugue_dependencia_bl_arriba_cuota_ii_solo_ss();
    }

        //AS83
    public function conyugue_dependencia_bl_hasta_abajo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AS84
    public function conyugue_dependencia_bl_abajo_cuota_i_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AS85
    public function conyugue_dependencia_bl_abajo_resto_bl_solo_ss() {
        return $this->conyugue_dependencia_bl_solo_ss()-$this->conyugue_dependencia_bl_hasta_abajo_solo_ss();
    }

    //AS86
    public function conyugue_dependencia_bl_abajo_tipo_solo_ss() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_solo_ss();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AS87
    public function conyugue_dependencia_bl_abajo_cuota_ii_solo_ss() {
        return $this->conyugue_dependencia_bl_abajo_tipo_solo_ss()*$this->conyugue_dependencia_bl_abajo_resto_bl_solo_ss()/100;
    }
    
    //AS88
    public function conyugue_dependencia_abajo_total_cuota_solo_ss() {
        return $this->conyugue_dependencia_bl_abajo_cuota_i_solo_ss()+$this->conyugue_dependencia_bl_abajo_cuota_ii_solo_ss();
    }

    //AT74
    public function conyugue_dependencia_bl_ss_capital() {
        return $this->conyugue_dependencia_bl_solo_ss()+$this->m_Parent->m_Dependencia_Lib->conyugue_incremento_bl();
    }

    //AT76
    public function conyugue_dependencia_bl_hasta_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    
    //AT77
    public function conyugue_dependencia_bl_arriba_cuota_i_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AT78
    public function conyugue_dependencia_bl_arriba_resto_bl_ss_capital() {
        return $this->conyugue_dependencia_bl_ss_capital()-$this->conyugue_dependencia_bl_hasta_ss_capital();
    }

    //AT79
    public function conyugue_dependencia_bl_arriba_tipo_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AT80
    public function conyugue_dependencia_bl_arriba_cuota_ii_ss_capital() {
        return $this->conyugue_dependencia_bl_arriba_tipo_ss_capital()*$this->conyugue_dependencia_bl_arriba_resto_bl_ss_capital()/100;
    }

    //AT81
    public function conyugue_dependencia_arriba_total_cuota_ss_capital() {
        return $this->conyugue_dependencia_bl_arriba_cuota_i_ss_capital()+$this->conyugue_dependencia_bl_arriba_cuota_ii_ss_capital();
    }

    //AT83
    public function conyugue_dependencia_bl_hasta_abajo_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AT84
    public function conyugue_dependencia_bl_abajo_cuota_i_ss_capital() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AT85
    public function conyugue_dependencia_bl_abajo_resto_bl_ss_capital() {
        return $this->conyugue_dependencia_bl_ss_capital()-$this->conyugue_dependencia_bl_hasta_abajo_ss_capital();
    }

    //AT86
    public function conyugue_dependencia_bl_ss_capital_abajo_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_capital();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AT87
    public function conyugue_dependencia_bl_abajo_cuota_ii_ss_capital() {
        return $this->conyugue_dependencia_bl_ss_capital_abajo_tipo()*$this->conyugue_dependencia_bl_abajo_resto_bl_ss_capital()/100;
    }
    
    //AT88
    public function conyugue_dependencia_abajo_total_cuota_ss_capital() {
        return $this->conyugue_dependencia_bl_abajo_cuota_i_ss_capital()+$this->conyugue_dependencia_bl_abajo_cuota_ii_ss_capital();
    }
    
    //AU74
    public function conyugue_dependencia_bl_ss_renta() {
        return $this->conyugue_dependencia_bl_solo_ss()+$this->m_Parent->m_Dependencia_Lib->conyugue_renta_anual_obtenida();
    }

    //AU76
    public function conyugue_dependencia_bl_hasta_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AU77
    public function conyugue_dependencia_bl_arriba_cuota_i_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AU78
    public function conyugue_dependencia_bl_arriba_resto_bl_ss_renta() {
        return $this->conyugue_dependencia_bl_ss_renta()-$this->conyugue_dependencia_bl_hasta_ss_renta();
    }

    //AU79
    public function conyugue_dependencia_bl_ss_renta_arriba_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AU80
    public function conyugue_dependencia_bl_arriba_cuota_ii_ss_renta() {
        return $this->conyugue_dependencia_bl_ss_renta_arriba_tipo()*$this->conyugue_dependencia_bl_arriba_resto_bl_ss_renta()/100;
    }

    
    //AU81
    public function conyugue_dependencia_arriba_total_cuota_ss_renta() {
        return $this->conyugue_dependencia_bl_arriba_cuota_i_ss_renta()+$this->conyugue_dependencia_bl_arriba_cuota_ii_ss_renta();
    }

    //AU83
    public function conyugue_dependencia_bl_hasta_abajo_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 1;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AU84
    public function conyugue_dependencia_bl_abajo_cuota_i_ss_renta() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 2;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }

    //AU85
    public function conyugue_dependencia_bl_abajo_resto_bl_ss_renta() {
        return $this->conyugue_dependencia_bl_ss_renta()-$this->conyugue_dependencia_bl_hasta_abajo_ss_renta();
    }

    //AU86
    public function conyugue_dependencia_bl_ss_renta_abajo_tipo() {
        $comunidad_autonoma = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        $base_liquidable = $this->conyugue_dependencia_bl_ss_renta();
        if($this->m_Parent->config->item("winp3_missing_fields")!="") {return;}
        if($comunidad_autonoma=="") {
            $this->log_error("Falta comunidad autónoma de residencia");
            return;
        }
        $columna = 4;

        return $this->m_Parent->fiscalidad_model->get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna);
    }
    
    //AU87
    public function conyugue_dependencia_bl_abajo_cuota_ii_ss_renta() {
        return $this->conyugue_dependencia_bl_ss_renta_abajo_tipo()*$this->conyugue_dependencia_bl_abajo_resto_bl_ss_renta()/100;
    }
    
    //AU88
    public function conyugue_dependencia_abajo_total_cuota_ss_renta() {
        return $this->conyugue_dependencia_bl_abajo_cuota_i_ss_renta()+$this->conyugue_dependencia_bl_abajo_cuota_ii_ss_renta();
    }
}    
?>