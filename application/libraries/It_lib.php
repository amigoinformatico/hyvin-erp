<?php
class It_Lib {
    private $m_Parent;
    
    public function __construct($parent) {
        $this->m_Parent = $parent[0];
    }
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }
    
    //D9
    public function titular_ingresos_actividad_autonomos() {
         return $this->m_Parent->m_Economicos_Lib->titular_autonomos_ingreso_bruto_mensual();
    }
    
    //D15
    public function titular_prestaciones_ss_siguientes_meses() {
        return $this->m_Parent->m_Pensiones->titular_baja_enfermedad_prestacion_segundo_mes();
    }

    //D36
    private function titular_prima_anual() {
        return $this->m_Parent->m_SimulacionRow['it_titular_prima_anual'];
    }
    
    //D39
    public function titular_importe_deducible_irpf() {
        return min($this->titular_prima_anual(), $this->m_Parent->m_Constantes->minimo_importe_deducible_irpf() );
    }

    //D40
    public function titular_tipo_marginal(){
        return $this->m_Parent->m_Fiscalidad->calculo_generales_hoy_titular_tipo_marginal_total();
    }

    
    //D45
    public function titular_indemnizacion() {
        if($this->m_Parent->m_SimulacionRow['titular_dias_indemnizacion_percibida'] == "" ||
           $this->m_Parent->m_SimulacionRow['titular_capital_propuesto_dia_baja'] == "" ) {
            $this->log_error(" Faltan datos para calcular indemnización del Titular");
            return;
        }     
        return $this->m_Parent->m_SimulacionRow['titular_dias_indemnizacion_percibida']*
               $this->m_Parent->m_SimulacionRow['titular_capital_propuesto_dia_baja'];
    }

    
    //F9
    public function conyugue_ingresos_actividad_autonomos() {
         return $this->m_Parent->m_Economicos_Lib->conyugue_autonomos_ingreso_bruto_mensual();
    }
    
    //F15
    public function conyugue_prestaciones_ss_siguientes_meses() {
        return $this->m_Parent->m_Pensiones->conyugue_baja_enfermedad_prestacion_segundo_mes();
    }

    //F36
    private function conyugue_prima_anual() {
        return $this->m_Parent->m_SimulacionRow['it_conyugue_prima_anual'];
    }
    
    //F39
    public function conyugue_importe_deducible_irpf() {
        return min($this->conyugue_prima_anual(), $this->m_Parent->m_Constantes->minimo_importe_deducible_irpf() );
    }
    
    //F40
    public function conyugue_tipo_marginal(){
        return $this->m_Parent->m_Fiscalidad->calculo_generales_hoy_conyugue_tipo_marginal_total();
    }
    
    //F45
    public function conyugue_indemnizacion() {
        if($this->m_Parent->m_SimulacionRow['conyugue_dias_indemnizacion_percibida'] == "" ||
           $this->m_Parent->m_SimulacionRow['conyugue_capital_propuesto_dia_baja'] == "" ) {
            $this->log_error(" Faltan datos para calcular indemnización del conyugue");
            return;
        }     
        return $this->m_Parent->m_SimulacionRow['conyugue_dias_indemnizacion_percibida']*
               $this->m_Parent->m_SimulacionRow['conyugue_capital_propuesto_dia_baja'];
    }
    
}

?>