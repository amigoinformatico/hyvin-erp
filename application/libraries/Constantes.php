<?php
class Constantes {
    private $m_Parent;
    
    public function __construct($parent) {
        $this->m_Parent = $parent[0];
    }
    
    public function anyo_estudio() {
        return $this->fecha_estudio()->format("Y");
    }
    
    public function minimo_importe_deducible_irpf() {
        return 500;
    }
    
    //C3
    public function fecha_estudio() {
        return new DateTime(); //Mejora: En configuracion
    }
    
    //C5
    public function ipc() {
        return $this->m_Parent->constantes_model->get_all()->ipc;
    }
    
    //C7
    public function rentabilidad_estimada() {
        return $this->m_Parent->constantes_model->get_all()->rentabilidad_estimada;
    }
    
    //C9
    public function tipo_cotiz_auton() {
        return $this->m_Parent->parametros_model->get_all()->tipo_cotiz_auton;
    }
    
    //C13
    public function edad_dependencia() {
        return $this->m_Parent->constantes_model->get_all()->edad_dependencia;
    }
    
    //C14
    public function edad_renta_pias() {
        return $this->m_Parent->constantes_model->get_all()->edad_renta_pias;
    }
    
    //C17
    public function gastos_entierro() {
        return $this->m_Parent->constantes_model->get_all()->gastos_entierro;
    }

    //C18
    public function costes_aceptacion_herencia() {
        return $this->m_Parent->constantes_model->get_all()->costes_aceptacion_herencia;
    }
    
    //C21
    public function adaptacion_vivienda() {
       return $this->m_Parent->constantes_model->get_all()->adaptacion_vivienda;
    }
    
    //C22
    public function adaptacion_vehiculos() {
        return $this->m_Parent->constantes_model->get_all()->adaptacion_vehiculos;
    }
    
    
}