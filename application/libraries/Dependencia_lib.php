<?php
class Dependencia_Lib {
    private $m_Parent;
    
    public function __construct($parent) {
        $this->m_Parent = $parent[0];
    }
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }
 
    //D6
    public function titular_fecha_nacimiento() {
        return $this->m_Parent->m_Titular_Lib->titular_fecha_nacimiento();
    }
    
    //D8
    public function titular_fecha_80_cumpleanyos() {
        $titular_fecha_nacimiento = $this->titular_fecha_nacimiento();
        
        return $titular_fecha_nacimiento->add(new DateInterval("P80Y"));
    }
    
    //D9
    public function titular_anyos_hasta_dependencia() {
        $titular_fecha_80_cumpleanyos = $this->titular_fecha_80_cumpleanyos();
        $hoy = new DateTime();
        return $hoy->diff($titular_fecha_80_cumpleanyos)->format("%a")/365.25;
    }
    
    //D11
    public function titular_fecha_jubilacion() {
        return $this->m_Parent->m_Pensiones->titular_fecha_jubilacion();
    }
    
    //D13
    public function titular_anyos_desde_jubilacion_a_dependencia() {
        return $this->titular_fecha_jubilacion()->diff($this->titular_fecha_80_cumpleanyos())->format("%a")/365.25;
    }
    
    //D16
    public function titular_coste_dependencia_actualizado() {
        $titular_coste_dependencia_hoy = $this->m_Parent->m_SimulacionRow['dependencia_titular_coste_dependencia_hoy'];
        $titular_anyos_hasta_dependencia = $this->titular_anyos_hasta_dependencia();

        return $titular_coste_dependencia_hoy*pow((1+$this->m_Parent->m_Constantes->ipc()/100),$titular_anyos_hasta_dependencia);
    }
    
    //D19
    public function titular_coste_total_dependencia() {
        $titular_anyos_dependencia =$this->m_Parent->m_SimulacionRow['dependencia_titular_anyos_dependencia'];
        $titular_coste_dependencia_actualizado = $this->titular_coste_dependencia_actualizado();
        return $titular_anyos_dependencia * $titular_coste_dependencia_actualizado*12;
    }
    
    //D21
    public function titular_necesidad_dependencia() {
        $titular_porcentaje_cobertura = $this->m_Parent->m_SimulacionRow['dependencia_titular_porcentaje_cobertura'];
        
        return $this->titular_coste_total_dependencia()*$titular_porcentaje_cobertura/100;
        
    }
    
    //D23 
    public function titular_contratante(){
        return "Titular";
    }
    
    //D24
    public function titular_modalidad() {
        return "Seguro de dependencia";
    }
    
    //D28
    private function titular_aportacion_mensual_propuesta(){
        return $this->m_Parent->m_SimulacionRow['dependencia_titular_aportacion_mensual'];
    }
    
    //D29
    public function titular_capital_jubilacion_si_aportacion() {
        $anyos_hasta_dependencia = $this->titular_anyos_hasta_dependencia();//D9
        $aportacion_mensual = $this->titular_aportacion_mensual_propuesta();//D28
        $rentabilidad_estimada = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        return $this->vf($aportacion_mensual, ($rentabilidad_estimada/100)/12, $anyos_hasta_dependencia*12);
    }
    
    public function vf($aportacion, $rentabilidad, $tiempo){
        return $aportacion*(((pow(1+$rentabilidad, $tiempo)-1)/$rentabilidad));
    }
    
    //D31 Segun revision 27-12-2017
    public function titular_capital_jubilacion(){
        return $this->titular_necesidad_dependencia();
        //return $this->m_Parent->m_SimulacionRow['dependencia_titular_capital_jubilacion'];
    }

    //D32
    private function titular_si_capital_aportacion_mensual_propuesta() {
        $meses = $this->titular_anyos_hasta_dependencia()*12;//D9
        $capital = $this->titular_capital_jubilacion(); //31
        $rentabilidad = $this->m_Parent->m_Constantes->rentabilidad_estimada();

        return $this->pago($capital, ($rentabilidad/100)/12, $meses);
    }
    
    public function pago($valor_final, $rentabilidad, $tiempo ){
        return $valor_final*($rentabilidad/(pow(1+$rentabilidad, $tiempo)-1));
    }
    
    // Modificada debido a que ahora siempre es por capital
    //D39 
    public function titular_importe_reducir_base_liquidadora(){ 
        return $this->m_Parent->m_SimulacionRow['dependencia_titular_aportacion_anual'];
 //        return $this->titular_si_capital_aportacion_mensual_propuesta()*12;
    }
    
    //D40
    public function titular_tipo_marginal_hoy() {
        return $this->m_Parent->m_Fiscalidad->calculo_generales_hoy_titular_tipo_marginal_total();
    }
    
    //D41
    public function titular_beneficio_fiscal() {
        return $this->titular_importe_reducir_base_liquidadora()*$this->titular_tipo_marginal_hoy()/100;
    }
    
    //D57
    public function titular_base_ss() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_bl_solo_ss();
    }
    
    //D58
    public function titular_tipo_marginal_ss() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_bl_arriba_tipo_solo_ss() + $this->m_Parent->m_Fiscalidad->titular_dependencia_bl_abajo_tipo_solo_ss();
    }
    
    //D59
    public function titular_cuota_estatal_ss() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_arriba_total_cuota_solo_ss();
    }

    //D60
    public function titular_cuota_autonomica_ss() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_abajo_total_cuota_solo_ss();
    }
    
    //D61
    public function titular_total_cuota_ss() {
        return $this->titular_cuota_estatal_ss()+$this->titular_cuota_autonomica_ss();
    }

    
    //E47
    public function titular_reparto_cobro_capital_final_capital() {
        return $this->titular_capital_jubilacion();
        //return $this->m_Parent->m_SimulacionRow['dependencia_titular_capital_jubilacion'];
    }
    
    //E49
    public function titular_incremento_bl() {
        return $this->titular_reparto_cobro_capital_final_capital();
    }

    //E57
    public function titular_base_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_bl_ss_capital();
    }
    
    //E58
    public function titular_tipo_marginal_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_bl_arriba_tipo_ss_capital() + $this->m_Parent->m_Fiscalidad->titular_dependencia_bl_ss_capital_abajo_tipo();
    }

    //E59
    public function titular_cuota_estatal_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_arriba_total_cuota_ss_capital();
    }
    
    //E60
    public function titular_cuota_autonomica_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_abajo_total_cuota_ss_capital();
    }

    //E61
    public function titular_total_cuota_ss_capital() {
        return $this->titular_cuota_estatal_ss_capital()+$this->titular_cuota_autonomica_ss_capital();
    }

    //E63
    public function titular_coste_fiscal_capital() {
        return $this->titular_total_cuota_ss_capital()-$this->titular_total_cuota_ss();
    }

    //F6
    public function conyugue_fecha_nacimiento() {
        return $this->m_Parent->m_Titular_Lib->conyugue_fecha_nacimiento();
    }
    
    //F8
    public function conyugue_fecha_80_cumpleanyos() {
        $conyugue_fecha_nacimiento = $this->conyugue_fecha_nacimiento();
        
        return $conyugue_fecha_nacimiento->add(new DateInterval("P80Y"));
    }
    
    //F9
    public function conyugue_anyos_hasta_dependencia() {
        $conyugue_fecha_80_cumpleanyos = $this->conyugue_fecha_80_cumpleanyos();
        $hoy = new DateTime();
        return $hoy->diff($conyugue_fecha_80_cumpleanyos)->format("%a")/365.25;
    }
    
    //F11
    public function conyugue_fecha_jubilacion() {
        return $this->m_Parent->m_Pensiones->conyugue_fecha_jubilacion();
    }
    
    //F13
    public function conyugue_anyos_desde_jubilacion_a_dependencia() {
        return $this->conyugue_fecha_jubilacion()->diff($this->conyugue_fecha_80_cumpleanyos())->format("%a")/365.25;
    }
    
    
    
    //F16
    public function conyugue_coste_dependencia_actualizado() {
        $conyugue_coste_dependencia_hoy = $this->m_Parent->m_SimulacionRow['dependencia_conyugue_coste_dependencia_hoy'];
        $conyugue_anyos_hasta_dependencia = $this->conyugue_anyos_hasta_dependencia();
        return $conyugue_coste_dependencia_hoy*pow((1+$this->m_Parent->m_Constantes->ipc()/100),$conyugue_anyos_hasta_dependencia);
    }
    
    //F19
    public function conyugue_coste_total_dependencia() {
        $conyugue_anyos_dependencia =$this->m_Parent->m_SimulacionRow['dependencia_conyugue_anyos_dependencia'];
        $conyugue_coste_dependencia_actualizado = $this->conyugue_coste_dependencia_actualizado();
        return $conyugue_anyos_dependencia * $conyugue_coste_dependencia_actualizado*12;
    }
    
    //F21
    public function conyugue_necesidad_dependencia() {
        $conyugue_porcentaje_cobertura = $this->m_Parent->m_SimulacionRow['dependencia_conyugue_porcentaje_cobertura'];
        
        return $this->conyugue_coste_total_dependencia()*$conyugue_porcentaje_cobertura/100;
        
    }

    //F23 
    public function conyugue_contratante(){
        return "Titular";
    }
    
    //F24
    public function conyugue_modalidad() {
        return "Seguro de dependencia";
    }

    //F28
    private function conyugue_aportacion_mensual_propuesta(){
        return $this->m_Parent->m_SimulacionRow['dependencia_conyugue_aportacion_mensual'];
    }
    
    //F29
    public function conyugue_capital_jubilacion_si_aportacion() {
        $anyos_hasta_dependencia = $this->conyugue_anyos_hasta_dependencia();
        $aportacion_mensual = $this->conyugue_aportacion_mensual_propuesta();
        $rentabilidad_estimada = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        return $this->vf($aportacion_mensual, ($rentabilidad_estimada/100)/12, $anyos_hasta_dependencia*12);
    }

    //F31
    public function conyugue_capital_jubilacion() {
        return $this->conyugue_necesidad_dependencia();
        //return $this->m_Parent->m_SimulacionRow['dependencia_conyugue_capital_jubilacion'];
    }
    

    //F32
    public function conyugue_si_capital_aportacion_mensual_propuesta() {
        $meses = $this->conyugue_anyos_hasta_dependencia()*12;
        $capital = $this->conyugue_capital_jubilacion(); 
        $rentabilidad = $this->m_Parent->m_Constantes->rentabilidad_estimada();

        return $this->pago($capital, ($rentabilidad/100)/12, $meses);
    }

    // Modificada debido a que ahora siempre es por capital
    //F39
    public function conyugue_importe_reducir_base_liquidadora(){ 
        return $this->m_Parent->m_SimulacionRow['dependencia_conyugue_aportacion_anual'];
        /*
        return $this->conyugue_si_capital_aportacion_mensual_propuesta()*12;
         */
    }
    
    //F40
    public function conyugue_tipo_marginal_hoy() {
        return $this->m_Parent->m_Fiscalidad->calculo_generales_hoy_conyugue_tipo_marginal_total();
    }

    //F41
    public function conyugue_beneficio_fiscal() {
        return $this->conyugue_importe_reducir_base_liquidadora()*$this->conyugue_tipo_marginal_hoy()/100;
    }
    
    //F47
    public function titular_reparto_cobro_capital_final_renta() {
        return $this->titular_reparto_cobro_capital_final_capital();
    }
    
    //F52
    public function titular_renta_anual_obtenida() {
        $anyos = $this->m_Parent->m_SimulacionRow['dependencia_titular_anyos_renta'];
        if(!is_numeric($anyos)) {
            $this->log_error("Valor incorrecto en Titular Años Renta");
            return;
        }
        return $this->titular_reparto_cobro_capital_final_renta()/$anyos;
    }
    
    //F53
    public function titular_renta_mensual_obtenida() {
        return $this->titular_renta_anual_obtenida()/12;
    }
    
    //F57
    public function titular_base_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_bl_ss_renta();
    }

    //F58
    public function titular_tipo_marginal_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_bl_ss_renta_arriba_tipo() + $this->m_Parent->m_Fiscalidad->titular_dependencia_bl_ss_renta_abajo_tipo();
    }

    //F59
    public function titular_cuota_estatal_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_arriba_total_cuota_ss_renta();
    }

    //F60
    public function titular_cuota_autonomica_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->titular_dependencia_abajo_total_cuota_ss_renta();
    }

    //F61
    public function titular_total_cuota_ss_renta() {
        return $this->titular_cuota_estatal_ss_renta()+$this->titular_cuota_autonomica_ss_renta();
    }

    //F64
    public function titular_coste_fiscal_renta_1() {
        return $this->titular_total_cuota_ss_renta()-$this->titular_total_cuota_ss();
    }
    
    //F65
    public function titular_coste_fiscal_x() {
        return $this->titular_coste_fiscal_renta_1()*$this->m_Parent->m_SimulacionRow['dependencia_titular_anyos_renta'];
    }
    
    //H57
    public function conyugue_base_ss() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_bl_solo_ss();
    }
    
    //H58
    public function conyugue_tipo_marginal_ss() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_bl_arriba_tipo_solo_ss() + $this->m_Parent->m_Fiscalidad->conyugue_dependencia_bl_abajo_tipo_solo_ss();
    }
    
    //H59
    public function conyugue_cuota_estatal_ss() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_arriba_total_cuota_solo_ss();
    }

    //H60
    public function conyugue_cuota_autonomica_ss() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_abajo_total_cuota_solo_ss();
    }
    
    //H61
    public function conyugue_total_cuota_ss() {
        return $this->conyugue_cuota_estatal_ss()+$this->conyugue_cuota_autonomica_ss();
    }

    
    //I47
    public function conyugue_reparto_cobro_capital_final_capital() {
        return $this->conyugue_capital_jubilacion();
     //   return $this->m_Parent->m_SimulacionRow['dependencia_conyugue_capital_jubilacion'];
    }
    
    //I49
    public function conyugue_incremento_bl() {
        return $this->conyugue_reparto_cobro_capital_final_capital();
    }

    //I57
    public function conyugue_base_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_bl_ss_capital();
    }
    
    //I58
    public function conyugue_tipo_marginal_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_bl_arriba_tipo_ss_capital() + $this->m_Parent->m_Fiscalidad->conyugue_dependencia_bl_ss_capital_abajo_tipo();
    }

    //I59
    public function conyugue_cuota_estatal_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_arriba_total_cuota_ss_capital();
    }
    
    //I60
    public function conyugue_cuota_autonomica_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_abajo_total_cuota_ss_capital();
    }

    //I61
    public function conyugue_total_cuota_ss_capital() {
        return $this->conyugue_cuota_estatal_ss_capital()+$this->conyugue_cuota_autonomica_ss_capital();
    }

    //I63
    public function conyugue_coste_fiscal_capital() {
        return $this->conyugue_total_cuota_ss_capital()-$this->conyugue_total_cuota_ss();
    }
    
    //J47
    public function conyugue_reparto_cobro_capital_final_renta() {
        return $this->conyugue_reparto_cobro_capital_final_capital();
    }

    //J52
    public function conyugue_renta_anual_obtenida() {
        $anyos = $this->m_Parent->m_SimulacionRow['dependencia_conyugue_anyos_renta'];

        if(!is_numeric($anyos)) {
            $this->log_error("Valor incorrecto en Cónyuge Años Renta");
            return;
        }
        return $this->conyugue_reparto_cobro_capital_final_renta()/$anyos;
    }

    //J53
    public function conyugue_renta_mensual_obtenida() {
        return $this->conyugue_renta_anual_obtenida()/12;
    }

    //J57
    public function conyugue_base_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_bl_ss_renta();
    }

    //J58
    public function conyugue_tipo_marginal_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_bl_ss_renta_arriba_tipo() + $this->m_Parent->m_Fiscalidad->conyugue_dependencia_bl_ss_renta_abajo_tipo();
    }

    //J59
    public function conyugue_cuota_estatal_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_arriba_total_cuota_ss_renta();
    }

    //J60
    public function conyugue_cuota_autonomica_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->conyugue_dependencia_abajo_total_cuota_ss_renta();
    }

    //J61
    public function conyugue_total_cuota_ss_renta() {
        return $this->conyugue_cuota_estatal_ss_renta()+$this->conyugue_cuota_autonomica_ss_renta();
    }

    //J64
    public function conyugue_coste_fiscal_renta_1() {
        return $this->conyugue_total_cuota_ss_renta()-$this->conyugue_total_cuota_ss();
    }
    
    //J65
    public function conyugue_coste_fiscal_x() {
        return $this->conyugue_coste_fiscal_renta_1()*$this->m_Parent->m_SimulacionRow['dependencia_conyugue_anyos_renta'];
    }

    
}
