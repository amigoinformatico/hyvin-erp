<?php
class Invalidez_Lib {
    private $m_Parent;
    
    public function __construct($parent) {
        $this->m_Parent = $parent[0];
    }
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }
    
    //D12
    public function titular_saldo_mensual() {
        $gastos = $data['titular_gastos_mensuales_familiares'];
        $ingresos = $data['titular_ingresos_conyugue'];
        $pension = $data['titular_pension_invalidez'];
        
        return $gastos+$ingresos+$pension;
    }

    //D16
    public function titular_fecha_jubilacion() {
        return $this->m_Parent->m_Pensiones->titular_fecha_jubilacion();
    }
    
    //D18
    public function titular_anyos_cobertura() {
        return $this->m_Parent->m_SimulacionRow['invalidez_titular_anyos_cobertura'];
        /*
        $titular_fecha_jubilacion = $this->titular_fecha_jubilacion();
        $hoy = new DateTime();
        return $hoy->diff($titular_fecha_jubilacion)->format("%a")/365.25;
         */
    }
    
    //D35
    public function titular_capital_propuesto() {
        if( $this->m_Parent->m_SimulacionRow['invalidez_titular_capital_propuesto'] == "") {
            $this->log_error(" Falta Titular capital propuesto");//TODO Comprovar que no sale y quitar.
            return;
        }
        return $this->m_Parent->m_SimulacionRow['invalidez_titular_capital_propuesto'];
    }
    
    //D37
    public function titular_prima_aproximada() {
        return $this->m_Parent->m_SimulacionRow['invalidez_titular_prima_aproximada'];
    }
    
        //F16
    public function conyugue_fecha_jubilacion() {
        return $this->m_Parent->m_Pensiones->conyugue_fecha_jubilacion();
    }
    
    //F18
    public function conyugue_anyos_cobertura() {
        return $this->m_Parent->m_SimulacionRow['invalidez_conyugue_anyos_cobertura'];
        /*
        $conyugue_fecha_jubilacion = $this->conyugue_fecha_jubilacion();
        $hoy = new DateTime();
        return $hoy->diff($conyugue_fecha_jubilacion)->format("%a")/365.25;
         */
    }
    
    //F35
    public function conyugue_capital_propuesto() {
        if( $this->m_Parent->m_SimulacionRow['invalidez_conyugue_capital_propuesto'] == "") { 
            $this->log_error(" Falta conyugue capital propuesto");//TODO comprobar que ya no sale.
            return;
        }
        
        return $this->m_Parent->m_SimulacionRow['invalidez_conyugue_capital_propuesto'];
    }
    
    //F37
    public function conyugue_prima_aproximada() {
        return $this->m_Parent->m_SimulacionRow['invalidez_conyugue_prima_aproximada'];
    }

}
?>
