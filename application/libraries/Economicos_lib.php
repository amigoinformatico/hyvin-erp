<?php
class Economicos_Lib {
    private $m_Parent;
    
    public function __construct($parent) {
        $this->m_Parent = $parent[0];
    }
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }

    
    //C7
    public function titular_regimen_ss() {
        return $this->m_Parent->m_SimulacionRow['titular_regimen_ss'];
    }
    
    //C12
    private function titular_anyos_cotizados_regimen_general() {
        return $this->m_Parent->m_SimulacionRow['titular_cotizado_ss_rg_anyos'];
    }
    
    //C13
    private function titular_meses_cotizados_regimen_general() {
        return $this->m_Parent->m_SimulacionRow['titular_cotizado_ss_rg_meses'];
    }

    //C15
    private function titular_anyos_cotizados_autonomos() {
        return $this->m_Parent->m_SimulacionRow['titular_cotizado_ss_aut_anyos'];
    }
    
    //C16
    private function titular_meses_cotizados_autonomos() {
        return $this->m_Parent->m_SimulacionRow['titular_cotizado_ss_aut_meses'];
    }
    

    //C18
    public function titular_anyos_cotizados() {
        $titular_anyos_cotizados_regimen_general = $this->titular_anyos_cotizados_regimen_general();
        $titular_meses_cotizados_regimen_general = $this->titular_meses_cotizados_regimen_general();
        $titular_anyos_cotizados_autonomos = $this->titular_anyos_cotizados_autonomos();
        $titular_meses_cotizados_autonomos = $this->titular_meses_cotizados_autonomos();
        
        if($titular_anyos_cotizados_regimen_general == "" ||
           $titular_meses_cotizados_regimen_general == "" ||
           $titular_anyos_cotizados_autonomos == "" ||
           $titular_meses_cotizados_autonomos == "") {
            $this->log_error("Titular: Falta información para calcular los años cotizados");
            return;
           }
           
        return floor(($titular_anyos_cotizados_regimen_general*12+$titular_meses_cotizados_regimen_general+
               $titular_anyos_cotizados_autonomos*12+$titular_meses_cotizados_autonomos) / 12);
    }
    
    //C19
    public function titular_meses_cotizados() {
        $titular_anyos_cotizados_regimen_general = $this->titular_anyos_cotizados_regimen_general();
        $titular_meses_cotizados_regimen_general = $this->titular_meses_cotizados_regimen_general();
        $titular_anyos_cotizados_autonomos = $this->titular_anyos_cotizados_autonomos();
        $titular_meses_cotizados_autonomos = $this->titular_meses_cotizados_autonomos();
        
        if($titular_anyos_cotizados_regimen_general == "" ||
           $titular_meses_cotizados_regimen_general == "" ||
           $titular_anyos_cotizados_autonomos == "" ||
           $titular_meses_cotizados_autonomos == "") {
            $this->log_error("Titular: Falta información para calcular los meses cotizados");
            return;
           }
           
        return ($titular_anyos_cotizados_regimen_general*12+$titular_meses_cotizados_regimen_general+
               $titular_anyos_cotizados_autonomos*12+$titular_meses_cotizados_autonomos) % 12;
    }
    
    //C22 
    private function titular_autonomos_cuota_mensual() {
        return $this->m_Parent->m_SimulacionRow['titular_autonomos_cuota_mensual'];
    }
    
    //C23
    public function titular_autonomos_ingreso_bruto_mensual() {
        return $this->m_Parent->m_SimulacionRow['titular_autonomos_ingreso_bruto_mensual'];
    }
    
    //C24
    private function titular_autonomos_retencion_irpf() {
        return $this->m_Parent->m_SimulacionRow['titular_autonomos_retencion_irpf'];
    }

    //C26
    public function titular_autonomos_neto_mensual_media() { //C23-C23*C24-C22
        return $this->titular_autonomos_ingreso_bruto_mensual()-
               $this->titular_autonomos_ingreso_bruto_mensual()*$this->titular_autonomos_retencion_irpf()/100-
               $this->titular_autonomos_cuota_mensual();
    }
    
    //C27
    public function titular_autonomos_base_cotizacion() {
        if( !($this->m_Parent->m_SimulacionRow['titular_regimen_ss']=="autonomo" ||
              $this->m_Parent->m_SimulacionRow['titular_regimen_ss']=="ambos" )) {
            return 0;
        }

        $base_cotizacion = $this->titular_autonomos_cuota_mensual()/($this->m_Parent->m_Constantes->tipo_cotiz_auton()/100);

        if( $base_cotizacion > $this->m_Parent->m_Informa_Bases_Pensiones->base_maxima_actual() ) {
            $base_cotizacion = $this->m_Parent->m_Informa_Bases_Pensiones->base_maxima_actual();
        }
        else if ( $base_cotizacion < $this->m_Parent->m_Informa_Bases_Pensiones->base_minima_actual() ){
            $base_cotizacion = $this->m_Parent->m_Informa_Bases_Pensiones->base_minima_actual();
        }

        return $base_cotizacion;
    }

    //C31
    private function titular_regimen_general_mes_ordinario() {
        if($this->m_Parent->m_SimulacionRow['titular_rg_neto_mes_ordinario'] != "") {
            return $this->m_Parent->m_SimulacionRow['titular_rg_neto_mes_ordinario'];
        }
        return 0;
    }
    
    //C32
    private function titular_regimen_general_paga_extra() {
        if($this->m_Parent->m_SimulacionRow['titular_rg_paga_extra'] != "") {
            return $this->m_Parent->m_SimulacionRow['titular_rg_paga_extra'];
        }
        return 0;
    }
    
    //C33
    private function titular_regimen_general_numero_pagas_extra() {
        if($this->m_Parent->m_SimulacionRow['titular_rg_numero_pagas_extra'] != "") {
            return $this->m_Parent->m_SimulacionRow['titular_rg_numero_pagas_extra'];
        }
        return 0;
    }
    
    //C34
    private function titular_regimen_general_retencion() {
        if($this->m_Parent->m_SimulacionRow['titular_rg_retencion'] != "") {
            return $this->m_Parent->m_SimulacionRow['titular_rg_retencion'];
        }
        return 0;
    }
    
    //C36
    private function titular_regimen_general_seguridad_social() {
        return $this->m_Parent->parametros_model->get_all()->regimen_general_seguridad_social;
    }
    
    //C38
    private function titular_regimen_general_neto_mensual() {
        return ($this->titular_regimen_general_mes_ordinario()*12+$this->titular_regimen_general_paga_extra()*$this->titular_regimen_general_numero_pagas_extra())/12;
    }
        
    //C39
    private function titular_regimen_general_bruto_mensual() {
        return $this->titular_regimen_general_neto_mensual()/
                (1-$this->titular_regimen_general_retencion()/100-$this->titular_regimen_general_seguridad_social()/100);
    }
    
    //C40
    public function titular_regimen_general_base_cotizacion() {
        $titular_regimen_general_bruto_mensual = $this->titular_regimen_general_bruto_mensual();
        $base_maxima_actual = $this->m_Parent->m_Informa_Bases_Pensiones->base_maxima_actual();
        
        return min($titular_regimen_general_bruto_mensual, $base_maxima_actual);
    }
    
    //C47
    private function titular_ingresos_distintos_ingresos() {
        if($this->m_Parent->m_SimulacionRow['titular_distintos_ingresos']=="") return 0;
        
        return $this->m_Parent->m_SimulacionRow['titular_distintos_ingresos'];
    }
    
    //C49
    private function titular_ingresos_distintos_retencion() {
        if( $this->m_Parent->m_SimulacionRow['titular_distintos_retencion'] == "") {return 0;}
        return $this->m_Parent->m_SimulacionRow['titular_distintos_retencion'];
    }
    
    //C51
    private function titular_ingresos_distintos_pagos_anyo() {
        switch($this->m_Parent->m_SimulacionRow['titular_distintos_periodicidad']) {
            case "anual": return 1;
            case "semestra": return 2;
            case "trimestral": return 4;
            case "mensual": return 12;
            default:
                $this->log_error("Titular: Falta periodicidad ingresos distintos al trabajo");
                return;
        }
    }
        
    //C52
    public function titular_ingresos_distintos_neto_mensual() {
        $titular_ingresos_distintos_ingresos = $this->titular_ingresos_distintos_ingresos();
        if( $titular_ingresos_distintos_ingresos == 0) {return 0;}
        $titular_ingresos_distintos_pagos_anyo = $this->titular_ingresos_distintos_pagos_anyo();
        if($this->m_Parent->config->item("winp3_missing_field") != "") {
            return; 
        }
        return $titular_ingresos_distintos_ingresos*$titular_ingresos_distintos_pagos_anyo/12;
    }
    
    //C53
    private function titular_ingresos_distintos_bruto_mensual() {
        $titular_ingresos_distintos_neto_mensual = $this->titular_ingresos_distintos_neto_mensual();
        $titular_ingresos_distintos_retencion=$this->titular_ingresos_distintos_retencion();
        if($this->m_Parent->config->item("winp3_missing_field") != "") {return; }
        return $titular_ingresos_distintos_neto_mensual/(1-$titular_ingresos_distintos_retencion/100);
    }
    
    //C58
    public function titular_total_ingresos_neto_mensual() {
        $suma=$this->titular_ingresos_distintos_neto_mensual()+ 
               $this->titular_regimen_general_neto_mensual() + 
               $this->titular_autonomos_neto_mensual_media(); 

        return $suma;
    }

    //C59
    public function titular_total_ingresos_bruto_mensual() {
        $titular_ingresos_distintos_bruto_mensual = $this->titular_ingresos_distintos_bruto_mensual();
        $titular_regimen_general_bruto_mensual = $this->titular_regimen_general_bruto_mensual();
        $titular_autonomos_neto_mensual_media = $this->titular_autonomos_neto_mensual_media();
        if($this->m_Parent->config->item("winp3_missing_field") != "") {return; }
        return $titular_ingresos_distintos_bruto_mensual+$titular_regimen_general_bruto_mensual+$titular_autonomos_neto_mensual_media;
    }
    
    //C60
    public function titular_total_ingresos_bruto_anual() {
        $titular_total_ingresos_bruto_mensual = $this->titular_total_ingresos_bruto_mensual();
        if($this->m_Parent->config->item("winp3_missing_field") != "") {return; }
        return $titular_total_ingresos_bruto_mensual*12;
    }
    
    //C64
    private function titular_gastos_capacidad_mensual_ahorro() {
        return $this->m_Parent->m_SimulacionRow['titular_ahorro_mensual'];
    }
    
    //C66
    private function titular_gastos_neto_mensual() {
        return $this->titular_total_ingresos_neto_mensual();
    }
    
    //C67
    public function titular_gastos_mensuales_media() {
        return $this->titular_gastos_neto_mensual()-$this->titular_gastos_capacidad_mensual_ahorro();
    }
    
    //D7
    public function conyugue_regimen_ss() {
        return $this->m_Parent->m_SimulacionRow['conyugue_regimen_ss'];
    }
    
            
    
    //D12
    private function conyugue_anyos_cotizados_regimen_general() {
        return $this->m_Parent->m_SimulacionRow['conyugue_cotizado_ss_rg_anyos'];
    }
    
    //D13
    private function conyugue_meses_cotizados_regimen_general() {
        return $this->m_Parent->m_SimulacionRow['conyugue_cotizado_ss_rg_meses'];
    }

    //D15
    private function conyugue_anyos_cotizados_autonomos() {
        return $this->m_Parent->m_SimulacionRow['conyugue_cotizado_ss_aut_anyos'];
    }
    
    //D16
    private function conyugue_meses_cotizados_autonomos() {
        return $this->m_Parent->m_SimulacionRow['conyugue_cotizado_ss_aut_meses'];
    }

    //D18
    public function conyugue_anyos_cotizados() {
        $tiene_conyugue = $this->m_Parent->m_SimulacionRow['tiene_conyugue'];
        if($tiene_conyugue==0){return 0;}

        $conyugue_anyos_cotizados_regimen_general = $this->conyugue_anyos_cotizados_regimen_general();
        $conyugue_meses_cotizados_regimen_general = $this->conyugue_meses_cotizados_regimen_general();
        $conyugue_anyos_cotizados_autonomos = $this->conyugue_anyos_cotizados_autonomos();
        $conyugue_meses_cotizados_autonomos = $this->conyugue_meses_cotizados_autonomos();
        
        if($conyugue_anyos_cotizados_regimen_general == "" ||
           $conyugue_meses_cotizados_regimen_general == "" ||
           $conyugue_anyos_cotizados_autonomos == "" ||
           $conyugue_meses_cotizados_autonomos == "") {
            $this->log_error("Cónyuge: Falta información para calcular los años cotizados");
            return;
           }
           
        return floor(($conyugue_anyos_cotizados_regimen_general*12+$conyugue_meses_cotizados_regimen_general+
               $conyugue_anyos_cotizados_autonomos*12+$conyugue_meses_cotizados_autonomos) / 12);
    }
    
    //D19
    public function conyugue_meses_cotizados() {
        $tiene_conyugue = $this->m_Parent->m_SimulacionRow['tiene_conyugue'];
        if($tiene_conyugue==0){return 0;}
        
        $conyugue_anyos_cotizados_regimen_general = $this->conyugue_anyos_cotizados_regimen_general();
        $conyugue_meses_cotizados_regimen_general = $this->conyugue_meses_cotizados_regimen_general();
        $conyugue_anyos_cotizados_autonomos = $this->conyugue_anyos_cotizados_autonomos();
        $conyugue_meses_cotizados_autonomos = $this->conyugue_meses_cotizados_autonomos();
        
        if($conyugue_anyos_cotizados_regimen_general == "" ||
           $conyugue_meses_cotizados_regimen_general == "" ||
           $conyugue_anyos_cotizados_autonomos == "" ||
           $conyugue_meses_cotizados_autonomos == "") {
           $this->log_error("Cónyuge: Falta información para calcular los meses cotizados");
           return;
        }
           
        return ($conyugue_anyos_cotizados_regimen_general*12+$conyugue_meses_cotizados_regimen_general+
               $conyugue_anyos_cotizados_autonomos*12+$conyugue_meses_cotizados_autonomos) % 12;
    }
    
    //D22 
    private function conyugue_autonomos_cuota_mensual() {
        return $this->m_Parent->m_SimulacionRow['conyugue_autonomos_cuota_mensual'];
    }
    
    //D23
    public function conyugue_autonomos_ingreso_bruto_mensual() {
        return $this->m_Parent->m_SimulacionRow['conyugue_autonomos_ingreso_bruto_mensual'];
    }
    
    //D24
    private function conyugue_autonomos_retencion_irpf() {
        return $this->m_Parent->m_SimulacionRow['conyugue_autonomos_retencion_irpf'];
    }

    //D26
    public function conyugue_autonomos_neto_mensual_media() {
        return $this->conyugue_autonomos_ingreso_bruto_mensual()-
               $this->conyugue_autonomos_ingreso_bruto_mensual()*$this->conyugue_autonomos_retencion_irpf()/100-
               $this->conyugue_autonomos_cuota_mensual();
    }
    
    //D27
    public function conyugue_autonomos_base_cotizacion() {
        if( !($this->m_Parent->m_SimulacionRow['conyugue_regimen_ss']=="autonomo" ||
              $this->m_Parent->m_SimulacionRow['conyugue_regimen_ss']=="ambos"  )) {
            return 0;
        }

        $base_cotizacion = $this->conyugue_autonomos_cuota_mensual()/($this->m_Parent->m_Constantes->tipo_cotiz_auton()/100);

        if( $base_cotizacion > $this->m_Parent->m_Informa_Bases_Pensiones->base_maxima_actual() ) {
            $base_cotizacion = $this->m_Parent->m_Informa_Bases_Pensiones->base_maxima_actual();
        }
        else if ( $base_cotizacion < $this->m_Parent->m_Informa_Bases_Pensiones->base_minima_actual() ){
            $base_cotizacion = $this->m_Parent->m_Informa_Bases_Pensiones->base_minima_actual();
        }

        return $base_cotizacion;
    }

    //D31
    private function conyugue_regimen_general_mes_ordinario() {
        if($this->m_Parent->m_SimulacionRow['conyugue_rg_neto_mes_ordinario'] != "") {
            return $this->m_Parent->m_SimulacionRow['conyugue_rg_neto_mes_ordinario'];
        }
        return 0;
    }
    
    //D32
    private function conyugue_regimen_general_paga_extra() {
        if($this->m_Parent->m_SimulacionRow['conyugue_rg_paga_extra'] != "") {
            return $this->m_Parent->m_SimulacionRow['conyugue_rg_paga_extra'];
        }
        return 0;
    }
    
    //D33
    private function conyugue_regimen_general_numero_pagas_extra() {
        if($this->m_Parent->m_SimulacionRow['conyugue_rg_numero_pagas_extra'] != "") {
            return $this->m_Parent->m_SimulacionRow['conyugue_rg_numero_pagas_extra'];
        }
        return 0;
    }
    
    //D34
    private function conyugue_regimen_general_retencion() {
        if($this->m_Parent->m_SimulacionRow['conyugue_rg_retencion'] != "") {
            return $this->m_Parent->m_SimulacionRow['conyugue_rg_retencion'];
        }
        return 0;
    }
    
    //D36
    private function conyugue_regimen_general_seguridad_social() {
        return $this->m_Parent->parametros_model->get_all()->regimen_general_seguridad_social;
    }
    
    //D38
    private function conyugue_regimen_general_neto_mensual() {
        return ($this->conyugue_regimen_general_mes_ordinario()*12+$this->conyugue_regimen_general_paga_extra()*$this->conyugue_regimen_general_numero_pagas_extra())/12;
    }
        
    //D39
    private function conyugue_regimen_general_bruto_mensual() {
        return $this->conyugue_regimen_general_neto_mensual()/
                (1-$this->conyugue_regimen_general_retencion()/100-$this->conyugue_regimen_general_seguridad_social()/100);
    }
    
    //D40
    public function conyugue_regimen_general_base_cotizacion() {
        $conyugue_regimen_general_bruto_mensual = $this->conyugue_regimen_general_bruto_mensual();
        $base_maxima_actual = $this->m_Parent->m_Informa_Bases_Pensiones->base_maxima_actual();
        
        return min($conyugue_regimen_general_bruto_mensual, $base_maxima_actual);
    }
    
    //D47
    private function conyugue_ingresos_distintos_ingresos() {
        if($this->m_Parent->m_SimulacionRow['conyugue_distintos_ingresos']=="") return 0;
        
        return $this->m_Parent->m_SimulacionRow['conyugue_distintos_ingresos'];
    }
    
    //D49
    private function conyugue_ingresos_distintos_retencion() {
        if( $this->m_Parent->m_SimulacionRow['conyugue_distintos_retencion'] == "") {return 0;}
        return $this->m_Parent->m_SimulacionRow['conyugue_distintos_retencion'];
    }
    
    //D51
    private function conyugue_ingresos_distintos_pagos_anyo() {
        switch($this->m_Parent->m_SimulacionRow['conyugue_distintos_periodicidad']) {
            case "anual": return 1;
            case "semestra": return 2;
            case "trimestral": return 4;
            case "mensual": return 12;
            default:
                $this->log_error("conyugue: Falta periodicidad ingresos distintos al trabajo");
                return;
        }
    }
        
    //D52
    private function conyugue_ingresos_distintos_neto_mensual() {
        $conyugue_ingresos_distintos_ingresos = $this->conyugue_ingresos_distintos_ingresos();
        if( $conyugue_ingresos_distintos_ingresos == 0) {return 0;}
        $conyugue_ingresos_distintos_pagos_anyo = $this->conyugue_ingresos_distintos_pagos_anyo();
        if($this->m_Parent->config->item("winp3_missing_field") != "") {return; }
        return $conyugue_ingresos_distintos_ingresos*$conyugue_ingresos_distintos_pagos_anyo/12;
    }
    
    //D53
    private function conyugue_ingresos_distintos_bruto_mensual() {
        $conyugue_ingresos_distintos_neto_mensual = $this->conyugue_ingresos_distintos_neto_mensual();
        $conyugue_ingresos_distintos_retencion=$this->conyugue_ingresos_distintos_retencion();
        if($this->m_Parent->config->item("winp3_missing_field") != "") {return; }
        return $conyugue_ingresos_distintos_neto_mensual/(1-$conyugue_ingresos_distintos_retencion/100);
    }
    
    //D58
    public function conyugue_total_ingresos_neto_mensual() {
        return $this->conyugue_ingresos_distintos_neto_mensual()+
               $this->conyugue_regimen_general_neto_mensual() +
               $this->conyugue_autonomos_neto_mensual_media();
    }
    
    //D59
    public function conyugue_total_ingresos_bruto_mensual() {
        $conyugue_ingresos_distintos_bruto_mensual = $this->conyugue_ingresos_distintos_bruto_mensual();
        $conyugue_regimen_general_bruto_mensual = $this->conyugue_regimen_general_bruto_mensual();
        $conyugue_autonomos_neto_mensual_media = $this->conyugue_autonomos_neto_mensual_media();
        if($this->m_Parent->config->item("winp3_missing_field") != "") {return; }
        return $conyugue_ingresos_distintos_bruto_mensual+$conyugue_regimen_general_bruto_mensual+$conyugue_autonomos_neto_mensual_media;
    }
    
    //D60
    public function conyugue_total_ingresos_bruto_anual() {
        $conyugue_total_ingresos_bruto_mensual = $this->conyugue_total_ingresos_bruto_mensual();
        if($this->m_Parent->config->item("winp3_missing_field") != "") {return; }
        return $conyugue_total_ingresos_bruto_mensual*12;
    }
    
    //D64
    private function conyugue_gastos_capacidad_mensual_ahorro() {
        return $this->m_Parent->m_SimulacionRow['conyugue_ahorro_mensual'];
    }
    
    //D66
    private function conyugue_gastos_neto_mensual() {
        return $this->conyugue_total_ingresos_neto_mensual();
    }
    
    //D67
    public function conyugue_gastos_mensuales_media() {
        return $this->conyugue_gastos_neto_mensual()-$this->conyugue_gastos_capacidad_mensual_ahorro();
    }
    
    //D78
    public function deudas_pendientes_familiares_hoy() {
        $deudas = 0;
        if($this->m_Parent->m_SimulacionRow['deuda1_capital_pendiente'] != "" ) {
            $deudas += $this->m_Parent->m_SimulacionRow['deuda1_capital_pendiente'];
        }

        if($this->m_Parent->m_SimulacionRow['deuda2_capital_pendiente'] != "" ) {
            $deudas += $this->m_Parent->m_SimulacionRow['deuda2_capital_pendiente'];
        }
        
        if($this->m_Parent->m_SimulacionRow['deuda3_capital_pendiente'] != "" ) {
            $deudas += $this->m_Parent->m_SimulacionRow['deuda3_capital_pendiente'];
        }

        return $deudas;
    }
    
    
    
    //D96
    public function titular_capitales_ahorro_fallecimiento() {
        $suma = 0;
        for($i = 1; $i<7; $i++){
            if($this->m_Parent->m_SimulacionRow['activos_ahorros_'.$i.'_afectado'] == 'titular') {
                $suma+= $this->m_Parent->m_SimulacionRow['activos_ahorros_'.$i.'_capital_hoy']; 
            }
        }

        return $suma;
    }

    //D97
    public function conyugue_capitales_ahorro_fallecimiento() {
        $suma = 0;
        for($i = 1; $i<7; $i++){
            if($this->m_Parent->m_SimulacionRow['activos_ahorros_'.$i.'_afectado'] == 'conyugue') {
                $suma+= $this->m_Parent->m_SimulacionRow['activos_ahorros_'.$i.'_capital_hoy']; 
            }
        }
        return $suma;
    }
    
    //D98
    public function ambos_capitales_ahorro_fallecimiento() {
        $suma = 0;
        for($i = 1; $i<7; $i++){
            if($this->m_Parent->m_SimulacionRow['activos_ahorros_'.$i.'_afectado'] == 'ambos') {
                $suma+= $this->m_Parent->m_SimulacionRow['activos_ahorros_'.$i.'_capital_hoy'];
            }
        }

        return $suma;
    }
    
    //D109
    public function titular_suma_capitales_seguro_vida_fallecimiento() {
        $suma = 0;
        for($i = 1; $i<6; $i++){

            if($this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_asegurado'] == 'titular') {
                $suma+= $this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_fallecimiento'];
            }
        }

        return $suma;
    }

    //D110
    public function conyugue_suma_capitales_seguro_vida_fallecimiento() {
        $suma = 0;
        for($i = 1; $i<6; $i++){
            if($this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_asegurado'] == 'conyugue') {
                $suma+= $this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_fallecimiento'];
            }
        }

        return $suma;
    }
    
    private function get_numeric($value) {
        if($this->m_Parent->m_SimulacionRow[$value] == "") {return 0;}
        return $this->m_Parent->m_SimulacionRow[$value];
    }
    
    
    
    //E67
    public function gastos_mensuales_media_familia() {
        return $this->titular_gastos_mensuales_media()+$this->conyugue_gastos_mensuales_media();                
    }
    
    //E109
    public function titular_suma_capitales_disponibles_seguro_vida_ipa() {
        $suma = 0;
        for($i = 1; $i<6; $i++){
            if($this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_asegurado'] == 'titular') {
                $suma+= $this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_ipa'];
            }
        }

        return $suma;
    }

    //E110
    public function conyugue_suma_capitales_disponibles_seguro_vida_ipa() {
        $suma = 0;
        for($i = 1; $i<6; $i++){
            if($this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_asegurado'] == 'conyugue') {
                $suma+= $this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_ipa'];
            }
        }

        return $suma;
    }
    
    //F109
    public function titular_suma_capitales_disponibles_seguro_vida_ipt() {
        $suma = 0;
        for($i = 1; $i<6; $i++){
            if($this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_asegurado'] == 'titular') {
                $suma+= $this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_ipt'];
            }
        }

        return $suma;
    }

    //F110
    public function conyugue_suma_capitales_disponibles_seguro_vida_ipt() {
        $suma = 0;
        for($i = 1; $i<6; $i++){
            if($this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_asegurado'] == 'conyugue') {
                $suma+= $this->m_Parent->m_SimulacionRow['activos_sv_'.$i.'_ipt'];
            }
        }

        return $suma;
    }
    
    //G96-G97-G98
    private function suma_capitales_disponibles_ahorro_jubilacion($afectado) {
        $suma = 0;
        for($i = 1; $i < 7; $i++){
            if( $this->m_Parent->m_SimulacionRow['activos_ahorros_'.$i.'_afectado']==$afectado){
                $suma += $this->m_Parent->m_SimulacionRow['activos_ahorros_'.$i.'_capital_jubilacion'];
            }
        }
        return $suma;
    }
    
    //G99
    public function suma_capitales_disponibles_ahorro_jubilacion_total() {
        $titular = $this->suma_capitales_disponibles_ahorro_jubilacion('titular');
        $conyugue = $this->suma_capitales_disponibles_ahorro_jubilacion('conyugue');
        $ambos = $this->suma_capitales_disponibles_ahorro_jubilacion('ambos');
        
        if($this->m_Parent->config->item("winp3_missing_field") != "") {return;}
        
        return $titular+$conyugue+$ambos;
    }

    
}