<?php
class Pensiones {
    private $m_Parent;
    
    public function __construct($parent) {
        $this->m_Parent = $parent[0];
    }
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }

    
    //D6
    private function titular_incapacidad_base_reguladora() {
        return $this->m_Parent->m_Bases->titular_fallecimiento_base_reguladora();
    }
    
    //D8 
    private function titular_ipt_porcentaje() {
        if($this->m_Parent->m_Titular_Lib->titular_edad()<55) {return 55;}
        else {return 75;}

    }
    
    //D9
    private function titular_ipt_calculo() {
        return $this->titular_incapacidad_base_reguladora()*$this->titular_ipt_porcentaje()/100;
    }
    
    //D10
    private function titular_ipt_minima() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->titular_incapacidad_permanente_total();
    }
    
    //D11
    public function titular_ipt_pension() {
        $ipt_calculo = $this->titular_ipt_calculo();//D9
        
        if( $ipt_calculo == 0){return 0;}
        
        $ipt_minima = $this->titular_ipt_minima();//D10
        $pension_maxima = $this->pension_maxima();
                
        if($ipt_calculo<$ipt_minima) {return $ipt_minima;}
        if($ipt_calculo>$pension_maxima) {return $pension_maxima;}
        return $ipt_calculo;
    }
    
    //D13
    private function titular_ipa_porcentaje() {
        return $this->m_Parent->parametros_model->get_all()->pensiones_titular_ipa_porcentaje;
    }
    
    //D14
    private function titular_ipa_calculo() {
        $titular_incapacidad_base_reguladora = $this->titular_incapacidad_base_reguladora();
        $titular_ipa_porcentaje=$this->titular_ipa_porcentaje();

        if($this->m_Parent->config->item("winp3_missing_field")!="") {return;}
        
        return $titular_incapacidad_base_reguladora*$titular_ipa_porcentaje/100;
    }
    
    //D15
    private function titular_ipa_minima() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->titular_incapacidad_permanente_absoluta();
    }
    
    //D16
    public function titular_ipa_pension() {
        $titular_ipa_calculo = $this->titular_ipa_calculo();
        $titular_ipa_minima = $this->titular_ipa_minima();
        $pension_maxima = $this->pension_maxima();
        
        if($this->m_Parent->config->item("winp3_missing_field")!="") {return;}
        
        if( $titular_ipa_calculo == 0){return 0;}
        
        if($titular_ipa_calculo<$titular_ipa_minima){return $titular_ipa_minima;}
        if($titular_ipa_calculo>$pension_maxima) {return $pension_maxima;}
        return $titular_ipa_calculo;
        
    }
    
    //D21
    private function titular_fallecimiento_base_reguladora() {
        return $this->m_Parent->m_Bases->titular_fallecimiento_base_reguladora();
    }

    
    //D23
    private function tiene_conyugue() {
        return $this->m_Parent->m_SimulacionRow['tiene_conyugue'];
    }
    
    //D24
    private function titular_viudedad_porcentaje() {
        //if( $this->tiene_conyugue() == 1) {return $this->m_Parent->parametros_model->get_all()->pensiones_titular_viudedad_porcentaje;} 
        //else {return 0;}
        if( $this->tiene_conyugue() == 1) {return 52;} 
        else {return 0;}

    }
    
    //D25
    private function titular_viudedad_calculo() {
        return $this->titular_fallecimiento_base_reguladora()*$this->titular_viudedad_porcentaje()/100;
    }
    
    //D26
    private function titular_viudedad_minima() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->viudedad();
    }
    
    //D27
    public function titular_viudedad_pension() {
        if($this->tiene_conyugue()==0) {return 0;}
        
        $titular_viudedad_calculo = $this->titular_viudedad_calculo();

        if( $titular_viudedad_calculo == 0 ){return 0;}
        
        $titular_viudedad_minima = $this->titular_viudedad_minima();
        $pension_maxima = $this->pension_maxima();
        
        if($titular_viudedad_calculo<$titular_viudedad_minima) {return $titular_viudedad_minima; }
        if($titular_viudedad_calculo>$pension_maxima) {return $pension_maxima; }

        return $titular_viudedad_calculo;
    }
    
    //D30
    private function titular_hijos_derecho_orfandad() {
        return $this->m_Parent->m_Titular_Lib->titular_hijos_derecho_orfandad();
    }
    
    //D31
    private function titular_queda_porcentaje_fallecimiento() {
        return 100 - $this->titular_viudedad_porcentaje();
    }
    
    //D33
    private function titular_orfandad_porcentaje() {
        $hijos_derecho_orfandad = $this->titular_hijos_derecho_orfandad();
        if( $hijos_derecho_orfandad==0) {return 0;}
        
        $tiene_conyugue = $this->tiene_conyugue();
        $titular_queda_porcentaje_fallecimiento = $this->titular_queda_porcentaje_fallecimiento();

        if( $tiene_conyugue == 1) {
            $valor = $hijos_derecho_orfandad*20;
        }
        else {
            $valor = $hijos_derecho_orfandad*20+52;
        }

        if( $valor > $titular_queda_porcentaje_fallecimiento) {return $titular_queda_porcentaje_fallecimiento;}
        else {return $valor;}
    }
    
    //D34
    private function titular_orfandad_calculo_todos() {//D21*D33

        return $this->titular_fallecimiento_base_reguladora()*$this->titular_orfandad_porcentaje()/100;
    }
    
    //D35
    private function titular_orfandad_calculo_uno() {
        $titular_hijos_derecho_orfandad = $this->titular_hijos_derecho_orfandad();
        if($titular_hijos_derecho_orfandad == 0) {return 0;}

        return $this->titular_orfandad_calculo_todos()/$titular_hijos_derecho_orfandad;
    }
    
    //D36
    private function titular_orfandad_minima() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->orfandad();
    }
    
    //D37
    private function titular_orfandad_pension() {
        $titular_orfandad_calculo_uno = $this->titular_orfandad_calculo_uno();//D35
        
        if( $titular_orfandad_calculo_uno == 0) {return 0;}
        
        $titular_orfandad_minima = $this->titular_orfandad_minima();//D36
        $pension_maxima = $this->pension_maxima(); //E1
        
        if($titular_orfandad_calculo_uno<$titular_orfandad_minima){return $titular_orfandad_minima;}
        if($titular_orfandad_calculo_uno>$pension_maxima){return $pension_maxima;}
        return $titular_orfandad_calculo_uno;
    }
    
    //D38
    public function titular_orfandad_pension_todos() {
        return $this->titular_orfandad_pension()*$this->titular_hijos_derecho_orfandad();
    }

    //D43
    public function titular_es_autonomo() {
        return( $this->m_Parent->m_Economicos_Lib->titular_regimen_ss() == 'autonomo' || 
            $this->m_Parent->m_Economicos_Lib->titular_regimen_ss() == 'ambos' );
    }
    
    //D44 
    private function titular_baja_enfermedad_bc() {
        return $this->m_Parent->m_Economicos_Lib->titular_autonomos_base_cotizacion();
    }
    
    //D45
    public function titular_br_diaria() {
        return $this->titular_baja_enfermedad_bc() / 30;
    }
    
    //D47
    public function titular_prestacion_primer_mes() {
        return 17*$this->titular_br_diaria()*0.6+10*$this->titular_br_diaria()*0.75;
    }
    
    //D48
    public function titular_baja_enfermedad_prestacion_segundo_mes() {
        return $this->titular_baja_enfermedad_bc()*0.75;
    }
    
    //D50 ->Economicos.22 [cuota mensual autonomos]
    
    //E1
    private function pension_maxima() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->pension_maxima();
    }
    
    //E6
    private function conyugue_incapacidad_base_reguladora() {
        return $this->m_Parent->m_Bases->conyugue_fallecimiento_base_reguladora();
    }

    //E8
    private function conyugue_ipt_porcentaje() {
        //if($this->m_Parent->m_Titular_Lib->conyugue_edad()<55) {return $this->m_Parent->parametros_model->get_all()->pensiones_conyugue_ipt_porcentaje_menor_55_anyos;}
        //else {return $this->m_Parent->parametros_model->pensiones_conyugue_ipt_porcentaje_mayor_55_anyos;}
        if($this->m_Parent->m_Titular_Lib->conyugue_edad()<55) {return 55;}
        else {return 75;}

    }

    //E9
    private function conyugue_ipt_calculo() {
        return $this->conyugue_incapacidad_base_reguladora()*$this->conyugue_ipt_porcentaje()/100;
    }
    
    //E10
    private function conyugue_ipt_minima() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->conyugue_incapacidad_permanente_total();
    }

    //E11
    public function conyugue_ipt_pension() {
        $ipt_calculo = $this->conyugue_ipt_calculo();//E9
        
        if( $ipt_calculo == 0){return 0;}
        
        $ipt_minima = $this->conyugue_ipt_minima();//E10
        $pension_maxima = $this->pension_maxima();
        
        if($ipt_calculo<$ipt_minima) {return $ipt_minima;}
        if($ipt_calculo>$pension_maxima) {return $pension_maxima;}
        return $ipt_calculo;
    }
    
    //E13
    private function conyugue_ipa_porcentaje() {
        return $this->m_Parent->parametros_model->get_all()->pensiones_conyugue_ipa_porcentaje;
    }
    
    //E14
    private function conyugue_ipa_calculo() {
        $conyugue_incapacidad_base_reguladora = $this->conyugue_incapacidad_base_reguladora();
        $conyugue_ipa_porcentaje=$this->conyugue_ipa_porcentaje();
        if($this->m_Parent->config->item("winp3_missing_field")!="") {return;}
        
        return $conyugue_incapacidad_base_reguladora*$conyugue_ipa_porcentaje/100;
    }
    
    //E15
    private function conyugue_ipa_minima() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->conyugue_incapacidad_permanente_absoluta();
    }

    //E16
    public function conyugue_ipa_pension() {
        $conyugue_ipa_calculo = $this->conyugue_ipa_calculo();
        $conyugue_ipa_minima = $this->conyugue_ipa_minima();
        $pension_maxima = $this->pension_maxima();
        
        if($this->m_Parent->config->item("winp3_missing_field")!="") {return;}
        
        if( $conyugue_ipa_calculo == 0){return 0;}
        
        if($conyugue_ipa_calculo<$conyugue_ipa_minima){return $conyugue_ipa_minima;}
        if($conyugue_ipa_calculo>$pension_maxima) {return $pension_maxima;}
        return $conyugue_ipa_calculo;
        
    }
    
    //E21
    private function conyugue_fallecimiento_base_reguladora() {
        return $this->m_Parent->m_Bases->conyugue_fallecimiento_base_reguladora();
    }

    //E24
    private function conyugue_viudedad_porcentaje() {
//        if( $this->tiene_conyugue() == 1) {return $this->m_Parent->parametros_model->get_all()->pensiones_conyugue_viudedad_porcentaje;} 
  //      else {return 0;}
        if( $this->tiene_conyugue() == 1) {return 52;} 
        else {return 0;}

    }
    
    //E25
    private function conyugue_viudedad_calculo() {
        return $this->conyugue_fallecimiento_base_reguladora()*$this->conyugue_viudedad_porcentaje()/100;
    }
    
    //E26
    private function conyugue_viudedad_minima() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->viudedad();
    }
    
    //E27
    public function conyugue_viudedad_pension() {
        $tiene_conyugue = $this->tiene_conyugue();
        if(!$tiene_conyugue){return 0;}
        
        $conyugue_viudedad_calculo = $this->conyugue_viudedad_calculo();
        
        if( $conyugue_viudedad_calculo == 0 ) {return 0;}
        
        $conyugue_viudedad_minima = $this->conyugue_viudedad_minima();
        $pension_maxima = $this->pension_maxima();
        
        if($conyugue_viudedad_calculo<$conyugue_viudedad_minima) {return $conyugue_viudedad_minima; }
        if($conyugue_viudedad_calculo>$pension_maxima) {return $pension_maxima; }
        
        return $conyugue_viudedad_calculo;
    }
    
    //E30
    private function conyugue_hijos_derecho_orfandad() {
        return $this->m_Parent->m_Titular_Lib->conyugue_hijos_derecho_orfandad();
    }
    
    //E31
    private function conyugue_queda_porcentaje_fallecimiento() {
        return 100 - $this->conyugue_viudedad_porcentaje();
    }
    
    //E33
    private function conyugue_orfandad_porcentaje() {
        $hijos_derecho_orfandad = $this->conyugue_hijos_derecho_orfandad();
        $tiene_conyugue = 1;
        $conyugue_queda_porcentaje_fallecimiento = $this->conyugue_queda_porcentaje_fallecimiento();
        
        if( $hijos_derecho_orfandad == 0) {return 0;}
        if( $tiene_conyugue == 1) {
            $valor = $hijos_derecho_orfandad*20;
        }
        else {
            $valor = $hijos_derecho_orfandad*20+52;
        }

        if( $valor > $conyugue_queda_porcentaje_fallecimiento) {return $conyugue_queda_porcentaje_fallecimiento;}
        else {return $valor;}
    }
    
    //E34
    private function conyugue_orfandad_calculo_todos() {
        return $this->conyugue_fallecimiento_base_reguladora()*$this->conyugue_orfandad_porcentaje()/100;
    }
    
    //E35
    private function conyugue_orfandad_calculo_uno() {
        $conyugue_hijos_derecho_orfandad = $this->conyugue_hijos_derecho_orfandad();
        if($conyugue_hijos_derecho_orfandad == 0) {return 0;}
        
        return $this->conyugue_orfandad_calculo_todos()/$conyugue_hijos_derecho_orfandad;
    }
    
    //E36
    private function conyugue_orfandad_minima() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->orfandad();
    }
    
    //E37
    private function conyugue_orfandad_pension() {
        $conyugue_orfandad_calculo_uno = $this->conyugue_orfandad_calculo_uno();
        
        if( $conyugue_orfandad_calculo_uno == 0 ){return 0;}
        $conyugue_orfandad_minima = $this->conyugue_orfandad_minima();
        $pension_maxima = $this->pension_maxima(); //E1
        
        if($conyugue_orfandad_calculo_uno<$conyugue_orfandad_minima){return $conyugue_orfandad_minima;}
        if($conyugue_orfandad_calculo_uno>$pension_maxima){return $pension_maxima;}
        return $conyugue_orfandad_calculo_uno;

    }
    
    //E38
    public function conyugue_orfandad_pension_todos() {
        return $this->conyugue_orfandad_pension()*$this->conyugue_hijos_derecho_orfandad();
    }

    //E43
    public function conyugue_es_autonomo() {
        return( $this->m_Parent->m_Economicos_Lib->conyugue_regimen_ss() == 'autonomo' || 
            $this->m_Parent->m_Economicos_Lib->conyugue_regimen_ss() == 'ambos' );
    }
    
    //E44 
    private function conyugue_baja_enfermedad_bc() {
        return $this->m_Parent->m_Economicos_Lib->conyugue_autonomos_base_cotizacion();
    }
    
    //E45
    public function conyugue_br_diaria() {
        return $this->conyugue_baja_enfermedad_bc() / 30;
    }
    
    //E47
    public function conyugue_prestacion_primer_mes() {
        return 17*$this->conyugue_br_diaria()*0.6+10*$this->conyugue_br_diaria()*0.75;
    }
    
    //E48
    public function conyugue_baja_enfermedad_prestacion_segundo_mes() {
        return $this->conyugue_baja_enfermedad_bc()*0.75;
    }
    
    //E50 ->Economicos.22 [cuota mensual autonomos]
    
    //J5
    private function titular_fecha_nacimiento() {
        return $this->m_Parent->m_Titular_Lib->titular_fecha_nacimiento();
    }
    
    //J6
    private function titular_fecha_hoy() {
        return $this->m_Parent->m_Constantes->fecha_estudio();
    }

    //J7
    private function titular_fecha_65_anyos() {
        $fecha_nacimiento = $this->titular_fecha_nacimiento();
        return $fecha_nacimiento->add(new DateInterval("P65Y"));
    }
    
    //J8
    private function titular_fecha_67_anyos() {
        $fecha_nacimiento = $this->titular_fecha_nacimiento();
        return $fecha_nacimiento->add(new DateInterval("P67Y"));
    }
    
    //J11
    private function titular_anyo_de_los_65() {
        return $this->titular_fecha_65_anyos()->format("Y");
    }
    
    //J12
    private function titular_meses_cotizados_hoy() {
        $titular_meses_cotizados = $this->m_Parent->m_Economicos_Lib->titular_meses_cotizados();
        $titular_anyos_cotizados = $this->m_Parent->m_Economicos_Lib->titular_anyos_cotizados();

        return $titular_anyos_cotizados*12+$titular_meses_cotizados;                
    }
    
    //J13
    private function titular_meses_hasta_65_anyos() {
        $titular_fecha_65_anyos = $this->titular_fecha_65_anyos();
        $titular_fecha_hoy = $this->titular_fecha_hoy();

        if($titular_fecha_65_anyos == "" || $titular_fecha_hoy==""){return;}
        
        $diff = ($titular_fecha_65_anyos->diff($titular_fecha_hoy));
        
        return floor($diff->days/365.25*12);
    }
    
    //J14 
    private function titular_meses_cotizados_65_anyos() {
        return $this->titular_meses_cotizados_hoy()+$this->titular_meses_hasta_65_anyos();
    }
    
    //J15
    private function titular_meses_necesarios() {
        $titular_anyos_65 = $this->titular_anyo_de_los_65();
        $jubilarse_anyos;
        $jubilarse_meses;
        $jubilarse_total_meses;
        $edad_sino_anyos;
        $edad_sino_meses;
        $edad_sino_total_meses;
        
        $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_a($titular_anyos_65, $jubilarse_anyos, $jubilarse_meses, $jubilarse_total_meses, $edad_sino_anyos, $edad_sino_meses, $edad_sino_total_meses);

        if($this->m_Parent->config->item("winp3_missing_field")!="") {return;}
        
        return $jubilarse_total_meses;
    }
    
    //J17
    private function titular_faltan_meses() {
        $titular_meses_necesarios = $this->titular_meses_necesarios();
        $titular_meses_cotizados_65_anyos = $this->titular_meses_cotizados_65_anyos();

        if($this->m_Parent->config->item("win3p_missing_fields")!="") {return;}

        if( $titular_meses_necesarios>$titular_meses_cotizados_65_anyos){return $titular_meses_necesarios-$titular_meses_cotizados_65_anyos;}
        else {return 0;}
    }
    
    //J19
    public function titular_fecha_jubilacion() {
        $faltan_meses = $this->titular_faltan_meses();
        $fecha_65_anyos = $this->titular_fecha_65_anyos();
        $fecha_67_anyos = $this->titular_fecha_67_anyos();
        
        if($this->m_Parent->config->item("win3p_missing_fields")!="") {return;}

        if($faltan_meses<24){
            return $fecha_65_anyos->add(new DateInterval("P".$faltan_meses."M"));
        }
        else {
            return $fecha_67_anyos;
        }
    }
    
    //J20
    public function titular_edad_jubilacion() {
        $titular_fecha_jubilacion = $this->titular_fecha_jubilacion();
        $titular_fecha_nacimiento = $this->titular_fecha_nacimiento();
        
        return $titular_fecha_nacimiento->diff($titular_fecha_jubilacion)->format("%a")/365.25;
    }
    
    
    
    //J23
    public function titular_anyo_jubilacion() {
        $fecha_jubilacion = $this->titular_fecha_jubilacion();
        return $fecha_jubilacion->format("Y");
    }
    
    //J24
    public function titular_anyos_para_br() {
        $anyo_jubilacion = $this->titular_anyo_jubilacion();
        $meses_computables;
        $divisor;
        $anyos_computables;
        $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_b($anyo_jubilacion, $meses_computables, $divisor, $anyos_computables);

        if($this->m_Parent->config->item("winp3_missing_field")!="") {return;}
        return $anyos_computables;
    }
    
    //J27
    private function titular_meses_cotizados_jubilacion() {
        $faltan_meses = $this->titular_faltan_meses();
        $meses_cotizados_65_anyos = $this->titular_meses_cotizados_65_anyos();

        return $faltan_meses+$meses_cotizados_65_anyos;
    }

    //J28
    private function titular_primer_tramo_hasta() {
        return $this->m_Parent->parametros_model->get_all()->pensiones_titular_primer_tramo_hasta;
    }
    
    //J29
    private function titular_segundo_tramo_hasta() {
        $titular_anyo_jubilacion = $this->titular_anyo_jubilacion();
        $columna = 3;
        return $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_c($titular_anyo_jubilacion, $columna);
    }
    
    //J30
    private function titular_tercer_tramo_hasta() {
        $titular_anyo_jubilacion = $this->titular_anyo_jubilacion();
        $columna = 4;
        return $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_c($titular_anyo_jubilacion, $columna);
    }
    
    //J31
    private function titular_porcentaje_br_primer_tramo() {
        return $this->m_Parent->parametros_model->get_all()->pensiones_titular_porcentaje_br_primer_tramo;
    }
    
    //J32
    private function titular_porcentaje_br_segundo_tramo() {
        $titular_anyo_jubilacion = $this->titular_anyo_jubilacion();
        $columna = 5;
        return $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_c($titular_anyo_jubilacion, $columna);
    }

    //J33
    private function titular_porcentaje_br_tercer_tramo() {
        $titular_anyo_jubilacion = $this->titular_anyo_jubilacion();
        $columna = 6;
        return $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_c($titular_anyo_jubilacion, $columna);
    }

    
    //J34
    private function titular_porcentaje_conseguido_primer_tramo() {
        $meses_cotizados_jubilacion = $this->titular_meses_cotizados_jubilacion();
        $primer_tramo_hasta = $this->titular_primer_tramo_hasta();
        $porcentaje_br_primer_tramo = $this->titular_porcentaje_br_primer_tramo();
        
        if( $meses_cotizados_jubilacion<$primer_tramo_hasta){return 0;}
        return $porcentaje_br_primer_tramo;
    }
    
    //J35
    private function titular_porcentaje_conseguido_segundo_tramo() {
        $titular_meses_cotizados_jubilacion = $this->titular_meses_cotizados_jubilacion();
        $titular_primer_tramo_hasta = $this->titular_primer_tramo_hasta();
        $titular_segundo_tramo_hasta = $this->titular_segundo_tramo_hasta();
        $titular_porcentaje_br_segundo_tramo = $this->titular_porcentaje_br_segundo_tramo();

        if($titular_meses_cotizados_jubilacion>$titular_primer_tramo_hasta){
            return (min($titular_segundo_tramo_hasta, $titular_meses_cotizados_jubilacion)-$titular_primer_tramo_hasta)*$titular_porcentaje_br_segundo_tramo;
        }
        else{
            return 0;
        }
    }
    
    //J36
    private function titular_porcentaje_conseguido_tercer_tramo() {
        $titular_meses_cotizados_jubilacion = $this->titular_meses_cotizados_jubilacion();
        $titular_segundo_tramo_hasta = $this->titular_segundo_tramo_hasta();
        $titular_tercer_tramo_hasta = $this->titular_tercer_tramo_hasta();
        $titular_porcentaje_br_tercer_tramo = $this->titular_porcentaje_br_tercer_tramo();
        
        if($titular_meses_cotizados_jubilacion>$titular_segundo_tramo_hasta){
            return (min($titular_tercer_tramo_hasta,$titular_meses_cotizados_jubilacion)-$titular_segundo_tramo_hasta)*$titular_porcentaje_br_tercer_tramo;
        }
        else {
            return 0;
        }
    }
    
    
    //J38
    private function titular_jubilacion_porcentaje() {
        return $this->titular_porcentaje_conseguido_primer_tramo()+$this->titular_porcentaje_conseguido_segundo_tramo()+$this->titular_porcentaje_conseguido_tercer_tramo();
    }
    
    //J41
    private function titular_pension_base_reguladora() {
        return $this->m_Parent->m_Bases->titular_jubilacion_base_reguladora();
    }
    
    //J42
    private function titular_anyos_para_jubilacion() {
        return $this->titular_anyo_jubilacion()-$this->m_Parent->m_Constantes->anyo_estudio();
    }

    //J44
    private function titular_pension_jubilacion_porcentaje() {
        return $this->titular_jubilacion_porcentaje();
    }
    
    //J45
    private function titular_jubilacion_calculo() {
        $titular_pension_base_reguladora = $this->titular_pension_base_reguladora();//J41
        $titular_pension_jubilacion_porcentaje = $this->titular_pension_jubilacion_porcentaje();//J44

        return $titular_pension_base_reguladora*$titular_pension_jubilacion_porcentaje/100;
    }
    
    //J47
    private function titular_jubilacion_minima_hoy() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->titular_pension_minima_jubilacion();
    }
    
    //J48
    private function titular_jubilacion_minima_fecha_jubilacion() {
        $titular_jubilacion_minima_hoy = $this->titular_jubilacion_minima_hoy();
        $titular_anyos_jubilacion = $this->titular_anyos_para_jubilacion();
        return $titular_jubilacion_minima_hoy*pow(1+$this->m_Parent->m_Constantes->ipc()/100,$titular_anyos_jubilacion);
    }

    //J50
    private function titular_jubilacion_maxima_hoy() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->pension_maxima();
    }
    
    //J51
    private function titular_jubilacion_maxima_fecha_jubilacion() {
        $titular_jubilacion_maxima_hoy = $this->titular_jubilacion_maxima_hoy();
        $titular_anyos_para_jubilacion = $this->titular_anyos_para_jubilacion();
        return $titular_jubilacion_maxima_hoy*pow(1+$this->m_Parent->m_Constantes->ipc()/100, $titular_anyos_para_jubilacion);
    }
    
    //J53
    public function titular_jubilacion_pension() {
        $titular_jubilacion_calculo = $this->titular_jubilacion_calculo();
        
        if( $titular_jubilacion_calculo == 0) {return 0;}
        
        $titular_jubilacion_maxima_fecha_jubilacion = $this->titular_jubilacion_maxima_fecha_jubilacion();
        $titular_jubilacion_minima_fecha_jubilacion = $this->titular_jubilacion_minima_fecha_jubilacion();

        if($titular_jubilacion_calculo>$titular_jubilacion_maxima_fecha_jubilacion) {return $titular_jubilacion_maxima_fecha_jubilacion;}
        if($titular_jubilacion_calculo<$titular_jubilacion_minima_fecha_jubilacion) {return $titular_jubilacion_minima_fecha_jubilacion;}
        return $titular_jubilacion_calculo;
    }

    //K5
    private function conyugue_fecha_nacimiento() {
        return $this->m_Parent->m_Titular_Lib->conyugue_fecha_nacimiento();
    }
    
    //K6
    private function conyugue_fecha_hoy() {
        return $this->m_Parent->m_Constantes->fecha_estudio();
    }

    //K7
    private function conyugue_fecha_65_anyos() {
        $fecha_nacimiento = $this->conyugue_fecha_nacimiento();
        return $fecha_nacimiento->add(new DateInterval("P65Y"));
    }
    
    //K8
    private function conyugue_fecha_67_anyos() {
        $fecha_nacimiento = $this->conyugue_fecha_nacimiento();
        return $fecha_nacimiento->add(new DateInterval("P67Y"));
    }
    
    //K11
    private function conyugue_anyo_de_los_65() {
        return $this->conyugue_fecha_65_anyos()->format("Y");
    }
    
    //K12
    private function conyugue_meses_cotizados_hoy() {
        $conyugue_meses_cotizados = $this->m_Parent->m_Economicos_Lib->conyugue_meses_cotizados();
        $conyugue_anyos_cotizados = $this->m_Parent->m_Economicos_Lib->conyugue_anyos_cotizados();

        return $conyugue_anyos_cotizados*12+$conyugue_meses_cotizados;                
    }
    
    //K13
    private function conyugue_meses_hasta_65_anyos() {
        $conyugue_fecha_65_anyos = $this->conyugue_fecha_65_anyos();
        $conyugue_fecha_hoy = $this->conyugue_fecha_hoy();

        if($conyugue_fecha_65_anyos == "" || $conyugue_fecha_hoy==""){return;}
        
        $diff = ($conyugue_fecha_65_anyos->diff($conyugue_fecha_hoy));
        return floor($diff->days/365.25*12);
    }
    
    //K14 
    private function conyugue_meses_cotizados_65_anyos() {
        return $this->conyugue_meses_cotizados_hoy()+$this->conyugue_meses_hasta_65_anyos();
    }
    
    //K15
    private function conyugue_meses_necesarios() {
        $conyugue_anyos_65 = $this->conyugue_anyo_de_los_65();
        $jubilarse_anyos;
        $jubilarse_meses;
        $jubilarse_total_meses;
        $edad_sino_anyos;
        $edad_sino_meses;
        $edad_sino_total_meses;
        
        $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_a($conyugue_anyos_65, $jubilarse_anyos, $jubilarse_meses, $jubilarse_total_meses, $edad_sino_anyos, $edad_sino_meses, $edad_sino_total_meses);

        if($this->m_Parent->config->item("winp3_missing_field")!="") {return;}
        
        return $jubilarse_total_meses;
    }
    
    //K17
    private function conyugue_faltan_meses() {
        $conyugue_meses_necesarios = $this->conyugue_meses_necesarios();
        $conyugue_meses_cotizados_65_anyos = $this->conyugue_meses_cotizados_65_anyos();

        if($this->m_Parent->config->item("win3p_missing_fields")!="") {return;}
        
        if( $conyugue_meses_necesarios>$conyugue_meses_cotizados_65_anyos){return $conyugue_meses_necesarios-$conyugue_meses_cotizados_65_anyos;}
        else {return 0;}
    }
    
    //K19
    public function conyugue_fecha_jubilacion() {
        $faltan_meses = $this->conyugue_faltan_meses();
        $fecha_65_anyos = $this->conyugue_fecha_65_anyos();
        $fecha_67_anyos = $this->conyugue_fecha_67_anyos();
        
        if($this->m_Parent->config->item("win3p_missing_fields")!="") {return;}

        if($faltan_meses<24){
            return $fecha_65_anyos->add(new DateInterval("P".$faltan_meses."M"));
        }
        else {
            return $fecha_67_anyos;
        }
    }
    
    //K20
    public function conyugue_edad_jubilacion() {
        $conyugue_fecha_jubilacion = $this->conyugue_fecha_jubilacion();
        $conyugue_fecha_nacimiento = $this->conyugue_fecha_nacimiento();
        return $conyugue_fecha_nacimiento->diff($conyugue_fecha_jubilacion)->format("%a")/365.25;
    }
    
    
    
    //K23
    public function conyugue_anyo_jubilacion() {
        $fecha_jubilacion = $this->conyugue_fecha_jubilacion();
        return $fecha_jubilacion->format("Y");
    }
    
    //K24
    public function conyugue_anyos_para_br() {
        $anyo_jubilacion = $this->conyugue_anyo_jubilacion();
        $meses_computables;
        $divisor;
        $anyos_computables;
        $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_b($anyo_jubilacion, $meses_computables, $divisor, $anyos_computables);

        if($this->m_Parent->config->item("winp3_missing_field")!="") {return;}
        
        return $anyos_computables;
    }
    
    //K27
    private function conyugue_meses_cotizados_jubilacion() {
        $faltan_meses = $this->conyugue_faltan_meses();
        $meses_cotizados_65_anyos = $this->conyugue_meses_cotizados_65_anyos();
        
        return $faltan_meses+$meses_cotizados_65_anyos;
    }

    //K28
    private function conyugue_primer_tramo_hasta() {
        return $this->m_Parent->parametros_model->get_all()->pensiones_conyugue_primer_tramo_hasta;
    }
    
    //K29
    private function conyugue_segundo_tramo_hasta() {
        $conyugue_anyo_jubilacion = $this->conyugue_anyo_jubilacion();
        $columna = 3;
        return $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_c($conyugue_anyo_jubilacion, $columna);
    }
    
    //K30
    private function conyugue_tercer_tramo_hasta() {
        $conyugue_anyo_jubilacion = $this->conyugue_anyo_jubilacion();
        $columna = 4;
        return $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_c($conyugue_anyo_jubilacion, $columna);
    }
    
    //K31
    private function conyugue_porcentaje_br_primer_tramo() {
        return $this->m_Parent->parametros_model->get_all()->pensiones_conyugue_porcentaje_br_primer_tramo;
    }
    
    //K32
    private function conyugue_porcentaje_br_segundo_tramo() {
        $conyugue_anyo_jubilacion = $this->conyugue_anyo_jubilacion();
        $columna = 5;
        return $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_c($conyugue_anyo_jubilacion, $columna);
    }

    //K33
    private function conyugue_porcentaje_br_tercer_tramo() {
        $conyugue_anyo_jubilacion = $this->conyugue_anyo_jubilacion();
        $columna = 6;
        return $this->m_Parent->simulacion_model->get_calculo_jubilacion_tabla_c($conyugue_anyo_jubilacion, $columna);
    }

    
    //K34
    private function conyugue_porcentaje_conseguido_primer_tramo() {
        $meses_cotizados_jubilacion = $this->conyugue_meses_cotizados_jubilacion();
        $primer_tramo_hasta = $this->conyugue_primer_tramo_hasta();
        $porcentaje_br_primer_tramo = $this->conyugue_porcentaje_br_primer_tramo();
        
        if( $meses_cotizados_jubilacion<$primer_tramo_hasta){return 0;}
        return $porcentaje_br_primer_tramo;
    }
    
    //K35
    private function conyugue_porcentaje_conseguido_segundo_tramo() {
        $conyugue_meses_cotizados_jubilacion = $this->conyugue_meses_cotizados_jubilacion();
        $conyugue_primer_tramo_hasta = $this->conyugue_primer_tramo_hasta();
        $conyugue_segundo_tramo_hasta = $this->conyugue_segundo_tramo_hasta();
        $conyugue_porcentaje_br_segundo_tramo = $this->conyugue_porcentaje_br_segundo_tramo();
        
        if($conyugue_meses_cotizados_jubilacion>$conyugue_primer_tramo_hasta){
            return (min($conyugue_segundo_tramo_hasta, $conyugue_meses_cotizados_jubilacion)-$conyugue_primer_tramo_hasta)*$conyugue_porcentaje_br_segundo_tramo;
        }
        else{
            return 0;
        }
    }
    
    //K36
    private function conyugue_porcentaje_conseguido_tercer_tramo() {
        $conyugue_meses_cotizados_jubilacion = $this->conyugue_meses_cotizados_jubilacion();
        $conyugue_segundo_tramo_hasta = $this->conyugue_segundo_tramo_hasta();
        $conyugue_tercer_tramo_hasta = $this->conyugue_tercer_tramo_hasta();
        $conyugue_porcentaje_br_tercer_tramo = $this->conyugue_porcentaje_br_tercer_tramo();
        
        if($conyugue_meses_cotizados_jubilacion>$conyugue_segundo_tramo_hasta){
            return (min($conyugue_tercer_tramo_hasta,$conyugue_meses_cotizados_jubilacion)-$conyugue_segundo_tramo_hasta)*$conyugue_porcentaje_br_tercer_tramo;
        }
        else {
            return 0;
        }
    }
    
    
    //K38
    private function conyugue_jubilacion_porcentaje() {
        return $this->conyugue_porcentaje_conseguido_primer_tramo()+$this->conyugue_porcentaje_conseguido_segundo_tramo()+$this->conyugue_porcentaje_conseguido_tercer_tramo();
    }
    
    //K41
    private function conyugue_pension_base_reguladora() {
        $this->m_Parent->m_Bases->conyugue_jubilacion_base_reguladora();
        return $this->m_Parent->m_Bases->conyugue_jubilacion_base_reguladora();
    }
    
    //K42
    private function conyugue_anyos_para_jubilacion() {
        return $this->conyugue_anyo_jubilacion()-$this->m_Parent->m_Constantes->anyo_estudio();
    }

    //K44
    private function conyugue_pension_jubilacion_porcentaje() {
        return $this->conyugue_jubilacion_porcentaje();
    }
    
    //K45
    private function conyugue_jubilacion_calculo() {
        $conyugue_pension_base_reguladora = $this->conyugue_pension_base_reguladora();
        $conyugue_pension_jubilacion_porcentaje = $this->conyugue_pension_jubilacion_porcentaje();

        return $conyugue_pension_base_reguladora*$conyugue_pension_jubilacion_porcentaje/100;
    }
    
    //K47
    private function conyugue_jubilacion_minima_hoy() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->conyugue_pension_minima_jubilacion();
    }
    
    //K48
    private function conyugue_jubilacion_minima_fecha_jubilacion() {
        $conyugue_jubilacion_minima_hoy = $this->conyugue_jubilacion_minima_hoy();
        $conyugue_anyos_jubilacion = $this->conyugue_anyos_para_jubilacion();
        return $conyugue_jubilacion_minima_hoy*pow(1+$this->m_Parent->m_Constantes->ipc()/100,$conyugue_anyos_jubilacion);
    }

    //K50
    private function conyugue_jubilacion_maxima_hoy() {
        return $this->m_Parent->m_Informa_Bases_Pensiones->pension_maxima();
    }
    
    //K51
    private function conyugue_jubilacion_maxima_fecha_jubilacion() {
        $conyugue_jubilacion_maxima_hoy = $this->conyugue_jubilacion_maxima_hoy();
        $conyugue_anyos_para_jubilacion = $this->conyugue_anyos_para_jubilacion();
        return $conyugue_jubilacion_maxima_hoy*pow(1+$this->m_Parent->m_Constantes->ipc()/100, $conyugue_anyos_para_jubilacion);
    }
    
    //K53
    public function conyugue_jubilacion_pension() {
        $conyugue_jubilacion_calculo = $this->conyugue_jubilacion_calculo();
        
        if( $conyugue_jubilacion_calculo == 0) {return 0;}
        
        $conyugue_jubilacion_maxima_fecha_jubilacion = $this->conyugue_jubilacion_maxima_fecha_jubilacion();
        $conyugue_jubilacion_minima_fecha_jubilacion = $this->conyugue_jubilacion_minima_fecha_jubilacion();
        if($conyugue_jubilacion_calculo>$conyugue_jubilacion_maxima_fecha_jubilacion) {return $conyugue_jubilacion_maxima_fecha_jubilacion;}
        if($conyugue_jubilacion_calculo<$conyugue_jubilacion_minima_fecha_jubilacion) {return $conyugue_jubilacion_minima_fecha_jubilacion;}
        
        return $conyugue_jubilacion_calculo;
    }
    
}    
?>