<?php
class Titular_Lib {
    private $m_Parent;
    
    public function __construct($params) {
        $this->m_Parent = $params[0];
    }

    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }

    //C6
    public function titular_fecha_nacimiento() {
        $day =  substr($this->m_Parent->m_SimulacionRow['titular_fecha_nacimiento'], 0, 2);
        $month = substr($this->m_Parent->m_SimulacionRow['titular_fecha_nacimiento'], 3, 2);
        $year = substr($this->m_Parent->m_SimulacionRow['titular_fecha_nacimiento'], 6, 4);
        $fecha_nacimiento = new DateTime();
        $fecha_nacimiento->setDate($year, $month, $day);
        return $fecha_nacimiento;       
    }

    //C7
    public function tiene_conyugue() {
        return $this->m_Parent->m_SimulacionRow['tiene_conyugue'];
    }
    
    //C10 
    public function titular_edad() {
        $fecha = $this->m_Parent->m_SimulacionRow['titular_fecha_nacimiento'];
        if( $fecha == "" ) {
            $this->log_error("Titular: Falta fecha nacimiento");
            return; 
        }
        list($d,$m,$Y) = explode("-",$fecha);
        $edad = date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y;
        if( $edad < 18 ){
            $this->log_error("El titular tiene menos de 18 años");
            return; 
        }
        return $edad;
    }
    
    //C16
    public function conyugue_fecha_nacimiento() {
        $day =  substr($this->m_Parent->m_SimulacionRow['conyugue_fecha_nacimiento'], 0, 2);
        $month = substr($this->m_Parent->m_SimulacionRow['conyugue_fecha_nacimiento'], 3, 2);
        $year = substr($this->m_Parent->m_SimulacionRow['conyugue_fecha_nacimiento'], 6, 4);
        $fecha_nacimiento = new DateTime();
        $fecha_nacimiento->setDate($year, $month, $day);
        return $fecha_nacimiento;       
    }

    //C17
    public function conyugue_edad() {
        $fecha = $this->m_Parent->m_SimulacionRow['conyugue_fecha_nacimiento'];
        if( $fecha == "" ) {
            $this->log_error("Cónyuge: Falta fecha nacimiento");
            return; 
        }
        list($d,$m,$Y) = explode("-",$fecha);

        $edad = date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y;
        
        if( $edad < 18 ){
            $this->log_error("El cónyuge tiene menos de 18 años");
            return; 
        }

        return $edad;
    }
    
    public function titular_hijo_derecho_orfandad($hijo) {
        if( $this->m_Parent->m_SimulacionRow['hijo_'.$hijo.'_de']=="" || $this->m_Parent->m_SimulacionRow['hijo_'.$hijo.'_de']=='solo_conyugue'){return 0;}
        
        if($this->hijo_edad($hijo)<25) {return 1;}
        else {return 0;}
    }

    public function conyugue_hijo_derecho_orfandad($hijo) {
        if( $this->m_Parent->m_SimulacionRow['hijo_'.$hijo.'_de']=="" || $this->m_Parent->m_SimulacionRow['hijo_'.$hijo.'_de']=='solo_titular'){return 0;}
        
        if($this->hijo_edad($hijo)<25) {return 1;}
        else {return 0;}
    }

    public function hijo_edad($hijo) {
        $fecha = $this->m_Parent->m_SimulacionRow['hijo_'.$hijo.'_fecha_nacimiento'];
        if( $fecha == "" ) {return 0; }
        list($d,$m,$Y) = explode("-",$fecha);
        return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
    }

    
    //C71
    public function titular_hijos_derecho_orfandad() {
        $numero = 0;
        $numero_hijos = $this->m_Parent->m_SimulacionRow['numero_hijos'];
        for( $i = 1; $i<=$numero_hijos; $i++ ) {
            if( $this->titular_hijo_derecho_orfandad($i)==1) {$numero++;}
        }
        
        return $numero;
    }
    
    //C73 
    private function fecha_nacimiento_menor() {
        $fecha_menor='';
        for($i = 1; $i<= $this->m_Parent->m_SimulacionRow['numero_hijos']; $i++) {
            $day =  substr($this->m_Parent->m_SimulacionRow['hijo_'.$i."_fecha_nacimiento"], 0, 2);
            $month = substr($this->m_Parent->m_SimulacionRow['hijo_'.$i."_fecha_nacimiento"], 3, 2);
            $year = substr($this->m_Parent->m_SimulacionRow['hijo_'.$i."_fecha_nacimiento"], 6, 4);
            $fecha_nacimiento = new DateTime();
            $fecha_nacimiento->setDate($year, $month, $day);
            
            if($i==1) {
                $fecha_menor = $fecha_nacimiento;
            }
            else {
                $fecha_menor = min($fecha_nacimiento, $fecha_menor);
            }
        }
        
        return $fecha_menor;
    }
    
    //C74
    private function veinticinco_anyos_del_menor() {
        $fecha_nacimiento_menor = $this->fecha_nacimiento_menor();
        if($fecha_nacimiento_menor=="") {return; }
        return $fecha_nacimiento_menor->add(new DateInterval("P25Y"));
    }
    
    //C75
    public function tiempo_restante_anyos() {
        $veinticinco_anyos_del_menor = $this->veinticinco_anyos_del_menor(); 
        if($veinticinco_anyos_del_menor==""){return;}
        $hoy = new DateTime();
        return $hoy->diff($veinticinco_anyos_del_menor)->format("%a")/365.25;
    }
    
    //D71
    public function conyugue_hijos_derecho_orfandad() {
        $numero = 0;
        for( $i = 1; $i<6; $i++ ) {
            if( $this->conyugue_hijo_derecho_orfandad($i)==1) {$numero++;}
        }
        
        return $numero;
    }

    
}