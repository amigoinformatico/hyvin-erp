<?php
class Fallecimiento_Lib {
    private $m_Parent;
    
    public function __construct($parent) {
        $this->m_Parent = $parent[0];
    }
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }
 
    //D7
    public function titular_gastos_mensuales_familiares() {
        return -($this->m_Parent->m_Economicos_Lib->gastos_mensuales_media_familia());//Ecponomicos.E67
    }
    
    //D9
    public function titular_ingresos_conyugue() {
        return $this->m_Parent->m_Economicos_Lib->conyugue_total_ingresos_neto_mensual();
    }
    
    //D10
    public function titular_pension_viudedad() {
        return $this->m_Parent->m_Pensiones->titular_viudedad_pension();//P.D27
    }
    
    //D11
    public function titular_pension_orfandad() {
        return $this->m_Parent->m_Pensiones->titular_orfandad_pension_todos();//P.D38
    }
    
    //D13
    public function titular_saldo_mensual() {
        return $this->titular_gastos_mensuales_familiares()+$this->titular_ingresos_conyugue()+
               $this->titular_pension_viudedad()+$this->titular_pension_orfandad();
    }
    
    //D15
    private function titular_porcentaje_cobertura() {
        return $this->m_Parent->m_SimulacionRow['fallecimiento_titular_porcentaje_cobertura'];
    }
    
    //D17
    public function titular_anyos_cobertura() {
        //return max( $this->m_Parent->m_Titular_Lib->tiempo_restante_anyos(), 5);
        return $this->m_Parent->m_SimulacionRow['fallecimiento_titular_anyos_cobertura'];
    }
    
    //D19
    public function titular_cobertura_necesaria() {
        if($this->titular_saldo_mensual() < 0 ) { 
            return $this->titular_saldo_mensual()*$this->titular_anyos_cobertura()*12*$this->titular_porcentaje_cobertura()/100;
        }
        else {
            return 0;
        }
    }
    
    //D21
    public function titular_deudas_pendientes() {
        return -($this->m_Parent->m_Economicos_Lib->deudas_pendientes_familiares_hoy());//E.D78
    }
    
    //D22
    public function titular_gastos_entierro() {
        return -($this->m_Parent->m_Constantes->gastos_entierro());
    }
    
    //D23
    public function titular_costes_aceptacion_herencia() {
        return -($this->m_Parent->m_Constantes->costes_aceptacion_herencia());
    }
    
    //D25
    public function titular_capitales_ahorro_titular() {
        return $this->m_Parent->m_Economicos_Lib->titular_capitales_ahorro_fallecimiento();
    }
    
    //D26
    public function titular_capitales_ahorro_ambos(){
        return $this->m_Parent->m_Economicos_Lib->ambos_capitales_ahorro_fallecimiento();
    }
    
    //D28
    public function titular_saldo_seguro_vida_necesario() {
        return $this->titular_cobertura_necesaria()+
               $this->titular_deudas_pendientes() + $this->titular_gastos_entierro()+
               $this->titular_costes_aceptacion_herencia() + $this->titular_capitales_ahorro_titular()+
               $this->titular_capitales_ahorro_ambos();
    }
    
    //D30 
    public function titular_seguros_ya_contratados() {
        return $this->m_Parent->m_Economicos_Lib->titular_suma_capitales_seguro_vida_fallecimiento();
    }

    //D32
    public function titular_necesidad_seguro_vida() {
        return -($this->titular_saldo_seguro_vida_necesario() + $this->titular_seguros_ya_contratados());
    }
    
    //D40
    public function beneficiario_relacion($beneficiario){
        return $this->m_Parent->m_SimulacionRow['fallecimiento_beneficiario_'.$beneficiario.'_relacion'];
    }
    
    //D41
    public function beneficiario_edad($beneficiario){
        return $this->m_Parent->m_SimulacionRow['fallecimiento_beneficiario_'.$beneficiario.'_edad_descendiente'];
    }
    
    //D42
    public function beneficiario_importe_cobrar_seguro_vida($beneficiario) {
        return $this->m_Parent->m_SimulacionRow['fallecimiento_beneficiario_'.$beneficiario.'_importe_cobrar'];
    }
    
    //D45
    public function bonificaciones_cuota() {
        $ccaa = $this->m_Parent->m_SimulacionRow['comunidad_residencia'];
        return $this->m_Parent->simulacion_model->get_bonificaciones_cuota($ccaa);
    }
    
    //E42
    public function beneficiario_2_importe_cobrar_seguro_vida() {
        return $this->m_Parent->m_SimulacionRow['fallecimiento_beneficiario_2_importe_cobrar'];
    }

    //F7 //Igual que D7
    public function conyugue_gastos_mensuales_familiares() {
        return -($this->m_Parent->m_Economicos_Lib->gastos_mensuales_media_familia());
    }
    
    //F9
    public function conyugue_ingresos_conyugue() {
        return $this->m_Parent->m_Economicos_Lib->titular_total_ingresos_neto_mensual();
    }
    
    //F10
    public function conyugue_pension_viudedad() {
        return $this->m_Parent->m_Pensiones->conyugue_viudedad_pension();
    }
    
    //F11
    public function conyugue_pension_orfandad() {
        return $this->m_Parent->m_Pensiones->conyugue_orfandad_pension_todos();
    }
    
    //F13
    public function conyugue_saldo_mensual() {
        return $this->conyugue_gastos_mensuales_familiares()+$this->conyugue_ingresos_conyugue()+
               $this->conyugue_pension_viudedad()+$this->conyugue_pension_orfandad();
    }
    
    //F15
    private function conyugue_porcentaje_cobertura() {
        return $this->m_Parent->m_SimulacionRow['fallecimiento_conyugue_porcentaje_cobertura'];
    }

    
    //F17
    public function conyugue_anyos_cobertura() {
        //return max( $this->m_Parent->m_Titular_Lib->tiempo_restante_anyos(), 5);    
        return $this->m_Parent->m_SimulacionRow['fallecimiento_conyugue_anyos_cobertura'];
    }
    
    //F19
    public function conyugue_cobertura_necesaria() {
        if($this->conyugue_saldo_mensual() < 0 ) { 
            return $this->conyugue_saldo_mensual()*$this->conyugue_anyos_cobertura()*12*$this->conyugue_porcentaje_cobertura()/100;
        }
        else {
            return 0;
        }
    }
    
    //F21
    public function conyugue_deudas_pendientes() {
        return -($this->m_Parent->m_Economicos_Lib->deudas_pendientes_familiares_hoy());
    }
    
    //F22
    public function conyugue_gastos_entierro() {
        return -($this->m_Parent->m_Constantes->gastos_entierro());
    }
    
    //F23
    public function conyugue_costes_aceptacion_herencia() {
        return -($this->m_Parent->m_Constantes->costes_aceptacion_herencia());
    }
    
    //F25
    public function conyugue_capitales_ahorro_conyugue() {
        return $this->m_Parent->m_Economicos_Lib->conyugue_capitales_ahorro_fallecimiento();
    }
    
    //F26
    public function conyugue_capitales_ahorro_ambos(){
        return $this->m_Parent->m_Economicos_Lib->ambos_capitales_ahorro_fallecimiento();
    }
    
    //F28
    public function conyugue_saldo_seguro_vida_necesario() {
        return $this->conyugue_cobertura_necesaria()+
               $this->conyugue_deudas_pendientes() + $this->conyugue_gastos_entierro()+
               $this->conyugue_costes_aceptacion_herencia() + $this->conyugue_capitales_ahorro_conyugue()+
               $this->conyugue_capitales_ahorro_ambos();
    }
    
    //F30 
    public function conyugue_seguros_ya_contratados() {
        return $this->m_Parent->m_Economicos_Lib->conyugue_suma_capitales_seguro_vida_fallecimiento();
    }
    
    //F32
    public function conyugue_necesidad_seguro_vida() {
        return -($this->conyugue_saldo_seguro_vida_necesario() + $this->conyugue_seguros_ya_contratados());
    }
    
    //F42
    public function beneficiario_3_importe_cobrar_seguro_vida() {
        return $this->m_Parent->m_SimulacionRow['fallecimiento_beneficiario_3_importe_cobrar'];
    }

    //G42
    public function beneficiario_4_importe_cobrar_seguro_vida() {
        return $this->m_Parent->m_SimulacionRow['fallecimiento_beneficiario_4_importe_cobrar'];
    }
    
    //H42
    public function beneficiario_5_importe_cobrar_seguro_vida() {
        return $this->m_Parent->m_SimulacionRow['fallecimiento_beneficiario_5_importe_cobrar'];
    }


    
}
