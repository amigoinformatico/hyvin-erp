<?php
class Informa_Bases_Pensiones {
    private $m_Parent;
    
    public function __construct($parent) {
        $this->m_Parent = $parent[0];
    }
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }

    
    //H13
    private function pension_minima_jubilacion_con_conyugue_a_cargo() {
        return $this->m_Parent->parametros_model->get_all()->pension_minima_jubilacion_con_conyugue_a_cargo;
    }
    
    //H14
    private function pension_minima_jubilacion_con_conyugue_a_no_cargo() {
        return $this->m_Parent->parametros_model->get_all()->pension_minima_jubilacion_con_conyugue_a_no_cargo;
    }
    
    //H15
    private function pension_minima_jubilacion_sin_conyugue() {
        return $this->m_Parent->parametros_model->get_all()->pension_minima_jubilacion_sin_conyugue;
    }
    
    
    //H18
    private function incapacidad_permanente_absoluta_con_conyugue_a_cargo() {
        return $this->m_Parent->parametros_model->get_all()->incapacidad_permanente_absoluta_con_conyugue_a_cargo;
    }
    
    //H19
    private function incapacidad_permanente_absoluta_con_conyugue_a_no_cargo() {
       return $this->m_Parent->parametros_model->get_all()->incapacidad_permanente_absoluta_con_conyugue_a_no_cargo; 
    }
    
    //H20
    private function incapacidad_permanente_absoluta_sin_conyugue() {
        return $this->m_Parent->parametros_model->get_all()->incapacidad_permanente_absoluta_sin_conyugue;
    }
    
    //H23
    private function incapacidad_permanente_total_con_conyugue_a_cargo() {
        return $this->m_Parent->parametros_model->get_all()->incapacidad_permanente_total_con_conyugue_a_cargo;
    }
    
    //H24
    private function incapacidad_permanente_total_con_conyugue_a_no_cargo() {
        return $this->m_Parent->parametros_model->get_all()->incapacidad_permanente_total_con_conyugue_a_no_cargo;
    }
    
    //H25
    private function incapacidad_permanente_total_sin_conyugue() {
        return $this->m_Parent->parametros_model->get_all()->incapacidad_permanente_total_sin_conyugue;
    }


    
    //H27
    public function viudedad() {
        return $this->m_Parent->parametros_model->get_all()->viudedad;
    }
    
    //H28
    public function orfandad() {
        return $this->m_Parent->parametros_model->get_all()->orfandad;
    }
    
    
    //H31
    public function pension_maxima() {
        return $this->m_Parent->parametros_model->get_all()->pension_maxima;
    }
    
    
    //J7
    private function tiene_conyugue() {
        return $this->m_Parent->m_Titular_Lib->tiene_conyugue();
    }
    
    //J8
    private function titular_regimen_seguridad_social() {
        return $this->m_Parent->m_SimulacionRow['titular_regimen_ss'];
    }
    
    //J13
    public function titular_pension_minima_jubilacion() {
        $tiene_conyugue = $this->tiene_conyugue();
        if($tiene_conyugue == 0) {return $this->pension_minima_jubilacion_sin_conyugue();}
        
        $conyugue_regimen_seguridad_social = $this->conyugue_regimen_seguridad_social();
        if( $conyugue_regimen_seguridad_social == "" ) {
            return $this->pension_minima_jubilacion_con_conyugue_a_cargo();
        }
        else {
            return $this->pension_minima_jubilacion_con_conyugue_a_no_cargo();
        }
    }
    
    //J18 
    public function titular_incapacidad_permanente_absoluta() {
        $tiene_conyugue = $this->tiene_conyugue();
        if($tiene_conyugue==0){return $this->incapacidad_permanente_absoluta_sin_conyugue();}
        
        $conyugue_regimen_seguridad_social = $this->conyugue_regimen_seguridad_social();
        if($conyugue_regimen_seguridad_social==""){
            return $this->incapacidad_permanente_absoluta_con_conyugue_a_cargo();
        }
        else {
            return $this->incapacidad_permanente_absoluta_con_conyugue_a_no_cargo();
        }
    }

    //J23 
    public function titular_incapacidad_permanente_total() {
        $tiene_conyugue = $this->tiene_conyugue();
        if($tiene_conyugue==0){return $this->incapacidad_permanente_total_sin_conyugue();}
        
        $conyugue_regimen_seguridad_social = $this->conyugue_regimen_seguridad_social();
        if($conyugue_regimen_seguridad_social==""){
            return $this->incapacidad_permanente_total_con_conyugue_a_cargo();
        }
        else {
            return $this->incapacidad_permanente_total_con_conyugue_a_no_cargo();
        }
    }

    //K8
    private function conyugue_regimen_seguridad_social() {
        return $this->m_Parent->m_Economicos_Lib->conyugue_regimen_ss();
    }

    //K13
    public function conyugue_pension_minima_jubilacion() {
        $tiene_conyugue = $this->tiene_conyugue();
        if($tiene_conyugue == 0) {return $this->pension_minima_jubilacion_sin_conyugue();}
        
        $titular_regimen_seguridad_social = $this->titular_regimen_seguridad_social();
        if( $titular_regimen_seguridad_social == "") {
            return $this->pension_minima_jubilacion_con_conyugue_a_cargo();
        }
        else {
            return $this->pension_minima_jubilacion_con_conyugue_a_no_cargo();
        }
    }
    
    //K18 
    public function conyugue_incapacidad_permanente_absoluta() {
        $tiene_conyugue = $this->tiene_conyugue();
        if($tiene_conyugue==0){return $this->incapacidad_permanente_absoluta_sin_conyugue();}
        
        $titular_regimen_seguridad_social = $this->titular_regimen_seguridad_social();
        if($titular_regimen_seguridad_social==""){
            return $this->incapacidad_permanente_absoluta_con_conyugue_a_cargo();
        }
        else {
            return $this->incapacidad_permanente_absoluta_con_conyugue_a_no_cargo();
        }
    }

    //K23 
    public function conyugue_incapacidad_permanente_total() {
        $tiene_conyugue = $this->tiene_conyugue();
        if($tiene_conyugue==0){return $this->incapacidad_permanente_total_sin_conyugue();}
        
        $titular_regimen_seguridad_social = $this->titular_regimen_seguridad_social();
        if($titular_regimen_seguridad_social==""){
            return $this->incapacidad_permanente_total_con_conyugue_a_cargo();
        }
        else {
            return $this->incapacidad_permanente_total_con_conyugue_a_no_cargo();
        }
    }
    
   
    // N10
    public function base_maxima_actual() {
        $minimo;
        $maximo;
        $ipc;
        $this->m_Parent->simulacion_model->historico_bases_ipc($this->m_Parent->m_Constantes->anyo_estudio(), $minimo, $maximo, $ipc );
        return $maximo;
    }
    
    // N11
    public function base_minima_actual() {
        $minimo;
        $maximo;
        $ipc;
        $this->m_Parent->simulacion_model->historico_bases_ipc($this->m_Parent->m_Constantes->anyo_estudio(), $minimo, $maximo, $ipc );

        return $minimo;
    }
    
}