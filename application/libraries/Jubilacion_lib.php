<?php
class Jubilacion_Lib {
    private $m_Parent;
    
    public function __construct($parent) {
        $this->m_Parent = $parent[0];
    }
    
    private function log_error($message){
        if(strpos($this->m_Parent->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->m_Parent->config->set_item('winp3_missing_field', $this->m_Parent->config->item('winp3_missing_field').$message." | ");
        }
    }
    
    //D3
    public function titular_fecha_nacimiento() {
        return $this->m_Parent->m_Titular_Lib->titular_fecha_nacimiento();
    }
    
    //D5
    public function titular_fecha_jubilacion() {
        return $this->m_Parent->m_Pensiones->titular_fecha_jubilacion();
    }
    
    //D7
    public function titular_anyos_hasta_jubilacion() {
        $titular_fecha_jubilacion = $this->titular_fecha_jubilacion();
        $hoy = new DateTime();
        return $hoy->diff($titular_fecha_jubilacion)->format("%a")/365.25;

    }
    
    //D9
    private function titular_gastos_mensuales_familiares_hoy() {
        return $this->m_Parent->m_Economicos_Lib->gastos_mensuales_media_familia();
    }
    
    //D11
    private function titular_gastos_reducidos_hoy() {
        $titular_gastos_mensuales_familiares_hoy = $this->titular_gastos_mensuales_familiares_hoy();
        $titular_porcentaje_reduccion_gastos = $this->m_Parent->m_SimulacionRow['jubilacion_titular_porcentaje_reduccion_gastos'];

        return $titular_gastos_mensuales_familiares_hoy-$titular_gastos_mensuales_familiares_hoy*$titular_porcentaje_reduccion_gastos/100;
    }
    
    //D14
    public function titular_gastos_reducidos_actualizado() {
        $titular_gastos_reducidos_hoy = $this->titular_gastos_reducidos_hoy();
        $ipc_estimado = $this->m_Parent->m_Constantes->ipc();
        $titular_anyos_hasta_jubilacion = $this->titular_anyos_hasta_jubilacion();
        
        return $titular_gastos_reducidos_hoy*pow(1+$ipc_estimado/100,$titular_anyos_hasta_jubilacion);
        
    }
    
    //D15
    public function titular_pension_jubilacion_propia_actualizado() {
        return $this->m_Parent->m_Pensiones->titular_jubilacion_pension()*14/12;
    }
    
    //D16
    public function titular_ingresos_conyugue_actualizado() {
        $conyugue_fecha_jubilacion = $this->conyugue_fecha_jubilacion();//F5
        $titular_fecha_jubilacion = $this->titular_fecha_jubilacion();//D5
        $conyugue_total_ingresos_bruto_mensual = $this->m_Parent->m_Economicos_Lib->conyugue_total_ingresos_bruto_mensual();
        $titular_anyos_hasta_jubilacion = $this->titular_anyos_hasta_jubilacion(); //D7
        $conyugue_anyos_hasta_jubilacion = $this->conyugue_anyos_hasta_jubilacion(); //F7
        $conyugue_pension_jubilacion_propia_actualizado = $this->conyugue_pension_jubilacion_propia_actualizado();//F15
        $diff_anyos = $titular_anyos_hasta_jubilacion-$conyugue_anyos_hasta_jubilacion;

        if($this->m_Parent->config->item("winp3_missing_field")!="") {
            $this->log_error("Faltan datos para calcular ingresos cónyuge actualizado");
            return;
        }
        
        if($conyugue_fecha_jubilacion>$titular_fecha_jubilacion) {
            return $conyugue_total_ingresos_bruto_mensual*pow(1+$this->m_Parent->m_Constantes->ipc()/100,$titular_anyos_hasta_jubilacion);
        }
        else {
            return $conyugue_pension_jubilacion_propia_actualizado*pow(1+$this->m_Parent->m_Constantes->ipc()/100,$diff_anyos);
        }
    }
    
    //D18
    public function titular_saldo_diferencial_mensual_cubrir() {
        return $this->titular_ingresos_conyugue_actualizado()+$this->titular_pension_jubilacion_propia_actualizado()-
                $this->titular_gastos_reducidos_actualizado();
    }
    
    //D46
    public function titular_capital_si_aportacion_pensiones() {
        $anyos_hasta_jubilacion = $this->titular_anyos_hasta_jubilacion();
        $aportacion_mensual = $this->m_Parent->m_SimulacionRow['jubilacion_titular_aportacion_pensiones'];
        $rentabilidad_estimada = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        $actual = $this->m_Parent->m_SimulacionRow['jubilacion_titular_traspaso_plan_pensiones'];
        
        $vf = $this->m_Parent->m_Dependencia_Lib->vf($aportacion_mensual, ($rentabilidad_estimada/100)/12, $anyos_hasta_jubilacion*12);
        $capitalizacion= $this->capitalizacion_valor_compuesto($actual, ($rentabilidad_estimada/100)/12, $anyos_hasta_jubilacion*12);
        return $vf + $capitalizacion;
    }

    private function capitalizacion_valor_compuesto( $actual, $rentabilidad, $meses ) {
        if( $actual =="" || $rentabilidad=="" || $meses==""){
            return 0;
        }
        return $actual*pow((1+$rentabilidad), $meses);
    }
    
    //D49
    public function titular_si_capital_aportacion_pensiones() {
        $meses = $this->titular_anyos_hasta_jubilacion()*12;
        $capital = $this->m_Parent->m_SimulacionRow['jubilacion_titular_capital_pensiones'];
        $rentabilidad = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        $actual = $this->m_Parent->m_SimulacionRow['jubilacion_titular_traspaso_plan_pensiones'];
        $capitalizacion= $this->capitalizacion_valor_compuesto($actual, ($rentabilidad/100)/12, $meses);
        $pago_valor= $this->pago_valor($capital, ($rentabilidad/100)/12, $meses, $capitalizacion);
        return $pago_valor;
    }
    
    private function pago_valor($capital, $rentabilidad, $meses, $capitalizacion){
        return ($capital-$capitalizacion)*($rentabilidad/(pow(1+$rentabilidad, $meses)-1));
    }
    
    //D60
    public function titular_importe_reducir_base_liquidadora() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_titular_aportacion_pensiones']*12;
    }
    
    //D61
    public function titular_tipo_marginal_hoy() {
        return $this->m_Parent->m_Fiscalidad->calculo_generales_hoy_titular_tipo_marginal_total();
    }

    //D62 
    public function titular_beneficio_fiscal_anual() {
        return $this->titular_importe_reducir_base_liquidadora()*$this->titular_tipo_marginal_hoy()/100;
    }
    
    //D63
    public function titular_beneficio_fiscal_acumulado() {
        return $this->titular_beneficio_fiscal_anual()*$this->titular_anyos_hasta_jubilacion();
    }

    //D68
    public function titular_aportaciones() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_titular_aportacion_pensiones']*12*$this->titular_anyos_hasta_jubilacion();
    }

    //D70
    public function titular_imputable_antes_2006() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_titular_anterior_2006']*pow(1+($this->m_Parent->m_Constantes->rentabilidad_estimada()/100), $this->titular_anyos_hasta_jubilacion());
    }
    
    //D89
    public function titular_base_ss() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_bl_solo_ss();
    }
    
    //D90
    public function titular_tipo_marginal_ss() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_bl_arriba_tipo_solo_ss() + $this->m_Parent->m_Fiscalidad->titular_jubilacion_bl_abajo_tipo_solo_ss();
    }
    
    //D91
    public function titular_cuota_estatal_ss() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_arriba_total_cuota_solo_ss();
    }
    
    //D92
    public function titular_cuota_autonomica_ss() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_abajo_total_cuota_solo_ss();
    }
    
    //D93
    public function titular_total_cuota_ss() {
        return $this->titular_cuota_estatal_ss()+$this->titular_cuota_autonomica_ss();
    }
    
    //D103
    public function titular_capital_final_pias() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_titular_capital_pias'];
    }
    
    //D106
    public function titular_porcentaje_exencion() {
        return $this->m_Parent->m_Fiscalidad->titular_pias_porcentaje_exencion();
    }
    
    //D108
    public function titular_pias_renta_anual_obtenida() {
        return $this->titular_capital_final_pias()/25;
    }
    
    //D109
    public function titular_pias_renta_anual_exenta() {
        return $this->m_Parent->m_Fiscalidad->titular_pias_renta_exenta();
    }
    
    //D110
    public function titular_pias_renta_anual_tributable() {
        return $this->m_Parent->m_Fiscalidad->titular_pias_renta_tributable();
    }
    
    //D111
    public function titular_pias_total_cuota_tributaria_anual() {
        return $this->m_Parent->m_Fiscalidad->titular_pias_total_cuota();
    }
    
    //D112
    public function titular_pias_renta_anual_neta() {
        return $this->titular_pias_renta_anual_obtenida()-$this->titular_pias_total_cuota_tributaria_anual();
    }
    
    //D114
    public function titular_pias_renta_mensual_obtenida() {
        return $this->titular_pias_renta_anual_obtenida()/12;
    }
    
    //D115
    public function titular_pias_total_cuota_tributaria_mensual() {
        return $this->titular_pias_total_cuota_tributaria_anual()/12;
    }
    
    //D116
    public function titular_pias_renta_mensual_neta() {
        return $this->titular_pias_renta_anual_neta()/12;
    }
    
    //D118
    public function titular_pias_coste_fiscal_operacion() {
        return $this->titular_pias_total_cuota_tributaria_anual()*25;
    }
    
    //D123
    public function titular_capital_final_sv() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_titular_capital_seguro_vida'];
    }
    
    //D124
    public function titular_aportaciones_sv() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_titular_aportacion_seguro_vida']*$this->titular_anyos_hasta_jubilacion()*12;
    }
    
    //D125
    public function titular_rendimiento_obtenido_sv() {
       return $this->titular_capital_final_sv()-$this->titular_aportaciones_sv();
    }
    
    //D127
    public function titular_total_cuota_operacion() {
        return $this->m_Parent->m_Fiscalidad->titular_sv_total_cuota();
    }
    
    //E21
    public function anyos_jubilacion_entre_conyugues() {
        return abs($this->titular_anyos_hasta_jubilacion() - $this->conyugue_anyos_hasta_jubilacion());
    }
    
    //E22
    public function diferencial_generado_periodo_1() {
        return max($this->titular_saldo_diferencial_mensual_cubrir(), $this->conyugue_saldo_diferencial_mensual_cubrir())*12*$this->anyos_jubilacion_entre_conyugues();
    }
    
    //E23
    public function resto_periodo_cobertura() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_titular_anyos_cobertura']-$this->anyos_jubilacion_entre_conyugues();
    }
    
    //E24
    public function diferencial_generado_periodo_2() {
        return min($this->titular_saldo_diferencial_mensual_cubrir(), $this->conyugue_saldo_diferencial_mensual_cubrir())*12*$this->resto_periodo_cobertura();    
    }
    
    //E26
    public function diferencial_generado_periodo_1_mas_2() {
        $diferencial_generado_periodo_2 = $this->diferencial_generado_periodo_2();
        $diferencial_generado_periodo_1 = $this->diferencial_generado_periodo_1();
        
        return $diferencial_generado_periodo_1+$diferencial_generado_periodo_2;
    }
    
    //E28
    public function diferencial_cubrir() {
        return $this->diferencial_generado_periodo_1_mas_2()*$this->m_Parent->m_SimulacionRow['jubilacion_porcentaje_cobertura']/100;
    }
    
    
    //E32
    public function capital_necesario_jubilacion() {
        $deudas = $this->m_Parent->m_SimulacionRow['jubilacion_deudas_previstas_jubilacion'];
        if($deudas==""){
            $this->log_error("Faltan Deudas Previstas Jubilación");
            return;
        }
        $diferencial = $this->diferencial_cubrir();
        
        return $deudas+$diferencial;        
    }
    
    //E34
    public function descobertura(){
        $ahorro = $this->m_Parent->m_Economicos_Lib->suma_capitales_disponibles_ahorro_jubilacion_total();
        $capital = $this->capital_necesario_jubilacion();
        if($this->m_Parent->config->item("winp3_missing_field")!="") {return;}
        return $ahorro + $capital;
    }

    //E46
    public function titular_capital_si_aportacion_pias() {
        $anyos_hasta_jubilacion = $this->titular_anyos_hasta_jubilacion();
        $aportacion_mensual = $this->m_Parent->m_SimulacionRow['jubilacion_titular_aportacion_pias'];
        $rentabilidad_estimada = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        return $this->m_Parent->m_Dependencia_Lib->vf($aportacion_mensual, ($rentabilidad_estimada/100)/12, $anyos_hasta_jubilacion*12);
    }
    
    //E49
    public function titular_si_capital_aportacion_pias() {

        $meses = $this->titular_anyos_hasta_jubilacion()*12;
        $capital = $this->m_Parent->m_SimulacionRow['jubilacion_titular_capital_pias'];
        $rentabilidad = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        $pago_valor = $this->m_Parent->m_Dependencia_Lib->pago($capital, ($rentabilidad/100)/12, $meses);
        return $pago_valor;
    }
    
    //E79
    public function titular_prestaciones_anteriores_2006() {
        return min($this->m_Parent->m_SimulacionRow['jubilacion_titular_reparto_cobro_capital'], $this->titular_imputable_antes_2006());
    }
    
    //E80
    public function titular_resto_percibir() {
        return max($this->m_Parent->m_SimulacionRow['jubilacion_titular_reparto_cobro_capital']-$this->titular_imputable_antes_2006(), 0);
    }
    
    //E81
    public function titular_incremento_bl() {
        return $this->titular_prestaciones_anteriores_2006()*0.6+$this->titular_resto_percibir();
    }
    
    //E89
    public function titular_base_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_bl_ss_capital();
    }

    //E90
    public function titular_tipo_marginal_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_bl_arriba_tipo_ss_capital() + $this->m_Parent->m_Fiscalidad->titular_jubilacion_bl_ss_capital_abajo_tipo();
    }
    
    //E91
    public function titular_cuota_estatal_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_arriba_total_cuota_ss_capital();
    }

    //E92
    public function titular_cuota_autonomica_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_abajo_total_cuota_ss_capital();
    }

    //E93
    public function titular_total_cuota_ss_capital() {
        return $this->titular_cuota_estatal_ss_capital()+$this->titular_cuota_autonomica_ss_capital();
    }

    //E95
    public function titular_coste_fiscal_capital() {
        return $this->titular_total_cuota_ss_capital()-$this->titular_total_cuota_ss();
    }
    
    //E98
    public function titular_coste_fiscal_total() {
        return $this->titular_coste_fiscal_capital()+$this->titular_coste_fiscal_x();
    }
    
    //F3
    public function conyugue_fecha_nacimiento() {
        return $this->m_Parent->m_Titular_Lib->conyugue_fecha_nacimiento();
    }

     //F5
    public function conyugue_fecha_jubilacion() {
        return $this->m_Parent->m_Pensiones->conyugue_fecha_jubilacion();
    }
    
    //F7
    public function conyugue_anyos_hasta_jubilacion() {
        $conyugue_fecha_jubilacion = $this->conyugue_fecha_jubilacion();
        $hoy = new DateTime();
        return $hoy->diff($conyugue_fecha_jubilacion)->format("%a")/365.25;

    }
    
    //F9
    private function conyugue_gastos_mensuales_familiares_hoy() {
        return $this->m_Parent->m_Economicos_Lib->gastos_mensuales_media_familia();
    }
    
    //F11
    private function conyugue_gastos_reducidos_hoy() {
        $conyugue_gastos_mensuales_familiares_hoy = $this->conyugue_gastos_mensuales_familiares_hoy();
        $conyugue_porcentaje_reduccion_gastos = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_porcentaje_reduccion_gastos'];

        return $conyugue_gastos_mensuales_familiares_hoy-$conyugue_gastos_mensuales_familiares_hoy*$conyugue_porcentaje_reduccion_gastos/100;
    }
  
    //F14
    public function conyugue_gastos_reducidos_actualizado() {
        $conyugue_gastos_reducidos_hoy = $this->conyugue_gastos_reducidos_hoy();
        $ipc_estimado = $this->m_Parent->m_Constantes->ipc();
        $conyugue_anyos_hasta_jubilacion = $this->conyugue_anyos_hasta_jubilacion();
        
        return $conyugue_gastos_reducidos_hoy*pow(1+$ipc_estimado/100,$conyugue_anyos_hasta_jubilacion);
        
    }
    
    //F15
    public function conyugue_pension_jubilacion_propia_actualizado() {
        return $this->m_Parent->m_Pensiones->conyugue_jubilacion_pension()*14/12;
    }

    //F16
    public function conyugue_ingresos_conyugue_actualizado() {
        $conyugue_fecha_jubilacion = $this->conyugue_fecha_jubilacion();//F5
        $titular_fecha_jubilacion = $this->titular_fecha_jubilacion();//D5
        $titular_total_ingresos_bruto_mensual = $this->m_Parent->m_Economicos_Lib->titular_total_ingresos_bruto_mensual();//C59
        $titular_anyos_hasta_jubilacion = $this->titular_anyos_hasta_jubilacion(); //D7
        $conyugue_anyos_hasta_jubilacion = $this->conyugue_anyos_hasta_jubilacion(); //F7
        $titular_pension_jubilacion_propia_actualizado = $this->titular_pension_jubilacion_propia_actualizado();//D15
        $diff_anyos = $conyugue_anyos_hasta_jubilacion-$titular_anyos_hasta_jubilacion;

        if($this->m_Parent->config->item("winp3_missing_field")!="") {
            $this->log_error("Faltan datos para calcular ingresos cónyuge actualizado");
            return;
        }
        
        if($titular_fecha_jubilacion>$conyugue_fecha_jubilacion) {
            return $titular_total_ingresos_bruto_mensual*pow(1+$this->m_Parent->m_Constantes->ipc()/100,$conyugue_anyos_hasta_jubilacion);
        }
        else {
            return $titular_pension_jubilacion_propia_actualizado*pow(1+$this->m_Parent->m_Constantes->ipc()/100,$diff_anyos);
        }
    }
    
    //F18
    public function conyugue_saldo_diferencial_mensual_cubrir() {
        return $this->conyugue_ingresos_conyugue_actualizado()+$this->conyugue_pension_jubilacion_propia_actualizado()-
                $this->conyugue_gastos_reducidos_actualizado();
    }

    //F46
    public function titular_capital_si_aportacion_seguro_vida() {
        $anyos_hasta_jubilacion = $this->titular_anyos_hasta_jubilacion();
        $aportacion_mensual = $this->m_Parent->m_SimulacionRow['jubilacion_titular_aportacion_seguro_vida'];
        $rentabilidad_estimada = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        return $this->m_Parent->m_Dependencia_Lib->vf($aportacion_mensual, ($rentabilidad_estimada/100)/12, $anyos_hasta_jubilacion*12);
    }

    
    //F77
    public function titular_reparto_cobro_capital_renta() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_titular_capital_pensiones']-$this->m_Parent->m_SimulacionRow['jubilacion_titular_reparto_cobro_capital'];
    }
    
    //F84
    public function titular_renta_anual_obtenida() {
        if( $this->m_Parent->m_SimulacionRow['jubilacion_titular_anyos_renta']==0){
            $this->log_error("Faltan años renta del titular");//TODO Si Ok quitar todo
            return;
        }
        
        return $this->titular_reparto_cobro_capital_renta()/$this->m_Parent->m_SimulacionRow['jubilacion_titular_anyos_renta'];
    }
    
    //F49
    public function titular_si_capital_aportacion_seguro_vida() {
        $meses = $this->titular_anyos_hasta_jubilacion()*12;
        $capital = $this->m_Parent->m_SimulacionRow['jubilacion_titular_capital_seguro_vida'];
        $rentabilidad = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        $pago_valor = $this->m_Parent->m_Dependencia_Lib->pago($capital, ($rentabilidad/100)/12, $meses);
        return $pago_valor;
    }

    //F85
    public function titular_renta_mensual_obtenida() {
        return $this->titular_renta_anual_obtenida()/12;
    }

    //F89
    public function titular_base_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_bl_ss_renta();
    }

    //F90
    public function titular_tipo_marginal_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_bl_ss_renta_arriba_tipo() + $this->m_Parent->m_Fiscalidad->titular_jubilacion_bl_ss_renta_abajo_tipo();
    }

    //F91
    public function titular_cuota_estatal_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_arriba_total_cuota_ss_renta();
    }
    
    //F92
    public function titular_cuota_autonomica_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->titular_jubilacion_abajo_total_cuota_ss_renta();
    }

    //F93
    public function titular_total_cuota_ss_renta() {
        return $this->titular_cuota_estatal_ss_renta()+$this->titular_cuota_autonomica_ss_renta();
    }
    
    //F96
    public function titular_coste_fiscal_renta_1() {
        return $this->titular_total_cuota_ss_renta()-$this->titular_total_cuota_ss();
    }
    
    //F97
    public function titular_coste_fiscal_x() {
        return $this->titular_coste_fiscal_renta_1()*$this->m_Parent->m_SimulacionRow['jubilacion_titular_anyos_renta'];
    }

    
    //F103
    public function conyugue_capital_final_pias() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_capital_pias'];
    }
    
    //F106
    public function conyugue_porcentaje_exencion() {
        return $this->m_Parent->m_Fiscalidad->conyugue_pias_porcentaje_exencion();
    }
    
    //F108
    public function conyugue_pias_renta_anual_obtenida() {
        return $this->conyugue_capital_final_pias()/25;
    }
    
    //F109
    public function conyugue_pias_renta_anual_exenta() {
        return $this->m_Parent->m_Fiscalidad->conyugue_pias_renta_exenta();
    }
    
    //F110
    public function conyugue_pias_renta_anual_tributable() {
        return $this->m_Parent->m_Fiscalidad->conyugue_pias_renta_tributable();
    }
    
    //F111
    public function conyugue_pias_total_cuota_tributaria_anual() {
        return $this->m_Parent->m_Fiscalidad->conyugue_pias_total_cuota();
    }
    
    //F112
    public function conyugue_pias_renta_anual_neta() {
        return $this->conyugue_pias_renta_anual_obtenida()-$this->conyugue_pias_total_cuota_tributaria_anual();
    }
    
    //F114
    public function conyugue_pias_renta_mensual_obtenida() {
        return $this->conyugue_pias_renta_anual_obtenida()/12;
    }
    
    //F115
    public function conyugue_pias_total_cuota_tributaria_mensual() {
        return $this->conyugue_pias_total_cuota_tributaria_anual()/12;
    }
    
    //F116
    public function conyugue_pias_renta_mensual_neta() {
        return $this->conyugue_pias_renta_anual_neta()/12;
    }
    
    //F118
    public function conyugue_pias_coste_fiscal_operacion() {
        return $this->conyugue_pias_total_cuota_tributaria_anual()*25;
    }

    //F123
    public function conyugue_capital_final_sv() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_capital_seguro_vida'];
    }
    
    //F124
    public function conyugue_aportaciones_sv() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_aportacion_seguro_vida']*$this->conyugue_anyos_hasta_jubilacion()*12;
    }
    
    //F125
    public function conyugue_rendimiento_obtenido_sv() {
       return $this->conyugue_capital_final_sv()-$this->conyugue_aportaciones_sv();
    }
    
    //F127
    public function conyugue_total_cuota_operacion() {
        return $this->m_Parent->m_Fiscalidad->conyugue_sv_total_cuota();
    }

    //G54
    public function suma_aportaciones_familiares() {
        $sol1 = $this->m_Parent->m_SimulacionRow['jubilacion_titular_aportacion_pensiones'];
        $sol2 = $this->m_Parent->m_SimulacionRow['jubilacion_titular_aportacion_pias'];
        $sol3 = $this->m_Parent->m_SimulacionRow['jubilacion_titular_aportacion_seguro_vida'];
        $sol4 = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_aportacion_pensiones'];
        $sol5 = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_aportacion_pias'];
        $sol6 = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_aportacion_seguro_vida'];
        
        return $sol1+$sol2+$sol3+$sol4+$sol5+$sol6;
    }
    
    //G55
    public function suma_capitales_obtenidos() {
        $sol1 = $this->m_Parent->m_SimulacionRow['jubilacion_titular_capital_pensiones'];
        $sol2 = $this->m_Parent->m_SimulacionRow['jubilacion_titular_capital_pias'];
        $sol3 = $this->m_Parent->m_SimulacionRow['jubilacion_titular_capital_seguro_vida'];
        $sol4 = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_capital_pensiones'];
        $sol5 = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_capital_pias'];
        $sol6 = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_capital_seguro_vida'];
        
        return $sol1+$sol2+$sol3+$sol4+$sol5+$sol6;
    }
    
    //H46
    public function conyugue_capital_si_aportacion_pensiones() {
        $anyos_hasta_jubilacion = $this->conyugue_anyos_hasta_jubilacion();
        $aportacion_mensual = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_aportacion_pensiones'];
        $rentabilidad_estimada = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        $actual = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_traspaso_plan_pensiones'];
        $vf = $this->m_Parent->m_Dependencia_Lib->vf($aportacion_mensual, ($rentabilidad_estimada/100)/12, $anyos_hasta_jubilacion*12);
        $capitalizacion= $this->capitalizacion_valor_compuesto($actual, ($rentabilidad_estimada/100)/12, $anyos_hasta_jubilacion*12);
        return $vf + $capitalizacion;
    }

    //H49 
    public function conyugue_si_capital_aportacion_pensiones() {
        $meses = $this->conyugue_anyos_hasta_jubilacion()*12;
        $capital = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_capital_pensiones'];
        $rentabilidad = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        $actual = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_traspaso_plan_pensiones'];
        $capitalizacion= $this->capitalizacion_valor_compuesto($actual, ($rentabilidad/100)/12, $meses);
        $pago_valor= $this->pago_valor($capital, ($rentabilidad/100)/12, $meses, $capitalizacion);
        return $pago_valor;
    }

    //H60
    public function conyugue_importe_reducir_base_liquidadora() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_aportacion_pensiones']*12;
    }

    //H61
    public function conyugue_tipo_marginal_hoy() {
        return $this->m_Parent->m_Fiscalidad->calculo_generales_hoy_conyugue_tipo_marginal_total();
    }
    
    //H62 
    public function conyugue_beneficio_fiscal_anual() {
        return $this->conyugue_importe_reducir_base_liquidadora()*$this->conyugue_tipo_marginal_hoy()/100;
    }

    //H63
    public function conyugue_beneficio_fiscal_acumulado() {
        return $this->conyugue_beneficio_fiscal_anual()*$this->conyugue_anyos_hasta_jubilacion();
    }

    //H68
    public function conyugue_aportaciones() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_aportacion_pensiones']*12*$this->conyugue_anyos_hasta_jubilacion();
    }

    //H70
    public function conyugue_imputable_antes_2006() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_anterior_2006']*pow(1+($this->m_Parent->m_Constantes->rentabilidad_estimada()/100), $this->conyugue_anyos_hasta_jubilacion());
    }

    //H89
    public function conyugue_base_ss() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_bl_solo_ss();
    }
    
    //H90
    public function conyugue_tipo_marginal_ss() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_bl_arriba_tipo_solo_ss() + $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_bl_abajo_tipo_solo_ss();
    }
    
    //H91
    public function conyugue_cuota_estatal_ss() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_arriba_total_cuota_solo_ss();
    }
    
    //H92
    public function conyugue_cuota_autonomica_ss() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_abajo_total_cuota_solo_ss();
    }
    
    //H93
    public function conyugue_total_cuota_ss() {
        return $this->conyugue_cuota_estatal_ss()+$this->conyugue_cuota_autonomica_ss();
    }


    //I46
    public function conyugue_capital_si_aportacion_pias() {
        $anyos_hasta_jubilacion = $this->conyugue_anyos_hasta_jubilacion();
        $aportacion_mensual = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_aportacion_pias'];
        $rentabilidad_estimada = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        return $this->m_Parent->m_Dependencia_Lib->vf($aportacion_mensual, ($rentabilidad_estimada/100)/12, $anyos_hasta_jubilacion*12);
    }

    //I49
    public function conyugue_si_capital_aportacion_pias() {
        $meses = $this->conyugue_anyos_hasta_jubilacion()*12;
        $capital = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_capital_pias'];
        $rentabilidad = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        $pago_valor = $this->m_Parent->m_Dependencia_Lib->pago($capital, ($rentabilidad/100)/12, $meses);
        return $pago_valor;
    }
  
    
    //I79
    public function conyugue_prestaciones_anteriores_2006() {
        return min($this->m_Parent->m_SimulacionRow['jubilacion_conyugue_reparto_cobro_capital'], $this->conyugue_imputable_antes_2006());
    }

    //I80
    public function conyugue_resto_percibir() {
        return max($this->m_Parent->m_SimulacionRow['jubilacion_conyugue_reparto_cobro_capital']-$this->conyugue_imputable_antes_2006(), 0);
    }

    //I81
    public function conyugue_incremento_bl() {
        return $this->conyugue_prestaciones_anteriores_2006()*0.6+$this->conyugue_resto_percibir();
    }

    //I89
    public function conyugue_base_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_bl_ss_capital();
    }

    //I90
    public function conyugue_tipo_marginal_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_bl_arriba_tipo_ss_capital() + $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_bl_ss_capital_abajo_tipo();
    }
    
    //I91
    public function conyugue_cuota_estatal_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_arriba_total_cuota_ss_capital();
    }

    //I92
    public function conyugue_cuota_autonomica_ss_capital() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_abajo_total_cuota_ss_capital();
    }

    //I93
    public function conyugue_total_cuota_ss_capital() {
        return $this->conyugue_cuota_estatal_ss_capital()+$this->conyugue_cuota_autonomica_ss_capital();
    }

    //I95
    public function conyugue_coste_fiscal_capital() {
        return $this->conyugue_total_cuota_ss_capital()-$this->conyugue_total_cuota_ss();
    }

    //I98
    public function conyugue_coste_fiscal_total() {
        return $this->conyugue_coste_fiscal_capital()+$this->conyugue_coste_fiscal_x();
    }
    
    //J46
    public function conyugue_capital_si_aportacion_seguro_vida() {
        $anyos_hasta_jubilacion = $this->conyugue_anyos_hasta_jubilacion();
        $aportacion_mensual = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_aportacion_seguro_vida'];
        $rentabilidad_estimada = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        return $this->m_Parent->m_Dependencia_Lib->vf($aportacion_mensual, ($rentabilidad_estimada/100)/12, $anyos_hasta_jubilacion*12);
    }

    //J49
    public function conyugue_si_capital_aportacion_seguro_vida() {
        $meses = $this->conyugue_anyos_hasta_jubilacion()*12;
        $capital = $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_capital_seguro_vida'];
        $rentabilidad = $this->m_Parent->m_Constantes->rentabilidad_estimada();
        $pago_valor = $this->m_Parent->m_Dependencia_Lib->pago($capital, ($rentabilidad/100)/12, $meses);
        return $pago_valor;
    }

    //J77
    public function conyugue_reparto_cobro_capital_renta() {
        return $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_capital_pensiones']-$this->m_Parent->m_SimulacionRow['jubilacion_conyugue_reparto_cobro_capital'];
    }
    
    //J84
    public function conyugue_renta_anual_obtenida() {
        if( $this->m_Parent->m_SimulacionRow['jubilacion_conyugue_anyos_renta']==0){
            $this->log_error("Faltan años renta del cónyuge");
            return;
        }
        
        return $this->conyugue_reparto_cobro_capital_renta()/$this->m_Parent->m_SimulacionRow['jubilacion_conyugue_anyos_renta'];
    }

    //J85
    public function conyugue_renta_mensual_obtenida() {
        return $this->conyugue_renta_anual_obtenida()/12;
    }

    //J89
    public function conyugue_base_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_bl_ss_renta();
    }

    //J90
    public function conyugue_tipo_marginal_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_bl_ss_renta_arriba_tipo() + $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_bl_ss_renta_abajo_tipo();
    }

    //J91
    public function conyugue_cuota_estatal_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_arriba_total_cuota_ss_renta();
    }
    
    //J92
    public function conyugue_cuota_autonomica_ss_renta() {
        return $this->m_Parent->m_Fiscalidad->conyugue_jubilacion_abajo_total_cuota_ss_renta();
    }

    //J93
    public function conyugue_total_cuota_ss_renta() {
        return $this->conyugue_cuota_estatal_ss_renta()+$this->conyugue_cuota_autonomica_ss_renta();
    }

    //J96
    public function conyugue_coste_fiscal_renta_1() {
        return $this->conyugue_total_cuota_ss_renta()-$this->conyugue_total_cuota_ss();
    }

    //J97
    public function conyugue_coste_fiscal_x() {
        return $this->conyugue_coste_fiscal_renta_1()*$this->m_Parent->m_SimulacionRow['jubilacion_conyugue_anyos_renta'];
    }


}