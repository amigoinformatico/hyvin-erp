<?php
use Illuminate\Database\Eloquent\Model as Eloquent;


class Links extends Eloquent
{

    protected $table = 'links';
    protected $fillable =['name'];

    protected $primaryKey = 'id';

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }

}