<?php
use Illuminate\Database\Eloquent\Model as Eloquent;


class Consulta extends Eloquent
{

    protected $table = 'consultas';
    protected $fillable =['name','file_name'];
    public $timestamps = false;

    protected $primaryKey = 'id';

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }

}