<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model{

	public function __construct(){
		parent::__construct();		
	}

	/* ***************** INDICE DE MÉTODOS *****************
	1. Login user.
		1.1. Obtener usuario.
		1.2. Clientes por administrador.
		1.3. Simulaciones de cliente.
	******************************************************** */

	//1. Login user:
	public function loginUser($user, $pwd){
		$sql = "SELECT * FROM users 
			WHERE (email = '$usuario' AND password = '$contrasena') 
			OR (username = '$usuario' AND password = '$contrasena')
			AND estado = 1
			AND administrador = 1
			LIMIT 1";
		$q = $this->db->query($sql);
	
		if($q->num_rows() > 0){
			$q = $q->row();
			$this->session->set_userdata('user_id',$q->id);
			$this->session->set_userdata('nombre',$q->nombre);
			$this->session->set_userdata('apellidos',$q->apellidos);			
			$this->session->set_userdata('correo',$q->email);
			$this->session->set_userdata('username',$q->username);

			return $q;
		}else{
			$this->session->set_flashdata('usuario_incorrecto','<div class="alert alert-danger text-center">Los datos introducidos son incorrectos</div>');
		}
	}

	//1.1. Obtener usuario:
	public function getUser($id){
		$sql = "SELECT * FROM simulaciones WHERE id = $id";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//1.2. Clientes por administrador:
	public function getClientesAdministrador($id){
		$sql = "SELECT * FROM users WHERE admin_id = $id";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//1.3. Simulaciones de cliente:
	public function getSimulacionesCliente($id){
		$sql = "SELECT * FROM simulaciones WHERE user = $id";
		$q = $this->db->query($sql);
		return $q->result();
	}


	public function getUsers(){
		$sql = "SELECT * FROM simulaciones";
		$q = $this->db->query($sql);
		return $q->result();
	}
}

