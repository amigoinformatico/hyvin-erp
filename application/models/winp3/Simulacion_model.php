<?php
class Simulacion_model extends CI_Model {

    private $m_Calculo_Jubilacion_Tabla_A;
    private $m_Calculo_Jubilacion_Tabla_A_Len;
    private $m_Calculo_Jubilacion_Tabla_A_Keys;
    private $m_Calculo_Jubilacion_Tabla_B;
    private $m_Calculo_Jubilacion_Tabla_B_Len;
    private $m_Calculo_Jubilacion_Tabla_B_Keys;
    private $m_Calculo_Jubilacion_Tabla_C;
    private $m_Calculo_Jubilacion_Tabla_C_Len;
    private $m_Calculo_Jubilacion_Tabla_C_Keys;
    private $m_Historico_Bases_IPC;
    private $m_Historico_Bases_IPC_Len;
    private $m_Historico_Bases_IPC_Keys;
    private $m_Bases_Sumas;
    private $m_Bases_Sumas_Keys;

    
    function __construct() {
        parent::__construct();
        date_default_timezone_set('Europe/Madrid');
    }
    
    private function log_error($message){
        if(strpos($this->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field').$message." | ");
        }
    }

    private function nueva_simulacion(){
        $this->nueva_simulacion_usuario($this->session->userdata('logged_user')['id']);
    }
    
    public function nueva_simulacion_usuario($user_id)
    {
        $sql="DELETE FROM simulaciones WHERE user=".$user_id;
        $this->db->query($sql);
        $sql="INSERT INTO simulaciones (user) VALUES(".$user_id.")";
        $this->db->query($sql);

    }
    
    function exporta_simulacion(){
        $fields = $this->db->field_data('simulaciones');

        $this->db->select('*');
        $this->db->from('simulaciones');
        $this->db->where('user', $this->session->userdata('logged_user')['id']);
        $query = $this->db->get();
        $row = $query->result_array()[0];

        $len = count($row);
        $keys = array_keys($row);
        $values=[];
        $index = 0;
        for($i = 0; $i < $len; $i++ ){
            if($i>0){
                $values[$index]   = $fields[$i]->name;
                $values[$index+1] = $fields[$i]->type;
                $values[$index+2] = $row[$keys[$i]];
                $index+=3;
            }
        }

        return $values;
    }
    
    function importa_simulacion($file_name) {
        $sql ="DELETE FROM simulaciones WHERE user=".$this->session->userdata('logged_user')['id'];
        $this->db->query($sql);
        
        $handle = fopen($file_name, "r");
        if ($handle) {
            $sql = "INSERT INTO simulaciones VALUES (".$this->session->userdata('logged_user')['id'];

            $i = 2;
            while (($field_name = fgets($handle)) !== false) {
                $field_name = trim($field_name);
                
                if( ($field_type = fgets($handle)) === false ) {
                    return false;
                }
                $field_type = trim($field_type);
                
                if( ($field_value = fgets($handle)) === false ) {
                    return false;
                }
                $field_value = trim($field_value);
                                
                $sql.=", ";
                
                if($field_type == "varchar" || $field_type == "date") {
                    if(strlen($field_value)==0){
                        $sql .= 'null';
                    }
                    else {
                        $sql .=("'".$field_value."'");
                    }
                }
                else if( $field_type == "int" || $field_type == "decimal") {
                    if(strlen($field_value)==0){
                        $sql .= 'null';
                    }
                    else {
                        $sql.= $field_value;
                    }
                }
                else {
                    return false;
                }
                
                $i++;
            }
            $sql.=")";

            if(!$this->db->query($sql)){
                fclose($handle);
                return false;
            }

            fclose($handle);
        } else {
            return false;
        } 
        return true;
    }
    
    function save($data) {
        if(!$this->session->userdata('logged_user')){
            $this->config->set_item("winp3_missing_field", "No se ha podido guardar. Caducó la sesión");
            return false;
        }
        
        $session_data = $this->session->userdata('logged_user');

        $this->db->where('user', $session_data['id'] );
        return $this->db->update('simulaciones', $data );
    }
   
    function get_all() {
        if(!$this->session->userdata('logged_user')){
            $this->config->set_item("winp3_missing_field", "No se han podido leer los datos. Caducó la sesión. Vuelva a identificarse");
            return false;
        }

        $session_data = $this->session->userdata('logged_user');
        $this->db->select('*');
        $this->db->from('simulaciones');
        $this->db->where('user', $session_data['id']);
        
        $query = $this->db->get();
        $row = $query->row();

        $data['titular_nombre'] = $row->titular_nombre;
        $data['titular_sexo'] = $row->titular_sexo;
        $data['titular_fecha_nacimiento'] = date('d-m-Y', strtotime($row->titular_fecha_nacimiento));
        $data['tiene_conyugue'] = $row->tiene_conyugue;
        $data['numero_hijos'] = $row->numero_hijos;
        $data['comunidad_residencia'] = $row->comunidad_residencia;
        $data['conyugue_nombre'] = $row->conyugue_nombre;
        $data['conyugue_sexo'] = $row->conyugue_sexo;
        $data['conyugue_fecha_nacimiento'] = date('d-m-Y', strtotime($row->conyugue_fecha_nacimiento));
        $data['titular_regimen_ss'] = $row->titular_regimen_ss;
        $data['conyugue_regimen_ss'] = $row->conyugue_regimen_ss;
        $data['titular_autonomos_ingreso_bruto_mensual'] = $row->titular_autonomos_ingreso_bruto_mensual;
        $data['conyugue_autonomos_ingreso_bruto_mensual'] = $row->conyugue_autonomos_ingreso_bruto_mensual;
        $data['titular_autonomos_cuota_mensual'] = $row->titular_autonomos_cuota_mensual;
        $data['conyugue_autonomos_cuota_mensual'] = $row->conyugue_autonomos_cuota_mensual;
        $data['it_titular_porcentaje_cobertura'] = $row->it_titular_porcentaje_cobertura;
        $data['it_conyugue_porcentaje_cobertura'] = $row->it_conyugue_porcentaje_cobertura;
        $data['titular_capital_propuesto_dia_baja'] = $row->titular_capital_propuesto_dia_baja;
        $data['conyugue_capital_propuesto_dia_baja'] = $row->conyugue_capital_propuesto_dia_baja;
        $data['titular_franquicia_dias'] = $row->titular_franquicia_dias;
        $data['conyugue_franquicia_dias'] = $row->conyugue_franquicia_dias;
        $data['titular_duracion_maxima_dias'] = $row->titular_duracion_maxima_dias;
        $data['conyugue_duracion_maxima_dias'] = $row->conyugue_duracion_maxima_dias;
        $data['titular_prima_anual'] = $row->titular_prima_anual;
        $data['conyugue_prima_anual'] = $row->conyugue_prima_anual;
        $data['titular_autonomos_retencion_irpf'] = $row->titular_autonomos_retencion_irpf;
        $data['conyugue_autonomos_retencion_irpf'] = $row->conyugue_autonomos_retencion_irpf;
        $data['titular_cotizado_ss_rg_anyos'] = $row->titular_cotizado_ss_rg_anyos;
        $data['titular_cotizado_ss_rg_meses'] = $row->titular_cotizado_ss_rg_meses;
        $data['titular_cotizado_ss_aut_anyos'] = $row->titular_cotizado_ss_aut_anyos;
        $data['titular_cotizado_ss_aut_meses'] = $row->titular_cotizado_ss_aut_meses;
        $data['conyugue_cotizado_ss_rg_anyos'] = $row->conyugue_cotizado_ss_rg_anyos;
        $data['conyugue_cotizado_ss_rg_meses'] = $row->conyugue_cotizado_ss_rg_meses;
        $data['conyugue_cotizado_ss_aut_anyos'] = $row->conyugue_cotizado_ss_aut_anyos;
        $data['conyugue_cotizado_ss_aut_meses'] = $row->conyugue_cotizado_ss_aut_meses;
        $data['titular_rg_neto_mes_ordinario'] =$row->titular_rg_neto_mes_ordinario;
        $data['titular_rg_paga_extra'] = $row->titular_rg_paga_extra;
        $data['titular_rg_numero_pagas_extra'] = $row->titular_rg_numero_pagas_extra;
        $data['titular_rg_retencion'] = $row->titular_rg_retencion;
        $data['conyugue_rg_neto_mes_ordinario'] =$row->conyugue_rg_neto_mes_ordinario;
        $data['conyugue_rg_paga_extra'] = $row->conyugue_rg_paga_extra;
        $data['conyugue_rg_numero_pagas_extra'] = $row->conyugue_rg_numero_pagas_extra;
        $data['conyugue_rg_retencion'] = $row->conyugue_rg_retencion;
        $data['titular_distintos_ingresos'] = $row->titular_distintos_ingresos;
        $data['titular_distintos_periodicidad'] = $row->titular_distintos_periodicidad;
        $data['titular_distintos_retencion'] = $row->titular_distintos_retencion;
        $data['conyugue_distintos_ingresos'] = $row->conyugue_distintos_ingresos;
        $data['conyugue_distintos_periodicidad'] = $row->conyugue_distintos_periodicidad;
        $data['conyugue_distintos_retencion'] = $row->conyugue_distintos_retencion;
        $data['titular_ahorro_mensual'] = $row->titular_ahorro_mensual;
        $data['conyugue_ahorro_mensual'] = $row->conyugue_ahorro_mensual;
        $data['deuda1_nombre'] = $row->deuda1_nombre;
        $data['deuda1_capital_pendiente'] = $row->deuda1_capital_pendiente;
        $data['deuda1_anyos_pendientes'] = $row->deuda1_anyos_pendientes;
        $data['deuda1_cuota'] = $row->deuda1_cuota;
        $data['deuda2_nombre'] = $row->deuda2_nombre;
        $data['deuda2_capital_pendiente'] = $row->deuda2_capital_pendiente;
        $data['deuda2_anyos_pendientes'] = $row->deuda2_anyos_pendientes;
        $data['deuda2_cuota'] = $row->deuda2_cuota;
        $data['deuda3_nombre'] = $row->deuda3_nombre;
        $data['deuda3_capital_pendiente'] = $row->deuda3_capital_pendiente;
        $data['deuda3_anyos_pendientes'] = $row->deuda3_anyos_pendientes;
        $data['deuda3_cuota'] = $row->deuda3_cuota;
        $data['activos_ahorros_1_afectado'] = $row->activos_ahorros_1_afectado;
        $data['activos_ahorros_1_tipo'] = $row->activos_ahorros_1_tipo;
        $data['activos_ahorros_1_capital_hoy']= $row->activos_ahorros_1_capital_hoy;
        $data['activos_ahorros_1_capital_jubilacion'] = $row->activos_ahorros_1_capital_jubilacion;
        $data['activos_ahorros_1_prima_mensual'] = $row->activos_ahorros_1_prima_mensual;
        $data['activos_ahorros_1_derechos_consolidados'] = $row->activos_ahorros_1_derechos_consolidados;
        $data['activos_ahorros_2_afectado'] = $row->activos_ahorros_2_afectado;
        $data['activos_ahorros_2_tipo'] = $row->activos_ahorros_2_tipo;
        $data['activos_ahorros_2_capital_hoy']= $row->activos_ahorros_2_capital_hoy;
        $data['activos_ahorros_2_capital_jubilacion'] = $row->activos_ahorros_2_capital_jubilacion;
        $data['activos_ahorros_2_prima_mensual'] = $row->activos_ahorros_2_prima_mensual;
        $data['activos_ahorros_2_derechos_consolidados'] = $row->activos_ahorros_2_derechos_consolidados;
        $data['activos_ahorros_3_afectado'] = $row->activos_ahorros_3_afectado;
        $data['activos_ahorros_3_tipo'] = $row->activos_ahorros_3_tipo;
        $data['activos_ahorros_3_capital_hoy']= $row->activos_ahorros_3_capital_hoy;
        $data['activos_ahorros_3_capital_jubilacion'] = $row->activos_ahorros_3_capital_jubilacion;
        $data['activos_ahorros_3_prima_mensual'] = $row->activos_ahorros_3_prima_mensual;
        $data['activos_ahorros_3_derechos_consolidados'] = $row->activos_ahorros_3_derechos_consolidados;
        $data['activos_ahorros_4_afectado'] = $row->activos_ahorros_4_afectado;
        $data['activos_ahorros_4_tipo'] = $row->activos_ahorros_4_tipo;
        $data['activos_ahorros_4_capital_hoy']= $row->activos_ahorros_4_capital_hoy;
        $data['activos_ahorros_4_capital_jubilacion'] = $row->activos_ahorros_4_capital_jubilacion;
        $data['activos_ahorros_4_prima_mensual'] = $row->activos_ahorros_4_prima_mensual;
        $data['activos_ahorros_4_derechos_consolidados'] = $row->activos_ahorros_4_derechos_consolidados;
        $data['activos_ahorros_5_afectado'] = $row->activos_ahorros_5_afectado;
        $data['activos_ahorros_5_tipo'] = $row->activos_ahorros_5_tipo;
        $data['activos_ahorros_5_capital_hoy']= $row->activos_ahorros_5_capital_hoy;
        $data['activos_ahorros_5_capital_jubilacion'] = $row->activos_ahorros_5_capital_jubilacion;
        $data['activos_ahorros_5_prima_mensual'] = $row->activos_ahorros_5_prima_mensual;
        $data['activos_ahorros_5_derechos_consolidados'] = $row->activos_ahorros_5_derechos_consolidados;
        $data['activos_ahorros_6_afectado'] = $row->activos_ahorros_6_afectado;
        $data['activos_ahorros_6_tipo'] = $row->activos_ahorros_6_tipo;
        $data['activos_ahorros_6_capital_hoy']= $row->activos_ahorros_6_capital_hoy;
        $data['activos_ahorros_6_capital_jubilacion'] = $row->activos_ahorros_6_capital_jubilacion;
        $data['activos_ahorros_6_prima_mensual'] = $row->activos_ahorros_6_prima_mensual;
        $data['activos_ahorros_6_derechos_consolidados'] = $row->activos_ahorros_6_derechos_consolidados;
        $data['activos_sv_1_asegurado'] = $row->activos_sv_1_asegurado;
        $data['activos_sv_1_fallecimiento'] = $row->activos_sv_1_fallecimiento;
        $data['activos_sv_1_ipa'] = $row->activos_sv_1_ipa;
        $data['activos_sv_1_ipt'] = $row->activos_sv_1_ipt;
        $data['activos_sv_1_prima_anual'] = $row->activos_sv_1_prima_anual;
        $data['activos_sv_2_asegurado'] = $row->activos_sv_2_asegurado;
        $data['activos_sv_2_fallecimiento'] = $row->activos_sv_2_fallecimiento;
        $data['activos_sv_2_ipa'] = $row->activos_sv_2_ipa;
        $data['activos_sv_2_ipt'] = $row->activos_sv_2_ipt;
        $data['activos_sv_2_prima_anual'] = $row->activos_sv_2_prima_anual;
        $data['activos_sv_3_asegurado'] = $row->activos_sv_3_asegurado;
        $data['activos_sv_3_fallecimiento'] = $row->activos_sv_3_fallecimiento;
        $data['activos_sv_3_ipa'] = $row->activos_sv_3_ipa;
        $data['activos_sv_3_ipt'] = $row->activos_sv_3_ipt;
        $data['activos_sv_3_prima_anual'] = $row->activos_sv_3_prima_anual;
        $data['activos_sv_4_asegurado'] = $row->activos_sv_4_asegurado;
        $data['activos_sv_4_fallecimiento'] = $row->activos_sv_4_fallecimiento;
        $data['activos_sv_4_ipa'] = $row->activos_sv_4_ipa;
        $data['activos_sv_4_ipt'] = $row->activos_sv_4_ipt;
        $data['activos_sv_4_prima_anual'] = $row->activos_sv_4_prima_anual;
        $data['activos_sv_5_asegurado'] = $row->activos_sv_5_asegurado;
        $data['activos_sv_5_fallecimiento'] = $row->activos_sv_5_fallecimiento;
        $data['activos_sv_5_ipa'] = $row->activos_sv_5_ipa;
        $data['activos_sv_5_ipt'] = $row->activos_sv_5_ipt;
        $data['activos_sv_5_prima_anual'] = $row->activos_sv_5_prima_anual;
        $data['it_titular_prima_anual'] = $row->it_titular_prima_anual;
        $data['it_conyugue_prima_anual'] = $row->it_conyugue_prima_anual;
        $data['titular_dias_indemnizacion_percibida'] = $row->titular_dias_indemnizacion_percibida;
        $data['fallecimiento_titular_porcentaje_cobertura'] = $row->fallecimiento_titular_porcentaje_cobertura;
        $data['fallecimiento_titular_anyos_cobertura'] = $row->fallecimiento_titular_anyos_cobertura;
        $data['fallecimiento_conyugue_anyos_cobertura'] = $row->fallecimiento_conyugue_anyos_cobertura;
        $data['fallecimiento_titular_capital_propuesto'] = $row->fallecimiento_titular_capital_propuesto;
        $data['fallecimiento_titular_modalidad'] = $row->fallecimiento_titular_modalidad;
        $data['fallecimiento_titular_prima_aproximada'] = $row->fallecimiento_titular_prima_aproximada;
        $data['fallecimiento_conyugue_porcentaje_cobertura'] = $row->fallecimiento_conyugue_porcentaje_cobertura;
        $data['fallecimiento_conyugue_capital_propuesto'] = $row->fallecimiento_conyugue_capital_propuesto;
        $data['fallecimiento_conyugue_modalidad'] = $row->fallecimiento_conyugue_modalidad;
        $data['fallecimiento_conyugue_prima_aproximada'] = $row->fallecimiento_conyugue_prima_aproximada;
        $data['fallecimiento_beneficiario_1_relacion'] = $row->fallecimiento_beneficiario_1_relacion;
        $data['fallecimiento_beneficiario_1_edad_descendiente'] = $row->fallecimiento_beneficiario_1_edad_descendiente;
        $data['fallecimiento_beneficiario_1_importe_cobrar'] = $row->fallecimiento_beneficiario_1_importe_cobrar;
        $data['fallecimiento_beneficiario_2_relacion'] = $row->fallecimiento_beneficiario_2_relacion;
        $data['fallecimiento_beneficiario_2_edad_descendiente'] = $row->fallecimiento_beneficiario_2_edad_descendiente;
        $data['fallecimiento_beneficiario_2_importe_cobrar'] = $row->fallecimiento_beneficiario_2_importe_cobrar;
        $data['fallecimiento_beneficiario_3_relacion'] = $row->fallecimiento_beneficiario_3_relacion;
        $data['fallecimiento_beneficiario_3_edad_descendiente'] = $row->fallecimiento_beneficiario_3_edad_descendiente;
        $data['fallecimiento_beneficiario_3_importe_cobrar'] = $row->fallecimiento_beneficiario_3_importe_cobrar;
        $data['fallecimiento_beneficiario_4_relacion'] = $row->fallecimiento_beneficiario_4_relacion;
        $data['fallecimiento_beneficiario_4_edad_descendiente'] = $row->fallecimiento_beneficiario_4_edad_descendiente;
        $data['fallecimiento_beneficiario_4_importe_cobrar'] = $row->fallecimiento_beneficiario_4_importe_cobrar;
        $data['fallecimiento_beneficiario_5_relacion'] = $row->fallecimiento_beneficiario_5_relacion;
        $data['fallecimiento_beneficiario_5_edad_descendiente'] = $row->fallecimiento_beneficiario_5_edad_descendiente;
        $data['fallecimiento_beneficiario_5_importe_cobrar'] = $row->fallecimiento_beneficiario_5_importe_cobrar;
        $data['fallecimiento_beneficiario_6_relacion'] = $row->fallecimiento_beneficiario_6_relacion;
        $data['fallecimiento_beneficiario_6_edad_descendiente'] = $row->fallecimiento_beneficiario_6_edad_descendiente;
        $data['fallecimiento_beneficiario_6_importe_cobrar'] = $row->fallecimiento_beneficiario_6_importe_cobrar;
        $data['fallecimiento_beneficiario_7_relacion'] = $row->fallecimiento_beneficiario_7_relacion;
        $data['fallecimiento_beneficiario_7_edad_descendiente'] = $row->fallecimiento_beneficiario_7_edad_descendiente;
        $data['fallecimiento_beneficiario_7_importe_cobrar'] = $row->fallecimiento_beneficiario_7_importe_cobrar;
        $data['tipo_invalidez'] = $row->tipo_invalidez;
        $data['invalidez_titular_porcentaje_cobertura'] = 100; //$row->invalidez_titular_porcentaje_cobertura;
        $data['invalidez_titular_capital_propuesto'] = $row->invalidez_titular_capital_propuesto;
        $data['invalidez_titular_modalidad'] = $row->invalidez_titular_modalidad;
        $data['invalidez_titular_prima_aproximada'] = $row->invalidez_titular_prima_aproximada;
        $data['invalidez_titular_primas_incluidas'] = $row->invalidez_titular_primas_incluidas;
        $data['invalidez_conyugue_porcentaje_cobertura'] = 100; //$row->invalidez_conyugue_porcentaje_cobertura;
        $data['invalidez_conyugue_capital_propuesto'] = $row->invalidez_conyugue_capital_propuesto;
        $data['invalidez_conyugue_modalidad'] = $row->invalidez_conyugue_modalidad;
        $data['invalidez_conyugue_prima_aproximada'] = $row->invalidez_conyugue_prima_aproximada;
        $data['invalidez_conyugue_primas_incluidas'] = $row->invalidez_conyugue_primas_incluidas;
        $data['invalidez_titular_anyos_cobertura'] = $row->invalidez_titular_anyos_cobertura;
        $data['invalidez_conyugue_anyos_cobertura'] = $row->invalidez_conyugue_anyos_cobertura;
        $data['jubilacion_titular_porcentaje_reduccion_gastos'] = $row->jubilacion_titular_porcentaje_reduccion_gastos;
        $data['jubilacion_titular_anyos_cobertura'] = $row->jubilacion_titular_anyos_cobertura;
        $data['jubilacion_conyugue_porcentaje_reduccion_gastos'] = $row->jubilacion_conyugue_porcentaje_reduccion_gastos;
        $data['jubilacion_conyugue_anyos_cobertura'] = $row->jubilacion_conyugue_anyos_cobertura;
        $data['jubilacion_porcentaje_cobertura'] = $row->jubilacion_porcentaje_cobertura;
        $data['dependencia_titular_coste_dependencia_hoy'] = $row->dependencia_titular_coste_dependencia_hoy;
        $data['dependencia_titular_anyos_dependencia'] = $row->dependencia_titular_anyos_dependencia;
        $data['dependencia_titular_porcentaje_cobertura'] = $row->dependencia_titular_porcentaje_cobertura;
        $data['dependencia_conyugue_coste_dependencia_hoy'] = $row->dependencia_conyugue_coste_dependencia_hoy;
        $data['dependencia_conyugue_anyos_dependencia'] = $row->dependencia_conyugue_anyos_dependencia;
        $data['dependencia_conyugue_porcentaje_cobertura'] = $row->dependencia_conyugue_porcentaje_cobertura;
//        $data['dependencia_titular_capital_jubilacion'] = $row->dependencia_titular_capital_jubilacion;
        $data['dependencia_titular_aportacion_mensual'] = $row->dependencia_titular_aportacion_mensual;
        $data['dependencia_titular_aportacion_anual'] = $row->dependencia_titular_aportacion_anual;
//        $data['dependencia_conyugue_capital_jubilacion'] = $row->dependencia_conyugue_capital_jubilacion;
        $data['dependencia_conyugue_aportacion_mensual'] = $row->dependencia_conyugue_aportacion_mensual;
        $data['dependencia_conyugue_aportacion_anual'] = $row->dependencia_conyugue_aportacion_anual;
        $data['dependencia_titular_anyos_renta'] = $row->dependencia_titular_anyos_renta;
        $data['dependencia_conyugue_anyos_renta'] = $row->dependencia_conyugue_anyos_renta;
        $data['hijo_1_nombre'] = $row->hijo_1_nombre;
        $data['hijo_1_sexo'] = $row->hijo_1_sexo;
        $data['hijo_1_fecha_nacimiento'] = date('d-m-Y', strtotime($row->hijo_1_fecha_nacimiento));
        $data['hijo_1_de'] = $row->hijo_1_de;
        $data['hijo_2_nombre'] = $row->hijo_2_nombre;
        $data['hijo_2_sexo'] = $row->hijo_2_sexo;
        $data['hijo_2_fecha_nacimiento'] = date('d-m-Y', strtotime($row->hijo_2_fecha_nacimiento));
        $data['hijo_2_de'] = $row->hijo_2_de;
        $data['hijo_3_nombre'] = $row->hijo_3_nombre;
        $data['hijo_3_sexo'] = $row->hijo_3_sexo;
        $data['hijo_3_fecha_nacimiento'] = date('d-m-Y', strtotime($row->hijo_3_fecha_nacimiento));
        $data['hijo_3_de'] = $row->hijo_3_de;
        $data['hijo_4_nombre'] = $row->hijo_4_nombre;
        $data['hijo_4_sexo'] = $row->hijo_4_sexo;
        $data['hijo_4_fecha_nacimiento'] = date('d-m-Y', strtotime($row->hijo_4_fecha_nacimiento));
        $data['hijo_4_de'] = $row->hijo_4_de;
        $data['hijo_5_nombre'] = $row->hijo_5_nombre;
        $data['hijo_5_sexo'] = $row->hijo_5_sexo;
        $data['hijo_5_fecha_nacimiento'] = date('d-m-Y', strtotime($row->hijo_5_fecha_nacimiento));
        $data['hijo_5_de'] = $row->hijo_5_de;
        $data['hijo_6_nombre'] = $row->hijo_6_nombre;
        $data['hijo_6_sexo'] = $row->hijo_6_sexo;
        $data['hijo_6_fecha_nacimiento'] = date('d-m-Y', strtotime($row->hijo_6_fecha_nacimiento));
        $data['hijo_6_de'] = $row->hijo_6_de;
        $data['hijo_7_nombre'] = $row->hijo_7_nombre;
        $data['hijo_7_sexo'] = $row->hijo_7_sexo;
        $data['hijo_7_fecha_nacimiento'] = date('d-m-Y', strtotime($row->hijo_7_fecha_nacimiento));
        $data['hijo_7_de'] = $row->hijo_7_de;
        
        $data['conyugue_dias_indemnizacion_percibida'] = $row->conyugue_dias_indemnizacion_percibida;
        $data['jubilacion_deudas_previstas_jubilacion'] = $row->jubilacion_deudas_previstas_jubilacion;
        $data['jubilacion_titular_forma_calculo_pensiones'] = $row->jubilacion_titular_forma_calculo_pensiones;
        $data['jubilacion_titular_forma_calculo_pias'] = $row->jubilacion_titular_forma_calculo_pias;
        $data['jubilacion_titular_forma_calculo_seguro_vida'] = $row->jubilacion_titular_forma_calculo_seguro_vida;
        $data['jubilacion_conyugue_forma_calculo_pensiones'] = $row->jubilacion_conyugue_forma_calculo_pensiones;
        $data['jubilacion_conyugue_forma_calculo_pias'] = $row->jubilacion_conyugue_forma_calculo_pias;
        $data['jubilacion_conyugue_forma_calculo_seguro_vida'] = $row->jubilacion_conyugue_forma_calculo_seguro_vida;
        $data['jubilacion_titular_aportacion_pensiones'] = $row->jubilacion_titular_aportacion_pensiones;
        $data['jubilacion_titular_aportacion_pias'] = $row->jubilacion_titular_aportacion_pias;
        $data['jubilacion_titular_aportacion_seguro_vida'] = $row->jubilacion_titular_aportacion_seguro_vida;
        $data['jubilacion_titular_capital_pensiones'] = $row->jubilacion_titular_capital_pensiones;
        $data['jubilacion_titular_capital_pias'] = $row->jubilacion_titular_capital_pias;
        $data['jubilacion_titular_capital_seguro_vida'] = $row->jubilacion_titular_capital_seguro_vida;
        $data['jubilacion_conyugue_capital_pensiones'] = $row->jubilacion_conyugue_capital_pensiones;
        $data['jubilacion_conyugue_capital_pias'] = $row->jubilacion_conyugue_capital_pias;
        $data['jubilacion_conyugue_capital_seguro_vida'] = $row->jubilacion_conyugue_capital_seguro_vida;        
        $data['jubilacion_conyugue_aportacion_pensiones'] = $row->jubilacion_conyugue_aportacion_pensiones;
        $data['jubilacion_conyugue_aportacion_pias'] = $row->jubilacion_conyugue_aportacion_pias;
        $data['jubilacion_conyugue_aportacion_seguro_vida'] = $row->jubilacion_conyugue_aportacion_seguro_vida;        
        $data['jubilacion_titular_traspaso_plan_pensiones'] = $row->jubilacion_titular_traspaso_plan_pensiones;
        $data['jubilacion_conyugue_traspaso_plan_pensiones'] = $row->jubilacion_conyugue_traspaso_plan_pensiones;
        $data['jubilacion_titular_anterior_2006'] = $row->jubilacion_titular_anterior_2006;
        $data['jubilacion_conyugue_anterior_2006'] = $row->jubilacion_conyugue_anterior_2006;
        $data['jubilacion_titular_reparto_cobro_capital'] = $row->jubilacion_titular_reparto_cobro_capital;
        $data['jubilacion_conyugue_reparto_cobro_capital'] = $row->jubilacion_conyugue_reparto_cobro_capital;
        $data['jubilacion_titular_anyos_renta'] = $row->jubilacion_titular_anyos_renta;
        $data['jubilacion_conyugue_anyos_renta'] = $row->jubilacion_conyugue_anyos_renta;
        $data['jubilacion_titular_edad_inicio_renta'] = $row->jubilacion_titular_edad_inicio_renta;
        $data['jubilacion_conyugue_edad_inicio_renta'] = $row->jubilacion_conyugue_edad_inicio_renta;

        return $data;
    }

    public function cache_historico_bases_ipc() {
        $this->db->select('*');
        $this->db->from('historico_bases_ipc');
        $this->db->order_by("anyo", "desc");
        
        $this->m_Historico_Bases_IPC = array();
        $query = $this->db->get();
        foreach($query->result() as $row ) {
            $this->m_Historico_Bases_IPC[$row->anyo]['minimo'] = $row->minimo;
            $this->m_Historico_Bases_IPC[$row->anyo]['maximo'] = $row->maximo;
            $this->m_Historico_Bases_IPC[$row->anyo]['ipc'] = $row->ipc;
        }
        
        $this->m_Historico_Bases_IPC_Len = count($this->m_Historico_Bases_IPC);
        $this->m_Historico_Bases_IPC_Keys = array_keys($this->m_Historico_Bases_IPC);

    }
    
    public function get_historico_bases_ipc( $anyo, &$anyo_datos, &$minimo, &$maximo, &$ipc ) {
        if(is_null($this->m_Historico_Bases_IPC)) {$this->cache_historico_bases_ipc();}

        for( $i = 0; $i < $this->m_Historico_Bases_IPC_Len; $i++ ){
            if( $anyo>=$this->m_Historico_Bases_IPC_Keys[$i]){
                $k = $this->m_Historico_Bases_IPC_Keys[$i];
                $row = $this->m_Historico_Bases_IPC[$k];
                
                $anyo_datos = $k;
                $minimo = $row['minimo'];
                $maximo = $row['maximo'];
                $ipc = $row['ipc'];
                return true;
            }
        }
        
        $this->log_error(" No se encuentra Historicos Bases IPC para el año ".$anyo);
        
        /*
        $this->db->select('*');
        $this->db->from('historico_bases_ipc');
        $this->db->order_by("anyo", "desc");
        
        $query = $this->db->get();
        foreach($query->result() as $row ) {
            $anyo_datos = $row->anyo;
            $minimo = $row->minimo;
            $maximo = $row->maximo;
            $ipc = $row->ipc;
            
            if( $row->anyo <= $anyo ){return true;}
        }

        $this->log_error(" No se encuentra Historicos Bases IPC para el año ".$anyo);
        
        return false;
         */
    }
    
    /*OLD
    public function get_historico_bases_ipc( $anyo, &$anyo_datos, &$minimo, &$maximo, &$ipc ) {
        $this->db->select('*');
        $this->db->from('historico_bases_ipc');
        $this->db->order_by("anyo", "desc");
        
        $query = $this->db->get();
        foreach($query->result() as $row ) {
            $anyo_datos = $row->anyo;
            $minimo = $row->minimo;
            $maximo = $row->maximo;
            $ipc = $row->ipc;
            
            if( $row->anyo <= $anyo ){return true;}
        }

        $this->log_error(" No se encuentra Historicos Bases IPC para el año ".$anyo);
        
        return false;
    }
    */
   
    public function get_comunidades_autonomas() {
        $this->db->select('*');
        $this->db->from('comunidades_autonomas');
        $this->db->order_by("id");

        $comunidades=array();
        $query = $this->db->get();
        foreach($query->result() as $row ) {
            $comunidad=array($row->id, $row->nombre );
            array_push($comunidades, $comunidad );
        }

        return $comunidades;        
    }
    
    public function get_bonificaciones_cuota( $comunidad_id ) {
        $this->db->select('bonificaciones_cuota');
        $this->db->from('comunidades_autonomas');
        $this->db->where('id', $comunidad_id);

        $query = $this->db->get();
        
        if( $query->num_rows() == 0) {return "";}
        
        $row = $query->row();
        return $row->bonificaciones_cuota;
    }

    private function cache_calculo_jubilacion_tabla_a() {
        $sql = "SELECT * FROM calculo_jubilacion_tabla_a ORDER BY anyo DESC";
        $query=$this->db->query($sql);
        
        $this->m_Calculo_Jubilacion_Tabla_A = array();
        
        foreach($query->result() as $row ) {
            $this->m_Calculo_Jubilacion_Tabla_A[$row->anyo]['jubilarse_anyos'] = $row->jubilarse_anyos;
            $this->m_Calculo_Jubilacion_Tabla_A[$row->anyo]['jubilarse_meses'] = $row->jubilarse_meses;
            $this->m_Calculo_Jubilacion_Tabla_A[$row->anyo]['jubilarse_total_meses'] = $row->jubilarse_total_meses;
            $this->m_Calculo_Jubilacion_Tabla_A[$row->anyo]['edad_sino_anyos'] = $row->edad_sino_anyos;
            $this->m_Calculo_Jubilacion_Tabla_A[$row->anyo]['edad_sino_meses'] = $row->edad_sino_meses;
            $this->m_Calculo_Jubilacion_Tabla_A[$row->anyo]['edad_sino_total_meses'] = $row->edad_sino_total_meses;
        }
        
        $this->m_Calculo_Jubilacion_Tabla_A_Len = count($this->m_Calculo_Jubilacion_Tabla_A);
        $this->m_Calculo_Jubilacion_Tabla_A_Keys = array_keys($this->m_Calculo_Jubilacion_Tabla_A);
    }
    
    //Pensiones O9
    public function get_calculo_jubilacion_tabla_a($anyo, &$jubilarse_anyos, &$jubilarse_meses, &$jubilarse_total_meses, &$edad_sino_anyos, &$edad_sino_meses, &$edad_sino_total_meses) {
        if(is_null($this->m_Calculo_Jubilacion_Tabla_A)) {$this->cache_calculo_jubilacion_tabla_a();}

        for($i = 0; $i < $this->m_Calculo_Jubilacion_Tabla_A_Len; $i++ ){
            if( $anyo>=$this->m_Calculo_Jubilacion_Tabla_A_Keys[$i]){
                $k = $this->m_Calculo_Jubilacion_Tabla_A_Keys[$i];
                $row = $this->m_Calculo_Jubilacion_Tabla_A[$k];
                $jubilarse_anyos = $row['jubilarse_anyos'];
                $jubilarse_meses = $row['jubilarse_meses'];
                $jubilarse_total_meses = $row['jubilarse_total_meses'];
                $edad_sino_anyos = $row['edad_sino_anyos'];
                $edad_sino_meses = $row['edad_sino_meses'];
                $edad_sino_total_meses = $row['edad_sino_total_meses'];
                return;
            }
        }
        $this->log_error("Calculo jubilación tabla A: no hay valor para año ".$anyo);

/*        
        //
        $sql = "SELECT * FROM calculo_jubilacion_tabla_a WHERE anyo<=".$anyo." ORDER BY anyo DESC LIMIT 1";
        $query=$this->db->query($sql);
        $row = $query->row();
        if($row ==null) {
            $this->log_error("Calculo jubilación tabla A: no hay valor para año ".$anyo);
            return;
        }
        
        $jubilarse_anyos = $row->jubilarse_anyos;
        $jubilarse_meses = $row->jubilarse_meses;
        $jubilarse_total_meses = $row->jubilarse_total_meses;
        $edad_sino_anyos = $row->edad_sino_anyos;
        $edad_sino_meses = $row->edad_sino_meses;
        $edad_sino_total_meses = $row->edad_sino_total_meses;
 */
    }
    
    private function cache_calculo_jubilacion_tabla_b() {
        $sql = "SELECT * FROM calculo_jubilacion_tabla_b ORDER BY anyo DESC";
        $query=$this->db->query($sql);

        $this->m_Calculo_Jubilacion_Tabla_B = array();
        
        foreach($query->result() as $row ) {
            $this->m_Calculo_Jubilacion_Tabla_B[$row->anyo]['meses_computables'] = $row->meses_computables;
            $this->m_Calculo_Jubilacion_Tabla_B[$row->anyo]['divisor'] = $row->divisor;
            $this->m_Calculo_Jubilacion_Tabla_B[$row->anyo]['anyos_computables'] = $row->anyos_computables;
        }
        
        $this->m_Calculo_Jubilacion_Tabla_B_Len = count($this->m_Calculo_Jubilacion_Tabla_B);
        $this->m_Calculo_Jubilacion_Tabla_B_Keys = array_keys($this->m_Calculo_Jubilacion_Tabla_B);
    }
    
    //Pensiones O27
    public function get_calculo_jubilacion_tabla_b($anyo, &$meses_computables, &$divisor, &$anyos_computables) {
        if(is_null($this->m_Calculo_Jubilacion_Tabla_B)) {$this->cache_calculo_jubilacion_tabla_b();}
        
        for( $i = 0; $i < $this->m_Calculo_Jubilacion_Tabla_B_Len; $i++ ){
            if( $anyo>=$this->m_Calculo_Jubilacion_Tabla_B_Keys[$i]){
                $k = $this->m_Calculo_Jubilacion_Tabla_B_Keys[$i];
                $row = $this->m_Calculo_Jubilacion_Tabla_B[$k];

                $meses_computables = $row['meses_computables'];
                $divisor = $row['divisor'];
                $anyos_computables = $row['anyos_computables'];
                return;
            }
        }
        
        $this->log_error("Calculo jubilación tabla B: no hay valor para año ".$anyo);
        
        /*
        $sql = "SELECT * FROM calculo_jubilacion_tabla_b WHERE anyo<=".$anyo." ORDER BY anyo DESC";
        $query=$this->db->query($sql);
        $row = $query->row();

        if($row ==null) {
            $this->log_error("Calculo jubilación tabla B: no hay valor para año ".$anyo);
            return;
        }
        
        $meses_computables = $row->meses_computables;
        $divisor = $row->divisor;
        $anyos_computables = $row->anyos_computables;

         */
    }
    
    //Pensiones O41
    private function cache_calculo_jubilacion_tabla_c() {
        $sql = "SELECT * FROM calculo_jubilacion_tabla_c ORDER BY anyo DESC";
        $query=$this->db->query($sql);
        
        $this->m_Calculo_Jubilacion_Tabla_C = array();
        
        foreach($query->result() as $row ) {
            $this->m_Calculo_Jubilacion_Tabla_C[$row->anyo]['columna_3'] = $row->columna_3;
            $this->m_Calculo_Jubilacion_Tabla_C[$row->anyo]['columna_4'] = $row->columna_4;
            $this->m_Calculo_Jubilacion_Tabla_C[$row->anyo]['columna_5'] = $row->columna_5;
            $this->m_Calculo_Jubilacion_Tabla_C[$row->anyo]['columna_6'] = $row->columna_6;
        }
        $this->m_Calculo_Jubilacion_Tabla_C_Len = count($this->m_Calculo_Jubilacion_Tabla_C);
        $this->m_Calculo_Jubilacion_Tabla_C_Keys = array_keys($this->m_Calculo_Jubilacion_Tabla_C);
    }
    
    public function get_calculo_jubilacion_tabla_c($anyo, $columna) {
        if(is_null($this->m_Calculo_Jubilacion_Tabla_C)) {$this->cache_calculo_jubilacion_tabla_c();}
        
        for( $i = 0; $i < $this->m_Calculo_Jubilacion_Tabla_C_Len; $i++ ){
            if( $anyo>=$this->m_Calculo_Jubilacion_Tabla_C_Keys[$i]){
                $k = $this->m_Calculo_Jubilacion_Tabla_C_Keys[$i];
                $row = $this->m_Calculo_Jubilacion_Tabla_C[$k];
                
                switch($columna){
                    case 3: return $row['columna_3'];
                    case 4: return $row['columna_4'];
                    case 5: return $row['columna_5']*100;
                    case 6: return $row['columna_6']*100;
                    default: $this->log_error("Columna no esperada en Calculo Jubilacion Tabla C =".$columna);
                        return;
                        
                }
            }
        }
        
        $this->log_error("Calculo jubilación tabla C: no hay valor para año ".$anyo);

    }
    
    private function cache_bases_sumas() {
        $this->db->select('*');
        $this->db->from('bases_sumas');
        $this->db->where('user', $this->session->userdata('logged_user')['id']);

        $query = $this->db->get();
        
        $this->m_Bases_Sumas = array();
        
        foreach($query->result() as $row){
            $this->m_Bases_Sumas[$row->anyo]['titular_sumas_bc'] = $row->titular_sumas_bc;
            $this->m_Bases_Sumas[$row->anyo]['conyugue_sumas_bc'] = $row->conyugue_sumas_bc;
        }
        
        $this->m_Bases_Sumas_Keys = array_keys($this->m_Bases_Sumas);
    }
    
    
    public function bases_sumas($anyo, $columna) {
        if(is_null($this->m_Bases_Sumas)) {$this->cache_bases_sumas();}
        
        if(!in_array($anyo, $this->m_Bases_Sumas_Keys)){
            $this->log_error("Tabla Bases sumas: no hay valor para año ".$anyo);
            return;
        }
        
        switch($columna){
            case 4: return $this->m_Bases_Sumas[$anyo]['titular_sumas_bc'];
            case 7: return $this->m_Bases_Sumas[$anyo]['conyugue_sumas_bc'];
            default: $this->log_error("Tabla Bases sumas: consulta a columna inesperada");
        }
                
    }

    public function br_fallecimiento_incapacidad(
            $anyo_actual, 
            $ipc, 
            $titular_autonomo_base_cotizacion,
            $titular_rg_base_cotizacion,
            $conyugue_autonomo_base_cotizacion,
            $conyugue_rg_base_cotizacion,
            &$titular, &$conyugue) {

        $titular = 0;
        $conyugue = 0; 
        $titular_autonomo = 0;
        $titular_rg = 0;
        $conyugue_autonomo = 0;
        $conyugue_rg = 0;
        $anyo = $anyo_actual;
        $index_revaloriz = 0;
        $revaloriz = 100;
        $prev_revaloriz = $revaloriz;
        
        for( $anyo = $anyo_actual; $anyo >= ($anyo_actual-7); $anyo--) {
            $sumas_bc_titular = 0;
            $sumas_bc_conyugue = 0;
            if($anyo==$anyo_actual){
                $titular_autonomo = $titular_autonomo_base_cotizacion;
                $titular_rg = $titular_rg_base_cotizacion;
                $conyugue_autonomo = $conyugue_autonomo_base_cotizacion;
                $conyugue_rg = $conyugue_rg_base_cotizacion;
                $sumas_bc_titular = $titular_autonomo + $titular_rg;
                $sumas_bc_conyugue = $conyugue_autonomo + $conyugue_rg;
                $b_maxima;
                $no_ipc;
                $no_minimo;
                $this->historico_bases_ipc( $anyo, $no_minimo, $b_maxima, $no_ipc );
                $revaloriz = 100;
                $titular = min($sumas_bc_titular, $b_maxima)*$revaloriz/100; 
                $conyugue= min($sumas_bc_conyugue, $b_maxima)*$revaloriz/100;
            }
            else {
                $titular_autonomo = $titular_autonomo - ($titular_autonomo*$ipc/100);
                $titular_rg = $titular_rg - ($titular_rg*$ipc/100);
                $sumas_bc_titular = $titular_autonomo + $titular_rg;
                $conyugue_autonomo = $conyugue_autonomo - ($conyugue_autonomo*$ipc/100);
                $conyugue_rg = $conyugue_rg - ($conyugue_rg*$ipc/100);
                $sumas_bc_conyugue = $conyugue_autonomo + $conyugue_rg;

                $b_maxima;
                $ipc_historico;
                $no_minimo;
                $this->historico_bases_ipc( $anyo, $no_minimo, $b_maxima, $ipc_historico );
                
                if($index_revaloriz == 1){
                    $revaloriz = 100;
                }
                else {
                    $revaloriz = ((100+$ipc_historico)*$prev_revaloriz)/100;
                }
                
                $titular = $titular + min($sumas_bc_titular, $b_maxima)*$revaloriz/100;
                $conyugue= $conyugue + min($sumas_bc_conyugue, $b_maxima)*$revaloriz/100;
            }
            
            $index_revaloriz++;
            $prev_revaloriz = $revaloriz;
        }
    }

    public function historico_bases_ipc( $anyo, &$minimo, &$maximo, &$ipc ) {

        if( !$this->get_historico_bases_ipc( $anyo, $anyo_datos, $minimo, $maximo, $ipc )) {
            $this->log_error("La tabla histórico bases ipc está vacía");
            return;
        }
        
        if( $anyo == $anyo_datos ) {return;}

        $diff = $anyo-$anyo_datos;
        $minimo = $minimo*pow(1+$ipc/100, $diff);
        $maximo = $maximo*pow(1+$ipc/100, $diff);
    }

    
}
?>
