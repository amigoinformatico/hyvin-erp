<?php
class Fiscalidad_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    private function log_error($message){
        if(strpos($this->config->item('winp3_missing_field'), $message)=== FALSE){
            $this->config->set_item('winp3_missing_field', $this->config->item('winp3_missing_field').$message." | ");
        }
    }


    public function get_irpf_por_ccaa_estatal($comunidad_autonoma, $base_liquidable, $columna ) {
        if( $comunidad_autonoma=="" ) {
            $this->log_error("Falta comunidad autónoma para calcular el IRPF");
            return;
        }
        
        if(!is_numeric($base_liquidable)) {
            $this->log_error("Falta base liquidable para calcular el IRPF");
            return;
        }

        $sql = 'SELECT * FROM irpf_por_ccaa_estatal WHERE comunidad_autonoma='.$comunidad_autonoma;
        $sql = $sql." AND columna_1 <=".$base_liquidable." ORDER BY columna_1 DESC";

        $qry = $this->db->query($sql);
        $row = $qry->row();
        
        if($row==null) {
            $this->log_error('No se ha encontrado valor de IRPF por comunidad autonoma Estatal para base liquidable '.$base_liquidable);
            return;
        }
        
        switch($columna) {
            case 1: return $row->columna_1;
            case 2: return $row->columna_2;
            case 4: return $row->porcentaje;
            default: show_error("Fiscalidad_model::get_irpf_por_ccaa_estatal columna inesperada ".$columna);
        }
    }

    public function get_irpf_por_ccaa_autonomico($comunidad_autonoma, $base_liquidable, $columna ) {
        if( $comunidad_autonoma=="" ) {
            $this->log_error("Falta comunidad autónoma para calcular el IRPF");
            return;
        }
        
        if(!is_numeric($base_liquidable)) {
            $this->log_error("Falta base liquidable para calcular el IRPF");
            return;
        }

        $sql = 'SELECT * FROM irpf_por_ccaa_autonomico WHERE comunidad_autonoma='.$comunidad_autonoma;
        $sql = $sql." AND columna_1 <=".$base_liquidable." ORDER BY columna_1 DESC";

        $qry = $this->db->query($sql);
        $row = $qry->row();
        
        if($row==null) {
            $this->log_error('No se ha encontrado valor de IRPF por comunidad autonoma Autonmómico para base liquidable '.$base_liquidable);
            return;
        }
        
        switch($columna) {
            case 1: return $row->columna_1;
            case 2: return $row->columna_2;
            case 4: return $row->porcentaje;
            default: show_error("Fiscalidad_model::get_irpf_por_ccaa_autonomico columna inesperada ".$columna);
        }
    }
    
    public function get_escala_gravamen_ahorro($comunidad_autonoma, $base_liquidable, $columna) {
        if( $comunidad_autonoma=="" ) {
            $this->log_error("Falta comunidad autónoma para calcular el IRPF");
            return;
        }

        if(!is_numeric($base_liquidable)) {
            $this->log_error("Falta base liquidable para calcular el IRPF");
            return;
        }

        $sql = 'SELECT * FROM escala_gravamen_ahorro WHERE comunidad_autonoma='.$comunidad_autonoma;
        $sql = $sql." AND columna_1 <=".$base_liquidable." ORDER BY columna_1 DESC";

        $qry = $this->db->query($sql);
        $row = $qry->row();

        if($row==null) {
            $this->log_error('Escala gravamen ahorro: No se ha encontrado valor de IRPF por comunidad autonoma para base liquidable '.$base_liquidable);
            return;
        }

        switch($columna){
            case 1: return $row->columna_1;
            case 2: return $row->columna_2;
            case 4: return $row->porcentaje;
            default: show_error("Fiscalidad_model: caso de columna no esperado ".$columna);
        }
    }

    public function isd_por_ccaa($comunidad_autonoma, $base_liquidable, $columna){
        if( $comunidad_autonoma=="" ) {
            $this->log_error("Falta comunidad autónoma para consulta ISD");
            return;
        }

        $sql = 'SELECT * FROM isd_por_ccaa WHERE comunidad_autonoma='.$comunidad_autonoma;
        $sql = $sql." AND ".$base_liquidable.">= columna_1 ORDER BY columna_1 DESC";

        $qry = $this->db->query($sql);
        $row = $qry->row();
        
        if($row==null) {
            $this->log_error('No se ha encontrado valor en la tabla de ISD para base liquidable '.$base_liquidable);
            return;
        }

        switch($columna) {
            case 1: return $row->columna_1;
            case 2: return $row->columna_2;
            case 4: return $row->columna_4;
            default: show_error("Fiscalidad_model::isd_por_ccaa columna inesperada ".$columna);
        }
    }
    
    public function reduccion_por_seguro_vida($comunidad_autonoma, &$titular, &$conyugue, $edad_beneficiario, &$beneficiario){
        $sql = 'SELECT * FROM reduccion_por_seguro_vida WHERE comunidad_autonoma='.$comunidad_autonoma;
        $qry = $this->db->query($sql);
        $row = $qry->row();
        
        if($row==null) {
            $this->log_error('No se ha encontrado valor en tabla reduccion por seguro de vida para comunidad autonoma '.$comunidad_autonoma);
            return;
        }
        
        $titular = $row->reduccion;
        $conyugue=$row->conyugue;
        
        switch($edad_beneficiario){
            case 0: $beneficiario = 0; break;
            case 1: $beneficiario = $row->descendiente_1;break;
            case 2: $beneficiario = $row->descendiente_2;break;
            case 3: $beneficiario = $row->descendiente_3;break;
            case 4: $beneficiario = $row->descendiente_4;break;
            case 5: $beneficiario = $row->descendiente_5;break;
            case 6: $beneficiario = $row->descendiente_6;break;
            case 7: $beneficiario = $row->descendiente_7;break;
            case 8: $beneficiario = $row->descendiente_8;break;
            case 9: $beneficiario = $row->descendiente_9;break;
            case 10: $beneficiario = $row->descendiente_10;break;
            case 11: $beneficiario = $row->descendiente_11;break;
            case 12: $beneficiario = $row->descendiente_12;break;
            case 13: $beneficiario = $row->descendiente_13;break;
            case 14: $beneficiario = $row->descendiente_14;break;
            case 15: $beneficiario = $row->descendiente_15;break;
            case 16: $beneficiario = $row->descendiente_16;break;
            case 17: $beneficiario = $row->descendiente_17;break;
            case 18: $beneficiario = $row->descendiente_18;break;
            case 19: $beneficiario = $row->descendiente_19;break;
            case 20: $beneficiario = $row->descendiente_20;break;
            case 21: $beneficiario = $row->descendiente_21;break;
            case 22: $beneficiario = $row->descendiente_22;break;
            case 23: $beneficiario = $row->descendiente_23;break;
            case 24: $beneficiario = $row->descendiente_24;break;
            case 25: $beneficiario = $row->descendiente_25;break;
            default: $this->log_error("En tabla reducción por seguro de vida->Edad descendiente no tratada = ".$edad_beneficiario);
        }
    }
}
?>
