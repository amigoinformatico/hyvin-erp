<?php

Class User_model extends CI_Model
{
    public function login($username, $password)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('username', $username);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $row = $query->row();
            if (password_verify($password, $row->password) && (1 == $row->estado)) {
                return $row;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function get_all()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->order_by('estado desc, apellidos');

        $query = $this->db->get();
        $users = [];
        $i = 0;

        foreach ($query->result() as $row) {
            $users[$i]['id'] = $row->id;
            $users[$i]['username'] = $row->username;
            $users[$i]['nombre'] = $row->nombre;
            $users[$i]['apellidos'] = $row->apellidos;
            $users[$i]['email'] = $row->email;
            $users[$i]['estado'] = $row->estado;
            $users[$i]['administrador'] = $row->administrador;

            $i++;
        }

        return $users;
    }

    public function add_user($parent)
    {

        $sql = "SELECT * FROM users WHERE username='" . $parent->input->post('user_name') . "'";
        $query = $this->db->query($sql);
        $row = $query->row();
        if ($row != null) {
            return false;
        }

        if ($parent->input->post('tipo_usuario') == 'admin') {
            $administrador = 1;
        } else {
            $administrador = 0;
        }

        $data = array(
            'username' => $parent->input->post('user_name'),
            'password' => password_hash($parent->input->post('password'), PASSWORD_DEFAULT),
            'nombre' => $parent->input->post('nombre'),
            'apellidos' => $parent->input->post('apellidos'),
            'email' => $parent->input->post('email'),
            'estado' => '1',
            'administrador' => $administrador,
            'contacto_correduria' => $parent->input->post('contacto_correduria'),
            'contacto_nombre' => $parent->input->post('contacto_nombre'),
            'contacto_telefono_1' => $parent->input->post('contacto_telefono_1'),
            'contacto_telefono_2' => $parent->input->post('contacto_telefono_2'),
            'contacto_direccion_1' => $parent->input->post('contacto_direccion_1'),
            'contacto_direccion_2' => $parent->input->post('contacto_direccion_2'),
            'contacto_mail' => $parent->input->post('contacto_mail'),
            'contacto_final_1' => $parent->input->post('contacto_final_1')
        );

        if (!$this->db->insert('users', $data)) {
            return 0;
        }

        return $this->db->insert_id();
    }

    public function update_user($parent)
    {

        $data = array(
            'nombre' => $parent->input->post('nombre'),
            'apellidos' => $parent->input->post('apellidos'),
            'email' => $parent->input->post('email'),
            'estado' => '1',
            'contacto_correduria' => $parent->input->post('contacto_correduria'),
            'contacto_nombre' => $parent->input->post('contacto_nombre'),
            'contacto_telefono_1' => $parent->input->post('contacto_telefono_1'),
            'contacto_telefono_2' => $parent->input->post('contacto_telefono_2'),
            'contacto_direccion_1' => $parent->input->post('contacto_direccion_1'),
            'contacto_direccion_2' => $parent->input->post('contacto_direccion_2'),
            'contacto_mail' => $parent->input->post('contacto_mail'),
            'contacto_final_1' => $parent->input->post('contacto_final_1')
        );

        if (trim($parent->input->post('password')) != '') {
            $data['password'] = password_hash($parent->input->post('password'), PASSWORD_DEFAULT);
        }

        if (!($parent->input->post('user_id') == $this->session->userdata('logged_user')['id'] ||
            $this->session->userdata('logged_user')['administrador'] == 1)) {
            return false;
        }

        $this->db->where('id', $parent->input->post('user_id'));
        return $this->db->update('users', $data);
    }

    public function deactivate_user($user_id)
    {

        $data = array(
            'estado' => '0'
        );

        $this->db->where('id', $user_id);
        return $this->db->update('users', $data);
    }

    public function activate_user($user_id)
    {

        $data = array(
            'estado' => '1'
        );

        $this->db->where('id', $user_id);
        return $this->db->update('users', $data);
    }

    public function get_user($user_id)
    {
        $sql = "SELECT * FROM users WHERE id='" . $user_id . "'";
        $query = $this->db->query($sql);
        $row = $query->row();
        if ($row == null) {
            return '';
        }

        $user['id'] = $row->id;
        $user['user_name'] = $row->username;
        $user['nombre'] = $row->nombre;
        $user['apellidos'] = $row->apellidos;
        $user['email'] = $row->email;
        $user['repetir_email'] = $row->email;
        $user['estado'] = $row->estado;
        $user['administrador'] = $row->administrador;
        $user['contacto_correduria'] = $row->contacto_correduria;
        $user['contacto_nombre'] = $row->contacto_nombre;
        $user['contacto_telefono_1'] = $row->contacto_telefono_1;
        $user['contacto_telefono_2'] = $row->contacto_telefono_2;
        $user['contacto_direccion_1'] = $row->contacto_direccion_1;
        $user['contacto_direccion_2'] = $row->contacto_direccion_2;
        $user['contacto_mail'] = $row->contacto_mail;
        $user['contacto_final_1'] = $row->contacto_final_1;

        return $user;
    }

    /**
     * @return mixed
     */
    public function read()
    {


        $query = $this->db->query("select * from `users`");

        return $query->result_array();

    }

    /**
     * @param $data
     * @return string
     */

    public function insert($data)
    {


        $this->user_name = $data['name']; // please read the below note

        $this->user_password = $data['pass'];

        $this->user_type = $data['type'];


        if ($this->db->insert('tbl_user', $this)) {

            return 'Data is inserted successfully';

        } else {

            return "Error has occured";

        }

    }

    /**
     * @param $id
     * @param $data
     * @return string
     */
    public function update($id, $data)
    {


        $this->user_name = $data['name']; // please read the below note

        $this->user_password = $data['pass'];

        $this->user_type = $data['type'];

        $result = $this->db->update('users', $this, array('user_id' => $id));

        if ($result) {

            return "Data is updated successfully";

        } else {

            return "Error has occurred";

        }

    }

    /**
     * @param $id
     * @return string
     */
    public function delete($id)
    {


        $result = $this->db->query("delete from `users` where user_id = $id");

        if ($result) {

            return "Data is deleted successfully";

        } else {

            return "Error has occurred";

        }

    }

}

?>