<?php
class Constantes_model extends CI_Model {
    
    private $m_Row;
    
    function __construct() {
        parent::__construct();
    }
   

    function get_all() {
        $session_data = $this->session->userdata('logged_user');

        if( $this->m_Row != "") {return $this->m_Row;}
        
        $this->db->select('*');
        $this->db->from('constantes');
        $this->db->where('user', $session_data['id'] );
        
        $query = $this->db->get();
        $this->m_Row = $query->row();
        return $this->m_Row;
    }

    function save($data) {
        $session_data = $this->session->userdata('logged_user');
        
        $this->db->where('user', $session_data['id'] );
        return $this->db->update('constantes', $data );
    }

}
