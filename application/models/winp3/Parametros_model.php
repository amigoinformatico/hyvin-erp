<?php
class Parametros_model extends CI_Model {
    
    private $m_Row;
    
    function __construct() {
        parent::__construct();
    }
   
    function get_all() {
        $session_data = $this->session->userdata('logged_user');

        if($this->m_Row != "") {return $this->m_Row;}
        
        $this->db->select('*');
        $this->db->from('parametros');
        $this->db->where('user', 1 );
        
        $query = $this->db->get();
        $m_Row = $query->row();
        return $m_Row;
    }
    
    function save($data) {
        $session_data = $this->session->userdata('logged_user');
        
        $this->db->where('user', 1 );
        return $this->db->update('parametros', $data );
    }
    
    function export_table($name, $ccaa){
        $skip_first = ($name=='escala_gravamen_ahorro' || $name=="irpf_por_ccaa_autonomico" || $name=="irpf_por_ccaa_estatal" ||
           $name=="isd_por_ccaa" || $name=="reduccion_por_seguro_vida" || $name=="comunidades_autonomas" );

        if($skip_first && $ccaa=='') {return '';}
        
        if( $name == "comunidades_autonomas") {
            return $this->export_comunidades_autonomas($ccaa);
        }
                        
        $columns=$this->db->list_fields($name);
        $j = 1;
        $line='';
        foreach($columns as $column){
            if($j>1 || !$skip_first){
                $line.= $column;
                if($j<(count($columns))){$line.=',';}
            }
            $j++;
        }
        $html[0]=$line;        

        $this->db->select('*');
        $this->db->from($name);
        if($skip_first){
            $this->db->where($columns[0],$ccaa);
        }
        $query = $this->db->get();
        
        $j = 1;
        foreach($query->result_array() as $row ) {
            $len = count($row);
            $keys = array_keys($row);
            $line='';
            for($i = 0; $i < $len; $i++ ){
                if($i>0 || !$skip_first){
                    $line.=$row[$keys[$i]];
                    if($i < ($len-1)) {$line.=',';}
                }
            }
            $html[$j] = $line;

            $j++;
        }
        
        return $html;
    }

    private function export_comunidades_autonomas($ccaa) {

        $this->db->select('bonificaciones_cuota');
        $this->db->from("comunidades_autonomas");
        $this->db->where("id",$ccaa);
        $query = $this->db->get();
        
        $j = 1;
        foreach($query->result() as $row ) {
            $html[0] = $row->bonificaciones_cuota;
            return $html;
        }
            /*
            $len = count($row);
            $keys = array_keys($row);
            $line='';
            for($i = 0; $i < $len; $i++ ){
                if($i==2){
                    $line .= $row[$keys[$i]];
                    $html = $line;
                }
    //            else {
      //              $line.=($row[$keys[$i]]."#");
        //        }
            }
            

            $j++;
        }

        return $html;
             * 
             */
        return "";
    }
    
    function export_table_user($user_id, $name){
        $columns=$this->db->list_fields($name);
        $j = 1;
        $line='';
        foreach($columns as $column){
            if($j>1){
                $line.= $column;
                if($j<(count($columns))){$line.=',';}
            }
            $j++;
        }
        $html[0]=$line;        

        $this->db->select('*');
        $this->db->from($name);
        $this->db->where('user', 1);
        $query = $this->db->get();
        
        $j = 1;
        foreach($query->result_array() as $row ) {
            $len = count($row);
            $keys = array_keys($row);
            $line='';
            for($i = 0; $i < $len; $i++ ){
                if($i>0){
                    $line.=$row[$keys[$i]];
                    if($i < ($len-1)) {$line.=',';}
                }
            }
            $html[$j] = $line;

            $j++;
        }
        
        return $html;
    }

    public function import_table_user($user_id, $name, $file_name){
        $sql ="DELETE FROM ".$name." WHERE user=".$user_id;

        $this->db->query($sql);
        
        $handle = fopen($file_name, "r");
        if ($handle) {
            $line = fgets($handle); //Primera linea con cabecera
            while (($line = fgets($handle)) !== false) {
                $values = explode(',', $line);
                $sql = "INSERT INTO ".$name." VALUES (".$user_id.", ";
                                
                $len = count($values);
                for($i = 0; $i<$len;$i++ ){
                    $sql.= $values[$i];
                    if($i < $len-1) $sql.=", ";
                }
                $sql.=")";

                if(!$this->db->query($sql)){
                    fclose($handle);
                    return false;
                }
            }

            fclose($handle);
        } else {
            return false;
        } 
        return true;
    }
    
    function import_table($name, $ccaa, $file_name){
                $con_ccaa = ($name=='escala_gravamen_ahorro' || $name=="irpf_por_ccaa_autonomico" || $name=="irpf_por_ccaa_estatal" ||
           $name=="isd_por_ccaa" || $name=="reduccion_por_seguro_vida" || "comunidades_autonomas" );

        if( $name == "comunidades_autonomas"){
            return $this->import_table_comunidades_autonomas($ccaa, $file_name);
        }
                
        $sql ="DELETE FROM ".$name;
        if($con_ccaa){$sql.=" WHERE comunidad_autonoma=".$ccaa;}

        $this->db->query($sql);
        
        $handle = fopen($file_name, "r");
        if ($handle) {
            $line = fgets($handle); //Primera linea con cabecera
            while (($line = fgets($handle)) !== false) {
                $values = explode(',', $line);
                $sql = "INSERT INTO ".$name." VALUES (";
                
                if($con_ccaa){
                    $sql.=($ccaa.", ");
                }
                
                $len = count($values);
                for($i = 0; $i<$len;$i++ ){
                    $sql.= $values[$i];
                    if($i < $len-1) $sql.=", ";
                }
                $sql.=")";
                
                if(!$this->db->query($sql)){
                    fclose($handle);
                    return false;
                }
            }

            fclose($handle);
        } else {
            return false;
        } 
        return true;
    }

    function import_table_comunidades_autonomas($ccaa, $file_name){
        $text = "";
        $handle = fopen($file_name, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $text.= $line;
            }
        }
        else {
            return false;
        }
        
        $sql = "UPDATE comunidades_autonomas SET bonificaciones_cuota='".$text."' WHERE id=".$ccaa;

        if(!$this->db->query($sql)){
            fclose($handle);
            return false;
        }

        fclose($handle);
        
        return true;
    }

    public function get_tablas_usuario_exportar() {
        $tablas=[];
                
        //array_push($tablas, array("bases_sumas", "3.Bases: Sumas BC" ));
        //array_push($tablas, array("br_fallecimiento_incapacidad", "3.Bases: BR fallecimiento incapacidad" ));
        array_push($tablas, array("constantes", "Parámetros" ));

        return $tablas;
    }

    
    public function get_tablas_administrador_exportar() {
        $tablas=[];
                
//        array_push($tablas, array("bases_sumas", "3.Bases: Sumas BC" ));
  //      array_push($tablas, array("br_fallecimiento_incapacidad", "3.Bases: BR fallecimiento incapacidad" ));
        array_push($tablas, array("calculo_jubilacion_tabla_a", "22. Cálculo Jubilación Tabla A"));
        array_push($tablas, array("calculo_jubilacion_tabla_b", "22. Cálculo Jubilación Tabla B"));
        array_push($tablas, array("calculo_jubilacion_tabla_c", "22. Cálculo Jubilación Tabla C"));
        array_push($tablas, array("escala_gravamen_ahorro", "23. Escala Gravamen Ahorro"));
        array_push($tablas, array("historico_bases_ipc", "21. Histórico Bases IPC"));
        array_push($tablas, array("irpf_por_ccaa_autonomico", "23. IRPF por comunidad autónoma Autonómico"));
        array_push($tablas, array("irpf_por_ccaa_estatal", "23. IRPF por comunidad autónoma Estatal"));
        array_push($tablas, array("isd_por_ccaa", "23. ISD por comunidad autónoma"));
        array_push($tablas, array("reduccion_por_seguro_vida", "23. Reducción por seguro vida"));
        array_push($tablas, array("comunidades_autonomas", "Textos Bonificaciones Cuota"));
        
        return $tablas;
        
    }

}
