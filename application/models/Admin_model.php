<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{

	public function __construct(){
		parent::__construct();		
	}

	/* ***************** INDICE DE MÉTODOS *****************
	1. Obtener grupos.
		1.1. Insertar nuevo grupo.
		1.2. Actualizar grupo.
		1.3. Activar - desactivar grupo.
		1.4. Eliminar grupo.
		1.5. Importar grupos antigua BD.
		1.6. Número empleados por grupo.
	2. Obtener restaurantes.
		2.1. Insertar nuevo centro.
		2.2. Obtener restaurante por id.
		2.3. Actualizar centro.
		2.4. Activar - desactivar centro.
		2.5. Eliminar centro.
		2.6. Restaurantes por grupo.
		2.7. Obtener todos los centros.
		2.8. Importar centros antigua BD.
		2.9. Número empleados por centro.
		2.10. Provincia del restaurante.
		2.11. Obtener centro por contrato.
	3. Obtener categorías profesionales.
		3.1. Insertar nueva categoría profesional.
		3.2. Actualizar categoría profesional.
		3.3. Activar - desactivar categoría profesional.
		3.4. Eliminar categoría profesional.
		3.5. Importar categorías antigua BD.
		3.6. Ámbito por categoría.
	4. Obtener contratos.
		4.1. Nueva modalidad de contrato.
		4.2. Obtener modalidad de contrato.
		4.3. Actualizar modalidad de contrato.
		4.4. Eliminar modalidad de contrato.
		4.5. Contrato vigente.
		4.6. Obtener contratos por empleado.
		4.7. Obtener contrato por id.
		4.8. Importar tipos de contrato desde antigua BD.
		4.9. Obtener contrato por usuario y fecha.
		4.10. Importar contratos de empleados desde antigua BD.
		4.11. Fechas de fin de período de pruebas por mes y centros.
		4.12. Contratos con fecha de finalización de período de pruebas por mes y centros.
		4.13. Vencimientos de contrato por centro y mes.
		4.14. Finalizació períodos de prueba.
	5. Activar contratos por fecha.
	6. Desactivar contratos vencidos.
		6.1. Obtener contratos vencidos.
		6.2. Obtener próximos vencimientos de contrato.
	7. Marcar como disfrutadas jornadas vencidas.
	8. Obtener informes por usuario.
		8.1. Insertar nuevo informe.
		8.2. Actualizar informe.
		8.3. Importar informes antigua BD.
	9. Obtener auditorías por centro.
		9.1. Insertar nueva auditoría.
		9.2. Eliminar auditoría.
		9.3. Actualizar campo documento.
		9.4. Importar auditorías antigua BD.
	10. Ventas por restaurante y período.
		10.1. Importar ventas antigua BD.
		10.2. Importe total ventas por restaurante y período.
		10.3. Insertar venta.
		10.4. Actualizar venta.
		10.5. Seleccionar venta por fecha y centro.
	11. Horas trabajadas por centro.
		11.1. Horas trabajadas por centro y fecha.
		11.2. Jornadas registradas por centro.
		11.3. Horas semanales convenidas por contrato de trabajo.
	12. Importar tabla docs desde antigua BD.
	13. Insertar fecha vacacionable.
		13.1. Eliminar fecha vacacionable.
		13.2. Obtener fecha vacacionables.	
		13.3. Importar fechas vacacionables desde antigua BD.
			13.3.1. Importar vacaciones desde antigua BD.
			13.3.2. Importar fechas vacaciones desde antigua BD.
		13.4. Vacaciones por empleado.
			13.4.1. Obtener vacaciones pendientes.
		13.5. Insertar en tbl_vacaciones.
		13.6. Actualizar días de vacaciones del empleado.
		13.7. Cancelar días de vacaciones.
		13.8. Abonar días de vacaciones.
			13.8.1. Guardar fechas días abonados.
			13.8.2. Obtener días abonados por usuario.
		13.9. Jornadas por empleado.
		13.10. Fechas de vacaciones del empleado.
		13.11. Insertar fecha de vacaciones.
		13.12. Eliminar fecha de vacaciones.
		13.13. Contar tipos de jornada de empleado.
		13.14. Actualizar estado de día vacacionable.
		13.15. Actualizar número de días disfrutados de vacaciones en tbl_vacaciones.
		13.16. Importar horas trabajadas desde antigua BD.
		13.17. Jornadas por centro de trabajo.
		13.18. Jornadas por centro y período.
		13.19. Tipos de jornada por centro.
		13.20. Insertar centro sin jornadas registradas.
		13.21. Centros con horarios pendientes.
		13.22. Horarios pendientes por centro.
		13.23. Eliminar fecha de horarios no registrados.
	14. Horarios por centro.
		14.1. Insertar horario empleado.
		14.2. Obtener horario empleado por id.
		14.3. Obtener horario empleado por user_id y fecha.
	15. Costes por centro y período.
		15.1. Semanas de un período de fechas.
		15.2. Coste por centro y fecha.
		15.3. Coste horas por empleado: contabilizadas todas las horas como ordinarias.
	16. Importar excel.
	17. Guardar nota.
		17.1. Obtener notas por centros y fecha.
		17.2. Obtener fechas de las notas por centros.
	18. Insertar anticipo.
		18.1. Obtener anticipos por centro.
		18.2. Obtener anticipos por empleado.
	19. Obtener otros conceptos.
		19.1. Insertar item otros conceptos.
		19.2. Actualizar item otros conceptos.
	20. Obtener incrementos salariales por contrato.
		20.1. Insertar incremento salarial.
		20.2. Agregar incremento en salario bruto.
		20.3. Activar incrementos salariales.
	21. Obtener vacaciones por días acumulados.
	22. Insertar centro encargado.
		22.1. Obtener centros por usuario.
		22.2. Obtener centros id por usuario.

	MODIFICACIONES ADMINISTRATIVAS:
	23. Agregar vacaciones a empleado.
		23.1. Modificar centro_id = 0 en tbl_jornadas.
	******************************************************** */

	//1. Obtener grupos:
	public function getGrupos($activo = false){
		$sql = "SELECT A.*,
			(SELECT count(B.centro_id) FROM tbl_centros AS B 
				WHERE B.grupo_id = A.grupo_id
				AND B.centro_activo < 10) nRestaurantes
			FROM tbl_grupos AS A";
		if($activo != false){
			$sql .= " WHERE A.grupo_activo = $activo";
		}
		$q = $this->db->query($sql);
		return $q->result();
	}

	//1.1. Insertar nuevo grupo:
	public function insertGrupo($data){
		$sql = "INSERT INTO tbl_grupos (grupo, grupo_alta, grupo_updated, grupo_activo, autor_id) VALUES(
			'".$data['grupo']."',
			NOW(),
			NOW(),
			'".$data['activo']."',
			'".$this->session->userdata('user_id')."'
		)";
		$this->db->query($sql);
	}

	//1.2. Actualizar grupo:
	public function updateGrupo($data){
		$sql = "UPDATE tbl_grupos SET 
			grupo = '".$data['grupo']."',
			grupo_updated = NOW()
			WHERE grupo_id = '".$data['grupo_id']."'";
		$this->db->query($sql);
	}

	//1.3. Activar - desactivar grupo:
	public function activarGrupo($id, $activo){
		$sql = "UPDATE tbl_grupos SET grupo_activo = $activo WHERE grupo_id = $id";
		$this->db->query($sql);
	}

	//1.4. Eliminar grupo:
	public function delete_grupo($id){
		$sql = "DELETE FROM tbl_grupos WHERE grupo_id = $id";
		$this->db->query($sql);
	}

	//1.5. Importar grupos antigua BD:
	public function import_grupos($data){
		$sql = "INSERT INTO tbl_grupos (grupo_id, grupo, grupo_alta, grupo_updated, grupo_activo, autor_id, grupo_baja) VALUES(
			'".$data['grupo_id']."',
			'".$data['grupo']."',
			'".$data['grupo_alta']."',
			'".$data['grupo_updated']."',
			'".$data['activo']."',
			'".$data['autor_id']."',
			'".$data['grupo_baja']."'
		)";
		$this->db->query($sql);	
	}

	//1.6. Número empleados por grupo:
	public function numEmpleadosGrupo($id){
		$sql = "SELECT COUNT(DISTINCT user_id) Total FROM tbl_users_contratos
			WHERE grupo_id = $id
			AND contrato_activo = 1";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//2. Obtener restaurantes:
	public function getRestaurantes($activo = false){
		$sql = "SELECT A.*, B.grupo 
			FROM tbl_centros AS A
			INNER JOIN tbl_grupos AS B
				ON A.grupo_id = B.grupo_id";
		if($activo != false){
			$sql .= " WHERE A.centro_activo = $activo";
		}
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//2.1. Insertar nuevo centro:
	public function insertCentro($data){
		$sql = "INSERT INTO tbl_centros (centro, grupo_id, poblacion_id, direccion, cp, centro_alta, centro_updated, centro_activo, autor_id, telf, fax, website) VALUES(
			'".$data['centro']."',
			'".$data['grupo_id']."',
			'".$data['poblacion_id']."',
			'".$data['direccion']."',
			'".$data['cp']."',
			NOW(),
			NOW(),
			'".$data['activo']."',
			'".$this->session->userdata('user_id')."',
			'".$data['telf']."',
			'".$data['fax']."',
			'".$data['website']."'
		)";
		$this->db->query($sql);	
	}

	//2.2. Obtener restaurante por id:
	public function getRestauranteId($id){
		$sql = "SELECT A.*, B.poblacion, C.provincia_id, C.provincia FROM tbl_centros AS A 
			INNER JOIN tbl_poblaciones AS B
				ON A.poblacion_id = B.poblacion_id
			INNER JOIN tbl_provincias AS C
				ON B.provincia_id = C.provincia_id
			WHERE centro_id = $id";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//2.3. Actualizar centro:
	public function updateCentro($data){
		$sql = "UPDATE tbl_centros SET
			centro = '".$data['centro']."',
			grupo_id = '".$data['grupo_id']."',
			poblacion_id = '".$data['poblacion_id']."',
			direccion = '".$data['direccion']."',
			cp = '".$data['cp']."',
			centro_updated = NOW(),
			centro_activo = '".$data['activo']."',
			telf = '".$data['telf']."',
			fax = '".$data['fax']."',
			website = '".$data['website']."'
			WHERE centro_id = '".$data['centro_id']."'";
		$this->db->query($sql);
	}

	//2.4. Activar - desactivar centro:
	public function activarCentro($id, $activo){
		$sql = "UPDATE tbl_centros SET centro_activo = $activo WHERE centro_id = $id";
		$this->db->query($sql);
	}

	//2.5. Eliminar centro:
	public function deleteCentro($id){
		$sql = "DELETE FROM tbl_centros WHERE centro_id = $id";
		$this->db->query($sql);
	}

	//2.6. Restaurantes por grupo:
	public function getCentrosGrupo($id){
		$sql = "SELECT * FROM tbl_centros 
			WHERE grupo_id = $id";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//2.7. Obtener todos los centros:
	public function getCentros($activo = ''){
		$sql = "SELECT * FROM tbl_centros";
		if($activo != ''){
			$sql .= " WHERE centro_activo = $activo";
		}
		$q = $this->db->query($sql);
		return $q->result();
	}

	//2.8. Importar centros antigua BD:
	public function import_centros($data){
		$sql = "INSERT INTO tbl_centros (centro_id, centro, grupo_id, poblacion_id, direccion, cp, centro_alta, centro_updated, centro_activo, autor_id, centro_baja, telf, fax, website) VALUES(
			'".$data['centro_id']."',
			'".$data['centro']."',
			'".$data['grupo_id']."',
			'".$data['poblacion_id']."',
			'".$data['direccion']."',
			'".$data['cp']."',
			'".$data['centro_alta']."',
			'".$data['centro_updated']."',
			'".$data['activo']."',
			'".$data['autor_id']."',
			'".$data['centro_baja']."',
			'".$data['telf']."',
			'".$data['fax']."',
			'".$data['website']."'
		)";
		$this->db->query($sql);		
	}

	//2.9. Número empleados por centro:
	public function numEmpleadosCentro($id){
		$sql = "SELECT COUNT(DISTINCT user_id) Total FROM tbl_users_contratos
			WHERE centro_id = $id
			AND contrato_activo = 1";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//2.10. Provincia del restaurante:
	public function getProvinciaCentro($id){
		$sql = "SELECT A.provincia_id, A.provincia FROM tbl_provincias AS A
			INNER JOIN tbl_poblaciones AS B
				ON A.provincia_id = B.provincia_id
			INNER JOIN tbl_centros AS C
				ON B.poblacion_id = C.poblacion_id
			WHERE C.centro_id = $id";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//2.11. Obtener centro por contrato:
	public function getCentroContrato($id){
		$sql = "SELECT centro_id FROM tbl_users_contratos WHERE user_contrato_id = $id";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//3. Obtener categorías profesionales:
	public function getCategorias($activo = false){
		$sql = "SELECT * FROM tbl_cargos";
		if($activo != false){
			$sql .= " WHERE activo = '$activo'";
		}
		$q = $this->db->query($sql);
		return $q->result();
	}

	//3.1. Insertar nueva categoría profesional:
	public function insertCategoria($data){
		$sql = "INSERT INTO tbl_cargos (cargo, activo, cargo_created, cargo_updated, ambito) VALUES(
			'".$data['cargo']."',
			'".$data['activo']."',
			NOW(),
			NOW(),
			'".$data['ambito']."')";
		$this->db->query($sql);
	}

	//3.2. Actualizar categoría profesional:
	public function updateCategoria($data){
		$sql = "UPDATE tbl_cargos SET 
			cargo = '".$data['cargo']."',
			ambito = '".$data['ambito']."'
			WHERE cargo_id = '".$data['cargo_id']."'";
		$this->db->query($sql);
	}

	//3.3. Activar - desactivar categoría profesional:
	public function activarCategoria($id, $activo){
		$sql = "UPDATE tbl_cargos SET activo = $activo WHERE cargo_id = $id";
		$this->db->query($sql);
	}

	//3.4. Eliminar categoría profesional:
	public function deleteCategoria($id){
		$sql = "DELETE FROM tbl_cargos WHERE cargo_id = $id";
		$this->db->query($sql);
	}

	//3.5. Importar categorías antigua BD:
	public function import_cargos($data){
		$sql = "INSERT INTO tbl_cargos (cargo_id, cargo, activo, ambito) VALUES(
			'".$data['cargo_id']."',
			'".$data['cargo']."',
			'".$data['activo']."',
			'".$data['ambito']."')";
		$this->db->query($sql);	
	}

	//3.6. Ámbito por categoría:
	public function getAmbitoCargo($id){
		$sql = "SELECT ambito FROM tbl_cargos 
			WHERE cargo_id = $id";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//4. Obtener contratos:
	public function getContratos($activo = false){
		$sql = "SELECT * FROM tbl_contratos";
		if($activo != false){
			$sql .= " WHERE vigente = '$vigente'";
		}
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//4.1. Nueva modalidad de contrato:
	public function insertContrato($data){
		$sql = "INSERT INTO tbl_contratos (contrato, contrato_alta, vigente, caducidad, duracion) VALUES (
			'".$data['contrato']."',
			'".$data['contrato_alta']."',
			'".$data['vigente']."',
			'".$data['caducidad']."',
			'".$data['duracion']."')";
		$this->db->query($sql);
	}

	//4.2. Obtener modalidad de contrato:
	public function getContratoTipoId($id){
		$sql = "SELECT * FROM tbl_contratos WHERE contrato_id = $id";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//4.3. Actualizar modalidad de contrato:
	public function updateContrato($data){
		$sql = "UPDATE tbl_contratos SET 
			contrato = '".$data['contrato']."',
			vigente = '".$data['vigente']."',
			duracion = '".$data['duracion']."',
			periodo_prueba = '".$data['periodo_prueba']."',
			demora = '".$data['demora']."'
			WHERE contrato_id = '".$data['contrato_id']."'";
		$this->db->query($sql);
	}

	//4.4. Eliminar modalidad de contrato.
	public function deleteContrato($id){
		$sql = "DELETE FROM tbl_contratos WHERE contrato_id = $id";
		$this->db->query($sql);
	}

	//4.5. Contrato vigente:
	public function activarContrato($id,$activo){
		$sql = "UPDATE tbl_contratos SET vigente = $activo WHERE contrato_id = $id";
		$this->db->query($sql);
	}

	//4.6. Obtener contratos por empleado:
	public function getContratosUser($id){
		$sql = "SELECT A.*, B.contrato, B.duracion, B.demora, B.caducidad, C.centro, D.grupo, E.cargo FROM tbl_users_contratos AS A
			INNER JOIN tbl_contratos AS B
				ON A.contrato_id = B.contrato_id
			INNER JOIN tbl_centros AS C
				ON A.centro_id = C.centro_id
			INNER JOIN tbl_grupos AS D
				ON C.grupo_id = D.grupo_id
			INNER JOIN tbl_cargos AS E
				ON A.cargo_id = E.cargo_id
			WHERE A.user_id = $id 
			AND A.centro_id > '0'
			ORDER BY A.contrato_inicio DESC";		
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//4.7. Obtener contrato por id:
	public function getContratoId($id){
		$sql = "SELECT A.*, B.cargo, C.grupo, D.centro, E.nombre, E.apellidos, F.contrato FROM tbl_users_contratos AS A
			INNER JOIN tbl_cargos AS B
				ON A.cargo_id = B.cargo_id
			INNER JOIN tbl_grupos AS C
				ON A.grupo_id = C.grupo_id
			INNER JOIN tbl_centros AS D
				ON A.centro_id = D.centro_id
			INNER JOIN tbl_users AS E
				ON A.user_id = E.user_id
			INNER JOIN tbl_contratos AS F
				ON A.contrato_id = F.contrato_id
			WHERE A.user_contrato_id = $id";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//4.8. Importar tipos de contrato desde antigua BD:
	public function import_contratos($data){
		$sql = "INSERT INTO tbl_contratos (contrato_id, contrato, contrato_alta, vigente, caducidad, duracion, demora) VALUES (
			'".$data['contrato_id']."',
			'".$data['contrato']."',
			'".$data['contrato_alta']."',
			'".$data['vigente']."',
			'".$data['caducidad']."',
			'".$data['duracion']."',
			'".$data['demora']."')";
		$this->db->query($sql);
	}

	//4.9. Obtener contrato por usuario y fecha:
	public function getContratoEmpleado($id, $fecha){
		$sql = "SELECT user_contrato_id, centro_id
			FROM tbl_users_contratos
			WHERE user_id = $id
			AND contrato_inicio <= '$fecha'
			AND contrato_fin_modificacion >='$fecha'
			ORDER BY user_contrato_id DESC
			LIMIT 1";
		$q = $this->db->query($sql);

		if($q != null){
			return $q->row();
		}else{
			return FALSE;
		}
	}

	//4.10. Importar contratos de empleados desde antigua BD:
	public function import_users_contratos($data){
		$sql = "INSERT INTO tbl_users_contratos (user_contrato_id, contrato_recurrente, user_id, cargo_id, centro_id, grupo_id, contrato_id, contrato_inicio, contrato_fin_modificacion, contrato_fin, fin_prueba, salario_bruto, salario_complementos, ss_coste, ss_bonificacion, horas_semana, coste_hora_extra, contrato_activo, doc_contrato, doc_id, ratio, causa_fin_contrato, contrato_validado, contrato_nivel, ambito) VALUES (
				'".$data['user_contrato_id']."',
				'".$data['contrato_recurrente']."',
				'".$data['user_id']."',
				'".$data['cargo_id']."',
				'".$data['centro_id']."',
				'".$data['grupo_id']."',
				'".$data['contrato_id']."',
				'".$data['contrato_inicio']."',
				'".$data['contrato_fin_modificacion']."',
				'".$data['contrato_fin']."',
				'".$data['fin_prueba']."',
				'".$data['salario_bruto']."',
				'".$data['salario_complementos']."',
				'".$data['ss_coste']."',
				'".$data['ss_bonificacion']."',
				'".$data['horas_semana']."',
				'".$data['coste_hora_extra']."',
				'".$data['contrato_activo']."',
				'".$data['doc_contrato']."',
				'".$data['doc_id']."',
				'".$data['ratio']."',
				'".$data['causa_fin_contrato']."',
				'".$data['contrato_validado']."',
				'".$data['contrato_nivel']."',
				'".$data['ambito']."')";	
		$this->db->query($sql);
	}

	//4.11. Fechas de fin de período de pruebas por mes y centros:
	public function getFinPrueba($centros, $any, $mes){
		$sql = "SELECT fin_prueba FROM tbl_users_contratos 
			WHERE centro_id IN ($centros)
			AND MONTH(fin_prueba) = '$mes' 
			AND YEAR(fin_prueba) = '$any'";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//4.12. Contratos con fecha de finalización de período de pruebas por mes y centros:
	public function getContratosFinPrueba($centros, $fecha){
		$sql = "SELECT A.fin_prueba, A.user_id, B.nombre, B.apellidos, C.centro, D.cargo, E.contrato FROM tbl_users_contratos AS A
			INNER JOIN tbl_users AS B
				ON A.user_id = B.user_id
			INNER JOIN tbl_centros AS C	
				ON A.centro_id = C.centro_id
			INNER JOIN tbl_cargos AS D
				ON A.cargo_id = D.cargo_id
			INNER JOIN tbl_contratos AS E
				ON A.contrato_id = E.contrato_id
			WHERE A.fin_prueba = '$fecha'
			AND A.centro_id IN ($centros)";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//4.13 Vencimientos de contrato por centro y mes:
	public function getFinContrato($centros, $any, $mes){
		$sql = "SELECT contrato_fin 
			FROM tbl_users_contratos 
			WHERE centro_id IN ($centros)
			AND MONTH(contrato_fin) = '$mes' 
			AND YEAR(contrato_fin) = '$any'";
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//4.14. Finalizació períodos de prueba:
	public function getPeriodosPrueba($dias_aviso, $centros = ''){
		$sql = "SELECT A.*, B.*, C.cargo, D.grupo, E.centro FROM tbl_users_contratos AS A
			INNER JOIN tbl_users AS B
				ON A.user_id = B.user_id
			INNER JOIN tbl_cargos AS C
				ON A.cargo_id = C.cargo_id
			INNER JOIN tbl_grupos AS D
				ON A.grupo_id = D.grupo_id
			INNER JOIN tbl_centros AS E
				ON A.centro_id = E.centro_id 
			WHERE (DATE_ADD(A.fin_prueba, INTERVAL -$dias_aviso DAY) < CURDATE())
			AND A.contrato_activo != 2 
			AND A.contrato_nivel = 1
			AND B.user_activo = 1";
			/*if($permisos < 900){
				//Administrador grupo:
				if($permisos > 600){
					$sql .= "AND D.id_grupo = $id_grupo ";
				}elseif($permisos > 500 && $permisos < 600){
					$sql .= "AND E.id_centro = $id_centro ";
				}
			}*/
		if($centros != ''){
			$sql .= " AND A.centro_id IN ($centros)"; 
		}
		$sql .= " ORDER BY A.contrato_fin DESC";
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//4.14. Contratos con vencimiento por mes y centros:
	public function getVencimientosContrato($centros, $fecha){
		$sql = "SELECT A.contrato_fin, A.user_id, B.nombre, B.apellidos, C.centro, D.cargo, E.contrato FROM tbl_users_contratos AS A
			INNER JOIN tbl_users AS B
				ON A.user_id = B.user_id
			INNER JOIN tbl_centros AS C	
				ON A.centro_id = C.centro_id
			INNER JOIN tbl_cargos AS D
				ON A.cargo_id = D.cargo_id
			INNER JOIN tbl_contratos AS E
				ON A.contrato_id = E.contrato_id
			WHERE A.contrato_fin = '$fecha'
			AND A.centro_id IN ($centros)";
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//5. Activar contratos por fecha:
	public function activaContratos(){
		$sql = "UPDATE tbl_users_contratos SET contrato_activo = 1 
			WHERE contrato_activo = ".INACTIVO_."
			AND contrato_inicio = CURDATE()";
		$this->db->query($sql);
	}

	//6. Desactivar contratos vencidos:
	public function venceContratos(){
		//Seleccionamos los contratos con fecha de vencimiento, y generamos continuación de contrato original para aquellas modificaciones que vencen en fecha con las condiciones originales del contrato:
		/*$sql = "SELECT * FROM tbl_users_contratos 
			WHERE contrato_activo = 1
			AND contrato_nivel = 2
			AND contrato_fin_modificacion < CURDATE()";
		$q = $this->db->query($sql);
		$data = $q->result();

		//printArr($data);

		foreach($data as $row){
			//Comprobamos si la fecha de fin de la modificación y fecha de finalización de contrato son diferentes:
			if($row->contrato_fin_modificacion < $row->contrato_fin){
				//Obtenemos datos del contrato original:
				$slc = "SELECT * FROM tbl_users_contratos WHERE user_contrato_id = ".$row->contrato_recurrente;
				$q1 = $this->db->query($slc);
				$row1 = $q1->row();

				//Creamos contrato de continuación del original:
				$hoy = getHoy();
				$inst = "INSERT INTO tbl_users_contratos (contrato_recurrente, user_id, cargo_id, centro_id, grupo_id, contrato_id, contrato_inicio, contrato_fin_modificacion, contrato_fin, salario_bruto, salario_complementos, ss_coste, ss_bonificacion, horas_semana, coste_hora_extra, contrato_activo, ratio, contrato_validado, contrato_nivel) VALUES (
					'".$row1->user_contrato_id."',
					'".$row1->user_id."',
					'".$row1->cargo_id."',
					'".$row1->centro_id."',
					'".$row1->grupo_id."',
					'".$row1->contrato_id."',
					'$hoy',
					'".$row1->contrato_fin."',
					'".$row1->contrato_fin."',
					'".$row1->salario_bruto."',
					'".$row1->salario_complementos."',
					'".$row1->ss_coste."',
					'".$row1->ss_bonificacion."',
					'".$row1->horas_semana."',
					'".$row1->coste_hora_extra."',
					'1',
					'".$row1->ratio."',
					'1',
					'3')";
				$this->db->query($inst);

				//Desactivamos anterior contrato modificado:
				$upd = "UPDATE tbl_users_contratos SET 
					contrato_activo = 2,
					causa_fin_contrato = 7
					WHERE user_contrato_id = ".$row->user_contrato_id;
				$this->db->query($upd);

			//si ambas fechas coinciden, desactivamos el contrato:
			}else{
				$upd = "UPDATE tbl_users_contratos SET 
					contrato_activo = 2,
					causa_fin_contrato = 1
					WHERE user_contrato_id = ".$row->user_contrato_id;
				$this->db->query($upd);
			}
		}*/

		//Para contratos modificados:
		$sql = "UPDATE tbl_users_contratos SET 
			contrato_activo = 2,
			causa_fin_contrato = 5
			WHERE contrato_activo = 1
			AND contrato_nivel = 2
			AND contrato_fin_modificacion < CURDATE()";
		$this->db->query($sql);

		//Desactivamos todos los contratos -originales y modificados- cuya fecha fin haya vencido:
		$sql1 = "UPDATE tbl_users_contratos SET 
			contrato_activo = 2,
			causa_fin_contrato = 1
			WHERE contrato_activo != 2
			AND contrato_fin < CURDATE()";
		$this->db->query($sql1);

		//Desactivamos todos los contratos -originales y modificados- cuya fecha fin modificación haya vencido:
		$sql1 = "UPDATE tbl_users_contratos SET 
			contrato_activo = 2,
			causa_fin_contrato = 5
			WHERE contrato_activo != 2
			AND contrato_nivel = 1
			AND contrato_fin_modificacion < CURDATE()";
		$this->db->query($sql1);
	}

	//6.1. Obtener contratos vencidos:
	public function getContratosVencidos($intervalo, $centros = ''){
		$sql = "SELECT A.*, B.nombre, B.apellidos, C.centro, D.cargo FROM tbl_users_contratos AS A
			INNER JOIN tbl_users AS B
				ON A.user_id = B.user_id
			INNER JOIN tbl_centros AS C
				ON A.centro_id = C.centro_id
			INNER JOIN tbl_cargos AS D
				ON A.cargo_id = D.cargo_id
			WHERE A.contrato_activo = 2
			AND A.contrato_nivel = 1
			AND A.contrato_fin > DATE_SUB(CURDATE(), INTERVAL $intervalo DAY)";
		if($centros != ''){
			$sql .= " AND A.centro_id IN ($centros)"; 
		}
		$sql .= " ORDER BY A.contrato_fin DESC";

		$q = $this->db->query($sql);
		return $q->result();
	}

	//6.2. Obtener próximos vencimientos de contrato:
	public function getProxVencimientos($dias_aviso, $centros = ''){
		$sql = "SELECT A.*, B.*, C.cargo, D.grupo, E.centro 
			FROM tbl_users_contratos AS A
			INNER JOIN tbl_users AS B
				ON A.user_id = B.user_id
			INNER JOIN tbl_cargos AS C
				ON A.cargo_id = C.cargo_id
			INNER JOIN tbl_grupos AS D
				ON A.grupo_id = D.grupo_id
			INNER JOIN tbl_centros AS E
				ON A.centro_id = E.centro_id 
			WHERE (DATE_ADD(A.contrato_fin_modificacion, INTERVAL -$dias_aviso DAY) < CURDATE()) 
			AND A.contrato_activo != 2 
			AND A.contrato_nivel = 1
			AND B.user_activo = 1";
			/*if($permisos < 900){
				//Administrador grupo:
				if($permisos > 600){
					$sql .= "AND D.id_grupo = $id_grupo ";
				}elseif($permisos > 500 && $permisos < 600){
					$sql .= "AND E.id_centro = $id_centro ";
				}
			}*/
		if($centros != ''){
			$sql .= " AND A.centro_id IN ($centros)"; 
		}
		 $sql .= " ORDER BY A.contrato_fin_modificacion ASC";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//7. Marcar como disfrutadas jornadas vencidas:
	public function setJornadasDisfrutadas(){
		$sql = "UPDATE tbl_jornadas SET fecha_disfrutada = '1'
			WHERE jfecha < CURDATE()";
		$this->db->query($sql);	
	}

	//8. Obtener informes por usuario:
	public function getInformesUser($id){
		$sql = "SELECT A.*, B.nombre, B.apellidos FROM tbl_informes AS A
			INNER JOIN tbl_users AS B
				ON A.user_id = B.user_id
			WHERE A.user_id = $id
			ORDER BY A.informe_created DESC";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//8.1. Insertar nuevo informe:
	public function insertInforme($data){
		$sql = "INSERT INTO tbl_informes (user_id, informe, creado_por, informe_created, updated_por, informe_updated) VALUES (
			'".$data['user_id']."',
			'".$data['informe']."',
			'".$data['autor_id']."',
			NOW(),
			'".$data['autor_id']."',
			NOW() )";
		$this->db->query($sql);
	}

	//8.2. Actualizar informe:
	public function updateInforme($data){
		$sql = "UPDATE tbl_informes SET
			informe = '".$data['informe']."',
			updated_por = '".$data['autor_id']."',
			informe_updated = NOW() 
			WHERE informe_id = '".$data['informe_id']."'";
		$this->db->query($sql);
	}

	//8.3. Importar informes antigua BD:
	public function import_informes($data){
		$sql = "INSERT INTO tbl_informes (informe_id, user_id, informe, creado_por, informe_created, updated_por, informe_updated) VALUES (
			'".$data['informe_id']."',
			'".$data['user_id']."',
			'".$data['informe']."',
			'".$data['creado_por']."',
			'".$data['informe_created']."',
			'".$data['updated_por']."',
			'".$data['informe_updated']."')";
		$this->db->query($sql);	
	}

	//9. Obtener auditorías por centro:
	public function getAuditoriasCentro($id, $desde = '', $hasta = ''){
		$sql = "SELECT * FROM tbl_auditorias 
			WHERE centro_id = $id";
		if($desde != ''){
			$sql .= " AND auditoria_fecha >= '$desde' ";
		}
		if($hasta != ''){
			$sql .= " AND auditoria_fecha <= '$hasta' ";
		}
		$sql .= " ORDER BY auditoria_fecha DESC";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//9.1. Insertar nueva auditoría:
	public function insertAuditoria($data){
		$sql = "INSERT INTO tbl_auditorias (centro_id, auditoria_fecha, auditoria_comentarios) VALUES(
			'".$data['centro_id']."',
			'".$data['fecha']."',
			'".$data['comentarios']."'
			)";
		$this->db->query($sql);

		//Recuperamos id insertado:
		$id = $this->db->insert_id();
		if($id != null){
			return $id;
		}else{
			return FALSE;
		}
	}

	//9.2. Eliminar auditoría:
	public function deleteAuditoria($id, $archivo){
		//Eliminamos registro:
		$sql = "DELETE FROM tbl_auditorias WHERE auditoria_id = $id";
		$this->db->query($sql);	

		//Eliminamos archivo:
		if($archivo != ''){
			unlink('./img/informes/'.$archivo);
		}

		$this->session->set_flashdata('foto_removed','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>La auditoría se ha eliminado correctamente</div>');
	}

	//9.3. Actualizar campo documento:
	public function update_doc_auditoria($id,$archivo){
		$sql = "UPDATE tbl_auditorias SET auditoria_doc = '$archivo' 
			WHERE auditoria_id = $id";
		$this->db->query($sql);	
	}

	//9.4. Importar auditorías antigua BD:
	public function import_auditoria($data){ 
		$sql = "INSERT INTO tbl_auditorias (auditoria_id, centro_id, auditoria_fecha, auditoria_doc, auditoria_comentarios) VALUES(
			'".$data['auditoria_id']."',
			'".$data['centro_id']."',
			'".$data['fecha']."',
			'".$data['doc']."',
			'".$data['comentarios']."'
			)";
		$this->db->query($sql);	
	}

	//10. Ventas por restaurante y período:
	public function getVentasCentro($id, $desde, $hasta){
		$sql = "SELECT * FROM tbl_ventas 
			WHERE centro_id = $id
			AND fecha_venta >= '$desde'
			AND fecha_venta <= '$hasta'
			ORDER BY fecha_venta ASC";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//10.1. Importar ventas antigua BD:
	public function import_ventas($data){
		$sql = "INSERT INTO tbl_ventas (venta_id, centro_id, user_id, fecha_venta, fecha_venta_updated, comensales_manana, comensales_noche, venta) VALUES (
			'".$data['venta_id']."',
			'".$data['centro_id']."',
			'".$data['user_id']."',
			'".$data['fecha_venta']."',
			'".$data['fecha_venta_updated']."',
			'".$data['comensales_manana']."',
			'".$data['comensales_noche']."',
			'".$data['venta']."'
			)";
		$this->db->query($sql);
	}

	//10.2. Importe total ventas por restaurante y período:
	public function getSumaVentasPeriodo($id, $desde, $hasta){
		$sql = "SELECT SUM(A.venta) VENTAS, A.* FROM tbl_ventas AS A
			WHERE A.centro_id = $id
			AND A.fecha_venta >= '$desde' 
			AND A.fecha_venta <= '$hasta'
			ORDER BY A.fecha_venta";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//10.3. Insertar venta:
	public function insertVenta($data){
		$sql = "INSERT INTO tbl_ventas (centro_id, user_id, fecha_venta, fecha_venta_updated, ".$data['campo'].") VALUES(
			'".$data['centro_id']."',
			'".$data['user_id']."',
			'".$data['fecha']."',
			NOW(),
			'".$data['valor']."'
		)";
		$this->db->query($sql);
	}
		
	//10.4. Actualizar venta:
	public function updVenta($data){
		$sql = "UPDATE tbl_ventas SET 
			user_id = '".$data['user_id']."',
			fecha_venta_updated = NOW(),
			".$data['campo']." = '".$data['valor']."'
			WHERE venta_id = '".$data['venta_id']."'";
		$this->db->query($sql);
	}

	//10.5. Seleccionar venta por fecha y centro:
	public function getVentaFechaCentro($id, $fecha){
		$sql = "SELECT * FROM tbl_ventas 
			WHERE centro_id = $id
			AND fecha_venta = '$fecha'";
		$q = $this->db->query($sql);

		if($q != null){
			return $q->row();
		}else{
			return FALSE;
		}
	}

	//11. Horas trabajadas por centro:
	public function getHorasCentro($id, $desde, $hasta){
		$sql = "SELECT SUM(A.total_horas) TOTAL_HORAS FROM tbl_jornadas AS A
			INNER JOIN tbl_users_contratos AS B
				ON A.user_contrato_id = B.user_contrato_id
			WHERE A.centro_id = $id
			AND A.jfecha >= '$desde'
			AND A.jfecha <= '$hasta'
			ORDER BY A.jfecha";
		$q = $this->db->query($sql);
		return $q->row();
	}

	//11.1. Horas trabajadas por centro y fecha:
	public function getHorasFecha($id, $fecha){
		$sql = "SELECT SUM(A.total_horas) TOTAL_HORAS FROM tbl_jornadas AS A
            INNER JOIN tbl_users_contratos AS B
                ON A.user_contrato_id = B.user_contrato_id
            WHERE A.centro_id = $id
            AND A.jfecha = '$fecha'";
		$q = $this->db->query($sql);
		return $q->row();	
	}

	//11.2. Jornadas registradas por centro:
	public function getRegistrosHorarios($id, $fecha){
		$sql = "SELECT A.jornada_id FROM tbl_jornadas AS A 
			INNER JOIN tbl_users_contratos AS B
				ON A.user_id = B.user_id
			WHERE A.jfecha = '$fecha'
			AND A.centro_id = $id
			AND B.contrato_activo = 1";
		$q = $this->db->query($sql);
		if($q != NULL){
			return $q->row();
		}else{
			return FALSE;
		}
	}

	//11.3. Horas semanales convenidas por contrato de trabajo:
	public function getHorasSemanas($id, $fecha){
		$sql = "SELECT user_contrato_id, user_id, horas_semana, primer_contrato FROM tbl_users_contratos
		WHERE user_id = $id
		AND contrato_inicio <= '$fecha'
		AND contrato_fin_modificacion >= '$fecha'";
		$q = $this->db->query($sql);
		return $q->row();	
	}

	//12. Importar tabla docs desde antigua BD:
	public function import_docs($data){
		$sql = "INSERT INTO tbl_docs (doc_id, doc, formato, peso, doc_fecha, doc_tipo, user_id) VALUES (
			'".$data['doc_id']."',
			'".$data['doc']."',
			'".$data['formato']."',
			'".$data['peso']."',
			'".$data['doc_fecha']."',
			'".$data['doc_tipo']."',
			'".$data['user_id']."'
			)";
		$this->db->query($sql);
	}

	//13. Insertar fecha vacacionable:
	public function insertFechaVacacionable($data){
		$sql = "INSERT INTO tbl_vacacionables (dia_vcble, mes_vcble, any_vcble, centro_id) VALUES (
			'".$data['dia']."',
			'".$data['mes']."',
			'".$data['any']."',
			'".$data['centro_id']."')";
		$this->db->query($sql);
	}

	//13.1. Eliminar fecha vacacionable:
	public function deteleFechaVacacionable($data){
		$sql = "DELETE FROM tbl_vacacionables 
			WHERE dia_vcble = '".$data['dia']."'
			AND mes_vcble = '".$data['mes']."'
			AND any_vcble = '".$data['any']."'
			AND centro_id = '".$data['centro_id']."'";
		$this->db->query($sql);
	}

	//13.2. Obtener fecha vacacionables:
	public function getVacacionables($data){
		$sql = "SELECT * FROM tbl_vacacionables 
			WHERE centro_id = '".$data['centro_id']."'	
			AND mes_vcble = '".$data['mes']."'
			AND any_vcble = '".$data['any']."'";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//13.3. Importar fechas vacacionables desde antigua BD:
	public function import_vacacionables($data){
		$sql = "INSERT INTO tbl_vacacionables (id_vcble, dia_vcble, mes_vcble, any_vcble, centro_id, estado_vcble) VALUES (
			'".$data['id_vcble']."',
			'".$data['dia']."',
			'".$data['mes']."',
			'".$data['any']."',
			'".$data['centro_id']."',
			'".$data['estado']."')";
		$this->db->query($sql);
	}

	//13.3.1. Importar vacaciones desde antigua BD:
	public function import_vacaciones($data){
		$sql = "INSERT INTO tbl_vacaciones (vacaciones_id, user_id, dias_acumulados, dias_disfrutados, dias_abonados) VALUES (
			'".$data['vacaciones_id']."',
			'".$data['user_id']."',
			'".$data['dias_acumulados']."',
			'".$data['dias_disfrutados']."',
			'".$data['dias_abonados']."'
			)";
		$this->db->query($sql);
	}

	//13.3.2. Importar fechas vacaciones desde antigua BD:
	public function import_jornadas_fechas($data){
		$sql = "INSERT INTO tbl_jornadas (user_id, fecha_disfrutada, jornada_tipo, jdia, jmes, jany, jfecha, centro_id) VALUES (
			'".$data['user_id']."',
			'".$data['fecha_disfrutada']."',
			'".$data['tipo']."',
			'".$data['jdia']."',
			'".$data['jmes']."',
			'".$data['jany']."',
			'".$data['jfecha']."',
			'".$data['centro_id']."'
			)";
		$this->db->query($sql);
	}

	//13.4. Vacaciones por empleado:
	public function getVacacionesEmpleado($id){
		$sql = "SELECT * FROM tbl_vacaciones WHERE user_id = $id";
		$q = $this->db->query($sql);
		return $q->row();	
	}

	//13.4.1 Obtener vacaciones pendientes:
	public function getVacacionesPdtes($id){
		$sql = "SELECT A.*, B.*, D.centro 
			FROM tbl_vacaciones AS A
			INNER JOIN tbl_jornadas AS B
				ON A.user_id = B.user_id
			INNER JOIN tbl_users_contratos AS C
				ON A.user_id = C.user_id
			INNER JOIN tbl_centros AS D
				ON C.centro_id = D.centro_id
			WHERE A.user_id = $id
			AND B.jornada_tipo = 'v'
			AND B.jfecha >= CURDATE()
			ORDER BY B.jfecha ASC";
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//13.5. Insertar en tbl_vacaciones:
	public function insertVacacionesEmpleado($data){
		$sql = "INSERT INTO tbl_vacaciones (user_id, dias_acumulados) VALUES (
			'".$data['user_id']."',
			'".$data['dias']."'
		)";
		$this->db->query($sql);
	}

	//13.6. Actualizar días de vacaciones del empleado:
	public function updateVacacionesEmpleado($data){
		$sql = "UPDATE tbl_vacaciones SET dias_acumulados = (dias_acumulados + ".$data['dias'].") WHERE user_id =".$data['user_id'];
		$this->db->query($sql);
	}

	//13.7. Cancelar días de vacaciones:
	public function cancelDiasVacaciones($user_id, $ndias){
		$sql = "UPDATE tbl_vacaciones SET dias_disfrutados = (dias_disfrutados + $ndias) WHERE user_id = $user_id";	
		$this->db->query($sql);
	}

	//13.8. Abonar días de vacaciones:
	public function abonarDiasVacaciones($user_id, $ndias){
		$sql = "UPDATE tbl_vacaciones SET dias_abonados = (dias_abonados + $ndias) WHERE user_id = $user_id";	
		$this->db->query($sql);
	}

	//13.8.1. Guardar fechas días abonados:
	public function insertFechasDiasAbonados($data){
		$sql = "INSERT INTO tbl_dias_abonados (user_id, abono_dias, abono_fecha) VALUES (
			'".$data['user_id']."',
			'".$data['abono_dias']."',
			'".$data['abono_fecha']."'
		)";
		$this->db->query($sql);
	}

	//13.8.2. Obtener días abonados por usuario:
	public function getDiasAbonados($id){
		$sql = "SELECT * FROM tbl_dias_abonados WHERE user_id = $id";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//13.9. Jornadas por empleado:
	public function getJornadasEmpleado($id, $mes, $any){
		$sql = "SELECT * FROM tbl_jornadas
			WHERE user_id = $id
			AND jmes = '$mes'
			AND jany = '$any'"; 
		$q = $this->db->query($sql);
		return $q->result();
	}

	//13.10. Fechas de vacaciones del empleado:
	public function getFechasVacacionesEmpleado($id, $mes, $any){
		$sql = "SELECT A.*, B.* FROM tbl_jornadas AS A
			INNER JOIN tbl_vacacionables AS B
				ON A.id_vcble = B.id_vcble
			WHERE A.user_id = $id 
			AND B.mes_vcble = '$mes'
			AND B.any_vcble = '$any'
			ORDER BY A.id_vcble";
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//13.11. Insertar fecha de vacaciones:
	public function insertJornada($data){
		$sql = "INSERT INTO tbl_jornadas (user_id, jornada_tipo, jdia, jmes, jany, jfecha, user_contrato_id, entra1, sale1, entra2, sale2, centro_id) VALUES (
			'".$data['user_id']."',
			'".$data['jornada_tipo']."',
			'".$data['jdia']."',
			'".$data['jmes']."',
			'".$data['jany']."',
			'".$data['fecha']."',
			'".$data['contrato']."',
			'".$data['jornada_tipo']."',
			'".$data['jornada_tipo']."',
			'".$data['jornada_tipo']."',
			'".$data['jornada_tipo']."',
			'".$data['centro_id']."')";
		$this->db->query($sql);

		//Recuperamos id insertado:
		$id = $this->db->insert_id();
		if($id != null){
			return $id;
		}
	}

	//13.12. Eliminar fecha de vacaciones:
	public function deleteJornada($user_id, $jornada_id){
		$sql = "DELETE FROM tbl_jornadas 
			WHERE jornada_id = $jornada_id
			AND user_id = $user_id";
		$this->db->query($sql);
	}

	//13.13. Contar tipos de jornada de empleado:
	public function countJornadasEmpleado($id, $tipo, $disfrutadas = ''){
		$sql = "SELECT count(jornada_id) totalJornadas 
			FROM tbl_jornadas
			WHERE user_id = $id
			AND jornada_tipo = '$tipo'";
		if($disfrutadas != ''){
			$sql .= " AND fecha_disfrutada = '$disfrutadas'";
		}
		$q = $this->db->query($sql);
		return $q->row();
	}

	//13.14. Actualizar estado de día vacacionable:
	public function updateEstadoVcble($id, $estado){
		$sql = "UPDATE tbl_vacacionables SET estado_vcble = $estado
			WHERE id_vcble = $id";
		$this->db->query($sql);
	}

	//13.15. Actualizar número de días disfrutados de vacaciones en tbl_vacaciones:
	public function updVacacionesDisfrutadas($id, $n){
		$sql = "UPDATE tbl_vacaciones SET dias_disfrutados = $n
			WHERE user_id = $id";
		$this->db->query($sql);
	}

	//13.16. Importar horas trabajadas desde antigua BD:
	public function import_horas_trabajadas($data){
		$p = explode('-', $data['fecha']);
		$sql = "INSERT INTO tbl_jornadas(user_id, jornada_tipo, jdia, jmes, jany, jfecha, user_contrato_id, entra1, sale1, entra2, sale2, total_horas, centro_id) VALUES (
			'".$data['user_id']."',
			'".$data['tipo']."',
			'".$p[2]."',
			'".$p[1]."',
			'".$p[0]."',
			'".$data['fecha']."',
			'".$data['contrato']."',
			'".$data['entra1']."',
			'".$data['sale1']."',
			'".$data['entra2']."',
			'".$data['sale2']."',
			'".$data['total_horas']."',
			'".$data['centro_id']."')";
		$this->db->query($sql);
	}

	//13.17. Jornadas por centro de trabajo:
	public function getJornadasCentro($id, $mes, $any){
		$sql = "SELECT A.*, B.nombre, B.apellidos, D.centro 
			FROM tbl_jornadas AS A
			INNER JOIN tbl_users AS B
				ON A.user_id = B.user_id
			INNER JOIN tbl_users_contratos AS C
				ON B.user_id = C.user_id
			INNER JOIN tbl_centros AS D
				ON C.centro_id = D.centro_id
			WHERE A.centro_id = $id
			AND A.jmes = '$mes'
			AND A.jany = '$any'
			GROUP BY A.jornada_id
			ORDER BY A.jornada_tipo"; 
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//13.18. Jornadas por centro y período:
	public function getJornadasCentroPeriodo($id, $desde, $hasta){
		$sql = "SELECT A.*, B.nombre, B.apellidos, C.contrato_inicio, C.contrato_fin_modificacion, C.contrato_fin, C.horas_semana, C.ambito, C.primer_contrato, C.salario_bruto, C.salario_complementos, C.coste_hora_extra, C.ss_coste, D.centro 
			FROM tbl_jornadas AS A
			INNER JOIN tbl_users AS B
				ON A.user_id = B.user_id
			INNER JOIN tbl_users_contratos AS C
				ON B.user_id = C.user_id
			INNER JOIN tbl_centros AS D
				ON C.centro_id = D.centro_id
			WHERE A.centro_id = $id
			AND A.jfecha >= '$desde'
			AND A.jfecha <= '$hasta'
			ORDER BY B.apellidos, A.jfecha ASC"; 
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//13.19. Tipos de jornada por centro:
	public function getTiposJornadaCentro($id, $desde, $hasta){
		$sql = "SELECT A.*, B.nombre, B.apellidos 
			FROM tbl_jornadas AS A
			INNER JOIN tbl_users AS B
				ON A.user_id = B.user_id
			INNER JOIN tbl_users_contratos AS C
				ON B.user_id = C.user_id
			INNER JOIN tbl_centros AS D
				ON C.centro_id = D.centro_id			
			WHERE A.centro_id = $id 
			AND A.jfecha >= '$desde' 
			AND A.jfecha <= '$hasta'
			ORDER BY A.user_id ASC";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//13.20. Insertar centro sin jornadas registradas:
	public function insertCentroNoJornadas($id, $fecha){
		$sql = "INSERT INTO tbl_registros_pendientes (centro_id, fecha) 
			SELECT * FROM (SELECT '$id', '$fecha') AS tmp
			WHERE NOT EXISTS (
				SELECT reg_id FROM tbl_registros_pendientes 
				WHERE centro_id = '$id'
				AND fecha = '$fecha' ) LIMIT 1";
		$this->db->query($sql);
	}

	//13.21. Centros con horarios pendientes:
	public function getHorariosPendientes(){
		$sql = "SELECT A.*, B.centro
			FROM tbl_registros_pendientes AS A
			INNER JOIN tbl_centros AS B
				ON A.centro_id = B.centro_id
			ORDER BY A.centro_id ASC";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//13.22. Horarios pendientes por centro:
	public function getHorariosPendientesCentro($id){
		$sql = "SELECT * FROM tbl_registros_pendientes 
			WHERE centro_id = $id
			ORDER BY fecha DESC";
		$q = $this->db->query($sql);
		return $q->result();	
	}

	//13.23. Eliminar fecha de horarios no registrados:
	public function deleteFechaNoRegistros($id){
		$sql = "DELETE FROM tbl_registros_pendientes WHERE reg_id = $id";
		$this->db->query($sql);
	}

	//14. Horarios por centro:
	public function getHorariosCentro($id, $desde, $hasta){

	}

	//14.1. Insertar horario empleado:
	public function insertHorarioEmpleado($data){
		$p = explode('-', $data['fecha']);
		if($p[2] < 10){
			$dia = substr($p[2],1);
		}else{
			$dia = $p[2];
		}
		$sql = "INSERT INTO tbl_jornadas(user_id, jornada_tipo, jdia, jmes, jany, jfecha, user_contrato_id, entra1, sale1, entra2, sale2, total_horas, centro_id) VALUES (
			'".$data['user_id']."',
			'".$data['jornada_tipo']."',
			'".$dia."',
			'".$p[1]."',
			'".$p[0]."',
			'".$data['fecha']."',
			'".$data['contrato']."',
			'".$data['entra1']."',
			'".$data['entra2']."',
			'".$data['sale1']."',
			'".$data['sale2']."',
			'".$data['total_horas']."',
			'".$data['centro_id']."')";
		$this->db->query($sql);
	}

	//14.2. Obtener horario empleado por id:
	public function getHorarioEmpleado($id){
		$sql = "SELECT * FROM tbl_horas_trabajadas WHERE id_user_contrato = $id";
		$q = $this->db->query($sql);
		return $q->row();	
	}

	//14.3. Obtener horario empleado por user_id y fecha:
	public function getHorarioEmpleadoFecha($id, $fecha){
		$sql = "SELECT A.*, B.horas_semana, B.coste_hora_extra, B.salario_bruto, B.salario_complementos, B.ss_coste FROM tbl_jornadas AS A
			LEFT JOIN tbl_users_contratos AS B
				ON A.user_contrato_id = B.user_contrato_id
			WHERE A.user_id = $id 
			AND A.jfecha = '$fecha'";
		$q = $this->db->query($sql);
		if($q != NULL){
			return $q->row();
		}else{
			return FALSE;
		}
	}

	//14.4. Actualizar horario empleado:
	public function updHorarioEmpleado($data){
		$sql = "UPDATE tbl_jornadas SET 
			jornada_tipo = '".$data['jornada_tipo']."' ";
		if($data['entra1'] != ''){
			$sql .= ", entra1 = '".$data['entra1']."' ";
		}
		if($data['sale1'] != ''){
			$sql .= ", sale1 = '".$data['sale1']."' ";
		}
		if($data['entra2'] != ''){
			$sql .= ", entra2 = '".$data['entra2']."' ";
		}
		if($data['sale2'] != ''){
			$sql .= ", sale2 = '".$data['sale2']."' ";
		}
		$sql .= ", total_horas = '".$data['total_horas']."'
			WHERE jornada_id = '".$data['jornada_id']."'";
		$this->db->query($sql);
	}

	//15. Costes por centro y periodo:
	public function getCosteCentroPeriodo($id, $desde, $hasta){
		//Para obtener el coste real del período, esto es, la suma del coste de horas ordinarias y coste de horas extras, procedemos con los siguientes pasos:
        //1. Obtenemos los empleados del centro:
		$sql = "SELECT user_id
			FROM tbl_users_contratos 
			WHERE centro_id = $id
			AND contrato_inicio <= '$desde'
			AND contrato_fin_modificacion >= '$hasta'
			GROUP BY user_id";
		$q = $this->db->query($sql);
		$obj = $q->result();
		
		//2. Creamos variables donde guardaremos el coste total de horas ordinarias y horas extras, así como el total de horas ordinarias y extras:
		$total_horas_ordinarias = '0';
		$total_horas_extras = '0';
    	$total_coste_horas_ordinarias = '0';
        $total_coste_horas_extras = '0';

        //3. Obtenemos el intervalo de días entre las fechas:
        $tiempo = strtotime($hasta) - strtotime($desde);
    	$dias = $tiempo / 86400; 

        //4. Obtenemos el coste por separado de horas ordinarias y extras para cada empleado:
        foreach($obj as $r){
            //4.1 Obtengo el número de horas trabajadas por cada empleado en las fechas indicadas:
            $sql1 = "SELECT (SELECT SUM(A.total_horas) FROM tbl_jornadas AS A
            		INNER JOIN tbl_users_contratos AS B
            			ON A.user_contrato_id = B.user_contrato_id
					WHERE A.jfecha >= '$desde'
					AND A.jfecha <= '$hasta' 
					AND B.user_id = ".$r->user_id.") totalTrabajadas,

				B.*
            	FROM tbl_jornadas AS A
            	INNER JOIN tbl_users_contratos AS B
            		ON A.user_contrato_id = B.user_contrato_id
            	WHERE A.jfecha >= '$desde'
            	AND A.jfecha <= '$hasta'
            	AND B.user_id = ".$r->user_id." 
            	GROUP BY B.user_id";
            $q1 = $this->db->query($sql1);
            $row1 = $q1->row();
            if($row1 != NULL){
            	$coste_hora = getHoraNormal($row1->salario_bruto, $row1->salario_complementos, $row1->ss_coste, $row1->horas_semana);
            }else{
            	$coste_hora = '';
            }

            //4.2 Extraemos nº horas ordinarias y extras:
            if($row1 != NULL){
            	$horas_extras = $row1->totalTrabajadas - $row1->horas_semana;
            }else{
            	$horas_extras = '';
            }
            //4.3 Coste horas ordinarias y extras:
            if($horas_extras > 0){
            	$coste_horas_ordinarias = $row1->horas_semana * $coste_hora;
            	$coste_horas_extras = $horas_extras * $row1->coste_hora_extra;

            	$horas_ordinarias = $row1->horas_semana;
            	$horasExtras = $horas_extras;
            }else{
            	if($row1 != NULL){
            		$coste_horas_ordinarias = $row1->totalTrabajadas * $coste_hora;
            		$horas_ordinarias = $row1->totalTrabajadas;
            	}else{
            		$coste_horas_ordinarias = '0';
            		$horas_ordinarias = '0';
            	}
            	$coste_horas_extras = '0';
            	$horasExtras = '0';
            }

            //4.4 Pasamos los valores obtenidos a sus respectivos totales:
            $total_coste_horas_ordinarias = $total_coste_horas_ordinarias + $coste_horas_ordinarias;
            $total_coste_horas_extras = $total_coste_horas_extras + $coste_horas_extras;

            $total_horas_ordinarias = $total_horas_ordinarias + $horas_ordinarias;
            $total_horas_extras = $total_horas_extras + $horasExtras;

        }
        /*$total_coste_horas_ordinarias = number_format($total_coste_horas_ordinarias, 2);
        $total_coste_horas_extras = number_format($total_coste_horas_extras, 2);*/

        $total_coste = array($total_coste_horas_ordinarias, $total_coste_horas_extras, $total_horas_ordinarias, $total_horas_extras);

		return $total_coste;
	}

	//15.1. Semanas de un período de fechas:
	public function getSemanasFechas($desde, $hasta){
		$sql = "SELECT WEEK(fecha_venta, 1) SEMANA, 
			YEAR(fecha_venta) ANY
			FROM tbl_ventas
			WHERE fecha_venta >= '$desde'
			AND fecha_venta <= '$hasta' 
			ORDER BY ANY, SEMANA";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//15.2. Coste por centro y fecha:
	public function getCosteCentroFecha($id, $fecha){
		//1. Obtenemos los empleados del centro:
		$sql = "SELECT user_id, user_contrato_id
			FROM tbl_users_contratos 
			WHERE centro_id = $id
			AND contrato_inicio <= '$fecha'
			AND contrato_fin_modificacion >= '$fecha'
			GROUP BY user_id
			ORDER BY user_contrato_id DESC";
		$q = $this->db->query($sql);
		$data = $q->result();


		//printArr($data);

		$total_coste_horas_ordinarias = '0';
        $total_coste_horas_extras = '0';
        foreach($data as $row){
	        //Obtengo el coste día de cada empleado: coste_hora x num_horas
	        $coste_dia = $this->getCosteEmpleado($row->user_contrato_id, $fecha);
	        if($coste_dia > 0){
		        $total_coste_horas_ordinarias = $total_coste_horas_ordinarias + $coste_dia;   
		        //e($coste_dia.'<br>'); 
		    }
		    
        }   
        //exit; 

        return number_format($total_coste_horas_ordinarias, 2);
	}

	//15.3. Coste horas por empleado: contabilizadas todas las horas como ordinarias: 
	public function getCosteEmpleado($user_contrato_id, $fecha){
		$sql = "SELECT A.*, B.* FROM tbl_users_contratos AS A
			INNER JOIN tbl_jornadas AS B
				ON A.user_contrato_id = B.user_contrato_id
			WHERE A.user_contrato_id = $user_contrato_id
			AND B.jfecha = '$fecha'
			AND B.jornada_tipo = 't'";
		$q = $this->db->query($sql);
		$row = $q->row();

		if($row != null){
			$coste_hora = getHoraNormal($row->salario_bruto, $row->salario_complementos, $row->ss_coste, $row->horas_semana);
			$total = $coste_hora * $row->total_horas;
			return $total;
		}else{
			return '0';
		}
	}

	//16. Importar excel:
	public function loadExcel($table_name,$sql){ 
    	//si existe la tabla
    	if($this->db->table_exists("$table_name")){
      		//si es un array y no está vacio
      		if(!empty($sql) && is_array($sql)){
        		//si se lleva a cabo la inserción
        		if($this->db->insert_batch("$table_name", $sql)){
          			return TRUE;
        		}else{
          			return FALSE;
        		}
      		}
    	}
  	}

  	//16.1. Actualizar desde excel:
  	public function updFromXls($tabla, $data, $pk){
  		$nocampos = array('ID','Empleado');
  		foreach($data as $arr){
  			$k = 0;
  			$sql = "UPDATE $tabla SET ";
  			foreach($arr as $key => $row){
  				if(!in_array($key, $nocampos)){
  					if($k > 1) $sql .= ", ";
  					$sql .= "$key = '".$row."' "; 
  				}
	  			$k++;
	  		}
	  		$sql .= " WHERE $pk = '".$arr['ID']."'";
  			$q = $this->db->query($sql);
  			if($q == NULL){
				return FALSE;
			}
  		}
  		return TRUE;
  	}

  	//17. Guardar nota:
  	public function insertNota($data){
  		$sql = "INSERT INTO tbl_notas (nota, user_id, centro_id, nota_fecha, nota_dia) VALUES(
			'".$data['nota']."',
			'".$data['user_id']."',
			'".$data['centro_id']."',
			NOW(),
			'".$data['fecha']."'
			)";
		$this->db->query($sql);

		//Recuperamos id insertado:
		$id = $this->db->insert_id();
		if($id != null){
			return $id;
		}else{
			return FALSE;
		}	
  	}

  	//17.1. Obtener notas por centros y fecha:
  	public function getNotas($centros, $fecha){
  		$sql = "SELECT A.*, B.centro FROM tbl_notas AS A
  			INNER JOIN tbl_centros AS B
  				ON A.centro_id = B.centro_id
  			WHERE A.centro_id IN ($centros)
  			AND A.nota_dia = '$fecha'";
  		$q = $this->db->query($sql);
		$data = $q->result();
		return $data;
  	}

  	//17.2. Obtener fechas de las notas por centros:
  	public function getFechasNotas($centros){
  		$sql = "SELECT nota_dia FROM tbl_notas
  			WHERE centro_id IN ($centros)";
  		$q = $this->db->query($sql);
		$data = $q->result();
		return $data;
  	}

  	//17.3. Actualizar nota:
  	public function updateNota($nota, $id){
  		$sql = "UPDATE tbl_notas SET nota = '$nota'
			WHERE nota_id = $id";
		$this->db->query($sql);
  	}

  	//18. Insertar anticipo:
  	public function insertAnticipo($data){
  		$sql = "INSERT INTO tbl_anticipos (user_id, centro_id, anticipo_importe, anticipo_fecha, anticipo_doc) VALUES (
			'".$data['user_id']."',
			'".$data['centro_id']."',
			'".$data['anticipo_importe']."',
			'".$data['anticipo_fecha']."',
			'".$data['anticipo_doc']."'	
  		)";
		$this->db->query($sql);
  	}

  	//18.1. Obtener anticipos por centro:
  	public function getAnticiposCentro($id){
  		$sql = "SELECT A.*, B.nombre, B.apellidos, C.centro 
  			FROM tbl_anticipos AS A
  			INNER JOIN tbl_users AS B
  				ON A.user_id = B.user_id
			INNER JOIN tbl_centros AS C
				ON A.centro_id = C.centro_id
  			WHERE A.centro_id = $id
  			ORDER BY A.anticipo_fecha DESC";
  		$q = $this->db->query($sql);
		return $q->result();
  	}

  	//18.2. Obtener anticipos por empleado:
  	public function getAnticiposUser($id){
  		$sql = "SELECT A.*, B.centro FROM tbl_anticipos AS A
  			INNER JOIN tbl_centros AS B
  				ON A.centro_id = B.centro_id
			WHERE A.user_id = $id ORDER BY A.anticipo_fecha DESC";
  		$q = $this->db->query($sql);
		return $q->result();
  	}

  	//19. Obtener otros conceptos:
  	public function getOtrosConceptosUser($id, $mes, $any){
  		$sql = "SELECT * FROM tbl_otros_conceptos 
  			WHERE user_id = $id
  			AND oc_mes = $mes
  			AND oc_any = '$any'
  			ORDER BY oc_mes ASC
  			LIMIT 1";
  		$q = $this->db->query($sql);
		return $q->row();
  	}

  	//19.1. Insertar item otros conceptos:
  	public function insertOtrosConceptos($data){
  		$sql = "INSERT INTO tbl_otros_conceptos (user_id, oc_importe, oc, oc_mes, oc_any) VALUES (
  			'".$data['user_id']."',
			'".$data['oc_importe']."',
			'".$data['oc']."',
			'".$data['oc_mes']."',
			'".$data['oc_any']."'	
  		)";
  		$this->db->query($sql);
  		
  		//Recuperamos id insertado:
		$id = $this->db->insert_id();
		if($id != null){
			return $id;
		}else{
			return FALSE;
		}
  	}

	//19.2. Actualizar item otros conceptos:
	public function updOtrosConceptos($data){
		$sql = "UPDATE tbl_otros_conceptos SET
			oc_importe = '".$data['oc_importe']."',
			oc = '".$data['oc']."'
			WHERE oc_id =  ".$data['oc_id'];
		$this->db->query($sql);
	}

	//20. Obtener incrementos salariales por contrato:
	public function getIncrementosContrato($id){
		$sql = "SELECT * FROM tbl_incrementos_salariales
			WHERE user_contrato_id = $id";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//20.1. Insertar incremento salarial:
	public function insertIncremento($data){
		$sql = "INSERT INTO tbl_incrementos_salariales (user_contrato_id, incremento, incremento_desde, incremento_created, incremento_activo) VALUES(
			'".$data['ucid']."',
			'".$data['incremento']."',
			'".$data['desde']."',
			NOW(),
			'".$data['activo']."')";
		$this->db->query($sql);

		//Incrementamos el salario en tbl_users_contratos:
		if($data['activo'] == 1){
			$this->addIncrementoSalarioBruto($data['ucid'], $data['incremento']);	
		}
	}

	//20.2. Agregar incremento en salario bruto:
	public function addIncrementoSalarioBruto($id, $incremento){
		$sql = "UPDATE tbl_users_contratos SET salario_bruto = (salario_bruto + $incremento) 
			WHERE user_contrato_id = $id";
		$this->db->query($sql);	
	}

	//20.3. Activar incrementos salariales:
	public function setIncrementosSalariales(){
		$sql = "SELECT is_id, user_contrato_id, incremento 
			FROM tbl_incrementos_salariales 
			WHERE incremento_desde <= CURDATE()
			AND incremento_activo = '0'";
		$q = $this->db->query($sql);
		$data = $q->result();

		if($data != ''){
			foreach($data as $row){
				$this->addIncrementoSalarioBruto($row->user_contrato_id, $row->incremento);

				$sql1 = "UPDATE tbl_incrementos_salariales SET incremento_activo = 1
					WHERE is_id = ".$row->is_id;
				$this->db->query($sql1);	
			}
		}
	}

	//21. Obtener vacaciones por días acumulados:
	public function getVacacionesAcumuladas($dias = 1000){
		$sql = "SELECT * FROM tbl_vacaciones 
			WHERE dias_acumulados >= $dias";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//21.1. Actualizar días acumulados de vacaciones:
	public function updateDiasAcumulados($dias, $id){
		$sql = "UPDATE tbl_vacaciones 
			SET dias_acumulados = '$dias'
			WHERE vacaciones_id = $id";
		$this->db->query($sql);
	}

	//22. Insertar centro encargado:
	public function insertCentroEncargado($user_id, $centro_id){
		//comprobamos que no exista:
		$sql = "SELECT id_ce FROM tbl_centros_encargados 
			WHERE user_id = $user_id
			AND centro_id = $centro_id";
		$q = $this->db->query($sql);
		$row = $q->row();

		//si no existe lo creamos:
		if(!isset($row->id_ce)){
			$sql = "INSERT INTO tbl_centros_encargados (user_id, centro_id) VALUES ($user_id, $centro_id)";
			$this->db->query($sql);
		}
	}

	//22.1. Obtener centros por usuario:
	public function getCentrosEncargado($user_id){
		$sql = "SELECT A.centro_id, B.centro, B.rte
			FROM tbl_centros_encargados AS A
			INNER JOIN tbl_centros AS B
				ON A.centro_id = B.centro_id
			WHERE A.user_id = $user_id
			GROUP BY B.centro_id";
		$q = $this->db->query($sql);
		return $q->result();
	}

	//22.2. Obtener centros id por usuario:
	public function getCentrosIdEncargado($user_id, $retorno = false){
		$sql = "SELECT centro_id 
			FROM tbl_centros_encargados
			WHERE user_id = $user_id
			GROUP BY centro_id";
		$q = $this->db->query($sql);
		if(!$retorno){
			return $q->result();
		}else{
			return $q->result_array();
		}
	}

	//22.3. Eliminar responsable de centro:
	public function deleteCentroEncargado($user_id, $centro_id){
		$sql = "DELETE FROM tbl_centros_encargados 
			WHERE user_id = $user_id
			AND centro_id = $centro_id";
		$this->db->query($sql);
	}

	//23. Agregar vacaciones a empleado:
	public function addVacacionesEmpleado($var = 0){
		if($var == 1){
			$user_id = 618;
			$tipo_jornada = 'v';
			$user_contrato_id = 1550;
			$centro_id = 18;
			$desde = strtotime('2018-10-01');
			$hasta = strtotime('2018-10-07');

			for($i=$desde; $i<=$hasta; $i+=86400){
				$fecha = date('Y-m-d', $i);

				$p = explode('-', $fecha);
				$dia = $p[2];
				$mes = $p[1];
				$any = $p[0];

				/*echo 'hora num: '.$i.'<br>';
				echo 'fecha: '.$fecha.'<br>';
				echo 'dia: '.$dia.'<br>';
				echo 'mes: '.$mes.'<br>';
				echo 'any: '.$any.'<br><br><br>';*/


				$sql = "INSERT INTO tbl_jornadas (user_id, fecha_disfrutada, jornada_tipo, jdia, jmes, jany, jfecha, user_contrato_id, entra1, sale1, entra2, sale2, total_horas, centro_id) VALUES (
					$user_id,
					1,
					'$tipo_jornada',
					'$dia',
					'$mes',
					'$any',
					'$fecha',
					$user_contrato_id,
					'$tipo_jornada',
					'$tipo_jornada',
					'$tipo_jornada',
					'$tipo_jornada',
					0,
					$centro_id
				)";
				$this->db->query($sql);
			}	
		}		
	}

	//23.1. Modificar centro_id = 0 en tbl_jornadas:
	public function addCentrosJornadas(){
		$sql = "SELECT A.jornada_id, A.user_contrato_id, B.centro_id 
			FROM tbl_jornadas AS A
			INNER JOIN tbl_users_contratos AS B
				ON A.user_contrato_id = B.user_contrato_id
			WHERE A.centro_id = 0";
		$q = $this->db->query($sql);
		$data = $q->result();

		foreach ($data as $row){
			$centroId = $row->centro_id;
			$jornadaId = $row->jornada_id;
			$sql1 = "UPDATE tbl_jornadas SET centro_id = $centroId
				WHERE jornada_id = $jornadaId";
			$this->db->query($sql1);
		}
	}
}