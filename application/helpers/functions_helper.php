<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//FUNCIONES VARIAS PHP:

//Indice de funciones:
//1. Generales:
	//1.1. e: función simplificación comando 'echo'.
	//1.2. tradPermisos: traducir permisos.
	//1.3. auth_randomText: cadena aleatoria.
	//1.4. auth_randomNum: número aleatorio.
	//1.5. json_to_array: pasar .json a array.
	//1.6. readContenido: leer contenido concreto del array.
	//1.7. readPag: leer info de página del array.
	//1.8. Indicador de posición del contenido.
	//1.9. Eliminar valor de array.
	//1.10. Input hidden para retorno al backend.
	//1.11. Imprimir array.
	//1.12. Obtener IP de usuario.
	//1.13. Cambio de semana.

//2. Cadenas:
	//2.1. stringSeo: eliminar caracteres especiales, espacios en blanco por -,... para cadenas SEO.
	//2.2. sanear: sanitización de cadenas de texto de formularios.
	//2.3. truncar: función para truncar texto.

//3. Fechas:
	//3.1. fechas: formateo de fechas sin horas.
		//3.1.1. Formateo de fechas con horas.
	//3.2. idiomasFecha: Estilo literal de fechas.
	//3.3. getHoy: hoy.
		//3.3.1. getAhora: ahora.
	//3.4. mesesMultidioma: meses multidioma.
		//3.4.1. Array meses.
	//3.5. getMes: extraer mes de fecha.
	//3.6. getAny: extraer año de fecha.
	//3.7. getDia: extraer día de fecha.
	//3.8. parseFechas: formateo fechas en inglés.
	//3.9. parseFechasEs: formateo fechas inglés a castellano.
	//3.10. diasTranscurridos: días transcurridos entre dos fechas.
	//3.11. getAniversario: obtener día y mes de fecha.
	//3.12. sumaHoras: sumar horas.
	//3.13. restaHoras: restar horas.
	//3.14.	horasFloat: valor numérico de horas.
	//3.15. nWeek: calcular número de semana del año.
	//3.16. fechasSemanaNum: fechas comprendidas en semana anual numérica.
	//3.17. sumaDias: sumar días a una fecha.
		//3.17.1. restaDias: restar días a una fecha
	//3.18. diffFechas: diferencia entre fechas.
	//3.19. check_in_range: validar si fecha existe en un rango.

//4. Combos:
	//4.1. Combobox continentes.
	//4.2. Combobox países.
	//4.3. Estados de reserva de servicios.
	//4.4. Combo estados de reserva.

//5. HTML:
	//5.1. Modal.
	//5.2. Tooltip contextual.
	//5.3. Formulario sugerencias.
	//5.4. CTA registro usuario.
	//5.5. CTA formulario comercio.
	//5.6. CTA formulario repartidor.
	//5.7. PDF proyecto coopmercio.
	//5.8. Iconos.
		//5.8.1. Icono editar.
		//5.8.2. Icono eliminar.
		//5.8.3. Icono abonar pago.
		//5.8.4. Icono ver.
		//5.8.5. Icono crear.
		//5.8.6. Icono incidencia.
		//5.8.7. Icono OK.
		//5.8.8. Icono KO.
	//5.9. Controles de capa html.

//6. Póliza line.
//7. Print number.
//*****************************************************************************************************************************************************************


//*********************** 1. GENERALES ***********************
//1.1. Función simplificación comando 'echo':


//1.2. Traducir permisos:
function tradPermisos($nivel = ''){
	$arr = array(
		USER_=>'Sin permisos',
		ADMIN_RTE_=>'Administrador restaurante',
		SUPER_=>'SuperAdministrador'
	);
	//MANAGER_RTE_=>'Manager restaurante',
	//ADMIN_GRUPO_=>'Administrador grupo',

	if($nivel == MANAGER_RTE_ || $nivel == ADMIN_GRUPO_){
		return 'permisos no reconocidos';
		exit;
	}

	if($nivel != ''){
		return $arr[$nivel];
	}else{
		return $arr;
	}
}

//1.3. Cadena aleatoria:
function auth_randomText($length){
	$key = "";
	$pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for($i=0;$i<$length;$i++){
  		$key .= $pattern{rand(0,35)};
	}
	return $key;
}

//1.4. Número aleatorio:
function auth_randomNum($length){
	$key = "";
	$pattern = "1234567890";
	for($i=0;$i<$length;$i++) {
  		$key .= $pattern{rand(0,9)};
	}
	return $key;
}

//1.5. Pasar .json a array:
function jsonToArray($file){
	$json = file_get_contents('./files/'.$file.'.json');
	return json_decode($json,TRUE);
}

//1.6. Leer contenido concreto del array:
function readContenido($posicion, $lang){
	$arr = jsonToArray('contenidos_'.$lang);
	$valor = '';
	foreach($arr as $row){
		if($row['posicion'] == $posicion){
			$valor = $row;
		}
	}
	return $valor;
}

//1.7. Leer info de página del array:
function readPag($pag){
	$arr = jsonToArray('paginas');
	$valor = '';
	foreach($arr as $row){
		if($row['pag_seo'] == $pag){
			$valor = $row;
		}
	}
	return $valor;
}

//1.8. Indicador de posición del contenido:
function Posicion($posicion, $id, $clase = ''){
	//Instamos el superobjeto CI para poder trabajar con sesiones:
	$CI =& get_instance();
    $permisos = $CI->session->userdata('permisos');
	if($permisos >= ADMIN_){
	?>

		<div class="posicion <?php if($clase != '') e($clase); ?>">
			<a target="_blank" rel="nofollow" href="<?php e(base_url()); ?>contenidos/editar/<?php e($id); ?>" title="editar este contenido">
				<i class="fa fa-pencil"></i>
			</a>

			<label><?php e($posicion); ?></label>
		</div>

	<?php
	}
}

//1.9. Eliminar valor de array:
function deleteFromArray($array, $deleteIt, $useOldKeys = FALSE){
    $key = array_search($deleteIt,$array,TRUE);
    if($key === FALSE) return FALSE;
    unset($array[$key]);

    if(!$useOldKeys) $array = array_values($array);
    return TRUE;
}

//1.10. Input hidden para retorno al backend:
function gotoBk(){
	$html = '<input type="hidden" name="gotobk" value="1">';
	return $html;
}

//1.11. Imprimir array:
function printArr($arr){
	e('<pre>');
		print_r($arr);
	e('</pre>');
}

//1.12. Obtener IP de usuario:
function getRealIP(){
    if(isset($_SERVER["HTTP_CLIENT_IP"])){
        return $_SERVER["HTTP_CLIENT_IP"];
    }elseif(isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    }elseif(isset($_SERVER["HTTP_X_FORWARDED"])){
        return $_SERVER["HTTP_X_FORWARDED"];
    }elseif(isset($_SERVER["HTTP_FORWARDED_FOR"])){
        return $_SERVER["HTTP_FORWARDED_FOR"];
    }elseif(isset($_SERVER["HTTP_FORWARDED"])){
        return $_SERVER["HTTP_FORWARDED"];
    }else{
        return $_SERVER["REMOTE_ADDR"];
    }
}

//1.13. Cambio de semana:
function movePeriodo($centro_id, $back, $next, $controller, $metodo){
	$back = strtotime('-7 day', strtotime($back));
	$desde = date('d/m/Y', $back);

	$next = strtotime('+1 day', strtotime($next));
	$hasta = date('d/m/Y', $next);
	?>

	<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
		<!-- Período anterior: -->
		<form action="<?php e(base_url().$controller.'/'.$metodo); ?>" method="post" accept-charset="utf-8" class="enlinea">
			<input type="hidden" name="centro_id" value="<?php e($centro_id); ?>">
			<input type="hidden" name="desde" value="<?php e($desde); ?>">
			<input type="submit" name="" value="&lsaquo;&lsaquo; Semana anterior" class="btn btn-primary btn_anterior">
		</form>

		<span class="hidden-xs"><b>&nbsp;&nbsp;&nbsp;&nbsp;Cambiar de semana&nbsp;&nbsp;&nbsp;&nbsp;</b></span>

		<!-- Período posterior: le paso como valor 'desde' el día siguiente al último del período actual -->
		<form action="<?php e(base_url().$controller.'/'.$metodo); ?>" method="post" accept-charset="utf-8" class="enlinea">
			<input type="hidden" name="centro_id" value="<?php e($centro_id); ?>">
			<input type="hidden" name="desde" value="<?php e($hasta); ?>">
			<input type="submit" name="" value="Semana posterior &rsaquo;&rsaquo;" class="btn btn-primary btn_posterior">
		</form>
	</div>
	<div class="clearfix"></div>
    <br>

<?php
}


//*********************** 2. CADENAS ***********************
//2.1. Eliminar caracteres especiales, espacios en blanco por -,... para cadenas SEO:
function stringSeo($String){
	$String = trim($String);
	$String = strtolower($String);
    /*$String = preg_replace("([äáàâãª])","a",$String);
    $String = preg_replace("/ÁÀÂÃÄ/","A",$String);
    $String = preg_replace("/ÍÌÎÏ/","I",$String);
	$String = preg_replace("/íìîï/","i",$String);
    $String = preg_replace("/éèêë/","e",$String);
    $String = preg_replace("/ÉÈÊË/","E",$String);
    $String = preg_replace("/óòôõöº/","o",$String);
    $String = preg_replace("/ÓÒÔÕÖ/","O",$String);
    $String = preg_replace("/úùûü/","u",$String);
    $String = preg_replace("/ÚÙÛÜ/","U",$String);
    $String = preg_replace("/^´`¨~/","",$String);*/
    $patron = array(
    	'/ä/' => 'a',
    	'/á/' => 'a',
    	'/à/' => 'a',
    	'/â/' => 'a',
    	'/ã/' => 'a',
    	'/ª/' => 'a',
    	'/ë/' => 'e',
    	'/é/' => 'e',
    	'/è/' => 'e',
    	'/ê/' => 'e',
    	'/ï/' => 'i',
    	'/í/' => 'i',
    	'/ì/' => 'i',
    	'/î/' => 'i',
    	'/ö/' => 'o',
    	'/ó/' => 'o',
    	'/ò/' => 'o',
    	'/ô/' => 'o',
    	'/õ/' => 'o',
    	'/º/' => 'o',
    	'/ü/' => 'u',
    	'/ú/' => 'u',
    	'/ù/' => 'u',
    	'/û/' => 'u',
    	'/Á/' => 'a',
        '/À/' => 'a',
        '/Â/' => 'a',
        '/Ã/' => 'a',
        '/Ä/' => 'a',
        '/É/' => 'e',
        '/È/' => 'e',
        '/Ê/' => 'e',
        '/Ë/' => 'e',
        '/Í/' => 'i',
        '/Ì/' => 'i',
        '/Î/' => 'i',
        '/Ï/' => 'i',
        '/Ó/' => 'o',
        '/Ò/' => 'o',
        '/Ô/' => 'o',
        '/Õ/' => 'o',
        '/Ö/' => 'o',
        '/Ú/' => 'u',
        '/Ù/' => 'u',
        '/Û/' => 'u',
        '/Ü/' => 'u'
    );
    $String = preg_replace(array_keys($patron), array_values($patron), $String);
    $String = str_replace("/","",$String);
    $String = str_replace(",","",$String);
    $String = str_replace(".","",$String);
    $String = str_replace("\\","",$String);
    $String = str_replace("'","",$String);
    $String = str_replace("\"","",$String);
    $String = str_replace("+","",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","n",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    $String = str_replace(" ", "-",$String);
    return $String;
}

//2.2. Sanitización de cadenas de texto de formularios:
function sanear($variable, $low = FALSE){
	$mq_activo = get_magic_quotes_gpc();
	//Si la función entre paréntesis existe, que la ejecute:
	if(function_exists("mysql_real_scape_string")){
		//Si la función get_magic_quotes_gpc está activa, la desactivamos para no interferir con mysql_real_scape_string:
		if($mq_activo){
			$variable = stripslashes($variable);
		}
		$variable = mysql_real_escape_string($variable);
	}else{
		if(!$mq_activo){
			$variable = addslashes($variable);
		}
	}
	//Eliminamos espacios en blanco:
	$variable = trim($variable);
	//Convertimos cadena texto a minúsculas:
	if($low != FALSE){
		//$variable = strtolower($variable);
		$variable = mb_strtolower($variable, "UTF-8");		//utilizo esta función para pasar a minúscula caracteres acentuados.
	}
	//Quitamos tags:
	$variable = strip_tags($variable);
	return $variable;
}

//2.3. Función para truncar texto:
function truncar($texto, $limite, $terminacion='...'){
	$strip_texto = strip_tags($texto);					//Primero elimino los tags HTML o PHP que pudiera contener el texto.
	//limito la función a cadenas de más de ... caracteres.
	if(strlen($strip_texto) > 30){
		$subcadena = substr($strip_texto, 0, $limite);		//Extraigo la subcadena del texto con un número de caracteres definido.
		$indiceUltimoEspacio = strrpos($subcadena, " ");	//Busco el último espacio en blanco de la cadena.
		return substr($strip_texto, 0, $indiceUltimoEspacio).$terminacion;	//y de nuevo extraigo la subcadena hasta donde se encuentra dicho espacio en blanco y le concateno la terminación.
	}else{
		return $strip_texto;
	}
}


//*********************** 3. FECHAS ************************
//3.1. Formateo de fechas sin horas:
function fechas($dato, $separador="/"){
	$forma_fecha = explode("-", $dato);
	$hora = explode(":", $forma_fecha[2]);
	$fecha_hora = explode(" ", $hora[0]);
	$fecha_convertida = $fecha_hora[0].$separador.$forma_fecha[1].$separador.$forma_fecha[0];
	return $fecha_convertida;
}

//3.1.1. Formateo de fechas con horas:
function fechaHora($dato){
	$forma_fecha = explode("-", $dato);
	$hora = explode(" ", $forma_fecha[2]);
	$fecha_convertida = $hora[0].'/'.$forma_fecha[1].'/'.$forma_fecha[0].', '.$hora[1];
	return $fecha_convertida;
}

//3.2. Estilo literal de fechas:
function idiomasFecha($idioma, $fecha){
	//Recibimos la fecha según nomenclatura inglesa: Y-m-d
	$forma_fecha = explode("-", $fecha);
	$dia = $forma_fecha[2];
	$mes = $forma_fecha[1];
	$mes = mesesMultidioma($mes, $idioma);
	$any = $forma_fecha[0];

	//Componemos la fecha según el formato de cada idioma:
	switch($idioma){
		case 'es':
			$r = $dia." de ".ucfirst($mes)." de ".$any;
			break;
		case 'en':
			$r = $dia." ".ucfirst($mes)." ".$any;
			break;
		case 'po':
			$r = $dia." ".ucfirst($mes)." ".$any;
			break;
	}
	return $r;
}

//3.3. Hoy:
function getHoy(){
	$hoy = date('Y-m-d');
	return $hoy;
}

//3.3.1. getAhora: ahora
function getAhora(){
	$ahora = date('Y-m-d H:i:s');
	return $ahora;
}

//3.4. Meses multidioma:
function mesesMultidioma($valor, $idioma){
	$valor = ltrim($valor, '0');
	$es = array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$ca = array('0', 'Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Decembre');
	$en = array('0', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September','October', 'November', 'December');
	$po = array('0', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
	switch($idioma){
		case 'es':
			$mes = $es[$valor];
			break;
		case 'ca':
			$mes = $ca[$valor];
			break;
		case 'en':
			$mes = $en[$valor];
			break;
		case 'po':
			$mes = $po[$valor];
			break;
	}
	return $mes;
}

//3.4.1. Array meses:
function getMeses($mes = false){
	$arr = array('01'=>'Enero', '02'=>'Febrero', '03'=>'Marzo', '04'=>'Abril', '05'=>'Mayo', '06'=>'Junio', '07'=>'Julio', '08'=>'Agosto', '09'=>'Septiembre', '10'=>'Octubre', '11'=>'Noviembre', '12'=>'Diciembre');
	if($mes != false){
		return $arr[$mes];
	}else{
		return $arr;
	}
}

//3.5. Extraer mes de fecha:
function getMes($fecha){
	$items = explode("-", $fecha);
	$mes = $items[1];
	return $mes;
}

//3.6. Extraer año de fecha:
function getAny($fecha){
	$items = explode("-", $fecha);
	$any = $items[0];
	return $any;
}

//3.7. Extraer día de fecha:
function getDia($fecha){
	$items = explode("-", $fecha);
	$dia = $items[2];
	$items2 = explode(" ", $dia);
	$dia2 = $items2[0];
	return $dia2;
}

//3.8. Formateo fechas en inglés:
function parseFechas($dato, $sep){
	$partes = explode($sep, $dato);
	$dia = $partes[0];
	$mes = $partes[1];
	$any = $partes[2];
	$fecha = trim($any)."-".trim($mes)."-".trim($dia);
	return $fecha;
}

//3.9. Formateo fechas inglés a castellano:
function parseFechasEs($data, $sep){
	$partes = explode($sep, $data);
	$any = $partes[0];
	$mes = $partes[1];
	$dia = $partes[2];
	$fecha = trim($dia)."-".trim($mes)."-".trim($any);
	return $fecha;
}

//3.10. Días transcurridos entre dos fechas:
function diasTranscurridos($fecha_i,$fecha_f){
	$dias = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
	$dias = abs($dias); $dias = floor($dias);
	return $dias;
}

//3.11. getAniversario: obtener día y mes de fecha:
function getAniversario($fecha, $sep){
	$p = explode($sep, $fecha);
	$mes = $p[1];
	$dia = $p[2];
	$aniversario = trim($dia)."-".trim($mes);
	return $aniversario;
}

//3.12. Sumar horas:
function sumaHoras($a, $b){
	$ha = strtotime($a);
	$hb = strtotime($b);
	$dif = $hb - $ha;
	$horas = $dif / 3600;
	return number_format($horas, 2);
}

//3.13. Restar horas:
function restaHoras($horaIni, $horaFin){
	return (date("H:i", strtotime("00:00") + strtotime($horaFin) - strtotime($horaIni)) );
	//return (date("H:i", strtotime($horaFin) - strtotime($horaIni)) );
}

//3.14. Valor numérico de horas:
function horasFloat($hora){
	//Convertimos a segundos:
	list($h, $m) = explode(':', $hora);
	$s = ($h*3600) + ($m*60);
	//Extraemos el nº de horas:
	$h = $s / 3600;
	return number_format($h, 2);
}

//3.15. Calcular número de semana del año:
function nWeek($fecha){
	//Devuelve año y número de semana de la fecha:
	return date('o-W', strtotime($fecha));
}

//3.16. Fechas comprendidas en semana anual numérica:
function fechasSemanaNum($week, $any){
	$first_date = strtotime("1 January ".$any);
	$w_day = date("w", $first_date);
	$d_week = 0;

	switch($w_day){
		case 1:
			$monday = $first_date;
			break;
		case 2:
		case 3:
		case 4:
			$d_week = 604800;
		default:
			$monday = strtotime("Monday", $first_date) - $d_week;
			break;
	}

	$plus_week = "+".($week-1)." week";
	$inicioS = strtotime($plus_week, $monday);
	$finS = $inicioS + (6 * 86400);

	$inicio = date('Y-m-d', $inicioS);
	$fin = date('Y-m-d', $finS);
	$arr = array($inicio, $fin);

	return $arr;
}

//3.17 Sumar días a una fecha:
function sumaDias($fecha, $dias){
	$nuevafecha = strtotime('+'.$dias.' day', strtotime($fecha));
	return date('Y-m-d', $nuevafecha);
}

//3.17.1. Restar días a una fecha:
function restaDias($fecha, $dias){
	$nuevafecha = strtotime('-'.$dias.' day', strtotime($fecha));
	return date('Y-m-d', $nuevafecha);
}

//3.18. Diferencia entre fechas:
function diffFechas($desde, $hasta, $unidad){
	$fechainicial = new DateTime($desde);
	$fechafinal = new DateTime($hasta);
	$diferencia = $fechainicial->diff($fechafinal);
	switch($unidad){
		//Días:    probar, no estoy seguro funcione ok.
		case 'd':
			$valor = ($diferencia->d);
			break;
		//Meses:
		case 'm':
			$valor = ($diferencia->y * 12)+$diferencia->m;
			break;
		//Años:
		case 'y':
			$valor = ($diferencia->y);
			break;
		default:
			$valor = '';
			break;
	}
	return $valor;
}

//3.19. Validar si fecha existe en un rango:
function check_in_range($start_date, $end_date, $evaluame){
    $start_ts = strtotime($start_date);
    $end_ts = strtotime($end_date);
    $user_ts = strtotime($evaluame);
    return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}

//*********************** 4. COMBOS ************************
//4.1. Combobox continentes:
function comboContinentes($arr){
	$data = array('Africa','América del Norte','América del Sur','Asia','Europa','Oceanía');
	$class = ($arr['class'] != '')? "class=\"".$arr['class']."\"":'';
	$multiple = ($arr['multiple'] == 1)? "multiple":'';
	$metas = ($arr['metas'] != '')? "data-metas=\"".$arr['metas']."\"":'';
	e("<select name=\"continente\" ".$class." ".$metas." ".$multiple.">");
		//Opción vacía:
		if($arr['selected'] == ''){
			e("<option value=\"\">".$arr['leyenda']."</option>");
		}
		foreach($data as $row){
			e("<option value=\"".$row."\" ");
				if($arr['selected'] == $row) e('selected');
			e(">".$row."</option>");
		}
	e("</select>");
}

//4.2. Combobox países:
function comboPaises($arr, $data){
	$id = ($arr['id'] != '')? "id=\"".$arr['id']."\"":'';
	$class = ($arr['class'] != '')? "class=\"".$arr['class']."\"":'';
	$required = ($arr['required'])? 'required':'';
	$multiple = ($arr['multiple'] == 1)? "multiple":'';
	e("<select name=\"pais_id\" ".$class." ".$id." ".$multiple." data-ctrl=\"".$arr['ctrl']."\" ".$required.">");
		//Opción vacía:
		if($arr['selected'] == ''){
			e("<option value=\"\">".$arr['leyenda']."</option>");
		}
		foreach($data as $row){
			e("<option value=\"".$row->pais_id."\" ");
				if($arr['selected'] == $row->pais_id) e('selected');
			e(">".$row->pais."</option>");
		}
	e("</select>");
}

//4.3. Estados de reserva de servicios:
function estadosReserva(){
	$data = array('pendiente', 'confirmada', 'cancelada');
	return $data;
}

//4.4. Combo estados de reserva:
function comboEstadosReserva($arr){
	$id = ($arr['id'] != '')? "id=\"".$arr['id']."\"":'';
	$class = ($arr['class'] != '')? "class=\"".$arr['class']."\"":'';
	e("<select name=\"pais_id\" ".$class." ".$id." ".$multiple." data-ctrl=\"".$arr['ctrl']."\">");
		//Opción vacía:
		if($arr['selected'] == ''){
			e("<option value=\"\">".$arr['leyenda']."</option>");
		}
		//Valores:
		$valores = estadosReserva();
		foreach($valores as $row){
			e("<option value=\"".$row."\" ");
				if($arr['selected'] == $row) e('selected');
			e(">".$row."</option>");
		}
	e("</select>");
}

//*********************** 5. HTML ************************
//5.1. Modal:
function modal($contenido){
?>

	<div class="bg-modal">
		<div class="modal-holder">

			<?php
			e($contenido);
			?>

		</div>
	</div>

<?php
}

//5.2. Tooltip contextual:
function tip($texto){
?>

	<span class="tip circle-1" title="<?php e($texto); ?>">
		<i class="fa fa-info"></i>
	</span>

<?php
}

//5.3. Formulario sugerencias:
function formSugerencias($texto,$tipo,$placeholder=""){
?>

	<div id="accordion-alt3" class="panel-group accordion-alt3 col-xs-12">
    	<div class="panel">
    		<div class="panel-heading">
				<h4 class="panel-title">
					<a href="#collapseOne-alt3" data-parent="#accordion-alt3" data-toggle="collapse">
						<i class="fa fa-angle-right bg-purple"></i>
						<?php e(ucfirst($texto)); ?>
					</a>
				</h4>
    		</div>
    		<div id="collapseOne-alt3" class="panel-collapse collapse">
				<div class="panel-body">
					<div class="col-xxs-12 col-xs-10 col-md-11">
						<input type="text" class="form-control" id="propuesta" data-tipo="<?php e($tipo); ?>" maxlength="100" placeholder="<?php e($placeholder); ?>">
					</div>

					<div class="col-xxs-12 col-xs-2 col-md-1">
						<button class="btn btn-purple btn-form-sugerencias"><?php e(ucfirst(lang('lblenviar'))); ?></button>
					</div>

					<div class="col-xs-12 resp-propuesta green">

					</div>
				</div>
    		</div>
    	</div>
    </div>

<?php
}

//5.4. CTA registro usuario:
function cta_registro_usuario(){
?>

	<div class="cta-two-content">
		<div class="row">
			<div class="col-xs-12">
				<h4><?php e(ucfirst(lang('lblregistrate'))); ?></h4>
				<p><?php e(lang('lbldescubre_registro')); ?></p>
				<br>
			</div>

			<div class="text-center">
				<a rel="nofollow" href="registro" class="btn btn-lg btn-blue">
					<i class="fa fa-user"></i> <?php e(ucfirst(lang('lblregistrate'))); ?>
				</a>
			</div>
		</div>
	</div>

<?php
}

//5.5. CTA formulario comercio:
function cta_registro_comercio(){
?>

	<div class="cta-two-content">
		<div class="row">
			<div class="col-xs-12">
				<h4><?php e(lang('ap_abre_tienda')); ?></h4>
				<p><?php e(lang('ap_abre_tienda_texto')); ?></p>
				<br>
			</div>

			<div class="text-center">
				<a rel="nofollow" href="<?php e(base_url()); ?>crear-comercio" class="btn btn-lg btn-orange">
					<i class="fa fa-thumb-tack"></i> <?php e(lang('ap_abrir_tienda')); ?>
				</a>
			</div>
		</div>
	</div>

<?php
}

//5.6. CTA formulario repartidor:
function cta_registro_repartidor(){
?>

	<div class="cta-two-content">
		<div class="row">
			<div class="col-xs-12">
				<h4><?php e(lang('ap_hazte_repartidor')); ?></h4>
				<p><?php e(lang('ap_hazte_repartidor_texto')); ?></p>
				<br>
			</div>

			<div class="text-center">
				<a rel="nofollow" href="<?php e(base_url()); ?>hazte-repartidor" class="btn btn-lg btn-green">
					<i class="fa fa-truck"></i> <?php e(lang('ap_hazte_repartidor')); ?>
				</a>
			</div>
		</div>
	</div>

<?php
}

//5.7. PDF proyecto coopmercio:
function get_pdf_coopmercio(){
?>

	<div class="cta-lila">
		<div class="row">
			<div class="col-xs-12">
				<h4><?php e(lang('lblproyecto_coopmercio')); ?></h4>
				<p><?php e(lang('lbldescarga_pdf_coopmercio')); ?></p>
			</div>

			<div class="text-center">
				<a target="_blank" rel="nofollow" href="<?php e(base_url()); ?>proyecto-coopmercio.pdf" title="<?php e(lang('lbldescarga_pdf')); ?>">
					<i class="fa fa-file-pdf-o"></i>
				</a>
			</div>
		</div>
	</div>

<?php
}

//5.8. Iconos.
//5.8.1. Icono editar:
function iconEditar(){
	$html = '<i class="fa fa-pencil-square-o"></i>';
	return $html;
}

//5.8.2. Icono eliminar:
function iconEliminar(){
	$html = '<i class="fa fa-trash"></i>';
	return $html;
}

//5.8.3. Icono abonar pago:
function iconPago(){
	$html = '<i class="fa fa-money"></i>';
	return $html;
}

//5.8.4. Icono ver:
function iconVer(){
	$html = '<i class="fa fa-eye"></i>';
	return $html;
}

//5.8.5. Icono crear:
function iconCrear(){
	$html = '<i class="fa fa-pencil"></i>';
	return $html;
}

//5.8.6. Icono incidencia:
function iconIncidencia($texto){
	$html = '<i class="fa fa-exclamation-circle rojo" title="'.$texto.'"></i>';
	return $html;
}

//5.8.7. Icono OK:
function iconOK($texto){
	$html = '<i class="fa fa-check verde" title="'.$texto.'"></i>';
	return $html;
}

//5.8.8. Icono KO:
function iconKO($texto){
	$html = '<i class="fa fa-ban rojo" title="'.$texto.'"></i>';
	return $html;
}

//5.9. Controles de capa html:
function rowControls(){
?>

	<div class="ibox-tools">
        <!-- Colapsar tabla: -->
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>

        <!--<a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
        </ul>-->

        <!-- Ocultar tabla: -->
        <a class="close-link">
            <i class="fa fa-times"></i>
        </a>
    </div>

<?php
}

//6. Póliza line:
function polizaLine($hito = '0'){
?>

	<div id="poliza-line">
    	<div class="row">
	    	<div class="col-sm hito <?php if($hito > 0) e('hito-done'); ?>">
	    		<a href="<?php e(base_url()); ?>polizas">
	    			<span class="hito-legend-first <?php if($hito == 1) e('hito1'); ?>">
	    				D. PERSONALES
	    			</span>
	    		</a>

				<?php if($hito >= 2){ ?>
	    			<a href="<?php e(base_url().'polizas/datos_economicos'); ?>">
	    		<?php } ?>
	    			
	    			<span class="hito-legend <?php if($hito == 1) e('hito1'); ?>">
	    				D. ECONÓMICOS	
	    			</span>

	    		<?php if($hito >= 2){ ?>
	    			</a>
	    		<?php } ?>

	    	</div>

	    	<div class="col-sm hito">

	    		<?php if($hito >= 3){ ?>
	    			<a href="<?php e(base_url().'polizas/incapacidad_temporal'); ?>">
	    		<?php } ?>

	    			<span class="hito-legend">
	    				E. INCAPACIDAD TEMP.	
	    			</span>

	    		<?php if($hito >= 3){ ?>
	    			</a>
	    		<?php } ?>

	    	</div>

	    	<div class="col-sm hito">
	    		<a href="<?php ($hito >= 3)? e(base_url().'polizas/fallecimiento'):'#'; ?>">
	    			<span class="hito-legend">
	    				R. FALLECIMIENTO	
	    			</span>
	    		</a>
	    	</div>
	    	<div class="col-sm hito">
	    		<a href="<?php ($hito >= 4)? e(base_url().'polizas/invalidez'):'#'; ?>">
	    			<span class="hito-legend">
	    				R. INVALIDEZ	
	    			</span>
	    		</a>
	    	</div>
	    	<div class="col-sm hito">
	    		<a href="<?php ($hito >= 5)? e(base_url().'polizas/jubilacion'):'#'; ?>">
	    			<span class="hito-legend">
	    				R. JUBILACIÓN	
	    			</span>
	    		</a>
	    	</div>
	    	<div class="col-sm hito">
	    		<a href="<?php ($hito >= 6)? e(base_url().'polizas/dependencia'):'#'; ?>">
	    			<span class="hito-legend">
	    				R. DEPENDENCIA	
	    			</span>
	    		</a>
	    	</div>	
    	</div>
    </div>	

<?php
}

//7. Print number:
function print_number(&$value){
    if(!isset($value)){return;}
    
    if(!is_numeric(trim($value))) {return;}
    echo number_format($value,2, ",", ".");
}

function hide_no_conyugue(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo 'hidden';
    }
}

function hide_no_autonomo(&$regimen_ss) {
    if(!($regimen_ss=='autonomo' || $regimen_ss=='ambos' || $regimen_ss=='')) {
        echo "hidden";
    }
}

function hide_no_rg(&$regimen_ss) {
    if(!($regimen_ss=='rg' || $regimen_ss=='ambos')) {
        echo "hidden";
    }
}

function print_ifset(&$var){
    if(isset($var)) {
        echo $var;
    }
}

function readonly_if_ppa(&$activos_ahorros_tipo){
    if(!($activos_ahorros_tipo=="" || $activos_ahorros_tipo=="PPA")){
    	echo "readonly=''";
    }
}

function print_date(&$value){ 
    if(!isset($value)){return;}
    
    if(gettype($value) == "string") {
        $parts = explode("-",$value);
        if(count($parts)!=3){$parts = explode("/", $value);}
        if(count($parts)!=3){
            echo $value;
            return;
        }
        if(strlen($parts[0])!=4){
            echo $value;
            return;
        }
        echo $parts[2]."-".$parts[1]."-".$parts[0];
    }
    else
        echo $value->format("d-m-Y");
}

function resalta_signe(&$value){
    if(!isset($value)) {return;}
    if(!is_numeric(trim($value))) {return;}

    if($value > 0) {echo "resalta_positiu";}
    if($value < 0) {echo "resalta_negatiu";}
}

function width_no_conyugue_1(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo '66%';
    }
    else {
        echo '33%';
    }
}

function width_no_conyugue_2(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo '0%';
    }
    else {
        echo '33%';
    }
}


function width_soluciones_1(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo '28%';
    }
    else {
        echo '14%';
    }
}

function width_soluciones_2(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo '0%';
    }
    else {
        echo '14%';
    }
}


function get_zero_ifnull(&$value) {
    if(!isset($value)){return 0;}
    
    if(!is_numeric(trim($value))) {return 0;}
    
    return $value;
}

function width_cobro_0(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo '33%';
    }
    else {
        echo '20%';
    }
}


function width_cobro_1(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo '33%';
    }
    else {
        echo '20%';
    }
}

function width_cobro_2(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo '0%';
    }
    else {
        echo '20%';
    }
}

function width_compara_1(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo '28%';
    }
    else {
        echo '14%';
    }
}

function width_compara_2(&$tiene_conyugue) {
    if(!isset($tiene_conyugue) || (isset($tiene_conyugue) && $tiene_conyugue=='0')) {
        echo '0%';
    }
    else {
        echo '14%';
    }
}

?>
