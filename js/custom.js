$(document).ready(function(){
	//Añadir línea de deuda:
    $('#add-line-deuda').on('click', function(e){
        e.preventDefault();
        var nrows = $('#tblDeudas tr').length;
        //alert(nrows);

        var html = '<tr>';
        html += '<td><div class="input-group"><input type="text" class="form-control" id="deuda'+nrows+'_nombre" name="deuda'+nrows+'_nombre"></div></td>';
        html += '<td><input type="text" min="0" step="any" class="form-control text-right" id="deuda'+nrows+'_capital_pendiente" name="deuda'+nrows+'_capital_pendiente"></td>';
        html += '<td><div class="input-group"><input type="text" min="1" max="100" class="form-control text-right" id="deuda3_anyos_pendientes" name="deuda3_anyos_pendientes"></div></td>';
        html += '<td><div class="input-group"><input type="text" min="0" step="any" class="form-control text-right" id="deuda3_cuota" name="deuda3_cuota"></div></td>';
        html += '</tr>';
        $('#tblDeudas').append(html);
    });

    //Añadir línea activos ahorros:
    $('#add-line-activo-ahorro').on('click', function(e){
        e.preventDefault();
        var nrows = $('#tblActivosAhorros tr').length;

        var html = '<tr>';
        html += '<td><select class="form-control" id="activos_ahorros_'+nrows+'_afectado" name="activos_ahorros_'+nrows+'_afectado"><option value=""></option><option value="titular">Titular</option><option value="conyugue">Cónyuge</option><option value="ambos">Ambos</option></select></td>';
        html += '<td><select class="form-control" id="activos_ahorros_'+nrows+'_tipo" name="activos_ahorros_'+nrows+'_tipo"><option value="">Plan de Pensiones</option><option value="PPA">PPA</option><option value="PIAS">PIAS</option><option value="seguro_ahorro">Seguro de Ahorro</option><option value="unit_linked">Unit Linked</option><option value="fondos_inversion">Fondos de Inversión</option><option value="otros">Otros</option></select></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="activos_ahorros_'+nrows+'_capital_hoy" name="activos_ahorros_'+nrows+'_capital_hoy"></div></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="activos_ahorros_'+nrows+'_capital_jubilacion" name="activos_ahorros_'+nrows+'_capital_jubilacion"></div></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="activos_ahorros_'+nrows+'_prima_mensual" name="activos_ahorros_'+nrows+'_prima_mensual"></div></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="activos_ahorros_'+nrows+'_derechos_consolidados" name="activos_ahorros_'+nrows+'_derechos_consolidados"></div></td>';
        html += '</tr>';
        $('#tblActivosAhorros').append(html);
    });

    //Añadir línea activos seguros:
    $('#add-line-activo-seguros').on('click', function(e){
        e.preventDefault();
        var nrows = $('#tblActivosSeguros tr').length;

        var html = '<tr>';
        html += '<td><select class="form-control" id="activos_sv_'+nrows+'_asegurado" name="activos_sv_'+nrows+'_asegurado"><option value=""></option><option value="titular">Titular</option><option value="conyugue">Cónyuge</option></select></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="activos_sv_'+nrows+'_fallecimiento"     name="activos_sv_'+nrows+'_fallecimiento"></div></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="activos_sv_'+nrows+'_ipa" name="activos_sv_'+nrows+'_ipa"></div></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="activos_sv_'+nrows+'_ipt" name="activos_sv_'+nrows+'_ipt"></div></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="activos_sv_'+nrows+'_prima_anual" name="activos_sv_'+nrows+'_prima_anual"></div></td>';
        html += '</tr>';
        $('#tblActivosSeguros').append(html);
    });

    //Añadir beneficiario:
    $('#add-line-beneficiario').on('click', function(e){
        e.preventDefault();
        var nrows = $('#tblBeneficiarios tr').length;

        var html = '<tr>';
        html += '<td>Beneficiario '+nrows+'</td>';
        html += '<td><select class="form-control" id="fallecimiento_beneficiario_'+nrows+'_relacion" name="fallecimiento_beneficiario_'+nrows+'_relacion"><option value=""></option><option value="conyugue">Cónyuge</option><option value="descendiente">Descendiente</option></select></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="fallecimiento_beneficiario_'+nrows+'_edad_descendiente" name="fallecimiento_beneficiario_'+nrows+'_edad_descendiente"></div></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" id="fallecimiento_beneficiario_'+nrows+'_importe_cobrar" name="fallecimiento_beneficiario_'+nrows+'_importe_cobrar"></div></td>';
        html += '<td><div class="input-group"><input type="text" class="form-control text-right" readonly></div></td>';
        html += '</tr>';
        $('#tblBeneficiarios').append(html);
    });		

    //Calculadora:
    $('#btnCalculadora').on('click', function(e){
        e.preventDefault();
        $('#modalCalculadora').fadeIn();
    });	
    $('.closeCalculadora').on('click', function(e){					
         e.preventDefault();
        $('#modalCalculadora').fadeOut();
    }); 
});
