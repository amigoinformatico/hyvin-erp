Dropzone.autoDiscover = false;
$(document).ready(function () {



    myDropzone = new Dropzone(".dropzone", {
        url: '/consultas/upload',
        addRemoveLinks: true, //This will show remove button
    });
    //Init Dropzone
    myDropzone.on("removedfile", function (file) {
        if (!file.name) { return; } // The file hasn't been uploaded
        $.ajax({
            type: 'POST',
            url: '/consultas/delete',
            dataType: "json",
            data: { file_name: file.name },
            success: function (result) {
                console.log("deleted")
            }
        });
    });
});