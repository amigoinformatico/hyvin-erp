//Table clientes:
function formatClientes(d){
	// `d` is the original data object for the row
	return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
		'<tr>'+
			'<td>&nbsp;</td>'+
			'<td><a class="hyvin-boton" href="">AÑADIR <i class="fa fa-plus"></i></a></td>'+
			'<td><a class="hyvin-boton" href="">ARCHIVOS <i class="fa fa-file"></i></a></td>'+
		'</tr>'+
	'</table>';
}

$(document).ready(function(){
	//Tabla pólizas:
	var tblPolizas = $('#tblPolizas').DataTable({
		"ajax": "/admin/getUsuarios",
		"searching": false,
		"paging":   false,
		"ordering": false,
		"info": false,
		"columns": [
			{
				"className":      'details-control',
				"orderable":      false,
				"data":           null,
				"defaultContent": '<span class="topo"></span>',
				"width": "26px"
			},
			{ "data": "titular_nombre" },
			{ "data": "titular_sexo" },
			{ "data": "titular_fecha_nacimiento" },
			{ "defaultContent": "N/A" }


		],
		"columnDefs": [
    		{ "className": "color_azul", "targets": [1] }
  		],
		"order": [[1, 'asc']]
	}); 
	/*$('#tblPolizas tbody').on('click', 'td.details-control', function (){
		var tr = $(this).closest('tr');
		var row = tblPolizas.row( tr );
 
		if ( row.child.isShown() ) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}
		else {
			// Open this row
			row.child( format(row.data()) ).show();
			tr.addClass('shown');
		}
	});*/

	//Tabla clientes:
	var tblClientes = $('#tblClientes').DataTable({
		"ajax": "/admin/getUsuarios",
		"searching": false,
		"paging":   false,
		"ordering": false,
		"info": false,
		"columns": [
			{
				"className":      'details-control',
				"orderable":      false,
				"data":           null,
				"defaultContent": '<p class="relativo"><input type="radio" class="rdb-detail" name="cliente" id="1"><label for="1"></label></p>',
				"width": "26px"
			},
            { "data": "titular_nombre" },
            { "data": "titular_sexo" },
            { "data": "titular_fecha_nacimiento" },
            { "defaultContent": "N/A" },
            { "data": "titular_prima_anual" }
		],
		"columnDefs": [
    		{ className: "color_azul", "targets": [1] }
  		],
		"order": [[1, 'asc']]
	});
	 
	// Add event listener for opening and closing details
	$('#tblClientes tbody').on('click', 'td.details-control .rdb-detail', function (){
		var tr = $(this).closest('tr');
		var row = tblClientes.row( tr );
 
		if(row.child.isShown()){
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}else{
			// Open this row
			row.child(formatClientes(row.data())).show();
			tr.addClass('shown');
		}
	});	
});